const withTM = require('next-transpile-modules')(['myproject-ucl', 'myproject-shared'])
module.exports = withTM({
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: {
        loader: '@svgr/webpack',
        options: { configFile: '.svgrrc.js' }
      }
    })
    return config
  },
  env: {
    NEXT_PUBLIC_BACKEND: process.env.NEXT_PUBLIC_BACKEND
  }
})
