import { Urls } from 'myproject-shared'

export const BREADCRUMB_UNIT = Object.freeze({
  home: {
    label: 'Home',
    url: Urls.home
  },
  account: {
    label: 'Account',
    url: Urls.account
  }
})
