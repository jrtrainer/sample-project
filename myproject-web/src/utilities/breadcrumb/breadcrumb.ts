import { BreadcrumbUnitProps } from 'myproject-ucl'
import { BREADCRUMB_UNIT } from './breadcrumb-constants'

export const getAccountBreadcrumbData = (isFull: boolean = false): BreadcrumbUnitProps[] => {
  const data = [BREADCRUMB_UNIT.account]
  if (isFull) {
    data.unshift(BREADCRUMB_UNIT.home)
  }
  return data
}
