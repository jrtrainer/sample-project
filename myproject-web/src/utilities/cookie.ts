import { serialize } from 'cookie'

// tslint:disable-next-line: no-magic-numbers
const ONE_YEAR = 60 * 60 * 24 * 365

interface CookieProps {
  key: string
  value: string
  options?: object
}

export const serializeCookie = ({ key, value, options }: CookieProps) => {
  return serialize(key, value, {
    path: '/',
    httpOnly: true,
    maxAge: ONE_YEAR,
    ...options
  })
}
