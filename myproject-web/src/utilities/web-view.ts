import { NextPageContext } from 'next'

export const isWebview = (ctx: NextPageContext) => {
  const {
    query: { flavour }
  } = ctx
  return flavour === 'app'
}
