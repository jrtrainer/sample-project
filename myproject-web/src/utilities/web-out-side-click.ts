import { RefObject, useEffect } from 'react'

export const useOutsideClick = (element: RefObject<HTMLElement>, callback: () => void) => {
  const handleClickOutside = (event: MouseEvent) => {
    const el = event.target
    if (element.current && el instanceof Node && !element.current.contains(el)) {
      callback()
    }
  }
  useEffect(() => {
    window.addEventListener('mousedown', handleClickOutside)
    return () => {
      window.removeEventListener('mousedown', handleClickOutside)
    }
  }, [])
}
