import React, { useLayoutEffect, useState } from 'react'
import { throttle } from 'lodash'

const SIZE_UPDATE_THROTTLE_MS = 33.33
export const useWindowSize = () => {
  const [size, setSize] = useState([0, 0])
  useLayoutEffect(() => {
    const updateSize = () => {
      setSize([window.innerWidth, window.innerHeight])
    }
    const throttledSize = throttle(updateSize, SIZE_UPDATE_THROTTLE_MS)
    window.addEventListener('resize', throttledSize)
    updateSize()
    return () => window.removeEventListener('resize', throttledSize)
  }, [])

  return size
}
