import _ from 'lodash'
import { Alert } from 'myproject-shared/gql-tags'
import { InboxDataContext, AlertType } from 'myproject-shared/providers/chat'
import { translate } from 'myproject-shared/utilities/i18n'
import { SegmentedControl } from 'myproject-ucl'
import React, { FC, useContext } from 'react'
import { NotificationsDropdownTab, NotificationsDropdownTabProps } from './notifications-dropdown-tab'
import { MessagesDropdownTabProps, MessagesDropdownTab } from './messages-dropdown-tab'

export const Inbox: FC = () => {
  const { inbox, inboxLoading, inboxError, refetchInbox, notifications, notificationsLoading, notificationsError, refetchNotification } = useContext(
    InboxDataContext
  )

  // TODO CLIENT-2702 replace this with a query/list that supplies messages without ads
  const messagesWithoutAds = inbox?.alertsWithAds.alertsWithAds.filter(alert => {
    return alert.__typename === 'Alert'
  }) as Alert[]

  const messagesDropdownProps: MessagesDropdownTabProps = {
    messages: messagesWithoutAds,
    loading: inboxLoading,
    error: inboxError,
    refetch: refetchInbox
  }

  const notificationsDropdownProps: NotificationsDropdownTabProps = {
    notifications: notifications?.alerts.alerts,
    loading: notificationsLoading,
    error: notificationsError,
    refetch: refetchNotification
  }

  const routes: any[] = [
    {
      key: 'Messages',
      title: translate('inbox-screen.messages'),
      tabContent: <MessagesDropdownTab {...messagesDropdownProps} />
    },
    {
      key: 'Notifications',
      title: translate('inbox-screen.notifications'),
      tabContent: <NotificationsDropdownTab {...notificationsDropdownProps} />
    }
  ]

  return <SegmentedControl tabs={routes} />
}
