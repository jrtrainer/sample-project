import React, { FC } from 'react'
import { Alert, translate, GraphGQLErrorParser, AnalyticsActionType, Urls } from 'myproject-shared'
import { Center, EmptyState, ErrorBearFailure, ActivityIndicator, EmailEnvelope, Flex, List, Margin, Text } from 'myproject-ucl'
import { isEmpty, take } from 'lodash'
import { AnalyticsInbox, EMPTY_STATE } from 'myproject-shared/analytics/myproject-analytics-inbox'
import { ApolloError } from 'apollo-client'
import { AlertType } from 'myproject-shared/providers/chat'
import { useWebNavigation } from '../../providers'
import { NotificationRowProps } from 'myproject-ucl/widgets/myproject-notification-row/myproject-notification-row.props'
import { NotificationRow } from 'myproject-ucl/widgets/myproject-notification-row/myproject-notification-row'
import Link from 'next/link'

const NOTIFICATIONS_TO_SHOW = 4

export interface NotificationsDropdownTabProps {
  notifications: Alert[] | undefined
  loading: boolean
  error: ApolloError | undefined
  refetch: () => Promise<any>
}

export const NotificationsDropdownTab: FC<NotificationsDropdownTabProps> = ({ notifications, loading, error, refetch }) => {
  const { navigateToActionPath } = useWebNavigation()
  const testId = 'inbox-dropdown.notifications-tab'

  if (loading) {
    return (
      <Center>
        <ActivityIndicator />
      </Center>
    )
  } else if (error) {
    const { title, message } = GraphGQLErrorParser(error)
    return (
      <Center>
        <EmptyState
          icon={ErrorBearFailure}
          title={title}
          subtitle={message}
          buttonTitle={translate('common-errors.server-error.button-title')}
          buttonHandler={refetch}
          testID='inbox-screen.error-state'
        />
      </Center>
    )
  } else if (isEmpty(notifications)) {
    AnalyticsInbox.trackInboxUIEvent(AlertType.NOTIFICATION, AnalyticsActionType.Show, EMPTY_STATE)
    return (
      <EmptyState
        icon={EmailEnvelope}
        title={translate('inbox-screen.empty-notifications-title')}
        subtitle={translate('inbox-screen.empty-notifications-subtitle')}
        iconTint='link'
        testID={testId + '.empty-alerts'}
      />
    )
  } else {
    const onVisibilityChanged = (visible: boolean, notification: Alert) => {
      if (visible && notification) {
        AnalyticsInbox.trackInboxUIEvent(AlertType.NOTIFICATION, AnalyticsActionType.Show, notification.objectId, notification.type)
      }
    }

    const renderRow = (notification: Alert, index: number) => {
      const handleClick = () => {
        AnalyticsInbox.trackInboxUIEvent(AlertType.NOTIFICATION, AnalyticsActionType.Click, notification.objectId, notification.type)
        navigateToActionPath(notification.actionPath || '')
      }

      const notificationRowProps: NotificationRowProps = {
        read: notification.read || notification.seen,
        avatar: notification.sender?.profile?.avatars?.squareImage,
        title: notification.notificationText?.trim() || '',
        dateAdded: notification.dateAdded,
        secondaryImage: (notification.contentThumbnails.length > 0 && notification.contentThumbnails[0]) || undefined,
        handleClick,
        testId: testId + '.' + index
      }
      return <NotificationRow {...notificationRowProps} />
    }

    const keyExtractor = (item: Alert) => {
      return item.id
    }

    return (
      <Flex direction='column' grow={1}>
        <List
          horizontal={false}
          data={take(notifications, NOTIFICATIONS_TO_SHOW)}
          keyExtractor={keyExtractor}
          renderItem={renderRow}
          showSeparator={true}
          onVisibilityChange={onVisibilityChanged}
        />
        <Margin direction='column' marginStep={4}>
          <Link href={Urls.inbox}>
            <a>
              <Text textType='secondaryBody2'>{translate('inbox-screen.view-all-notifications')}</Text>
            </a>
          </Link>
        </Margin>
      </Flex>
    )
  }
}
