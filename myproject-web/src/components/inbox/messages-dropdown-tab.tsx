import React, { FC } from 'react'
import { Alert, GraphGQLErrorParser, translate, AnalyticsActionType, Urls } from 'myproject-shared'
import {
  Center,
  ActivityIndicator,
  EmptyState,
  ErrorBearFailure,
  EmailEnvelope,
  InboxRowProps,
  InboxRow,
  Flex,
  List,
  Margin,
  Text
} from 'myproject-ucl'
import { AnalyticsInbox, EMPTY_STATE } from 'myproject-shared/analytics/myproject-analytics-inbox'
import { isEmpty, take } from 'lodash'
import { ApolloError } from 'apollo-client'
import { AlertType } from 'myproject-shared/providers/chat'
import Router from 'next/router'
import Link from 'next/link'

const MESSAGES_TO_SHOW = 4

export interface MessagesDropdownTabProps {
  messages: Alert[] | undefined
  loading: boolean
  error: ApolloError | undefined
  refetch: () => Promise<any>
}

export const MessagesDropdownTab: FC<MessagesDropdownTabProps> = ({ messages, loading, error, refetch }) => {
  const testId = 'inbox-dropdown.messages-tab'

  if (loading) {
    return (
      <Center>
        <ActivityIndicator />
      </Center>
    )
  } else if (error) {
    const { title, message } = GraphGQLErrorParser(error)
    return (
      <Center>
        <EmptyState
          icon={ErrorBearFailure}
          title={title}
          subtitle={message}
          buttonTitle={translate('common-errors.server-error.button-title')}
          buttonHandler={refetch}
          testID='inbox-screen.error-state'
        />
      </Center>
    )
  } else if (isEmpty(messages)) {
    AnalyticsInbox.trackInboxUIEvent(AlertType.MESSAGE, AnalyticsActionType.Show, EMPTY_STATE)
    return (
      <EmptyState
        icon={EmailEnvelope}
        title={translate('inbox-screen.empty-alerts-title')}
        subtitle={translate('inbox-screen.empty-alerts-subtitle')}
        iconTint='link'
        testID={testId + '.empty-alerts'}
      />
    )
  } else {
    const onVisibilityChanged = (visible: boolean, message: Alert) => {
      if (visible && message) {
        AnalyticsInbox.trackInboxUIEvent(AlertType.MESSAGE, AnalyticsActionType.Show, message.objectId, message.type)
      }
    }

    const renderRow = (message: Alert, index: number) => {
      const handleClick = () => {
        AnalyticsInbox.trackInboxUIEvent(AlertType.MESSAGE, AnalyticsActionType.Click, message.objectId, message.type)
        Router.push(Urls.discussion(message.objectId))
      }

      const messageRowProps: InboxRowProps = {
        read: message.read || message.seen,
        avatar: message.sender?.profile?.avatars?.squareImage,
        isTruyouVerified: message.sender?.profile?.isTruyouVerified || false,
        isAutosDealer: message.sender?.profile?.isAutosDealer || false,
        title: (message.title && message.title.trim()) || '',
        notificationText: (message.notificationText && message.notificationText.trim()) || '',
        dateAdded: message.dateAdded,
        secondaryImage: (message.contentThumbnails.length > 0 && message.contentThumbnails[0]) || undefined,
        handleClick,
        testID: testId + '.' + index
      }
      return <InboxRow {...messageRowProps} />
    }

    const keyExtractor = (item: Alert) => {
      return item.id
    }

    return (
      <Flex direction='column' grow={1}>
        <List
          horizontal={false}
          data={take(messages, MESSAGES_TO_SHOW)}
          keyExtractor={keyExtractor}
          renderItem={renderRow}
          showSeparator={true}
          onVisibilityChange={onVisibilityChanged}
        />
        <Margin direction='column' marginStep={4}>
          <Link href={Urls.inbox}>
            <a>
              <Text textType='secondaryBody2'>{translate('inbox-screen.view-all-messages')}</Text>
            </a>
          </Link>
        </Margin>
      </Flex>
    )
  }
}
