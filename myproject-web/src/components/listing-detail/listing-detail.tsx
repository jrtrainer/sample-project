import { ListingDetailProps } from './listing-detail.props'
import {
  LISTING_DETAIL,
  GET_LISTING_WITH_LISTING_ID,
  DateFormatter,
  ConditionParser,
  Listing,
  CategoryAttributeMap,
  splitCategories,
  sortCategoriesByPriority,
  Photo,
  OpeningHour,
  PublicLocation,
  Urls,
  isListingSold,
  UserAutosDealerItems,
  translate,
  AutosTypicalFeatures,
  ListingCategoryBreadcrumbs,
  FullfilmentDetails,
  AnalyticsElementType,
  ShippingOption,
  ListingState,
  mergeVehicleAndCategoryAttributes,
  determineEstimatedDeliveryDates,
  determineShippingPrice,
  BEST_SHIPPING_OPTION,
  Query,
  isCarsAndTrucks,
  AccountDataContext
} from 'myproject-shared'
import { useLazyQuery, useQuery } from '@apollo/react-hooks'
import {
  ActivityIndicator,
  Center,
  UserProfilePropTypes,
  Margin,
  Flex,
  Button,
  Text,
  Stack,
  Separator,
  Spacer,
  ListingPhotoList,
  AttributeList,
  ListingCTAInfoSection,
  ExpandingTextSection
} from 'myproject-ucl'
import _ from 'lodash'
import { property } from 'lodash'
import { FC } from 'react'
import { useModal, PurchaseProtectionSection, RelatedSearchTopics, ShareDialog } from '../../widgets'
import { ReportItem } from '../report'
import { ItemDetailAnalyticsController } from 'myproject-shared/analytics/item-detail/myproject-analytics-item-detail'
import { ItemDetailSellerType, ItemDetailType, ItemDetailElementName } from 'myproject-shared/analytics/constants/item-detail-constants'
import { useSendFirstMessage } from '../../widgets/chat/use-send-first-message'
import { SoldIndicator } from './listing-sold-indicator'
import ListingSavedListsModalContent from '../../../pages/saved/listing-saved-lists-modal'
import { ListingMapView } from './listing-map-view'
import { useRouter } from 'next/router'
import { formatMoney, MoneyTrimType } from 'myproject-ucl/formatters/number-formatters'
import { ShippingProps } from 'myproject-ucl/widgets/myproject-shipping-details/myproject-listing-shipping-details.props'
import { useContext } from 'react'

const VEHICLE_HISTORY_EXPERIMENT = 'vinaudit_1'
const LISTING_MARGIN_STEP = 4
const LISTING_SPACER_STEP = 4
const STACK_DIRECTION = 'column'
const CHILD_SEPARATION_STEP = 1
const POSTED_PREFIX = 'Posted '
const PRIMARY_CNT = 3
const ATTRIBUTE_TRUNCATE_THRESHOLD = 7

const ActionButtons: FC<{ listing: Listing; isOwnItem: boolean }> = ({ listing, isOwnItem }) => {
  const { showModal, closeModal } = useModal()
  const reportItem = () => {
    const modalId = `report-item-${listing.id}`
    const onDone = () => {
      closeModal(modalId)
    }
    showModal({
      content: <ReportItem listing={listing} onDone={onDone} />,
      withCloseIcon: true,
      id: modalId
    })
  }

  // NOTE: For when Saved and Share are added, the whole action section (Saved, Report, Shared) should be disabled if it is your own item.
  return (
    <Margin marginStep={LISTING_MARGIN_STEP}>
      <Stack direction={STACK_DIRECTION} childSeparationStep={CHILD_SEPARATION_STEP} grow={1}>
        <Separator />
        <Spacer sizeStep={LISTING_SPACER_STEP} direction='column' />
        {!isOwnItem && (
          <Stack direction='row' childSeparationStep={CHILD_SEPARATION_STEP} axisDistribution='space-around' height={32}>
            <Flex direction='column' grow={1}>
              <Button buttonType='secondary' buttonSize='large' title={'Report'} onClick={reportItem} />
            </Flex>
          </Stack>
        )}
        <Spacer sizeStep={LISTING_SPACER_STEP} direction='column' />
      </Stack>
    </Margin>
  )
}

export const ListingDetail = (props: ListingDetailProps) => {
  const listingId = props.listingId

  const shouldUseItemEndpoint = _.parseInt(listingId).toString() === listingId
  const queryToUse = shouldUseItemEndpoint ? LISTING_DETAIL : GET_LISTING_WITH_LISTING_ID
  const queryVariables = shouldUseItemEndpoint
    ? {
        listingId: _.parseInt(listingId),
        vehicleHistoryExperiment: VEHICLE_HISTORY_EXPERIMENT,
        withNewEndpoint: true
      }
    : { listingId }

  const { data, loading } = useQuery(queryToUse, {
    variables: queryVariables,
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true
  })

  if (loading) {
    return (
      <Center>
        <ActivityIndicator size='large' />
      </Center>
    )
  }

  const listing = property<object, Listing>('listing')(data) as Listing
  return <ListingDetailContents listing={listing} listingId={listingId} />
}

const ListingDetailContents: FC<{ listing: Listing; listingId: string }> = props => {
  const { listing, listingId } = props

  const [fetchBestShippingOption, { data: bestShippingOptionData, loading: shippingLoading }] = useLazyQuery<Query>(BEST_SHIPPING_OPTION)
  const { loading: accountLoading, data: userData } = useContext(AccountDataContext)

  const { showModal, closeModal } = useModal()
  const router = useRouter()

  const isMerchant = property('sku')(listing) != null
  const listingState = property('state')(listing) as ListingState
  const categoryId = property('category.id')(listing) as string
  const categoryName = property('category.name')(listing) as string
  const categoryLevelOneName = property('category.levelOneName')(listing) as string
  const categoryLevelTwoName = property('category.levelTwoName')(listing) as string
  const categoryLevelThreeName = property('category.levelThreeName')(listing) as string
  const photos = property('photos')(listing) as Photo[]
  const listingTitle = property('title')(listing) as string
  const price = formatMoney(listing?.price, MoneyTrimType.NoDoubleZeros)

  // Show original price when the current price is lower and this isn't a merchant listing
  const listingOriginalPrice = property('originalPrice')(listing) as string
  const originalPrice =
    !isMerchant && listingOriginalPrice && _.parseInt(listing.price) > _.parseInt(listingOriginalPrice) ? '$' + listingOriginalPrice : undefined
  const location = property('locationDetails.locationName')(listing) as string
  const postDate = property('postDate')(listing) as string
  const dateCompare = POSTED_PREFIX + DateFormatter.formatFromNow(postDate)
  const condition = property('condition')(listing) as number
  const description = property('description')(listing) as string

  const itemLocationName = listing?.locationDetails?.locationName || undefined
  const itemLocationLatitude = listing?.locationDetails?.latitude ? _.toNumber(listing.locationDetails.latitude) : undefined
  const itemLocationLongitude = listing?.locationDetails?.longitude ? _.toNumber(listing.locationDetails.longitude) : undefined

  // Seller profile
  const profile = isMerchant ? property('merchantProfile')(listing) : property('owner.profile')(listing)
  const avatar = isMerchant ? (property('avatar.small.url')(profile) as string) : (property('avatars.squareImage')(profile) as string)
  const name = isMerchant ? (property('storeName')(profile) as string) : (property('name')(profile) as string)
  const joined = property('owner.profile.dateJoined')(listing) as string
  const count = property('ratingSummary.count')(profile) as number
  const average = property('ratingSummary.average')(profile) as number

  const saved = property('saved')(listing) as boolean
  const sellerId = property('owner.id')(listing) as string
  const categoryAttributeMap = property('categoryAttributeMap')(listing) as [CategoryAttributeMap]
  const quantity = property('quantity')(listing) as number

  const { primary: primaryAttributes, secondary: secondaryAttributes } = splitCategories(sortCategoriesByPriority(categoryAttributeMap))

  const ownerId = property('owner.id')(listing) as string
  const isOwnItem = !accountLoading && !!userData && _.toString(sellerId) === _.toString(userData?.id)
  // Autos
  const vehicleAttributes = mergeVehicleAndCategoryAttributes(categoryAttributeMap, listing?.vehicleAttributes)
  const vehicleId = _.first(listing?.categoryAttributeMap?.find(category => category.attributeName === 'vinauditVehicleId')?.attributeValue)
  const isAutosDealer = property('owner.profile.isAutosDealer')(listing) as boolean
  const websiteLink = property('owner.profile.websiteLink')(listing) as string
  const openingHours = property('owner.profile.openingHours')(listing) as OpeningHour[]
  const publicLocation = property('owner.profile.publicLocation')(listing) as PublicLocation
  const clickToCallEnabled = property('owner.profile.clickToCallEnabled')(listing) as boolean
  const clickToCallPhoneNumber = property('owner.profile.c2cPhoneNumber.nationalNumber')(listing) as string

  // Shipping
  const fulfillmentDetails = listing?.fulfillmentDetails as FullfilmentDetails
  const shippingOptions = listing?.shippingOptions as ShippingOption[]
  const primaryShippingOption = shippingOptions?.length > 0 ? shippingOptions[0] : undefined
  const estimatedDeliveryDate = determineEstimatedDeliveryDates(
    primaryShippingOption,
    fulfillmentDetails,
    bestShippingOptionData?.bestShippingOption ?? undefined
  )
  const localPickupEnabled = !isMerchant && (fulfillmentDetails?.localPickupEnabled as boolean)
  const buyNowEnabled = fulfillmentDetails?.buyItNowEnabled as boolean
  const shippingEnabled = isMerchant || (fulfillmentDetails?.shippingEnabled as boolean)
  const shippingPrice = determineShippingPrice(
    primaryShippingOption,
    fulfillmentDetails,
    bestShippingOptionData?.bestShippingOption ?? undefined
  ) as number
  const sellerPaysShipping = (fulfillmentDetails?.sellerPaysShipping || shippingPrice === 0) as boolean

  const tagLine = isAutosDealer
    ? translate('public-profile.verified-autos-dealer')
    : isMerchant
    ? translate('public-profile.verified-pro-seller')
    : undefined

  // Analytics values
  const sellerType = isAutosDealer ? ItemDetailSellerType.AutoDealer : isMerchant ? ItemDetailSellerType.Merchant : ItemDetailSellerType.Consumer
  const shippingType = localPickupEnabled
    ? shippingEnabled
      ? ItemDetailType.LocalAndShipping
      : ItemDetailType.LocalOnly
    : ItemDetailType.ShippingOnly
  const originScreenName = '' // TODO implement origin screen name

  const shippingProps: ShippingProps = {
    estimatedDeliveryDateStart: estimatedDeliveryDate.startDate,
    estimatedDeliveryDateEnd: estimatedDeliveryDate.endDate,
    shippingPrice,
    sellerPaysShipping,
    isMerchant,
    location
  }

  const userProfileProps: UserProfilePropTypes = {
    profile: {
      id: ownerId,
      avatar,
      name,
      rating: {
        count,
        average
      },
      joined,
      isAutosDealer,
      isMerchant,
      tagLine
    },
    onClick: () => {
      router.push(Urls.profile(ownerId))
    }
  }

  // Load best shipping option immediately for merchants
  if (isMerchant && !shippingLoading && !bestShippingOptionData) {
    const shippingOfferOptions = listing.shippingOptions?.map(option => {
      return {
        name: option?.name,
        price: option?.price,
        priority: option?.priority
      }
    })

    fetchBestShippingOption({ variables: { listingId, shippingOptions: shippingOfferOptions } })
  }

  const trackClickEvent = (elementName: ItemDetailElementName, elementType?: AnalyticsElementType) => {
    ItemDetailAnalyticsController.trackItemDetailUIEventClick(elementName, sellerType, listingId, originScreenName, shippingType, elementType)
  }

  const trackShowEvent = (elementName: ItemDetailElementName, elementType?: AnalyticsElementType) => {
    ItemDetailAnalyticsController.trackItemDetailUIEventShow(elementName, sellerType, listingId, originScreenName, shippingType, elementType)
  }

  const { openDiscussionOrFirstModal } = useSendFirstMessage(_.parseInt(listingId), isAutosDealer)

  const ask = (elementName: ItemDetailElementName) => {
    trackClickEvent(elementName)
    openDiscussionOrFirstModal()
  }

  const makeOffer = (elementName: ItemDetailElementName) => {
    trackClickEvent(elementName)
    console.log('make offer!')
  }

  const buyNow = (elementName: ItemDetailElementName) => {
    trackClickEvent(elementName)
    console.log('buy now!')
  }

  const shareListing = (elementName: ItemDetailElementName) => {
    const modalId = `share-listing-item-${listingId}`

    trackClickEvent(elementName)

    showModal({
      content: <ShareDialog listingId={listingId} title={listingTitle} />,
      withCloseIcon: true,
      id: modalId
    })
  }

  const saveListing = (elementName: ItemDetailElementName) => {
    trackClickEvent(elementName)
    const modalId = `save-listing-item-${listing.id}`
    const onClose = () => {
      closeModal(modalId)
    }
    showModal({
      content: <ListingSavedListsModalContent onClose={onClose} listingId={_.toString(listing.id)} />,
      withCloseIcon: true,
      id: modalId
    })
  }

  const onClickItem = (item: Listing) => {
    router.push(Urls.item(`${item.id}`))
  }

  return (
    <Margin direction={STACK_DIRECTION} grow={1} marginLeftStep={12} marginRightStep={12}>
      <ListingCategoryBreadcrumbs
        categoryId={categoryId}
        levelOneName={categoryLevelOneName}
        levelTwoName={categoryLevelTwoName}
        levelThreeName={categoryLevelThreeName}
      />
      <Spacer direction='column' sizeStep={4} />
      <Stack direction='row' childSeparationStep={4}>
        {isListingSold(listingState, quantity) && <SoldIndicator />}
        <Text textType='headline3'>{listingTitle}</Text>
      </Stack>
      <Spacer direction='column' sizeStep={4} />
      <Stack direction='row' grow={1} childSeparationStep={6} wrap={'wrap'}>
        <Stack direction={STACK_DIRECTION} grow={1}>
          <ListingPhotoList photos={photos} />

          {vehicleId && (
            <>
              <Margin direction='column' marginLeftStep={4} marginRightStep={4} marginTopStep={4}>
                <Stack direction='column' childSeparationStep={4}>
                  <Separator />
                  <Text textType='headline3'>{translate('listing-detail.typical-features')}</Text>
                  <AutosTypicalFeatures vehicleId={vehicleId} mobileLayout={false} />
                </Stack>
              </Margin>
            </>
          )}

          {primaryAttributes && !vehicleAttributes && (
            <AttributeList
              title={translate('listing-detail.details')}
              expandThreshold={PRIMARY_CNT}
              categoryAttributes={primaryAttributes}
              trackClickEvent={trackClickEvent}
            />
          )}
          <ItemDescription description={description} />
          {secondaryAttributes && !vehicleAttributes && (
            <AttributeList
              title={translate('listing-detail.additional_details')}
              expandThreshold={ATTRIBUTE_TRUNCATE_THRESHOLD}
              categoryAttributes={secondaryAttributes}
              trackClickEvent={trackClickEvent}
            />
          )}
          {!isAutosDealer && <PurchaseProtectionSection isProSeller={isMerchant} />}
          <ActionButtons listing={listing} isOwnItem={isOwnItem} />
          {!isAutosDealer && !isMerchant && itemLocationLatitude && itemLocationLongitude && (
            <ListingMapView latitude={itemLocationLatitude} longitude={itemLocationLongitude} trackClickEvent={trackClickEvent} />
          )}
          {!isAutosDealer && !isMerchant && (
            <Margin marginLeftStep={4} marginBottomStep={4}>
              <Stack direction='column' childSeparationStep={4}>
                <Separator direction='column' />
                <Text textType='headline3'>{translate('listing-detail.related-searches')}</Text>
                <RelatedSearchTopics categoryId={categoryId} />
              </Stack>
            </Margin>
          )}
        </Stack>
        <ListingCTAInfoSection
          listingId={listingId}
          sellerId={ownerId}
          price={price}
          condition={ConditionParser.conditionNumberToString(condition, isCarsAndTrucks(categoryId))}
          categoryName={categoryName || categoryLevelThreeName || categoryLevelTwoName || categoryLevelOneName}
          ask={() => ask(ItemDetailElementName.Ask)}
          buyNow={() => buyNow(ItemDetailElementName.BuyNow)}
          makeOffer={() => makeOffer(ItemDetailElementName.MakeOffer)}
          share={() => shareListing(ItemDetailElementName.ShareTop)}
          isSaved={saved}
          save={() => saveListing(ItemDetailElementName.SaveTop)}
          sellerProfileProps={userProfileProps}
          postedTime={dateCompare + (itemLocationName ? ' in ' + itemLocationName : '')}
          vehicleAttributes={vehicleAttributes}
          isAutoDealer={isAutosDealer}
          isMerchant={isMerchant}
          chat={() => ask(ItemDetailElementName.Chat)}
          phoneNumber={clickToCallPhoneNumber}
          dealerWebsiteLink={websiteLink}
          dealerPublicLocation={publicLocation}
          dealerOpeningHours={openingHours}
          originalPrice={originalPrice}
          shippingEnabled={shippingEnabled}
          buyItNowEnabled={buyNowEnabled}
          isFirmOnPrice={listing?.isFirmOnPrice}
          localPickupEnabled={localPickupEnabled}
          shippingProps={shippingProps}
          location={location}
          trackClickEvent={trackClickEvent}
          trackShowEvent={trackShowEvent}
        />
      </Stack>
      {isAutosDealer && (
        <>
          <Stack direction='column' childSeparationStep={4}>
            <Separator />
            <Text textType='headline3'>{`${translate('listing-detail.more-from')} ${name}`}</Text>
            <UserAutosDealerItems
              userId={ownerId}
              onClickItem={onClickItem}
              useLoadMoreConfirmationButton={true}
              customLoadMoreButtonText={`${translate('listing-detail.view-more-from')} ${name}`}
            />
          </Stack>
        </>
      )}
    </Margin>
  )
}

const ItemDescription: FC<{ description: string }> = props => {
  const { description } = props

  return (
    <Margin marginStep={LISTING_MARGIN_STEP}>
      <Stack direction={STACK_DIRECTION} childSeparationStep={CHILD_SEPARATION_STEP} grow={1}>
        <Separator />
        <Spacer sizeStep={LISTING_SPACER_STEP} direction='column' />
        <Text textType='headline3'>{translate('listing-detail.description')}</Text>
        <Spacer sizeStep={LISTING_SPACER_STEP} direction='column' />
        <ExpandingTextSection
          textType='secondaryBody2'
          truncateThreshold={7}
          expandButtonTitle={translate('listing-detail.read_more')}
          collapseButtonTitle={translate('listing-detail.see_less')}
        >
          {description}
        </ExpandingTextSection>
        <Spacer sizeStep={LISTING_SPACER_STEP} direction='column' />
      </Stack>
    </Margin>
  )
}
