import React, { FC } from 'react'
import { useColorForTextColor, Margin, Text } from 'myproject-ucl'
import { StyleSheet, css } from 'aphrodite'

const LISTING_SOLD = 'SOLD'

const SoldIndicator: FC = () => {
  const style = StyleSheet.create({
    soldBackground: {
      borderRadius: 4,
      backgroundColor: useColorForTextColor('secondary')
    }
  })
  return (
    <div className={css(style.soldBackground)}>
      <Margin marginBottomStep={1} marginTopStep={1} marginRightStep={4} marginLeftStep={4}>
        <Text textType='primaryBody1' color='alwaysLight'>
          {LISTING_SOLD}
        </Text>
      </Margin>
    </div>
  )
}

export { SoldIndicator }
