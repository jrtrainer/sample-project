import React, { FC } from 'react'
import { Margin, Stack, Separator, Spacer, useColor } from 'myproject-ucl'
import _ from 'lodash'
import { Text } from 'myproject-ucl/controls/myproject-text/myproject-text'
import { translate } from 'myproject-shared'
import { ItemDetailElementName } from 'myproject-shared/analytics/constants/item-detail-constants'
import GoogleMapReact, { MapOptions } from 'google-map-react'
import { StyleSheet, css } from 'aphrodite/no-important'
import { openURLInNewTab, buildGoogleMapsURLForLocation } from '../../utilities'
import { GOOGLE_MAPS_WEB_KEY_DEBUG } from 'myproject-shared/utilities/api/keys'

const MAP_MIN_HEIGHT = 160
const DEFAULT_ZOOM = 11

export interface ListingMapViewProps {
  latitude: number
  longitude: number
  trackClickEvent: (elementName: ItemDetailElementName) => void
}

const ListingMapView: FC<ListingMapViewProps> = props => {
  const { longitude, latitude, trackClickEvent } = props

  const styles = StyleSheet.create({
    map: {
      width: '100%',
      minHeight: MAP_MIN_HEIGHT,
      flexGrow: 1
    }
  })

  const center = {
    lat: latitude,
    lng: longitude
  }

  const launchInMaps = () => {
    if (trackClickEvent) {
      trackClickEvent(ItemDetailElementName.Map)
    }

    openURLInNewTab(buildGoogleMapsURLForLocation(latitude, longitude))
  }

  const mapOptions: MapOptions = {
    zoomControl: false,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: false,
    gestureHandling: 'none'
  }

  return (
    <Margin marginLeftStep={4} marginRightStep={4} direction='column'>
      <Stack direction='column'>
        <Separator direction='column' />
        <Spacer direction='column' sizeStep={4} />
        <div className={css(styles.map)}>
          <GoogleMapReact
            bootstrapURLKeys={{ key: GOOGLE_MAPS_WEB_KEY_DEBUG }}
            defaultCenter={center}
            defaultZoom={DEFAULT_ZOOM}
            options={mapOptions}
            onClick={launchInMaps}
          >
            <ListingAreaCircle lat={latitude} lng={longitude} />
          </GoogleMapReact>
        </div>
        <Margin marginTopStep={2}>
          <Text textType='tertiaryBody2' color='secondary'>
            {translate('listing-detail.map_disclaimer')}
          </Text>
        </Margin>
      </Stack>
    </Margin>
  )
}

const CIRCLE_SIZE = 120
// tslint:disable-next-line: no-magic-numbers
const CIRCLE_OFFSET_FROM_CENTER = -(CIRCLE_SIZE / 2)
/**
 *
 * @param _props Unused here but used by the parent GoogleMapReact to place this element
 */
const ListingAreaCircle: FC<{ lat: number; lng: number }> = _props => {
  const { colors } = useColor()
  const style = StyleSheet.create({
    circle: {
      width: CIRCLE_SIZE,
      height: CIRCLE_SIZE,
      position: 'relative',
      top: CIRCLE_OFFSET_FROM_CENTER,
      left: CIRCLE_OFFSET_FROM_CENTER,
      opacity: 0.5,
      backgroundColor: colors.palouseGreen,
      borderRadius: '50%'
    }
  })
  return <div className={css(style.circle)} />
}

export { ListingMapView }
