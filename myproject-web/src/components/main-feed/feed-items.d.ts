import { FeedItem } from 'myproject-shared'

export interface FeedItemProps {
  mobileLayout: boolean
  item: FeedItem
  index: number
  visibilityChangeHandler: (visible: boolean, index: number) => void
}

export interface FeedItemAdProps extends FeedItemProps {
  onTrackImpression: (adId: string, feedbackUrl: string) => void
}
