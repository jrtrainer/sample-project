import React from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'
import { propertyOf } from 'lodash'
import { useScreen, VisibilityCheck, Text, Stack, Touchable, RemoteImage, Spacer, Margin, Border, Background } from 'myproject-ucl'
import { FeedItemAdProps } from './feed-items.d'
import { Urls, translate, AnalyticsBrowse, BrowseElementName, SearchScreenName } from 'myproject-shared'
import Router from 'next/router'

export const FeedItemSellerAd = (props: FeedItemAdProps): React.ReactElement => {
  const { item, index, mobileLayout, visibilityChangeHandler, onTrackImpression } = props
  const tile = propertyOf(item)('tile')
  const adId = propertyOf(tile)('ouAdId')
  const impressionUrl = propertyOf(tile)('impressionFeedbackUrls.0')
  const listing = propertyOf(tile)('listing')
  const title = propertyOf(listing)('title')
  const price = propertyOf(listing)('price')
  const location = propertyOf(listing)('locationDetails.locationName')
  const photos = propertyOf(listing)('photos')
  const imgUrl = propertyOf(photos)('0.list.url')

  const styleSheet = StyleSheet.create({
    feedItemSellerAd: { zIndex: 1, minWidth: 0, overflow: 'hidden' }
  })

  const handleVisibilityChange = (visible: boolean, vIndex: number) => {
    if (visible && adId && impressionUrl) {
      onTrackImpression(adId, impressionUrl)
    }

    visibilityChangeHandler(visible, vIndex)
  }

  const { screenName } = useScreen()
  const handlePressFeedItem = (event: React.MouseEvent) => {
    event.preventDefault()
    AnalyticsBrowse.trackButtonClick({
      elementName: BrowseElementName.TileClick,
      screenName: screenName as SearchScreenName
    })
    Router.push(Urls.item(listing.id))
  }

  return (
    <div className={css(styleSheet.feedItemSellerAd)}>
      <VisibilityCheck key={index} index={index} onVisibilityChange={handleVisibilityChange}>
        <Touchable href={Urls.item(listing.id)} onPress={handlePressFeedItem}>
          <Stack direction='column' grow={1}>
            <RemoteImage source={{ uri: imgUrl }} width='100%' resizeMode='cover' aspectRatio={1}>
              <Margin direction='column' grow={1} marginStep={1}>
                <Border direction={'column'} cornerRadius='small' lineWeight='none' grow={1} shrink={0}>
                  <Background type='overlay' />
                  <Text textAlign='center' textType='tertiaryBody2' color='primaryAlt'>
                    {translate('common-labels.promoted')}
                  </Text>
                </Border>
              </Margin>
            </RemoteImage>
            {!mobileLayout && (
              <Stack direction='column' grow={1}>
                <Spacer direction='column' sizeStep={1} />
                <Text numberOfLines={1} textType='primaryBody1'>
                  {title}
                </Text>
                <Text textType='primaryBody2'>{translate('common-labels.currency') + price || ''}</Text>
                <Spacer direction='column' sizeStep={2} />
                <Text textType='tertiaryBody2' color='secondary'>
                  {location || ' '}
                </Text>
              </Stack>
            )}
          </Stack>
        </Touchable>
      </VisibilityCheck>
    </div>
  )
}
