import { FeedItem, SearchResponse } from 'myproject-shared'

export interface SearchInputBarProps {
  term?: string
}

interface SearchResponsePropsType {
  feedItems: FeedItem[]
  next_page_cursor?: string
  query?: string
}
export interface MainFeedProps {
  searchInitState?: SearchStateProps
  searchResponse: SearchResponse
  onFirstLoadMoreCallback?(): void
}

export interface FeedItemsGridProps {
  feedItems: FeedItem[]
  onEndReachedThreshold?: number
  onEndReached?(info: { distanceFromEnd: number }): void
  onPressSaveSearchBanner?(): void
  renderSaveSearchBannerOnlyPlaceholder?(): React.ReactElement
}
