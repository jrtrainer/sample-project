import React from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'
import { propertyOf } from 'lodash'
import {
  Flex,
  VisibilityCheck,
  Text,
  Stack,
  Touchable,
  RemoteImage,
  Spacer,
  Margin,
  Border,
  ShippingTruckFill,
  SVG,
  Background,
  useScreen
} from 'myproject-ucl'
import { FeedItemProps } from './feed-items.d'
import { Urls, translate, AnalyticsBrowse, BrowseElementName, SearchScreenName } from 'myproject-shared'
import Router from 'next/router'

const FeedItemShippingLabel = (props: { shippingPrice?: number }) => {
  const { shippingPrice } = props

  return (
    <Flex grow={1} height='100%' direction='column' axisDistribution='flex-end'>
      <Margin marginStep={1} direction={shippingPrice ? 'row' : 'column'} grow={0}>
        <Border direction={shippingPrice ? 'row' : 'column'} cornerRadius='small' lineWeight='none'>
          <Background type='trust' />
          <Margin marginStep={1} marginTopStep={0.5} marginBottomStep={0.5} shrink={0} grow={0}>
            <Stack grow={1} direction='row' crossAxisDistribution='center' axisDistribution='center' childSeparationStep={1}>
              <SVG localSVG={{ ...ShippingTruckFill, size: { width: 12, height: 12 } }} tint='alwaysLight' />
              {!shippingPrice && (
                <Text color='alwaysLight' textType='tertiaryBody2'>
                  {translate('common-labels.free-shipping')}
                </Text>
              )}
            </Stack>
          </Margin>
        </Border>
      </Margin>
    </Flex>
  )
}

export const FeedItemListing = (props: FeedItemProps): React.ReactElement => {
  const { item, index, mobileLayout, visibilityChangeHandler } = props
  const listing = propertyOf(item)('tile')
  const title = propertyOf(listing)('title')
  const price = propertyOf(listing)('price')
  const location = propertyOf(listing)('locationDetails.locationName')
  const photos = propertyOf(listing)('photos')
  const imgUrl = propertyOf(photos)('0.list.url')
  const shippingEnabled = propertyOf(listing)('fulfillmentDetails.shippingEnabled')
  const shippingPrice = propertyOf(listing)('fulfillmentDetails.shippingPrice')

  const { screenName } = useScreen()
  const handlePressFeedItem = (event: React.MouseEvent) => {
    event.preventDefault()
    AnalyticsBrowse.trackButtonClick({
      elementName: BrowseElementName.TileClick,
      screenName: screenName as SearchScreenName
    })
    Router.push(Urls.item(listing.id))
  }
  const styleSheet = StyleSheet.create({
    feedItemListing: { zIndex: 1, minWidth: 0, overflow: 'hidden' }
  })

  return (
    <div className={css(styleSheet.feedItemListing)}>
      <VisibilityCheck key={index} index={index} onVisibilityChange={visibilityChangeHandler}>
        <Touchable href={Urls.item(listing.id)} onPress={handlePressFeedItem}>
          <Stack direction='column' grow={1}>
            <RemoteImage source={{ uri: imgUrl }} width='100%' resizeMode='cover' aspectRatio={1}>
              {shippingEnabled && <FeedItemShippingLabel shippingPrice={shippingPrice} />}
            </RemoteImage>
            {!mobileLayout && (
              <Stack direction='column' grow={1}>
                <Spacer direction='column' sizeStep={1} />
                <Text numberOfLines={1} textType='primaryBody1'>
                  {title}
                </Text>{' '}
                <Text textType='primaryBody2'>{translate('common-labels.currency') + price || ''}</Text>
                <Spacer direction='column' sizeStep={2} />
                <Text textType='tertiaryBody2' color='secondary'>
                  {location || ' '}
                </Text>
              </Stack>
            )}
          </Stack>
        </Touchable>
      </VisibilityCheck>
    </div>
  )
}
