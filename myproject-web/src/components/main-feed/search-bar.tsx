import { Input, Search } from 'myproject-ucl'
import React, { FC, useState } from 'react'
import { SearchInputBarProps } from './props.d'
import { useRouter } from 'next/router'

export const SearchInputBar: FC<SearchInputBarProps> = () => {
  const { query } = useRouter()
  const [searchTerm, setSearchTerm] = useState<string | undefined>(query.q as string)

  return (
    // TODO Get rid of hard coded values after https://myproject.atlassian.net/browse/CLIENT-574
    <form style={{ width: '464px' }} action='/'>
      <Input
        controlName='q'
        text={searchTerm}
        focusState='unfocused'
        hint='Search OfferUp'
        textChangeHandler={setSearchTerm}
        leadingIcon={Search}
      />
    </form>
  )
}
