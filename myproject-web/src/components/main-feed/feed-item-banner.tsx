import React from 'react'
import { Margin, Text } from 'myproject-ucl'
import { FeedItemProps } from './feed-items.d'
import { css, StyleSheet } from 'aphrodite/no-important'
import { propertyOf } from 'lodash'

enum Margins {
  Vertical = 4
}

export const FeedItemBanner: React.FC<FeedItemProps> = props => {
  const { item, mobileLayout } = props
  const bannerTile = propertyOf(item)('banner')
  const title = propertyOf(bannerTile)('label')
  const titleSize = mobileLayout ? 'headline3' : 'headline2'

  const styleSheet = StyleSheet.create({
    feedItemBanner: { zIndex: 1, minWidth: 0, gridColumn: '1 / -1', overflow: 'hidden' }
  })

  return (
    <div className={css(styleSheet.feedItemBanner)}>
      <Margin direction='column' marginTopStep={Margins.Vertical} marginBottomStep={Margins.Vertical}>
        <Text textType={titleSize}>{title}</Text>
      </Margin>
    </div>
  )
}
