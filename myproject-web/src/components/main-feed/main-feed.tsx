import React, { FC, useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import _ from 'lodash'
import { FeedItemsGrid, MainFeedProps } from './'
import { Button, ActivityIndicator, Center, Margin, GridItemType } from 'myproject-ucl'
import { useFeed, useSearch, FeedEmptyPlaceholder, useAuth, Urls, FeedOptionProps, translate } from 'myproject-shared'

const ENDREACHED_THREADHOLD = 5
const LOADING_INDICATOR_MARGIN_STEP = 5

export const MainFeed: FC<MainFeedProps> = ({ searchResponse, onFirstLoadMoreCallback }) => {
  const [enableLoadMore, setEnableLoadMore] = useState(false)

  const { asPath } = useRouter()
  const auth = useAuth()
  const { state: searchState } = useSearch()
  const [forceReloadSearch, setForceReloadSearch] = useState(false)
  const { state, setFeedData } = useSearch()
  React.useEffect(() => {
    setForceReloadSearch(!forceReloadSearch)
  }, [state.searchParams])

  const { loading, feedItems, feedOptions, searchAlert, isEndOfFeedReached, loadMore } = useFeed({
    searchParams: searchState.searchParams,
    prefetchData: searchResponse && {
      feedItems: searchResponse.feedItems,
      nextPageCursor: searchResponse.nextPageCursor,
      searchAlert: searchResponse.searchAlert
    }
  })
  useEffect(() => {
    setForceReloadSearch(!forceReloadSearch)
  }, [searchState.searchParams])

  React.useEffect(() => {
    if (feedOptions && (!_.isEqual(feedOptions, state.feedOptions) || !_.isEqual(searchAlert, state.searchAlert))) {
      setFeedData({
        feedOptions: feedOptions as FeedOptionProps[],
        searchAlert: searchAlert || undefined
      })
    }
  }, [searchAlert, feedOptions])

  const handleClickLoadMore = () => {
    setEnableLoadMore(true)
    if (onFirstLoadMoreCallback) {
      onFirstLoadMoreCallback()
    }
    loadMore()
  }
  const handleSaveAlertPress = () => {
    if (!auth.isAuthed) {
      window.location.assign(`${Urls.login}?redirect=${encodeURIComponent(asPath)}`)
      return
    }
  }

  const firstTileIsListing = feedItems && feedItems.length && feedItems[0].type === GridItemType.Item
  const shouleShowLoadMoreButton = !enableLoadMore && !isEndOfFeedReached && firstTileIsListing

  const renderSaveSearchPlaceholder = () => {
    return <FeedEmptyPlaceholder query={searchState?.searchParams?.query ?? undefined} onSaveAlertPress={handleSaveAlertPress} />
  }

  return (
    <>
      {feedItems && (
        <>
          {feedItems.length ? (
            <>
              <FeedItemsGrid
                feedItems={feedItems}
                onEndReachedThreshold={ENDREACHED_THREADHOLD}
                onEndReached={enableLoadMore ? loadMore : undefined}
                renderSaveSearchBannerOnlyPlaceholder={renderSaveSearchPlaceholder}
              />
              {shouleShowLoadMoreButton && (
                <Margin marginTopStep={LOADING_INDICATOR_MARGIN_STEP} marginBottomStep={LOADING_INDICATOR_MARGIN_STEP}>
                  <Center>
                    <Button
                      buttonType='primary'
                      buttonSize='large'
                      title={translate('common-actions.view-more-items')}
                      onClick={handleClickLoadMore}
                    />
                  </Center>
                </Margin>
              )}
            </>
          ) : (
            <>
              <FeedEmptyPlaceholder query={searchState?.searchParams?.query ?? undefined} onSaveAlertPress={handleSaveAlertPress} />
            </>
          )}
        </>
      )}
      {loading && (
        <Margin marginTopStep={LOADING_INDICATOR_MARGIN_STEP} marginBottomStep={LOADING_INDICATOR_MARGIN_STEP}>
          <Center>
            <ActivityIndicator size='large' testID='search-screen.activity-indicator' />
          </Center>
        </Margin>
      )}
    </>
  )
}
