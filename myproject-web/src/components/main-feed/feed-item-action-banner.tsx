import React from 'react'
import { FeedItemProps } from './feed-items.d'
import { css, StyleSheet } from 'aphrodite/no-important'

interface FeedItemActionBannerProps extends FeedItemProps {
  renderBanner?(): React.ReactElement
}

export const FeedItemActionBanner: React.FC<FeedItemActionBannerProps> = props => {
  const { renderBanner } = props

  const styleSheet = StyleSheet.create({
    feedItemActionBanner: { zIndex: 1, minWidth: 0, gridColumn: '1 / -1', overflow: 'hidden' }
  })

  return (
    <div className={css(styleSheet.feedItemActionBanner)}>
      {renderBanner && renderBanner()}
      {!renderBanner && <></>}
    </div>
  )
}
