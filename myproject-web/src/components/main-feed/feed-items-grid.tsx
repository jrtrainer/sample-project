import { propertyOf } from "lodash"
import { useInfiniteScroll, Grid } from "myproject-ucl"
import React, { FC } from "react"
import { FeedItemsGridProps } from "./props.d"
import { useMediaQuery } from "../../providers/media-query-provider/media-query-provider"
import { MediaName } from "myproject-ucl/utilities/media-query/media-query"
import { FeedItemProps } from "./feed-items.d"
import { FeedItem } from "myproject-shared/gql-tags"
import { FeedItemAd } from "./feed-item-ad"
import { FeedItemListing } from "./feed-item-listing"
import { FeedItemActionBanner } from "./feed-item-action-banner"
import { FeedItemSellerAd } from "./feed-item-seller-ad"
import { FeedItemBanner } from "./feed-item-banner"
import { useAdTracking } from "myproject-shared/utilities/hooks/ads/use-ad-tracking"

export const FeedItemsGrid: FC<FeedItemsGridProps> = props => {
  const { feedItems, onEndReached, onEndReachedThreshold, renderSaveSearchBannerOnlyPlaceholder } = props
  const { handleItemVisibilityChange } = useInfiniteScroll({
    data: feedItems,
    onEndReachedThreshold,
    onEndReached,
  })
  const { mediaQueryName } = useMediaQuery()
  const mobileLayout = mediaQueryName === MediaName.Mobile
  const feedProps: Omit<FeedItemProps, "index" | "item"> = {
    visibilityChangeHandler: handleItemVisibilityChange,
    mobileLayout,
  }

  const trackAdImpression = useAdTracking()

  const _renderItem = (it: FeedItem, i: number): React.ReactElement => {
    const type = propertyOf(it)("type")

    switch (type) {
      case "ad":
        return <FeedItemAd {...feedProps} item={it} index={i} key={i} onTrackImpression={trackAdImpression} />
      case "item":
        return <FeedItemListing {...feedProps} item={it} index={i} key={i} />
      case "seller_ad":
        return <FeedItemSellerAd {...feedProps} item={it} index={i} key={i} onTrackImpression={trackAdImpression} />
      case "action_banner":
        if (feedItems.length === 1) {
          return (
            <FeedItemActionBanner
              {...feedProps}
              item={it}
              index={i}
              key={i}
              renderBanner={renderSaveSearchBannerOnlyPlaceholder}
            />
          )
        }
        return <></>
      case "banner":
        return <FeedItemBanner {...feedProps} item={it} index={i} key={i} />

      default:
        return <></>
    }
  }

  return (
    <div style={{ justifyContent: "center" }}>
      {feedItems.length > 0 && <Grid data={feedItems} renderItem={_renderItem} />}
    </div>
  )
}
