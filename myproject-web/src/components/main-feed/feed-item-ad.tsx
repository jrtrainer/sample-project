import React from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'
import { propertyOf } from 'lodash'
import { VisibilityCheck, Border, Flex, Background, Text, Stack, Touchable, Margin, RemoteImage, Spacer, useScreen } from 'myproject-ucl'
import { FeedItemAdProps } from './feed-items.d'
import { translate, AnalyticsBrowse, BrowseElementName, SearchScreenName } from 'myproject-shared'

interface AdTextProps {
  title: string
  price: string
  seller?: string
  mobileLayout?: boolean
}

const AdText: React.FC<AdTextProps> = props => {
  const { title, price, seller, mobileLayout } = props
  const titleTextType = mobileLayout ? 'tertiaryBody2' : 'primaryBody1'
  const priceTextType = mobileLayout ? 'tertiaryBody2' : 'primaryBody2'
  const adTextType = mobileLayout ? 'tertiaryBody2' : 'secondaryBody2'

  return (
    <Margin direction='column' marginStep={mobileLayout ? 2 : 0} grow={1}>
      <Stack direction='column' grow={1}>
        <Text numberOfLines={1} textType={titleTextType}>
          {title}
        </Text>
        <Text textType={priceTextType}>{price}</Text>
        <Stack grow={1} direction='row'>
          <Flex grow={1} shrink={1}>
            <Text textType={adTextType} color='secondary' numberOfLines={1}>
              {seller || ' '}
            </Text>
          </Flex>
          <Border cornerRadius='small' lineWeight='none' grow={0} shrink={0}>
            <Background type='overlay' />
            <Margin marginStep={0.5}>
              <Text textType={adTextType} color='primaryAlt'>
                {translate('common-labels.ad')}
              </Text>
            </Margin>
          </Border>
        </Stack>
      </Stack>
    </Margin>
  )
}

export const FeedItemAd: React.FC<FeedItemAdProps> = props => {
  const { mobileLayout, item, index, visibilityChangeHandler, onTrackImpression } = props
  const adTile = propertyOf(item)('tile')
  const adId = propertyOf(adTile)('ouAdId')
  const impressionUrl = propertyOf(adTile)('impressionFeedbackUrl')
  const imgUrl = propertyOf(adTile)('imageUrl')
  const imgWidth = propertyOf(adTile)('imageWidth')
  const imgHeight = propertyOf(adTile)('imageHeight')
  const title = propertyOf(adTile)('itemName')
  const seller = propertyOf(adTile)('sellerName')
  const price = translate('common-labels.currency') + propertyOf(adTile)('price') || ''
  const contentUrl = propertyOf(adTile)('contentUrl')
  const { screenName } = useScreen()

  const styleSheet = StyleSheet.create({
    feedItemAd: {
      position: 'relative',
      zIndex: 2,
      minWidth: 0,
      gridRow: mobileLayout ? 'auto / span 2' : 'auto',
      overflow: 'hidden'
    }
  })

  const handlePressFeedItem = (_event: React.MouseEvent) => {
    AnalyticsBrowse.trackButtonClick({
      elementName: BrowseElementName.TileClick,
      screenName: screenName as SearchScreenName
    })
  }

  const handleVisibilityChange = (visible: boolean, vIndex: number) => {
    if (visible) {
      onTrackImpression(adId, impressionUrl)
    }

    visibilityChangeHandler(visible, vIndex)
  }

  return (
    <div className={css(styleSheet.feedItemAd)}>
      <VisibilityCheck key={index} index={index} onVisibilityChange={handleVisibilityChange}>
        <Touchable href={contentUrl} onPress={handlePressFeedItem}>
          {mobileLayout && (
            <>
              <Margin direction='column' marginTopStep={8}>
                <RemoteImage source={{ uri: imgUrl }} width='100%' resizeMode='contain' aspectRatio={imgWidth / imgHeight} />
              </Margin>
              <div style={{ position: 'absolute', left: 0, right: 0, bottom: 0 }}>
                <AdText title={title} seller={seller} price={price} mobileLayout={mobileLayout} />
              </div>
            </>
          )}
          {!mobileLayout && (
            <Stack direction='column' grow={1}>
              <RemoteImage source={{ uri: imgUrl }} width='100%' resizeMode='contain' aspectRatio={1} />
              <Spacer direction='column' sizeStep={1} />
              <AdText title={title} seller={seller} price={price} mobileLayout={mobileLayout} />
            </Stack>
          )}
        </Touchable>
      </VisibilityCheck>
    </div>
  )
}
