import { getMediaNameBasedOnWindowWidth, Media, MediaName } from "myproject-ucl"
import React, { createContext, FC, useContext, useEffect, useState } from "react"
import { useGlobal } from "../global-provider"

interface MediaQueryProviderType {
  mediaQueryName?: MediaName
  setMediaQueryName: (_mqName: MediaName) => void
}

export const MediaQueryContext = createContext<MediaQueryProviderType>({
  setMediaQueryName: (_mqName: MediaName) => {},
})

export const useMediaQuery = () => {
  let mobileMQ: MediaQueryList
  let tabletMQ: MediaQueryList
  let desktopMQ: MediaQueryList
  const { mediaQueryName, setMediaQueryName } = useContext(MediaQueryContext)

  const handleMediaQueryStatusChange = () => {
    setMediaQueryName(getMediaNameBasedOnWindowWidth() || MediaName.Desktop)
  }

  const addMQChangeEventListener = (mq: MediaQueryList) => {
    try {
      mq.addEventListener("change", handleMediaQueryStatusChange)
    } catch (e) {
      // Fallback for older browser
      mq.addListener(handleMediaQueryStatusChange)
    }
  }
  const removeMQChangeEventListener = (mq: MediaQueryList) => {
    try {
      mq.removeEventListener("change", handleMediaQueryStatusChange)
    } catch (e) {
      // Fallback for older browser
      mq.addListener(handleMediaQueryStatusChange)
    }
  }
  useEffect(() => {
    handleMediaQueryStatusChange()
    if (getMediaNameBasedOnWindowWidth() === MediaName.Mobile) {
      mobileMQ = window.matchMedia(Media[MediaName.Mobile])
      addMQChangeEventListener(mobileMQ)
    }
    if (getMediaNameBasedOnWindowWidth() === MediaName.Tablet) {
      tabletMQ = window.matchMedia(Media[MediaName.Tablet])
      addMQChangeEventListener(tabletMQ)
    }
    if (getMediaNameBasedOnWindowWidth() === MediaName.Desktop) {
      desktopMQ = window.matchMedia(Media[MediaName.Desktop])
      addMQChangeEventListener(desktopMQ)
    }
    return () => {
      if (mobileMQ) {
        removeMQChangeEventListener(mobileMQ)
      }
      if (tabletMQ) {
        removeMQChangeEventListener(tabletMQ)
      }
      if (desktopMQ) {
        removeMQChangeEventListener(desktopMQ)
      }
    }
  }, [])

  return {
    mediaQueryName,
  }
}
export const MediaQueryProvider: FC = ({ children }) => {
  const { device } = useGlobal()
  let initialMediaQueryName = MediaName.Desktop
  if (device) {
    if (device.isMobile) {
      initialMediaQueryName = MediaName.Mobile
    } else if (device.isTablet) {
      initialMediaQueryName = MediaName.Tablet
    }
  }
  const [mediaQueryName, setMediaQueryName] = useState<MediaName>(initialMediaQueryName)
  return (
    <MediaQueryContext.Provider value={{ mediaQueryName, setMediaQueryName }}>{children}</MediaQueryContext.Provider>
  )
}
