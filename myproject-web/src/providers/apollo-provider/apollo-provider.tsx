import { ApolloProvider } from "@apollo/react-common"
import { AppInitialProps } from "next/app"
import React, { FC } from "react"
import { useApolloGraphQl } from "myproject-shared/network/apollo"

export const RNApolloProvider: FC<AppInitialProps> = ({ children, pageProps }) => {
  const apolloClient = useApolloGraphQl(pageProps.initialApolloState)

  return <ApolloProvider client={apolloClient}>{children}</ApolloProvider>
}
