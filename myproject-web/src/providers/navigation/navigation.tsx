import React, { createContext, FC, useContext } from 'react'
import { useRouter } from 'next/router'
import URLPattern from 'url-pattern'
import { useModal } from '../../widgets'
import _ from 'lodash'
import { Text, Margin } from 'myproject-ucl'
export const WebNavigationContext = createContext<{
  navigateToActionPath: (actionPath: string) => Promise<void>
}>({
  navigateToActionPath: async (_actionPath: string) => {}
})
export const WebNavigationProvider: FC = ({ children }) => {
  const router = useRouter()

  const navigateToActionPath = async (actionPath: string) => {
    await Promise.all(_.map(listConfigPattern, match => match(actionPath)))
  }

  const { showModal } = useModal()
  const openModal = (pattern: string, content: (parameters: any) => JSX.Element) => {
    return (actionPath: string) => {
      const match = isMatchWithPattern(pattern, actionPath)
      if (!match) {
        return
      }
      showModal({
        content: content(match)
      })
    }
  }
  const isMatchWithPattern = (pattern: string, actionPath: string) => {
    const urlPattern = new URLPattern(pattern)
    const parameters = urlPattern.match(actionPath)
    if (urlPattern.match(actionPath)) {
      return parameters
    }
    return false
  }
  const routeToPage = (pattern: string, path?: string) => {
    return async (actionPath: string) => {
      const match = isMatchWithPattern(pattern, actionPath)
      if (!match) {
        return
      }
      if (!path) {
        await router.push(actionPath)
        return
      }
      await router.push(new URLPattern(path).stringify(match))
    }
  }

  const listConfigPattern = [
    // TO-DO once components implemented on web
    openModal('/shipping/view_offer/item/:itemId/discussion/:discussionId/buyer/:buyerId', (parameters: any) => (
      <Margin direction='column' marginStep={4}>
        <Text>View Offer Example</Text>
        <Text>Parameters</Text>
        <Text>{JSON.stringify(parameters)}</Text>
      </Margin>
    )),
    routeToPage('/payments/:paymentid/receipt'),
    routeToPage('/search*')
  ]
  return (
    <WebNavigationContext.Provider
      value={{
        navigateToActionPath
      }}
    >
      {children}
    </WebNavigationContext.Provider>
  )
}

export const useWebNavigation = () => {
  const { navigateToActionPath } = useContext(WebNavigationContext)
  return {
    navigateToActionPath
  }
}
