import React, { createContext, FC, useContext } from "react"
import { DeviceInfoProps } from "../../server/utils/device"
import { User } from "myproject-shared/gql-tags"

interface GlobalProps {
  device?: DeviceInfoProps
  authUser?: User
}

const initValue: GlobalProps = {}

export const GlobalContext = createContext<GlobalProps>(initValue)

export const useGlobal = (): GlobalProps => {
  return useContext(GlobalContext)
}
export const GlobalProvider: FC<{ value: GlobalProps }> = props => {
  const { value, children } = props
  return <GlobalContext.Provider value={value}>{children}</GlobalContext.Provider>
}
