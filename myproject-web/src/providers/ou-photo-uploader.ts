import { Uploadable, UploadPhotoFunctionType } from 'myproject-shared'

export const SinglePhotoUploader: UploadPhotoFunctionType = async (s3BucketUrl: string, file: Uploadable) => {
  await fetch(s3BucketUrl, {
    method: 'PUT',
    headers: {
      'Content-Type': ''
    },
    body: file as File
  })
}
