import { MediaName, Flex } from "myproject-ucl"
import React from "react"
import { useMediaQuery } from "../../providers"
import { DialogLayoutProp } from "./myproject-dialog-props"

export const DialogContainer: React.FC<DialogLayoutProp> = ({ children, width, height }) => {
  const { mediaQueryName } = useMediaQuery()
  const isMobile = mediaQueryName === MediaName.Mobile
  return (
    <Flex width={isMobile ? "100%" : width} height={isMobile ? "100%" : height}>
      {children}
    </Flex>
  )
}
