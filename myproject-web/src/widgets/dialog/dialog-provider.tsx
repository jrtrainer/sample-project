import React from "react"
import { useModal } from "../myproject-modal"
import { ErrorDialog, AffirmRejectDialogContent, Flex, MediaName } from "myproject-ucl"
import { ShowErrorDialogParam, ShowAffirmRejectDialogParam, DialogLayoutProp } from "./myproject-dialog-props"
import uuid from "uuid"
import { useMediaQuery } from "../../providers"
import { DialogContainer } from "./myproject-dialog-container"

const DialogContext = React.createContext({
  showErrorDialog: (_param: ShowErrorDialogParam) => {},
  showAffirmRejectDialog: (_param: ShowAffirmRejectDialogParam) => {},
})

export const useDialog = () => React.useContext(DialogContext)

export const DialogProvider: React.FC = ({ children }) => {
  const { showModal, closeModal } = useModal()

  const showErrorDialog = (param: ShowErrorDialogParam) => {
    const modalId = `error-dialog-${uuid.v4()}`
    const onDismiss = () => {
      closeModal(modalId)
      param.onDismissPressed && param.onDismissPressed()
    }
    showModal({
      ...param,
      content: (
        <DialogContainer {...param}>
          <ErrorDialog {...param} onDismissPressed={onDismiss} />
        </DialogContainer>
      ),
      id: modalId,
    })
  }

  const showAffirmRejectDialog = (param: ShowAffirmRejectDialogParam) => {
    const modalId = `affirm-reject-dialog-${uuid.v4()}`
    const onDismiss = () => {
      closeModal(modalId)
    }

    showModal({
      ...param,
      content: (
        <DialogContainer {...param}>
          <AffirmRejectDialogContent {...param} dismiss={onDismiss} />
        </DialogContainer>
      ),
      id: modalId,
    })
  }

  return <DialogContext.Provider value={{ showErrorDialog, showAffirmRejectDialog }}>{children}</DialogContext.Provider>
}
