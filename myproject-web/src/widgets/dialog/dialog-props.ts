import { AffirmRejectDialogProps, ErrorDialogProps, LayoutContainerProps } from 'myproject-ucl'
import { ModalProps } from '../myproject-modal'

export interface DialogLayoutProp {
  width?: LayoutContainerProps['width']
  height?: LayoutContainerProps['height']
}

export interface ShowErrorDialogParam extends ErrorDialogProps, Pick<ModalProps, 'withCloseIcon' | 'canCloseItself'>, DialogLayoutProp {}

export interface ShowAffirmRejectDialogParam
  extends Omit<AffirmRejectDialogProps, 'dismiss'>,
    Pick<ModalProps, 'withCloseIcon' | 'canCloseItself'>,
    DialogLayoutProp {
  onDismiss?: () => void
}
