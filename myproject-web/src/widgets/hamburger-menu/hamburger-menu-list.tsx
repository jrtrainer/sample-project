import { Text, Stack } from "myproject-ucl"
import React, { FC } from "react"
import { HamburgerMenuListProps } from "./myproject-hamburger-menu.d"
import { HamburgerMenuListItem } from "./hamburger-menu-list-item"
import { MENU_ITEM_MARGIN } from "./hamburger-menu-constants"
export const HamburgerMenuList: FC<HamburgerMenuListProps> = ({ list }) => {
  const { title, items } = list
  return (
    <Stack direction="column" crossAxisDistribution="flex-start" childSeparationStep={MENU_ITEM_MARGIN}>
      <Text textType={"headline3"} color="primary">
        {title}
      </Text>
      {items.map((item, index) => (
        <HamburgerMenuListItem item={item} key={index} />
      ))}
    </Stack>
  )
}
