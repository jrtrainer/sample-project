import React, { FC, useState } from "react"
import Link from "next/link"
import {
  LogoPrimary,
  ViewBlock,
  Stack,
  SVG,
  ActionMenu,
  Spacer,
  SpacerFlex,
  Separator,
  MediaQuery,
  MediaName,
  useColorForBackgroundColor,
  ActionClose,
} from "myproject-ucl"
import { HeaderNav } from "../myproject-header-nav"
import { PageMargin } from "../page"
import { StyleSheet, css } from "aphrodite/no-important"
import { MENU_LISTS, MENU_ITEM_MARGIN, CONTAINER_MARGIN } from "./hamburger-menu-constants"
import { HamburgerMenuList } from "./hamburger-menu-list"
import { HambugerMenuNavAuthed } from "./myproject-hambuger-menu-nav-authed"
import { useAuth } from "myproject-shared"

export const HamburgerMenu: FC = () => {
  const auth = useAuth()
  const [open, setOpen] = useState(false)
  const backgroundColor = useColorForBackgroundColor("primary")
  const STYLES = StyleSheet.create({
    container: {
      position: "fixed",
      height: "100%",
      width: "100%",
      top: 0,
      left: 0,
      overflow: "auto",
      zIndex: 1,
      backgroundColor,
    },
    logo: {
      width: "99px",
      height: "24px",
      [MediaQuery[MediaName.Desktop]]: {
        width: "124px",
        height: "30px",
      },
    },
  })
  const handlePressMenu = () => {
    setOpen(!open)
  }
  if (!open) {
    return <SVG localSVG={ActionMenu} tint={"primary"} onPress={handlePressMenu} />
  }
  return (
    <ViewBlock className={css(STYLES.container)}>
      <PageMargin>
        <Spacer direction="column" sizeStep={CONTAINER_MARGIN} />
        <Stack
          direction="row"
          crossAxisDistribution="center"
          axisDistribution="center"
          childSeparationStep={MENU_ITEM_MARGIN}>
          <SVG localSVG={ActionClose} tint={"primary"} onPress={handlePressMenu} />
          <Link href="/">
            <a>
              <SVG
                localSVG={{
                  SVG: LogoPrimary.SVG,
                }}
                className={css(STYLES.logo)}
              />
            </a>
          </Link>
          <SpacerFlex />
          {!auth.isAuthed && <HeaderNav />}
        </Stack>
      </PageMargin>
      <Spacer direction="row" sizeStep={MENU_ITEM_MARGIN} />
      <Stack direction="column" childSeparationStep={MENU_ITEM_MARGIN}>
        {!!auth.isAuthed && (
          <Stack direction="column" childSeparationStep={MENU_ITEM_MARGIN}>
            <HambugerMenuNavAuthed />
            <Separator />
          </Stack>
        )}
        {MENU_LISTS.map((list, index) => (
          <Stack key={index} direction="column" childSeparationStep={MENU_ITEM_MARGIN}>
            <PageMargin>
              <HamburgerMenuList list={list} />
            </PageMargin>
            <Separator />
          </Stack>
        ))}
      </Stack>
    </ViewBlock>
  )
}
