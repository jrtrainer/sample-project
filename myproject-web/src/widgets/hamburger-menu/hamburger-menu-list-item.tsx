import { Text, Touchable } from "myproject-ucl"
import React, { FC } from "react"
import { HamburgerMenuListItemProps } from "./myproject-hamburger-menu.d"

export const HamburgerMenuListItem: FC<HamburgerMenuListItemProps> = ({ item }) => {
  return (
    <Touchable href={item.href}>
      <Text textType={"secondaryBody2"} color={"primary"}>
        {item.title}
      </Text>
    </Touchable>
  )
}
