export const CONTAINER_MARGIN = 4
export const MENU_ITEM_MARGIN = 4
export const MENU_LISTS = [
  {
    title: 'Sell',
    items: [
      {
        title: 'Individual seller',
        href: ''
      },
      {
        title: 'Pro seller',
        href: ''
      },
      {
        title: 'Auto dealerships',
        href: ''
      }
    ]
  },
  {
    title: 'Help',
    items: [
      {
        title: 'Help center',
        href: 'https://help.myproject.com'
      },
      {
        title: 'Community',
        href: ''
      },
      {
        title: 'Blog',
        href: 'https://blog.myproject.com'
      },
      {
        title: 'Sign out',
        href: ''
      }
    ]
  }
]
