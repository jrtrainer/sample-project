import { TextColors } from 'myproject-ucl'

export interface HamburgerMenuListItemDetailProps {
  title: string
  href: string
}
export interface HamburgerMenuListItemProps {
  item: HamburgerMenuListItemDetailProps
}

export interface HamburgerMenuListDetailProps {
  title: string
  items: HamburgerMenuListItemDetailProps[]
}

export interface HamburgerMenuListProps {
  list: HamburgerMenuListDetailProps
}
