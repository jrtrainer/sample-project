import React, { FC } from "react"
import { StyleSheet, css } from "aphrodite/no-important"
import {
  useColor,
  useMargin,
  HeartLine,
  Touchable,
  Stack,
  Text,
  Avatar,
  SVG,
  TabBarMyOffersOutline,
  TabBarNotificationsOutline,
  Flex,
  Separator,
} from "myproject-ucl"
import { useAuth } from "myproject-shared"
import _ from "lodash"

const MARGIN = 4
export const HambugerMenuNavAuthed: FC = () => {
  const auth = useAuth()
  const { colors } = useColor()
  const { baseMargin } = useMargin()
  const STYLES = StyleSheet.create({
    item: {
      borderRight: `1px solid ${colors.limestone}`,
      ":last-child": {
        borderRight: 0,
      },
    },
    rotate90Left: {
      transform: "rotate(-90deg)",
    },
    image: {
      width: 48,
      height: 48,
      objectFit: "cover",
      display: "block",
    },
  })
  return (
    <Flex direction={"row"}>
      <Touchable href={""}>
        <Stack direction={"column"} childSeparationStep={0} crossAxisDistribution={"center"}>
          <SVG tint={"primary"} localSVG={HeartLine} />
          <Text textType="secondaryBody2">Saves</Text>
        </Stack>
      </Touchable>
      <Separator direction="row" />
      <Touchable href={"/inbox"}>
        <Stack direction={"column"} childSeparationStep={0} crossAxisDistribution={"center"}>
          <SVG tint={"primary"} localSVG={TabBarNotificationsOutline} />
          <Text textType="secondaryBody2">Inbox</Text>
        </Stack>
      </Touchable>
      <Separator direction="row" />
      <Touchable href={"/selling/"}>
        <Stack direction={"column"} childSeparationStep={0} crossAxisDistribution={"center"}>
          <SVG tint={"primary"} localSVG={TabBarMyOffersOutline} />
          <Text textType="secondaryBody2">Selling</Text>
        </Stack>
      </Touchable>
      <Separator direction="row" />
      <Touchable href={"/account"}>
        <Stack direction={"column"} childSeparationStep={0} crossAxisDistribution={"center"}>
          <Avatar size={24} source={{ uri: _.property<any, string>("user.profile.avatars.squareImage")(auth) }} />
          <Text textType="secondaryBody2">Account</Text>
        </Stack>
      </Touchable>
    </Flex>
  )
}
