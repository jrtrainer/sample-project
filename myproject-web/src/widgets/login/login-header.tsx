import { LogoPrimaryWithTag, SVG, Text, Button } from 'myproject-ucl'
import { Stack, Margin } from 'myproject-ucl/controls/layout'
import React, { FC } from 'react'
import { useRouter } from 'next/router'
export const LoginHeader: FC = ({ children }) => {
  const navigator = useRouter()
  const onCancelClick = () => {
    navigator.back()
  }
  return (
    <div style={{ position: 'relative', width: '100%' }}>
      <Stack direction='column' crossAxisDistribution='center' grow={1} width='100%' childSeparationStep={4}>
        <Text textType='headline2' textAlign='center'>
          {children}
        </Text>
        <div style={{ position: 'absolute', right: 0 }}>
          <Button buttonSize='small' buttonType='flat' title='Cancel' onClick={onCancelClick} />
        </div>
        <Margin marginTopStep={18} marginBottomStep={6}>
          <SVG localSVG={LogoPrimaryWithTag} />
        </Margin>
      </Stack>
    </div>
  )
}
