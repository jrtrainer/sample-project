import { AuthScreen, AuthScreenElement, Urls } from 'myproject-shared'
import { AnalyticsAuthShared } from 'myproject-shared/analytics/account/myproject-analytics-auth-shared'
import { NextRouter } from 'next/router'
import { getWebViewLinks } from 'myproject-shared/utilities/settings/myproject-server-settings'

const webViewLinks = getWebViewLinks()

export const openTerms = (screen: AuthScreen) => {
  AnalyticsAuthShared.trackLoginUiEventClick(screen, AuthScreenElement.TermsOfService)
  window.open(webViewLinks.OfferUpTerms, '_blank')
}

export const openPrivacyPolicy = (screen: AuthScreen) => {
  AnalyticsAuthShared.trackLoginUiEventClick(screen, AuthScreenElement.PrivacyPolicy)
  window.open(webViewLinks.OfferupPrivacy, '_blank')
}

export const openHelp = (screen: AuthScreen) => {
  AnalyticsAuthShared.trackLoginUiEventClick(screen, AuthScreenElement.Help)
  window.open(webViewLinks.HelpCenter, '_blank')
}

export const openForgotPassword = (screen: AuthScreen, router?: NextRouter) => {
  AnalyticsAuthShared.trackLoginUiEventClick(screen, AuthScreenElement.ForgotPassword)
  router?.push(Urls.resetPassword)
}
