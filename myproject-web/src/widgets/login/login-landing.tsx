import { css, StyleSheet } from 'aphrodite/no-important'
import { useRouter } from 'next/router'
import { FacebookLoginButton, GoogleLoginButton } from 'myproject-shared/components/auth/login'
import { LoginAttributionText } from './login-attribution-text'
import { AppleLoginButton, EmailLoginButton, Center, SpacerFlex, Stack, Text } from 'myproject-ucl'
import React, { FC } from 'react'
import { version } from '../../../package.json'
import { LoginHeader } from './login-header'
import { AuthScreen, Urls, ExceptionAction, AnalyticsDebug, translate } from 'myproject-shared'
import { useDialog } from '..'
const styles = StyleSheet.create({
  cancelButtonContainer: {
    position: 'absolute',
    right: 30,
    top: 30
  },
  bottomAttributionText: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center'
  }
})
export const LoginLanding: FC = () => {
  const navigator = useRouter()
  const { showErrorDialog } = useDialog()

  const onFacebookStart = () => {}
  const onFacebookCancel = () => {}
  const onFacebookFailure = () => {}
  const onFacebookSuccess = () => {}

  const onErrorGoogle = (error: any) => {
    AnalyticsDebug.logError(error)

    if (error.title || error.code || error.message || error.httpStatusCode) {
      showErrorDialog({
        ouException: error,
        onActionItemPressed: (_action: ExceptionAction) => {},
        onDismissPressed: () => {},
        withCloseIcon: true,
        canCloseItself: true
      })
    }
  }
  const onContinueWithGoogle = () => {
    const url = (navigator.query.redirect as string) || Urls.account

    // we force a client side refresh so that the auth middleware can requery the user
    window.location.assign(url)
  }
  const onContinueWithApple = () => {}

  const onLoginClick = () => {
    navigator.push('/login-with-email')
  }

  const TopButtons = () => (
    <>
      <Stack direction='column' grow={1} width='100%' childSeparationStep={4}>
        <FacebookLoginButton onSuccess={onFacebookSuccess} onCancel={onFacebookCancel} onClick={onFacebookStart} onFailure={onFacebookFailure} />
        <GoogleLoginButton onFailure={onErrorGoogle} onSuccess={onContinueWithGoogle} />
        <AppleLoginButton buttonText='Continue with Apple' onClick={onContinueWithApple} isFullWidth={true} />
        <EmailLoginButton buttonText={translate('auth.login')} onClick={onLoginClick} isFullWidth={true} />
      </Stack>
    </>
  )

  const Footer = () => (
    <Stack direction='column' childSeparationStep={4}>
      <LoginAttributionText screen={AuthScreen.LoginCreateAccount} />
      <div className={css(styles.bottomAttributionText)}>
        <Center>
          <Text color='secondary' textType='tertiaryBody2' textAlign='center'>
            {`Version ${version}`}
          </Text>
        </Center>
      </div>
    </Stack>
  )

  return (
    <Stack direction='column' width='100%' grow={1} crossAxisDistribution='center' childSeparationStep={4}>
      <LoginHeader>{translate('auth.signup-or-login')}</LoginHeader>
      <TopButtons />
      <SpacerFlex />
      <Footer />
    </Stack>
  )
}
