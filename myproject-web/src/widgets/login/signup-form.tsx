import Link from "next/link"
import {
  GraphGQLErrorParser,
  ValidatedForm,
  ValidatedFormButton,
  ValidatedFormInput,
  SIGNUP_MUTATION,
  useAuth,
  Urls,
  translate,
} from "myproject-shared"
import { useMutation } from "@apollo/react-hooks"
import { AuthScreen, AnalyticsAuth, AuthScreenElement, AuthChannel } from "myproject-shared/analytics"
import { ActivityIndicator, SpacerFlex, Stack, Text, Touchable, RequiredValidator, EmailValidator } from "myproject-ucl"
import React, { useState } from "react"
import { LoginAttributionText } from "./login-attribution-text"
import { LoginHeader } from "./login-header"
import { useRouter } from "next/router"
import { AnalyticsAuthShared } from "myproject-shared/analytics/account/myproject-analytics-auth-shared"
import { Container } from ".."

export const SignupForm: React.FC = () => {
  const router = useRouter()
  const [signup] = useMutation(SIGNUP_MUTATION)
  const [error, setError] = useState<string | undefined>()
  const auth = useAuth()
  const [loading, setLoading] = useState(false)
  const onSignup = async () => {
    AnalyticsAuthShared.trackAccountCreationUiEventClick(AuthScreen.CreateAccount, AuthScreenElement.CreateAccount)

    try {
      setError(undefined)
      setLoading(true)
      const { data } = await signup({
        variables: {
          email,
          name,
          password,
          clientType: "web",
        },
      })
      auth.handleSuccessfulAuth(data.signup)
      AnalyticsAuthShared.trackUserRegistered(AuthChannel.Email)
      AnalyticsAuth.trackSignUpWithEmail()
      router.replace((router.query.redirect as string) || Urls.account)
    } catch (e) {
      const { message } = GraphGQLErrorParser(e)
      setError(message)
      setLoading(false)
    }
  }

  const [email, setEmail] = useState<string | undefined>()
  const [name, setName] = useState<string | undefined>()
  const [password, setPassword] = useState<string | undefined>()

  const onAlreadyHaveAccountPressed = () => {
    AnalyticsAuthShared.trackAccountCreationUiEventClick(AuthScreen.CreateAccount, AuthScreenElement.HaveAccount)
    router.push(Urls.loginWithEmail)
  }

  return (
    <Container>
      <ValidatedForm
        validators={{
          email: [RequiredValidator, EmailValidator],
          name: [RequiredValidator],
          password: [RequiredValidator],
        }}>
        <Stack direction="column" childSeparationStep={9}>
          <LoginHeader>{translate("auth.signup-title")}</LoginHeader>
          <Stack direction="column" childSeparationStep={4}>
            <ValidatedFormInput
              role="email"
              text={email}
              keyboardType="email-address"
              title={translate("auth.email")}
              hint={"Ex. johndoe@myproject.com"}
              textChangeHandler={setEmail}
            />

            <ValidatedFormInput role="name" text={name} title={translate("auth.name")} textChangeHandler={setName} />

            <ValidatedFormInput
              role="password"
              text={password}
              title={translate("auth.password")}
              secureTextEntry={true}
              textChangeHandler={setPassword}
            />
          </Stack>
          <Stack direction="column" crossAxisDistribution="center" childSeparationStep={4}>
            <Touchable onPress={onAlreadyHaveAccountPressed}>
              <Text color="link" textAlign="center" textType="secondaryBody1">
                {translate("auth.already-have-account")}
              </Text>
            </Touchable>
            <SpacerFlex />

            {loading && (
              <>
                <ActivityIndicator size="large" />
                <SpacerFlex />
              </>
            )}

            {error && (
              <Text textAlign="center" color="error" textType="tertiaryBody2">
                {error}
              </Text>
            )}
          </Stack>
          <LoginAttributionText screen={AuthScreen.CreateAccount} />
          <ValidatedFormButton
            title={translate("auth.signup")}
            buttonSize="large"
            buttonType={loading ? "disabled" : "primary"}
            onClick={onSignup}
          />
        </Stack>
      </ValidatedForm>
    </Container>
  )
}
