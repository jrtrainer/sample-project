import { AuthScreen } from 'myproject-shared/analytics/constants'
import { Text } from 'myproject-ucl'
import React, { FC } from 'react'
import { StyleSheet, css } from 'aphrodite/no-important'
import { openPrivacyPolicy, openTerms } from './login-common'

export interface LoginAttributionTextProps {
  screen: AuthScreen
}

export const LoginAttributionText: FC<LoginAttributionTextProps> = ({ screen }) => {
  const onPressTos = () => {
    openTerms(screen)
  }
  const onPressPolicy = () => {
    openPrivacyPolicy(screen)
  }

  return (
    <div className={css(styles.bottomAttributionText)}>
      <Text color='secondary' textType='tertiaryBody2'>
        {'By signing up or logging in, you agree to the '}
      </Text>
      <Text color='link' textType='tertiaryBody2' onPress={onPressTos}>
        {'OfferUp Terms of Service'}
      </Text>
      <Text color='secondary' textType='tertiaryBody2'>
        {' and '}
      </Text>
      <Text color='link' textType='tertiaryBody2' onPress={onPressPolicy}>
        {'Privacy Policy'}
      </Text>
    </div>
  )
}

const styles = StyleSheet.create({
  bottomAttributionText: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    textAlign: 'center'
  }
})
