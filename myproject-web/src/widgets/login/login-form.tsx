import { useRouter } from 'next/router'
import {
  AUTH_ERROR_CODE,
  GraphGQLErrorParser,
  LOGIN_MUTATION,
  ValidatedForm,
  ValidatedFormButton,
  ValidatedFormInput,
  translate,
  useAuth,
  Urls
} from 'myproject-shared'
import { useMutation } from '@apollo/react-hooks'
import { RequiredValidator, EmailValidator, ActivityIndicator, Text, Touchable, Button, Flex, SpacerFlex, Stack } from 'myproject-ucl'
import { AuthChannel, AuthScreen, AuthScreenElement, AnalyticsAuth } from 'myproject-shared/analytics'
import { AnalyticsAuthShared } from 'myproject-shared/analytics/account/myproject-analytics-auth-shared'
import React, { FC, useState, useEffect } from 'react'
import { LoginAttributionText } from './login-attribution-text'
import { LoginHeader } from './login-header'
import { openForgotPassword } from '.'
import { isEmpty } from 'lodash'

export const LoginForm: FC = () => {
  const [email, setEmail] = useState<string | undefined>()
  const [password, setPassword] = useState<string | undefined>()
  const [emailError, setEmailError] = useState<string | undefined>()
  const [passwordError, setPasswordError] = useState<string | undefined>()
  const [isInvalidCredentials, setIsInvalidCredentials] = useState<boolean>(false)
  const [loading, setLoading] = useState(false)
  const auth = useAuth()
  const [login] = useMutation(LOGIN_MUTATION)
  const router = useRouter()

  const onLogin = async () => {
    setEmailError(undefined)
    setPasswordError(undefined)

    setLoading(true)
    let successfulLogin = false
    try {
      const { data } = await login({
        variables: {
          email,
          password
        }
      })

      auth.handleSuccessfulAuth(data.login)
      successfulLogin = true
      AnalyticsAuth.trackLoginWithEmail()

      AnalyticsAuthShared.trackUserLoggedIn(AuthChannel.Email)
      AnalyticsAuthShared.trackLoginUiEventClick(AuthScreen.EmailLogin, AuthScreenElement.Login, true)
    } catch (error) {
      const { code, message } = GraphGQLErrorParser(error)

      if (code === AUTH_ERROR_CODE.MFA_REQUIRED) {
        router.replace('/verify-mfa')
        return
      } else if (code === AUTH_ERROR_CODE.INVALID_CREDENTIALS) {
        setEmailError(translate('auth.username-password-error'))
        setPasswordError(translate('auth.username-password-error'))
        setIsInvalidCredentials(true)
      } else {
        // TODO: Put in an error dialog via DialogProvider
        setPasswordError(message)
      }

      AnalyticsAuthShared.trackLoginUiEventClick(AuthScreen.EmailLogin, AuthScreenElement.Login, false)
    }

    // this is done separately to seprate error handling between graphql layer (above), and routing layer
    if (successfulLogin) {
      const url = (router.query.redirect as string) || Urls.account

      // we force a client side refresh so that the auth middleware can requery the user
      window.location.assign(url)
    }

    setLoading(false)
  }

  const onForgotPasswordClick = () => {
    openForgotPassword(AuthScreen.EmailLogin, router)
  }

  const onSignupClick = () => {
    AnalyticsAuthShared.trackLoginUiEventClick(AuthScreen.EmailLogin, AuthScreenElement.CreateAccount)
    router.push(Urls.register)
  }

  // Submit is visually, but not funcationally, disabled until the user enters something in each required field. Below works upon
  // screen entry with how isSubmitDisabled  is initially, but will be overridden by it after errors appear
  const isSubmitVisuallyDisabled = isEmpty(email) || isEmpty(password)

  // Disable the Submit button if there is any error text displayed
  const isSubmitDisabled = loading || isInvalidCredentials || !isEmpty(emailError) || !isEmpty(passwordError)

  // If the credentials were invalid, we need to clear the error on both fields when either of them is used.
  // We only want to clear the changed field for all other errors.
  const onEmailChanged = (changedText?: string) => {
    setEmail(changedText)
    setEmailError(undefined)

    if (isInvalidCredentials) {
      setPasswordError(undefined)
      setIsInvalidCredentials(false)
    }
  }

  const onPasswordChanged = (changedText?: string) => {
    setPassword(changedText)
    setPasswordError(undefined)

    if (isInvalidCredentials) {
      setEmailError(undefined)
      setIsInvalidCredentials(false)
    }
  }

  return (
    <Stack direction='column' width='100%'>
      <ValidatedForm
        validators={{
          email: [RequiredValidator, EmailValidator],
          password: [RequiredValidator]
        }}
      >
        <Stack direction='column' childSeparationStep={6} grow={1}>
          <LoginHeader>{translate('auth.login-title')}</LoginHeader>
          <Stack childSeparationStep={4} direction='column'>
            <ValidatedFormInput
              role='email'
              text={email}
              keyboardType='email-address'
              title={translate('auth.email-address')}
              hint={'Ex. johndoe@myproject.com'}
              textChangeHandler={onEmailChanged}
              error={emailError}
            />
            <Stack childSeparationStep={1} direction='column'>
              <ValidatedFormInput
                role='password'
                text={password}
                title={translate('auth.password')}
                secureTextEntry={true}
                textChangeHandler={onPasswordChanged}
                error={passwordError}
              />
              <Touchable onPress={onForgotPasswordClick}>
                <Text color='link' textType='secondaryBody2'>
                  {translate('auth.forgot-your-password')}
                </Text>
              </Touchable>
            </Stack>
          </Stack>
          <SpacerFlex />
          <Flex direction='column' crossAxisDistribution='center'>
            {loading && <ActivityIndicator size='large' />}
            <Button onClick={onSignupClick} title={translate('auth.signup')} buttonSize='small' buttonType='flat' />
          </Flex>
          <LoginAttributionText screen={AuthScreen.EmailLogin} />

          <ValidatedFormButton
            buttonType={isSubmitVisuallyDisabled ? 'disabled' : 'primary'}
            title={translate('auth.login-button')}
            buttonSize='large'
            disabled={isSubmitDisabled}
            onClick={onLogin}
          />
        </Stack>
      </ValidatedForm>
    </Stack>
  )
}
