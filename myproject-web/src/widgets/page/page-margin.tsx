import React, { FC } from "react"
import { StyleSheet, css } from "aphrodite/no-important"
import { MediaQuery, MediaName, useMargin } from "myproject-ucl"

interface PageMarginProps {
  children: React.ReactNode
}
const MARGIN = {
  [MediaName.Mobile]: 4,
  [MediaName.Tablet]: 6,
  [MediaName.Desktop]: 12,
}

export const PageMargin: FC<PageMarginProps> = ({ children }) => {
  const { baseMargin } = useMargin()
  const STYLES = StyleSheet.create({
    container: {
      marginLeft: MARGIN[MediaName.Mobile] * baseMargin,
      marginRight: MARGIN[MediaName.Mobile] * baseMargin,
      [MediaQuery[MediaName.Tablet]]: {
        marginLeft: MARGIN[MediaName.Tablet] * baseMargin,
        marginRight: MARGIN[MediaName.Tablet] * baseMargin,
      },
      [MediaQuery[MediaName.Desktop]]: {
        marginLeft: MARGIN[MediaName.Desktop] * baseMargin,
        marginRight: MARGIN[MediaName.Desktop] * baseMargin,
      },
    },
  })
  return <div className={css(STYLES.container)}>{children}</div>
}
