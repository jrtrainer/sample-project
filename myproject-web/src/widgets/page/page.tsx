import React, { FC, useEffect } from "react"
import { css, StyleSheet } from "aphrodite/no-important"
import { ViewBlock, ScreenProvider, ScreenProviderProps } from "myproject-ucl"
import { AnalyticsApp } from "myproject-shared"
import { Header } from "../myproject-header"
import { Footer } from "../myproject-footer"
import Router from "next/router"

export const _PAGE_HEIGHT = "80vh"

interface PageProps extends ScreenProviderProps {
  showCategoryBarHeader?: boolean
  showSearchInputBar?: boolean
  showFooter?: boolean
  testID?: string
  children: React.ReactNode
}

export const Page: FC<PageProps> = props => {
  const { showCategoryBarHeader = false, showSearchInputBar = true, showFooter = true, screenName, testID, children } = props
  const STYLES = StyleSheet.create({
    container: {
      minHeight: _PAGE_HEIGHT,
    },
  })
  useEffect(() => {
    screenName && AnalyticsApp.trackScreenView(screenName, Router.route)
  }, [])
  return (
    <ScreenProvider screenName={screenName}>
      <ViewBlock testID={testID}>
        <Header showCategoryBar={showCategoryBarHeader} showSearchInputBar={showSearchInputBar} />
        <ViewBlock className={css(STYLES.container)}>{children}</ViewBlock>
        {showFooter && <Footer />}
      </ViewBlock>
    </ScreenProvider>
  )
}
