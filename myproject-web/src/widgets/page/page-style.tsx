import { StyleSheet, Extension, SelectorHandler, StyleDeclarationMap, CSSProperties } from 'aphrodite/no-important'

const globalSelectorHandler: SelectorHandler = (selector: string, _: string, generateSubtreeStyles: (selector: string) => string) => {
  if (selector[0] !== '*') {
    return null
  }
  return generateSubtreeStyles(selector.slice(1))
}
const globalExtension: Extension = { selectorHandler: globalSelectorHandler }
const { css } = StyleSheet.extend([globalExtension])

export const cssGlobal = (globalStyles: any) => {
  const styles: StyleDeclarationMap | CSSProperties = {}
  Object.keys(globalStyles).forEach(key => {
    styles['*' + key] = globalStyles[key]
  })
  css(
    StyleSheet.create({
      global: styles
    }).global
  )
}
