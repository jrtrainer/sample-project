import React from "react"
import { MarginProps, MediaName, Margin } from "myproject-ucl"
import { useMediaQuery } from "../../providers"

const MARGIN_STEP_ON_DESKTOP = 25

export const PageContentMargin: React.FC<MarginProps> = ({ children, ...rest }) => {
  const { mediaQueryName } = useMediaQuery()
  const isDesktop = mediaQueryName === MediaName.Desktop
  return (
    <Margin
      {...rest}
      marginLeftStep={isDesktop ? MARGIN_STEP_ON_DESKTOP : 0}
      marginRightStep={isDesktop ? MARGIN_STEP_ON_DESKTOP : 0}>
      {children}
    </Margin>
  )
}
