import { css, StyleSheet } from "aphrodite/no-important"
import React, { FC } from "react"

export const Container: FC = ({ children }) => {
  const STYLES = StyleSheet.create({
    container: {
      maxWidth: "1440px",
      margin: "0 auto",
    },
  })
  return <div className={css(STYLES.container)}>{children}</div>
}
