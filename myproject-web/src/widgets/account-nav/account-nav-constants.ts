import { Urls, translate } from 'myproject-shared'

export const SECTIONS = [
  {
    label: translate('account-stack.account-screen.section-label.transactions'),
    subSections: [
      {
        label: translate('account-stack.account-screen.purchases-and-sales'),
        link: Urls.transactions
      },
      {
        label: translate('account-stack.account-screen.payments-and-deposit-methods'),
        link: Urls.paymentAccounts
      },
      {
        label: translate('account-stack.account-screen.shipping-addresses'),
        link: ''
      }
    ]
  },
  {
    label: translate('account-stack.account-screen.section-label.saves'),
    subSections: [
      {
        label: translate('account-stack.account-screen.saved-items'),
        link: ''
      },
      {
        label: translate('account-stack.account-screen.saved-search-alerts'),
        link: Urls.searchAlerts
      }
    ]
  },
  {
    label: translate('account-stack.account-screen.section-label.account'),
    subSections: [
      {
        label: translate('account-stack.account-screen.account-settings'),
        link: Urls.settings
      },
      {
        label: translate('account-stack.account-screen.custom-profile-link'),
        link: ''
      }
    ]
  }
]
