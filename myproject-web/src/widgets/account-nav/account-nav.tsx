import React, { FC } from "react"
import {
  Stack,
  SVG,
  OpenNewTabIcon,
  useColor,
  Margin,
  ViewBlock,
  List,
  Border,
  Text,
  Touchable,
  Separator,
} from "myproject-ucl"
import { useAuth, Urls, translate } from "myproject-shared"
import { StyleSheet, css } from "aphrodite/no-important"
import { SECTIONS } from "./myproject-account-nav-constants"
import Router from "next/router"
import _ from "lodash"

interface SectionObject {
  label: string
  subSections: SubSectionObject[]
}
interface SubSectionObject {
  label: string
  link: string
}
const MARGIN = 4
export const AccountNav: FC = () => {
  const auth = useAuth()
  const currentUserId = _.propertyOf(auth)("user.id").toString()
  const { colors } = useColor()
  const STYLES = StyleSheet.create({
    subSectionSelected: {
      backgroundColor: colors.palouseGreenHighlight,
    },
  })
  const renderSection = (section: SectionObject, index: number) => {
    return (
      <Stack direction="column" key={index}>
        <Margin marginStep={MARGIN} marginTopStep={2} marginBottomStep={2}>
          <Text textType="primaryBody1">{section.label}</Text>
        </Margin>
        <List data={section.subSections} renderItem={renderSubSection} />
      </Stack>
    )
  }
  const renderSubSection = (sub: SubSectionObject, index: number) => {
    const isSelected = Router.asPath === sub.link
    const className = isSelected ? css(STYLES.subSectionSelected) : undefined
    const handlePressLink = () => {
      Router.push(sub.link)
    }
    return (
      <Touchable key={index} onPress={handlePressLink} className={className}>
        <Margin marginStep={MARGIN} marginTopStep={2} marginBottomStep={2}>
          <Text textType={isSelected ? "secondaryBody1" : "secondaryBody2"}>{sub.label}</Text>
        </Margin>
      </Touchable>
    )
  }

  return (
    <Border color="limestone" cornerRadius="small">
      <Stack direction="column">
        <Margin marginStep={MARGIN} marginTopStep={2} marginBottomStep={2}>
          <Text textType="headline3">{translate("account-stack.account-screen.account-title")}</Text>
        </Margin>
        <List data={SECTIONS} renderItem={renderSection} />
        <Margin marginStep={MARGIN} marginTopStep={2} marginBottomStep={2}>
          <Separator direction="column" />
          <Stack direction="column" childSeparationStep={2}>
            <Separator direction="column" />
            <Touchable href={Urls.profile(currentUserId)}>
              <Stack direction="row" childSeparationStep={1}>
                <Text textType="secondaryBody2" color="link">
                  {translate("account-stack.account-screen.view-public-profile")}
                </Text>
                <SVG localSVG={OpenNewTabIcon} tint="link" />
              </Stack>
            </Touchable>
          </Stack>
        </Margin>
      </Stack>
    </Border>
  )
}
