import { NextPageContext } from 'next'
import { getDeviceInfo } from '../utils/device/device'

export const deviceMiddleware = async ({ req }: NextPageContext) => {
  const device = getDeviceInfo(req)
  return device
}
