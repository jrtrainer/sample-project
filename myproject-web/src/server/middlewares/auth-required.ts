import HttpStatus from 'http-status-codes'
import { NextPageContext } from 'next'
import { ME_QUERY } from 'myproject-shared/gql-tags'
import { initializeApollo } from 'myproject-shared/network/apollo'
import { buildContextData } from '../utils/context-data'

export const authRequiredMiddleWare = (controller: (ctx: NextPageContext) => {} | Promise<{}>) => async (ctx: NextPageContext) => {
  const apolloClient = initializeApollo()
  try {
    await apolloClient.query({
      query: ME_QUERY,
      context: buildContextData(ctx),
      fetchPolicy: 'network-only'
    })
    return controller(ctx)
  } catch (error) {
    if (ctx.res && ctx.req) {
      ctx.res.writeHead(HttpStatus.MOVED_TEMPORARILY, {
        location: `/login?redirect=${encodeURIComponent(ctx.req.url || '/')}`
      })
      ctx.res.end()
    }
  }
  return {}
}
