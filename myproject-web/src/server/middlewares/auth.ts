import { NextPageContext } from 'next'
import { ME_QUERY, Query } from 'myproject-shared/gql-tags'
import { buildContextData } from '../utils/context-data'
import { initializeApollo } from 'myproject-shared/network/apollo'

export const authMiddleWare = async (ctx: NextPageContext) => {
  const apolloClient = initializeApollo()
  try {
    const { data } = await apolloClient.query<Query>({
      query: ME_QUERY,
      context: buildContextData(ctx),
      fetchPolicy: 'network-only'
    })
    return data.me
  } catch (error) {}
  return undefined
}
