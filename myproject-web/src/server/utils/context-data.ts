import { NextPageContext } from 'next'
import { propertyOf } from 'lodash'

export const buildContextData = (context: NextPageContext) => {
  return {
    internal: true,
    headers: propertyOf(context)('req.headers')
  }
}
