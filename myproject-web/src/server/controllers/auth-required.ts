import { authRequiredMiddleWare } from '../middlewares/auth-required'

const AuthRequiredBaseController = () => ({})
export const AuthRequiredController = authRequiredMiddleWare(AuthRequiredBaseController)
