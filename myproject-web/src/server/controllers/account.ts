import { authRequiredMiddleWare } from '../middlewares/auth-required'

const AccountBaseController = () => ({})
export const AccountController = authRequiredMiddleWare(AccountBaseController)
