import { GetServerSideProps, GetServerSidePropsContext } from 'next'
import { GET_SEARCH_FEED, SearchParam, Query, SearchResponse, SearchAlert } from 'myproject-shared/gql-tags'
import {
  SearchStateProps,
  getSortFilterOptions,
  SearchParamsProps,
  getDefaultSearchParamsFromFeedOptions,
  FeedOptionProps
} from 'myproject-shared/providers'
import { initializeApollo } from 'myproject-shared/network/apollo'
import { sanitizeUndefinedToNull } from 'myproject-shared/utilities'
import { MainFeedProps } from '../../components/main-feed/props'

export const SearchController: GetServerSideProps<MainFeedProps> = async (ctx: GetServerSidePropsContext) => {
  const { query } = ctx
  const apolloClient = initializeApollo()

  const searchParamsFromURL: SearchParamsProps = Object.keys(query).reduce((accumulator: SearchParamsProps, param) => {
    const value = query[param]
    if (!value) {
      return accumulator
    }
    if (typeof value === 'string') {
      accumulator[param] = value
    } else {
      accumulator[param] = value.join(',')
    }
    return accumulator
  }, {})
  sanitizeUndefinedToNull(searchParamsFromURL)

  if (!searchParamsFromURL.query) {
    searchParamsFromURL.query = searchParamsFromURL.q
  }

  const searchRequestParamsAsKVPs: SearchParam[] = Object.keys(searchParamsFromURL).map(key => ({
    key,
    value: searchParamsFromURL[key]
  }))

  const emptySearchInitState = {
    searchParams: searchParamsFromURL,
    feedOptions: [],
    sortOptions: [],
    filterOptions: []
  }

  const parseSuccessfulSearchResponse = (searchResult: SearchResponse, searchParams: SearchParamsProps) => {
    const { feedOptions, searchAlert } = searchResult

    if (feedOptions == null) {
      return {
        props: {
          searchInitState: emptySearchInitState,
          searchResponse: searchResult,
          initialApolloState: apolloClient.cache.extract()
        }
      }
    }

    const { sortOptions, filterOptions } = getSortFilterOptions(feedOptions as FeedOptionProps[])

    const searchInitState: SearchStateProps = {
      searchParams: {
        ...getDefaultSearchParamsFromFeedOptions(feedOptions, true),
        ...searchParams
      },
      feedOptions: feedOptions as FeedOptionProps[],
      sortOptions,
      filterOptions,
      searchAlert: searchAlert as SearchAlert | undefined
    }
    sanitizeUndefinedToNull(searchInitState)

    return {
      props: {
        searchInitState,
        searchResponse: searchResult,
        initialApolloState: apolloClient.cache.extract()
      }
    }
  }

  try {
    const { data } = await apolloClient.query<Query, { searchParams: SearchParam[] }>({
      query: GET_SEARCH_FEED,
      variables: {
        searchParams: searchRequestParamsAsKVPs
      }
    })
    return parseSuccessfulSearchResponse(data.search!, searchParamsFromURL)
  } catch (err) {
    console.log('SearchController Error', JSON.stringify(err, null, 2))
    return {
      props: {
        searchInitState: emptySearchInitState,
        searchResponse: {}
      }
    }
  }
}
