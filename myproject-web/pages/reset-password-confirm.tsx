import { useRouter } from "next/router"
import { PasswordResetConfirm, translate, Url, Urls } from "myproject-shared"
import { Stack, Button, Text } from "myproject-ucl"
import React, { FC } from "react"
import { Modal, Page, useModal } from "../src/widgets"
import { LoginHeader } from "../src/widgets/login/login-header"
const CONFIRMATION_MODAL_ID = "password-reset-confirmation"
const ConfirmationDialog: FC = () => {
  const router = useRouter()
  const { closeModal } = useModal()
  const goToLogin = async () => {
    await router.push(Urls.loginWithEmail)
    closeModal(CONFIRMATION_MODAL_ID)
  }
  return (
    <Stack direction="column" width="100%" height="100%" axisDistribution="center" childSeparationStep={4}>
      <Text textAlign="center" textType="headline3">
        {translate("auth.reset-password.success")}
      </Text>
      <Text textAlign="center" textType="primaryBody2">
        {translate("auth.reset-password.success-message")}
      </Text>
      <Button
        buttonSize="large"
        title={translate("auth.reset-password.got-it")}
        buttonType={"primary"}
        onClick={goToLogin}
      />
    </Stack>
  )
}
const ResetPaswordConfirm: FC = () => {
  const router = useRouter()
  const confirmationToken = router.query.confirmation
  const { showModal } = useModal()
  const onSuccess = () => {
    showModal({
      id: CONFIRMATION_MODAL_ID,
      withCloseIcon: false,
      content: <ConfirmationDialog />,
    })
  }
  return (
    <Stack direction="column" width="100%">
      <LoginHeader>{translate("auth.reset-password.title")}</LoginHeader>
      <PasswordResetConfirm confirmationToken={confirmationToken as string} onSuccess={onSuccess} />
    </Stack>
  )
}
const ResetPaswordConfirmPage = () => {
  return (
    <Page>
      <Modal open={true} withCloseIcon={false}>
        <ResetPaswordConfirm />
      </Modal>
    </Page>
  )
}

export { ResetPaswordConfirmPage as default }
