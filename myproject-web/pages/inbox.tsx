import { css, StyleSheet } from "aphrodite/no-important"
import { NextPage } from "next"
import { useRouter } from "next/router"
import {
  BING_AD_TYPE_NAME,
  InboxMessages,
  InboxNotifications,
  Urls,
  AnalyticsActionType,
  ALERT_TYPE_NAME,
} from "myproject-shared"
import { Alert, BingAd } from "myproject-shared/gql-tags/generated-types/myproject-types"
import {
  Border,
  Margin,
  SegmentedControl,
  Spacer,
  Text,
  Button,
  SelectableContextProvider,
  Flex,
} from "myproject-ucl"
import { FC, useState } from "react"
import { Container, Hidden, Page, PageMargin } from "../src/widgets"
import { AnalyticsInbox } from "myproject-shared/analytics/myproject-analytics-inbox"
import { AlertType } from "myproject-shared/providers/chat"
import { useWebNavigation } from "../src/providers"
const styles = StyleSheet.create({
  page: {
    minHeight: "100vh",
  },
})

const Inbox: FC = () => {
  const router = useRouter()
  const { navigateToActionPath } = useWebNavigation()
  const [editing, setEditing] = useState<boolean>(false)

  const onMessageSelect = (item: Alert | BingAd) => {
    if (item.__typename === ALERT_TYPE_NAME) {
      const alert = item as Alert
      AnalyticsInbox.trackInboxUIEvent(AlertType.MESSAGE, AnalyticsActionType.Click, alert.objectId, alert.type)
      router.push(Urls.discussion(alert.objectId))
    }
  }

  const onNotificationSelect = (item: Alert) => {
    AnalyticsInbox.trackInboxUIEvent(AlertType.NOTIFICATION, AnalyticsActionType.Click, item.objectId, item.type)
    navigateToActionPath(item.actionPath || "")
  }
  const routes: any[] = [
    {
      key: "Inbox",
      title: "Messages",
      tabContent: (
        <SelectableContextProvider multiSelect={true}>
          <InboxMessages onMessageSelect={onMessageSelect} editing={editing} />
        </SelectableContextProvider>
      ),
    },
    {
      key: "Notifications",
      title: "Notifications",
      tabContent: (
        <SelectableContextProvider multiSelect={true}>
          <InboxNotifications onMessageSelect={onNotificationSelect} editing={editing} />
        </SelectableContextProvider>
      ),
    },
  ]

  const editClick = () => {
    setEditing(!editing)
  }

  return (
    <>
      <PageMargin>
        <Container>
          <Margin marginTopStep={4} marginBottomStep={4}>
            <Text textType="headline3">Inbox</Text>
            <Flex direction="row" grow={1} />
            <Button title={editing ? "Cancel" : "Edit"} buttonType="flat" buttonSize="small" onClick={editClick} />
          </Margin>
        </Container>
      </PageMargin>
      <Container>
        <Border direction="column" grow={1} lineWeight="light" cornerRadius="small" color="limestone">
          <div className={css(styles.page)}>
            <SegmentedControl tabs={routes} />
          </div>
        </Border>
        <Hidden mobile={true} tablet={true}>
          <Spacer direction="column" sizeStep={4} />
        </Hidden>
      </Container>
    </>
  )
}
const InboxPage: NextPage = () => (
  <Page>
    <Inbox />
  </Page>
)

export { InboxPage as default }
