import {
  Stack,
  SpacerFlex,
  Text,
  ActivityIndicator,
  Flex,
  Button,
  RequiredValidator,
  EmailValidator,
} from "myproject-ucl"
import React, { useState, FC } from "react"
import { Page, useModal } from "../src/widgets"
import {
  ValidatedForm,
  GraphGQLErrorParser,
  RESET_PASSWORD_MUTATION,
  ValidatedFormInput,
  ValidatedFormButton,
  translate,
  AuthScreenElement,
  Urls,
} from "myproject-shared"
import { useMutation } from "@apollo/react-hooks"
import { LoginHeader } from "../src/widgets/login/login-header"
import { Modal } from "../src/widgets"
import { AnalyticsAuthShared } from "myproject-shared/analytics/account/myproject-analytics-auth-shared"
import { useRouter } from "next/router"
const CONFIRMATION_MODAL_ID = "password-reset-confirmation"

const ResetPasword: FC = () => {
  const [email, setEmail] = useState<string | undefined>()
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState<string | undefined>()
  const [resetPassword] = useMutation(RESET_PASSWORD_MUTATION)
  const makeRequest = async () => {
    setLoading(true)
    setError(undefined)
    try {
      await resetPassword({
        variables: {
          email,
        },
      })
      AnalyticsAuthShared.trackResetPasswordEventClick(AuthScreenElement.Send, true)
    } catch (e) {
      const { message } = GraphGQLErrorParser(e)
      setError(message)
      AnalyticsAuthShared.trackResetPasswordEventClick(AuthScreenElement.Send, false)
    }
    setLoading(false)
  }
  const { closeModal, showModal } = useModal()
  const ConfirmationDialog: FC = () => {
    const router = useRouter()
    const goToLogin = async () => {
      await router.push(Urls.loginWithEmail)
      closeModal(CONFIRMATION_MODAL_ID)
    }
    return (
      <Stack direction="column" width="100%" height="100%" axisDistribution="center" childSeparationStep={4}>
        <Text textAlign="center" textType="headline3">
          {translate("auth.reset-password.dialog-title")}
        </Text>
        <Text textAlign="center" textType="primaryBody2">
          {translate("auth.reset-password.dialog-content")}
        </Text>
        {loading && <ActivityIndicator size="large" />}
        <Button
          buttonSize="large"
          title={translate("auth.reset-password.resend")}
          buttonType="flat"
          onClick={makeRequest}
        />
        <Button
          buttonSize="large"
          title={translate("auth.reset-password.got-it")}
          buttonType={"primary"}
          onClick={goToLogin}
        />
      </Stack>
    )
  }
  const onSendClick = async () => {
    await makeRequest()
    showModal({
      id: CONFIRMATION_MODAL_ID,
      withCloseIcon: false,
      content: <ConfirmationDialog />,
    })
  }
  return (
    <Stack direction="column" height="90vh" width="100%">
      <ValidatedForm
        validators={{
          email: [RequiredValidator, EmailValidator],
        }}>
        <Stack direction="column" crossAxisDistribution="center" grow={1} childSeparationStep={4}>
          <LoginHeader>{translate("auth.reset-password.title")}</LoginHeader>
          <Text textType="primaryBody2">{translate("auth.reset-password.reset-password-note")}</Text>
          <Stack childSeparationStep={4} grow={1} width="100%" direction="column">
            <ValidatedFormInput
              role="email"
              text={email}
              keyboardType="email-address"
              title={translate("auth.email-address")}
              textChangeHandler={setEmail}
              hint={translate("auth.email-address-hint")}
            />
          </Stack>
          <SpacerFlex />
          {loading && (
            <>
              <ActivityIndicator size="large" />
              <SpacerFlex />
            </>
          )}
          {error && (
            <Text textAlign="center" color="error" textType="tertiaryBody2">
              {error}
            </Text>
          )}
          <Flex direction="column" grow={1} width="100%">
            <ValidatedFormButton
              title={translate("auth.reset-password.send")}
              buttonSize="large"
              buttonType={loading ? "disabled" : "primary"}
              onClick={onSendClick}
            />
          </Flex>
        </Stack>
      </ValidatedForm>
    </Stack>
  )
}
const ResetPaswordPage = () => {
  return (
    <Page>
      <Modal open={true} withCloseIcon={false}>
        <ResetPasword />
      </Modal>
    </Page>
  )
}

export { ResetPaswordPage as default }
