import React from "react"
import { SignupForm } from "../src/widgets/login/signup-form"
import { Page, Modal } from "../src/widgets"

const SignUpPage = () => {
  return (
    <Page>
      <Modal open={true} withCloseIcon={false}>
        <SignupForm />
      </Modal>
    </Page>
  )
}

export { SignUpPage as default }
