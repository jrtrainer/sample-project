import React, { useState } from 'react'
import axios from 'axios'
import { propertyOf } from 'lodash'

const Signup = () => {
  const [error, setError] = useState(null)
  const handleSignup = (evt: React.FormEvent<HTMLFormElement>): void => {
    evt.preventDefault()
    const evtTarget = propertyOf(evt.target)
    axios
      .post('/api/signup', {
        email: evtTarget('email.value'),
        password: evtTarget('password.value'),
        name: evtTarget('email.name')
      })
      .then(() => {
        window.location.replace('/')
      })
      .catch(err => {
        setError(err)
      })
  }
  return (
    <>
      <form action='POST' onSubmit={handleSignup}>
        <input id='email' type='text' placeholder='Ex.johndoe@mail.com' />
        <input id='name' type='text' placeholder='Name' />
        <input id='password' type='password' />
        <input type='submit' value='signup' />
        {error && <p>Email is used. Please try with different one.</p>}
      </form>
    </>
  )
}

export { Signup as default }
