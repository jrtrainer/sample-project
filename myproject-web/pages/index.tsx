import React, { useState } from "react"
import { NextPage, GetServerSideProps } from "next"
import { Spacer } from "myproject-ucl/controls"
import { SearchScreenName } from "myproject-shared/analytics"
import { SearchProvider } from "myproject-shared/providers"
import { Page, Hidden, Container, ModalProvider } from "../src/widgets"
import { MainFeed, MainFeedProps } from "../src/components/main-feed"
import { HomepageController } from "../src/server/controllers"
import { useSearchURLQueryParams } from "../src/utilities/url/use-search-url-query-params"

const MainFeedPageContents = (props: MainFeedProps) => {
  const screenName = SearchScreenName.Browse
  const { searchResponse } = props

  const [showFooter, setShowFooter] = useState(true)
  const handleFirstLoadMore = () => {
    setShowFooter(false)
  }

  useSearchURLQueryParams()

  return (
    <Page showCategoryBarHeader={true} showFooter={showFooter} testID="app-root" screenName={screenName}>
      <Hidden mobile={true} tablet={true}>
        <Spacer direction="column" sizeStep={6} />
      </Hidden>
      <Hidden desktop={true}>
        <Spacer direction="column" sizeStep={3} />
      </Hidden>
      <Container>
        <MainFeed searchResponse={searchResponse} onFirstLoadMoreCallback={handleFirstLoadMore} />
      </Container>
    </Page>
  )
}

const MainFeedPage: NextPage<MainFeedProps> = props => {
  const { searchInitState } = props
  return (
    <SearchProvider initState={searchInitState}>
      <ModalProvider>
        <MainFeedPageContents {...props} />
      </ModalProvider>
    </SearchProvider>
  )
}

export const getServerSideProps: GetServerSideProps = HomepageController

export { MainFeedPage as default }
