import React, { useState } from "react"
import { NextPage, GetServerSideProps } from "next"
import Router from "next/router"
import { Spacer, Margin, Stack, Text, Flex } from "myproject-ucl/controls"
import { SearchProvider } from "myproject-shared/providers"
import { Page, PageMargin, Hidden, ModalProvider } from "../src/widgets"
import { SearchFilterPanel } from "../src/widgets/myproject-search-filter-panel"
import { MainFeed, MainFeedProps } from "../src/components/main-feed"
import { SearchController } from "../src/server/controllers"
import { SearchFilterMobileEntryPoint } from "../src/widgets/myproject-search-filter-panel/myproject-search-filter-mobile-entry-point"
import { SearchSortDropDown } from "../src/widgets/myproject-search-filter-panel/myproject-search-sort-drop-down"
import { useSearchURLQueryParams } from "../src/utilities/url/use-search-url-query-params"

enum LAYTS {
  FilterBarWidth = 250,
  FilterBarMarginRight = 6,
  PageTopMargin = 6,
  HeaderBottomMargin = 2,
}

const SearchScreenContents = (props: MainFeedProps) => {
  const { searchResponse } = props
  const [showFooter, setShowFooter] = useState(true)
  const handleFirstLoadMore = () => {
    setShowFooter(false)
  }

  useSearchURLQueryParams()

  const headerTitle = Router.query.q
  return (
    <Page showCategoryBarHeader={true} showFooter={showFooter}>
      <Hidden mobile={true} tablet={true}>
        <Spacer direction="column" sizeStep={LAYTS.PageTopMargin} />
        <PageMargin>
          <Stack direction="row" grow={1}>
            <Margin direction="column" width={LAYTS.FilterBarWidth} marginRightStep={LAYTS.FilterBarMarginRight}>
              <SearchFilterPanel />
            </Margin>
            <Stack direction="column" grow={1} shrink={0} childSeparationStep={LAYTS.HeaderBottomMargin}>
              <Stack direction="row">
                <Flex grow={1}>{headerTitle && <Text textType="headline1">{headerTitle}</Text>}</Flex>
                <SearchSortDropDown />
              </Stack>
              <MainFeed searchResponse={searchResponse} onFirstLoadMoreCallback={handleFirstLoadMore} />
            </Stack>
          </Stack>
        </PageMargin>
      </Hidden>
      <Hidden desktop={true}>
        <Spacer direction="column" sizeStep={LAYTS.PageTopMargin} />
        <Stack direction="column" grow={1} shrink={0} childSeparationStep={LAYTS.HeaderBottomMargin}>
          <PageMargin>
            <Stack direction="column" grow={1} shrink={0} childSeparationStep={LAYTS.HeaderBottomMargin}>
              {headerTitle && <Text textType="headline1">{headerTitle}</Text>}
              <SearchFilterMobileEntryPoint />
            </Stack>
          </PageMargin>
          <MainFeed searchResponse={searchResponse} onFirstLoadMoreCallback={handleFirstLoadMore} />
        </Stack>
      </Hidden>
    </Page>
  )
}

const MainFeedPage: NextPage<MainFeedProps> = props => {
  const { searchInitState } = props

  return (
    <SearchProvider initState={searchInitState}>
      <ModalProvider>
        <SearchScreenContents {...props} />
      </ModalProvider>
    </SearchProvider>
  )
}

export const getServerSideProps: GetServerSideProps = SearchController

export { MainFeedPage as default }
