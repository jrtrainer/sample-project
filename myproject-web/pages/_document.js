import Document, { Head, Main, NextScript } from 'next/document'
import { StyleSheetServer } from 'aphrodite/no-important'

export default class Document extends Document {
  static async getInitialProps({ renderPage }) {
    const { html, css } = StyleSheetServer.renderStatic(() => renderPage())
    const ids = css.renderedClassNames
    return { ...html, css, ids }
  }

  constructor(props) {
    super(props)
    /* Take the renderedClassNames from aphrodite (as generated
    in getInitialProps) and assign them to __NEXT_DATA__ so that they
    are accessible to the client for rehydration. */
    const { __NEXT_DATA__, ids } = props
    if (ids) {
      __NEXT_DATA__.ids = this.props.ids
    }
  }

  render() {
    /* Make sure to use data-aphrodite attribute in the style tag here
    so that aphrodite knows which style tag it's in control of when
    the client goes to render styles. If you don't you'll get a second
    <style> tag */
    return (
      <html lang='en-US'>
        <Head>
          <meta
            name='description'
            content='Instantly connect with local buyers and sellers on OfferUp! Buy and sell everything from cars and trucks, electronics, furniture, and more.'
          />
          <meta charSet='utf-8' />
          <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />

          <meta name='google-site-verification' content='hVckgnfxPSIIYHASW6k-BapqZdaFc19eRe0nI8CneNM' />
          <meta name='p:domain_verify' content='f9dac38f246f8c2fa8b2523dbf502a7b' />

          <meta name='robots' content='NOODP' />
          <meta name='robots' content='index, follow' />

          <meta name='twitter:card' content='summary' />
          <meta name='twitter:site' content='@myproject' />

          <meta name='twitter:title' content='Buy &amp; Sell Locally - OfferUp - Buy. Sell. Simple.' />
          <meta
            name='twitter:description'
            content='Instantly connect with local buyers and sellers on OfferUp! Buy and sell everything from cars and trucks, electronics, furniture, and more.'
          />
          <meta name='twitter:image' content='https://assets.myproject-stg.com/web/images/app-icon/icon-114x114.848443e0.png' />

          <meta name='twitter:app:name:iphone' content='OfferUp' />
          <meta name='twitter:app:name:ipad' content='OfferUp' />
          <meta name='twitter:app:name:googleplay' content='OfferUp' />
          <meta name='twitter:app:id:iphone' content='468996152' />
          <meta name='twitter:app:id:ipad' content='468996152' />
          <meta name='twitter:app:id:googleplay' content='com.myproject' />

          <meta property='al:ios:app_store_id' content='468996152' />
          <meta property='al:ios:app_name' content='OfferUp' />
          <meta property='al:android:package' content='com.myproject' />
          <meta property='al:android:app_name' content='OfferUp' />

          <meta property='fb:app_id' content='245858875457163' />
          <meta property='og:site_name' content='OfferUp' />
          <meta property='og:type' content='website' />
          <meta property='og:url' content='https://myproject-stg.com/' />
          <meta property='og:title' content='Buy &amp; Sell Locally - OfferUp - Buy. Sell. Simple.' />
          <meta
            property='og:description'
            content='Instantly connect with local buyers and sellers on OfferUp! Buy and sell everything from cars and trucks, electronics, furniture, and more.'
          />

          <link rel='shortcut icon' href='images/app-icon/favicon.png' id='favicon-ico'></link>
          <style data-aphrodite dangerouslySetInnerHTML={{ __html: this.props.css.content }} />
        </Head>
        <body>
          <Main />
          <NextScript />
          <script
            type='text/javascript'
            src='https://myproject.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/8ur02a/b/23/a44af77267a987a660377e5c46e0fb64/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=9c37357a'
          ></script>
        </body>
      </html>
    )
  }
}
