import {
  GraphGQLErrorParser,
  ValidatedForm,
  ValidatedFormButton,
  ValidatedFormInput,
  VERIFY_PHONE_CODE_MUTATION,
} from "myproject-shared"
import { useMutation } from "@apollo/react-hooks"
import { RequiredValidator } from "myproject-ucl"
import { Flex, Margin, Spacer, Stack, SVG, Text, TwoFactorLock, Button } from "myproject-ucl"
import React, { FC, useState } from "react"
import { Page, Modal } from "../src/widgets"

const VerifyMFA: FC = () => {
  const [verifyCode] = useMutation(VERIFY_PHONE_CODE_MUTATION)
  const [code, setCode] = useState<string | undefined>()
  const [errorMessage, setErrorMessage] = useState<string | undefined | null>()

  const onVerifyCodeClicked = async () => {
    setErrorMessage(undefined)
    try {
      await verifyCode({
        variables: {
          code,
        },
      })
    } catch (error) {
      const { message } = GraphGQLErrorParser(error)
      setErrorMessage(message || error.message)
    }
  }
  const onResendClicked = () => {}
  return (
    <Stack direction="column" height="90vh" width="100%">
      <ValidatedForm
        validators={{
          code: [RequiredValidator],
        }}>
        <Stack direction="column" childSeparationStep={6} grow={1}>
          <Flex direction="column" crossAxisDistribution="center">
            <Text textType="headline2">{"Secure Your Account"}</Text>
          </Flex>
          <Spacer direction="column" sizeStep={6} />
          <Flex direction="column" crossAxisDistribution="center">
            <SVG localSVG={TwoFactorLock} tint={"primary"} />
            <Spacer direction="column" sizeStep={6} />
            <Text textType="headline3" textAlign="center">
              {"Enter code"}
            </Text>
            <Spacer direction="column" sizeStep={2} />
            <Flex direction="column" crossAxisDistribution="center">
              <Text textType="primaryBody2" textAlign="center">
                {'We sent a code to the number ending in "xxxx". Please enter the code:'}
              </Text>
            </Flex>
          </Flex>
          <Spacer direction="column" sizeStep={3} />
          <Flex direction="column" grow={1}>
            <ValidatedFormInput
              role="code"
              title={"Code"}
              keyboardType="number-pad"
              text={code}
              textChangeHandler={setCode}
            />
          </Flex>
          <Spacer direction="column" sizeStep={4} />
          {errorMessage && (
            <Text textAlign="center" color="error" textType="tertiaryBody2">
              {errorMessage}
            </Text>
          )}
          <Button onClick={onResendClicked} title={"Resend"} buttonSize="large" buttonType="flat" />
          <Button onClick={() => {}} title={"What is this?"} buttonSize="large" buttonType="flat" />
          <ValidatedFormButton onClick={onVerifyCodeClicked} title={"Verify"} buttonSize="large" buttonType={"primary"} />
        </Stack>
      </ValidatedForm>
    </Stack>
  )
}
const VerifyMFAPage = () => {
  return (
    <Page>
      <Modal open={true}>
        <Margin direction="column">
          <VerifyMFA />
        </Margin>
      </Modal>
    </Page>
  )
}

export { VerifyMFAPage as default }
