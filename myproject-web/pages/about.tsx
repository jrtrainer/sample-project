import HttpStatus from 'http-status-codes'
import { NextPage, NextPageContext } from 'next'
import Link from 'next/link'
import { HeartFill, Stack, SVG, Text } from 'myproject-ucl'
import React from 'react'
import { isWebview } from '../src/utilities/web-view'

const Page: NextPage = () => {
  return (
    <>
      <Stack direction='column' childSeparationStep={4} crossAxisDistribution='center'>
        <Text textType='headline1' textAlign='center'>
          About OfferUp
        </Text>
        <Text textType='headline2'>buy. sell. simple.</Text>
        <Stack direction='row' childSeparationStep={3}>
          <Link href='/privacy'>
            <a>
              <Text textType='headline3' color='link'>
                Privacy Policy
              </Text>
            </a>
          </Link>
          <Text textType='headline3'>|</Text>
          <Link href='/terms'>
            <a>
              <Text textType='headline3' color='link'>
                Terms of Service
              </Text>
            </a>
          </Link>
        </Stack>
        <Text textType='primaryBody2'>
          Local buying and selling has never been simpler. OfferUp makes it easy to quickly post items for sale and find amazing local deals right
          from your smartphone. Start instant conversations in the app with buyers and sellers nearby, and build their confidence by earning 5-star
          ratings and your TruYou badge. If you're looking for a better way to make money on the things you don't need, save money on the things you
          want, and feel great about discovering, renewing and exchanging more value right where you are, you've come to the right marketplace.
        </Text>
        <Text textType='headline1'>Our Story</Text>
        <Text textType='primaryBody2'>
          OfferUp’s founders launched the app in 2011 after trying to reclaim space in their homes and sell things their families no longer used. When
          they realized there was no easy way to buy and sell locally starting from a smartphone, they teamed up to build a marketplace that combined
          the simplicity of mobile with the power of human connection. Now millions of amazing deals and great connections are made every day on
          OfferUp, and we're so glad you're part of it.
        </Text>
        <Stack direction='row' childSeparationStep={3}>
          <SVG
            localSVG={{
              SVG: HeartFill.SVG,
              size: {
                height: 16,
                width: 16
              }
            }}
            tint='link'
          />
          <Text textType='primaryBody1'>Team OfferUp</Text>
        </Stack>
      </Stack>
    </>
  )
}

Page.getInitialProps = async (ctx: NextPageContext) => {
  if (isWebview(ctx) || !ctx.res || !ctx.req) {
    return true
  }
  ctx.res.writeHead(HttpStatus.MOVED_TEMPORARILY, {
    location: 'https://about.myproject.com'
  })
  ctx.res.end()
  return true
}
export { Page as default }
