import { StyleSheet } from "aphrodite"
import { AppProps } from "next/app"
import { AppContextType } from "next/dist/next-server/lib/utils"
import Head from "next/head"
import { AccountDataProvider, AuthProvider, RealtimeConnectionProvider, PhotoUploadProvider } from "myproject-shared"
import { InboxDataProvider } from "myproject-shared/providers/chat"
import { BackgroundContainer, LoadingContextProvider, ThemeProvider } from "myproject-ucl"
import "myproject-ucl/assets/fonts-web/lato.css"
import React from "react"
import { GlobalProvider, MediaQueryProvider, SinglePhotoUploader } from "../src/providers"
import { WebNavigationProvider } from "../src/providers"
import { authMiddleWare, deviceMiddleware } from "../src/server/middlewares"
import { DialogProvider } from "../src/widgets/dialog"
import { ModalProvider } from "../src/widgets/myproject-modal/myproject-modal-provider"
import { cssGlobal } from "../src/widgets/page/page-style"
import { ApolloProvider } from "../src/providers/apollo-provider"

declare var window: any
if (typeof window !== "undefined") {
  /* StyleSheet.rehydrate takes an array of rendered classnames,
  and ensures that the client side render doesn't generate
  duplicate style definitions in the <style data-aphrodite> tag */
  StyleSheet.rehydrate(window.__NEXT_DATA__.ids)
}

const App = ({ Component, pageProps }: AppProps) => {
  cssGlobal({
    body: {
      margin: 0,
      overflow: "auto !important",
      overflowX: "hidden !important",
    },
    a: {
      textDecoration: "none",
      whiteSpace: "nowrap",
    },
    ":focus": {
      outline: "none",
    },
  })
  const { globalProps, componentProps } = pageProps
  const { authUser } = globalProps
  return (
    <>
      <Head>
        <title>Buy &amp; Sell Locally - OfferUp - Buy. Sell. Simple.</title>
      </Head>
      <GlobalProvider value={globalProps}>
        <MediaQueryProvider>
          <ThemeProvider>
            <ApolloProvider pageProps={pageProps}>
              <AuthProvider user={authUser}>
                <AccountDataProvider>
                  <RealtimeConnectionProvider>
                    <PhotoUploadProvider uploader={SinglePhotoUploader}>
                      <InboxDataProvider>
                        <LoadingContextProvider>
                          <BackgroundContainer>
                            <ModalProvider>
                              <DialogProvider>
                                <WebNavigationProvider>
                                  <Component {...componentProps} />
                                </WebNavigationProvider>
                              </DialogProvider>
                            </ModalProvider>
                          </BackgroundContainer>
                        </LoadingContextProvider>
                      </InboxDataProvider>
                    </PhotoUploadProvider>
                  </RealtimeConnectionProvider>
                </AccountDataProvider>
              </AuthProvider>
            </ApolloProvider>
          </ThemeProvider>
        </MediaQueryProvider>
      </GlobalProvider>
    </>
  )
}

App.getInitialProps = async ({ Component, ctx }: AppContextType) => {
  const device = await deviceMiddleware(ctx)
  const authUser = await authMiddleWare(ctx)
  const globalProps = {
    device,
    authUser,
  }
  let componentProps = {}
  if (Component.getInitialProps) {
    componentProps = await Component.getInitialProps(ctx)
  }
  return {
    pageProps: {
      globalProps,
      componentProps,
    },
  }
}

export { App as default }
