import { NextPage } from "next"
import { MediaName } from "myproject-ucl/utilities/media-query/media-query"
import React from "react"
import { useMediaQuery } from "../src/providers/media-query-provider/media-query-provider"
import { AccountController } from "../src/server/controllers"
import { Hidden, Page } from "../src/widgets"
import { AccountMobile } from "../src/widgets/account/account-mobile"
import { AccountSettings } from "./accounts/settings"

const AccountPage: NextPage = () => {
  const { mediaQueryName } = useMediaQuery()
  const showFooter = mediaQueryName === MediaName.Desktop

  return (
    <Page showCategoryBarHeader={true} showFooter={showFooter}>
      <Hidden desktop={true}>
        <AccountMobile />
      </Hidden>
      <Hidden mobile={true} tablet={true}>
        <AccountSettings />
      </Hidden>
    </Page>
  )
}

AccountPage.getInitialProps = AccountController

export { AccountPage as default }
