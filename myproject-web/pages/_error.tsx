import { NextPage } from "next"
import { Page, Hidden, Container } from "../src/widgets"
import { Spacer, Center, EmptyState, ErrorBearShipping, ErrorBearFailure } from "myproject-ucl"
import HttpStatus from "http-status-codes"
import { translate } from "myproject-shared"

interface ErrorProps {
  statusCode?: number
}

const Error: NextPage<ErrorProps> = ({ statusCode }) => {
  const Error404 = () => {
    return (
      <EmptyState
        icon={ErrorBearShipping}
        title={translate("common-errors.not-found.title")}
        testID="error-screen.404"
      />
    )
  }
  const Error500 = () => {
    return (
      <EmptyState
        icon={ErrorBearFailure}
        title={translate("common-errors.server-error.title")}
        subtitle={translate("common-errors.server-error.subtitle")}
        testID="error-screen.500"
      />
    )
  }
  return (
    <Page showCategoryBarHeader={true} showFooter={true}>
      <Hidden mobile={true} tablet={true}>
        <Spacer direction="column" sizeStep={6} />
      </Hidden>
      <Hidden desktop={true}>
        <Spacer direction="column" sizeStep={3} />
      </Hidden>
      <Container>
        <Center>{statusCode === HttpStatus.NOT_FND ? <Error404 /> : <Error500 />}</Center>
      </Container>
    </Page>
  )
}

Error.getInitialProps = ({ res, err }): ErrorProps => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : HttpStatus.NOT_FND
  return { statusCode }
}

export { Error as default }
