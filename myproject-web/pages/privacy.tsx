import { NextPage, GetStaticProps } from "next"
import React from "react"
import { GET_TERMS_OF_SERVICE, TermsOfService, Query } from "myproject-shared/gql-tags"
import { translate } from "myproject-shared/utilities/i18n"
import { initializeApollo } from "myproject-shared/network/apollo"
import { SECS_PER_HR } from "myproject-shared/constants"
import { Page, PageMargin, Container } from "../src/widgets"
import { TermsAndPrivacyContent, TermsAndPrivacyContentProps } from "../src/widgets/account/terms-and-privacy"

const PrivacyPage: NextPage<{ termsOfService: TermsOfService }> = ({
  termsOfService: { privacyPolicy, activationDateGlobal },
}) => {
  const termsProps: TermsAndPrivacyContentProps = {
    title: translate("auth.privacy-policy"),
    datePublished: activationDateGlobal,
    bodyHTML: privacyPolicy,
  }

  return (
    <Page>
      <PageMargin>
        <Container>
          <TermsAndPrivacyContent {...termsProps} />
        </Container>
      </PageMargin>
    </Page>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  const apolloClient = initializeApollo()
  const { data } = await apolloClient.query<Query>({
    query: GET_TERMS_OF_SERVICE,
  })
  return {
    props: {
      termsOfService: data.termsOfService,
      initialApolloState: apolloClient.cache.extract(),
    },
    revalidate: SECS_PER_HR,
  }
}

export { PrivacyPage as default }
