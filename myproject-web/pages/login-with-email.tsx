import React from "react"
import { Page, LoginForm, Modal } from "../src/widgets"

const LoginPage = () => {
  return (
    <Page>
      <Modal open={true} withCloseIcon={false}>
        <LoginForm />
      </Modal>
    </Page>
  )
}

export { LoginPage as default }
