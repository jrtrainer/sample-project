import { Margin } from "myproject-ucl"
import React from "react"
import { LoginLanding } from "../src/widgets/login/login-landing"
import { Modal, Page } from "../src/widgets"

const LoginPage = () => {
  return (
    <Page>
      <Modal open={true} withCloseIcon={false}>
        <LoginLanding />
      </Modal>
    </Page>
  )
}

export { LoginPage as default }
