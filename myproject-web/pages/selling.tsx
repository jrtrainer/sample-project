import React from "react"
import { Text, List, Margin, Stack, SpacerFlex, Button, Spacer, MediaName } from "myproject-ucl"
import { Page, BreadCrumbHeader, Container, LoadingPage } from "../src/widgets"
import { Query, MY_OFFERS_SELLING, Listing, Urls } from "myproject-shared"
import { useQuery } from "@apollo/react-hooks"
import { AuthRequiredController } from "../src/server/controllers"
import { NextPage } from "next"
import { translate } from "myproject-shared"
import { MyOffersListItem, MOBILE_MARGIN, SellingEmptyState } from "../src/widgets/selling"
import { useMediaQuery } from "../src/providers"
import { useRouter } from "next/router"

const DESKTOP_BREADCRUMB = [
  {
    label: "Home",
    url: Urls.home,
  },
  {
    label: "Selling",
  },
]

const MOBILE_BREADCRUMB = [
  {
    label: "Home",
    url: Urls.home,
  },
]

const SellingPage: NextPage = () => {
  const { data, loading, refetch, error } = useQuery<Query>(MY_OFFERS_SELLING)
  const { mediaQueryName } = useMediaQuery()
  const router = useRouter()
  const isDesktop = mediaQueryName === MediaName.Desktop
  const containerMargin = isDesktop ? 0 : MOBILE_MARGIN

  if (loading) {
    return <LoadingPage />
  }

  const listings = data?.fetchMyOfferSellingData?.listings || []

  const onMarkSold = () => {
    refetch()
  }

  const onArchive = () => {
    refetch()
  }

  const navigateToItemDashboard = (listingId: string) => {
    router.push(Urls.itemDashboard(listingId))
  }

  const navigateToSellFaster = (listing: Listing) => {
    if (listing.id) {
      router.push(Urls.promote(listing.id))
    }
  }

  const navigateToRateBuyer = (listing: Listing) => {
    if (listing.id) {
      router.push(Urls.rateTransaction(listing.id))
    }
  }

  const renderItem = (item: Listing, index: number) => {
    return (
      <MyOffersListItem
        listing={item}
        tapHandler={navigateToItemDashboard}
        onMarkSold={onMarkSold}
        onSellFaster={navigateToSellFaster}
        onArchive={onArchive}
        onRateBuyer={navigateToRateBuyer}
        testID={"my-offers.item." + index}
      />
    )
  }

  const navigateToArchivedOffers = () => {
    router.push(Urls.archivedOffers)
  }

  return (
    <Page>
      <BreadCrumbHeader desktopData={DESKTOP_BREADCRUMB} mobileData={MOBILE_BREADCRUMB} />
      <Container>
        <Margin
          direction={"column"}
          marginLeftStep={containerMargin}
          marginRightStep={containerMargin}
          marginTopStep={MOBILE_MARGIN}
          grow={1}>
          <Stack direction="row" crossAxisDistribution="center" grow={1}>
            <Text textType="headline1">{translate("my-offers.title")}</Text>
            <SpacerFlex />
            <Button
              buttonType={"flat"}
              buttonSize={"small"}
              title={translate("my-offers.archived-nav")}
              onClick={navigateToArchivedOffers}
            />
          </Stack>
          <Spacer direction={"column"} sizeStep={4} />
          {listings.length === 0 ? (
            <SellingEmptyState
              title={translate("my-offers.empty")}
              subtitle={translate("my-offers.web-empty-subtext")}
              showDownloadAppButton={true}
            />
          ) : (
            <List data={listings} renderItem={renderItem} />
          )}
        </Margin>
      </Container>
    </Page>
  )
}

SellingPage.getInitialProps = AuthRequiredController

export { SellingPage as default }
