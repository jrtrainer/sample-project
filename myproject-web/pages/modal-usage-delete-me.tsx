import { NextPage } from 'next'
import { Exception, ExceptionAction } from 'myproject-shared'
import { Stack, Text } from 'myproject-ucl'
import React from 'react'
import { useDialog, useModal } from '../src/widgets'

const Page: NextPage = () => {
  const { showModal } = useModal()
  const { showErrorDialog, showAffirmRejectDialog } = useDialog()

  const onOpen = () => {
    showModal({
      content: (
        <Stack direction='column'>
          <Text textType='headline2'>This is Modal Header</Text>
          <Text textType='primaryBody2'>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever
            since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only
            five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
            release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker
            including versions of Lorem Ipsum. asdf
          </Text>
        </Stack>
      ),
      withCloseIcon: true
    })
  }

  const onError = () => {
    showErrorDialog({
      ouException: {
        title: 'error occured',
        message: "don't know why it happened. don't know why it happened. don't know why it happened.",
        actions: [{ actionPath: '/someActionPath', label: 'custom button' }],
        dismissible: true
      } as Exception,
      onActionItemPressed: (action: ExceptionAction) => {
        console.log(action)
      },
      onDismissPressed: () => {
        console.log('dissmissed')
      },
      withCloseIcon: false,
      width: 300
    })
  }

  const onAffirmReject = () => {
    showAffirmRejectDialog({
      affirmText: 'Agree',
      body:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      title: 'Title',
      rejectText: 'Dismiss',
      onDismiss: () => {},
      onAffirm: () => {},
      dismissOnReject: true,
      onReject: () => {},
      width: 460
    })
  }

  return (
    <div style={{ color: 'white' }}>
      <button onClick={onOpen}>open modal</button>
      <button onClick={onError}>open error dialog</button>
      <button onClick={onAffirmReject}>affirm reject dialog</button>
    </div>
  )
}
export { Page as default }
