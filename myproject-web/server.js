const tracer = require('dd-trace')
tracer.init()

const { createServer } = require('http')
const { parse } = require('url')
const next = require('next')
const fs = require('fs')

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()
/*
 * This is hardcoded curated list as only js for current node allowed here
 * Need this for transition from legacy web service on public webpage
 * https://nextjs.org/docs/advanced-features/custom-server
 */
const neededTrailingSlashUrls = ['/search', '/terms', '/privacy']

app.prepare().then(() => {
  createServer((req, res) => {
    const parsedUrl = parse(req.url, true)
    const { pathname, query } = parsedUrl
    /* By default, nextjs is not favor of trailing slash ending in route structure
     * Route with trailing slash are currently not working in non dynamic route,
     * so that we need to make sure routes that needed to be working for SEO & UX purposes
     * These routes are served w/o trailing slash e.g. /search
     * https://github.com/vercel/next.js/issues/5214
     */
    let checkedPathNameWoTrailingSlash
    if (pathname.endsWith('/')) {
      checkedPathNameWoTrailingSlash = pathname.substring(0, pathname.lastIndexOf('/'))
    }
    if (checkedPathNameWoTrailingSlash && neededTrailingSlashUrls.includes(checkedPathNameWoTrailingSlash)) {
      app.render(req, res, checkedPathNameWoTrailingSlash, query)
    } else {
      handle(req, res, parsedUrl)
    }
  }).listen(3000, err => {
    // todo: needs to be 8080 for deployment, override at local to random port when config is setup
    if (err) throw err
    console.log('> Ready on http://localhost:8080')
  })
})
