declare module "*.svg" {
  import { SVGColors } from "myproject-ucl"
  const content: React.FunctionComponent<React.SVGProps<SVGSVGElement> & SVGColors>
  export default content
}
