import React from "react"
import { StyleSheet, StyleProp, ViewStyle } from "react-native"
import { ToggleProps } from "./toggle.props"
import { Switch } from "react-native"
import { useColor } from "myproject-ucl/themes"
import { Platform } from "react-native"
import { Flex } from "../layout/flex"
import { Margin } from "../layout/margin"
import { Stack } from "../layout/stack"
import { Text } from "../text"
import { TooltipProvider } from "../../widgets/tooltip"
import { InputHelpIcon } from "../input"

export const Toggle = (props: ToggleProps) => {
  const { disabled, onChange, state, title, testID } = props
  const { colors } = useColor()

  const backgroundColor = Platform.OS === "ios" ? colors.crystal : colors.granite
  const thumbColor = Platform.OS === "ios" ? undefined : colors.limestone
  const borderStyles: StyleProp<ViewStyle> =
    Platform.OS === "ios" && state === true
      ? {
          borderWidth: StyleSheet.hairlineWidth,
          borderColor: colors.granite,
        }
      : undefined

  const onValueChange = (value: boolean): void => {
    onChange(value)
  }
  return (
    <Flex direction="row" grow={1} testID={(testID || "myproject-ucl") + ".toggle"}>
      <Margin direction="column" grow={0} marginLeftStep={2}>
        <Stack direction="column" childSeparationStep={2}>
          {title && (
            <Flex grow={1} direction="row">
              {title && <Text textType="primaryBody5" text={title} testID={(testID || "myproject-ucl") + ".input.title"} />}
            </Flex>
          )}
          <Flex grow={1} direction="column" axisDistribution="center">
            <Switch
              trackColor={{ false: backgroundColor, true: colors.lightGreen }}
              ios_backgroundColor={colors.crystal}
              thumbColor={!state ? thumbColor : colors.crystal}
              onValueChange={onValueChange}
              value={state}
              disabled={disabled}
              style={borderStyles}
              testID={testID || "myproject-ucl.toggle"}
              accessibilityLabel={testID || "myproject-ucl.toggle"}
            />
          </Flex>
        </Stack>
      </Margin>
    </Flex>
  )
}
