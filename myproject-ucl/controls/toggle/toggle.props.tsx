export interface ToggleProps {
  disabled?: boolean
  onChange: (state: boolean) => void
  state: boolean
  title: string
  /**
   * Used to locate this view in end-to-end tests.
   */
  testID?: string
}
