import React, { useRef } from "react"
import {
  Animated,
  ImageStyle,
  Platform,
  StyleProp,
  StyleSheet,
  Text,
  TextStyle,
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
  View,
  ViewStyle,
} from "react-native"
import { useColor, useFont, useMargin, useColorForTextColor } from "../../themes"
import { ButtonPropsNative } from "./button-props.native"
import { textColorForCurrentButtonType, isJSXElement, isLocalSVGSource } from "./button-shared"
import invariant from "invariant"
import { SVG } from "../image"
import { shadow } from "../../themes/shadow"
import { useScreen } from "../../hooks"
import _ from "lodash"
import { ActivityIndicator } from "../activity-indicator"
import { Overlay } from "../layout/overlay"
import { Flex } from "../layout/flex"
import { TooltipProvider } from "../../widgets/tooltip"
import { InputHelpIcon } from "../input"
import { Stack } from "../layout/stack"
import { Margin } from "../layout/margin"
const TRANSPARENT = "#FFFFFF00"
const ANIMATION_DURATION = 100

export const Button = (props: ButtonPropsNative) => {
  const {
    onClick,
    onLongClick,
    placeholder,
    title,
    header,
    subtitle,
    nextFocusDown,
    nextFocusForward,
    nextFocusLeft,
    nextFocusRight,
    nextFocusUp,
    disabled,
    testID,
    leftIcon,
    rightIcon,
    buttonType,
    buttonSize,
    onLayout,
    loading,
    doNotApplySidePadding,
  } = props

  invariant(buttonType !== undefined, "Must have button type")
  // invariant(title !== undefined, "Must have button title")

  const fonts = useFont().fonts
  const colors = useColor().colors
  const margin = useMargin().baseMargin
  const { screenName, screenRoute } = useScreen()

  const backgroundColorAnimation = useRef(new Animated.Value(0)).current

  const buttonStyles: StyleProp<ViewStyle> = [styles.button]
  const textStyles: StyleProp<TextStyle> = [styles.text]
  const subtitleStyles: StyleProp<TextStyle> = [styles.subtitle]
  const iconStyles: StyleProp<ImageStyle> = [styles.icon]

  const getBackgroundInterpolation = (from: string, to: string) => {
    return backgroundColorAnimation.interpolate({
      inputRange: [0, 1],
      outputRange: [from, to],
    })
  }
  const applyBorderStyles = () => {
    switch (buttonType) {
      case "secondary":
        buttonStyles.push(shadow.shadow)
        textStyles.push({ color: colors.green })
        subtitleStyles.push({ color: colors.green })
        break
      case "bordered":
        buttonStyles.push({ borderWidth: 1, borderColor: colors.grey })
        textStyles.push({ color: colors.black })
        break
      case "disabled":
        buttonStyles.push({ borderWidth: 1, borderColor: colors.limestone })
        break
      default:
        break
    }
  }

  const applyBackgroundColorToStyles = () => {
    let bgColor
    switch (buttonType) {
      default:
      case "primary":
        bgColor = getBackgroundInterpolation(colors.green, colors.green)
        break
      case "secondary":
        bgColor = getBackgroundInterpolation(colors.crystal, colors.crystalPressed)
        break
      case "tertiary":
        bgColor = getBackgroundInterpolation(colors.limestone, colors.limestonePressed)
        break
      case "flat":
        bgColor = getBackgroundInterpolation(TRANSPARENT, colors.crystalPressed)
        break
      case "disabled":
        bgColor = getBackgroundInterpolation(colors.disabled, colors.disabled)
        break
      case "bordered":
        bgColor = getBackgroundInterpolation(colors.crystal, colors.crystalPressed)
        break
    }
    buttonStyles.push({ backgroundColor: bgColor })
  }

  const textColorName = textColorForCurrentButtonType(buttonType)
  const textColor = useColorForTextColor(textColorName)

  const applyTextColorStyles = () => {
    textStyles.push({ color: textColor })
    subtitleStyles.push({ color: textColor })
  }

  const applyTextDimensionStyles = () => {
    switch (buttonSize) {
      default:
      case "large":
        textStyles.push(fonts.primaryBody1)
        break
      case "dropdown":
        textStyles.push(fonts.primaryBody6)
        break
      case "small":
        textStyles.push(fonts.secondaryBody2)
        break
    }
  }

  const applyButtonDimensionStyles = () => {
    let buttonDimensions

    switch (buttonSize) {
      default:
      case "large":
        buttonDimensions = { paddingHorizontal: margin * 2, height: 46 }
        break
      case "small":
        buttonDimensions = { paddingHorizontal: margin * 2, height: 32 }
        break
    }

    if (doNotApplySidePadding) {
      buttonDimensions = { ...buttonDimensions, paddingHorizontal: 0 }
    }

    buttonStyles.push(buttonDimensions)
  }

  const startBackgroundColorAnimation = (targetEndValue: number) => {
    requestAnimationFrame(() => {
      Animated.timing(backgroundColorAnimation, {
        toValue: targetEndValue,
        duration: ANIMATION_DURATION,
      }).start()
    })
  }

  applyBackgroundColorToStyles()
  applyTextDimensionStyles()
  applyTextColorStyles()
  applyBorderStyles()
  applyButtonDimensionStyles()

  const renderLeftIcon = () => {
    let iconJSX
    if (isJSXElement(leftIcon)) {
      iconJSX = leftIcon
    } else if (isLocalSVGSource(leftIcon)) {
      iconJSX = <SVG localSVG={leftIcon} tint={textColorName} />
    } else {
      return undefined
    }

    return <View style={title || subtitle ? iconStyles : {}}>{iconJSX}</View>
  }
  const renderRightIcon = () => {
    let iconJSX
    if (isJSXElement(rightIcon)) {
      iconJSX = rightIcon
    } else if (isLocalSVGSource(rightIcon)) {
      iconJSX = <SVG localSVG={rightIcon} tint={textColorName} />
    } else {
      return undefined
    }

    return <View style={title || subtitle ? iconStyles : {}}>{iconJSX}</View>
  }
  const clickHandler = () => {
    // UCLAnalyticsController.trackButtonEvent({
    //   screenName,
    //   screenRoute,
    //   actionType: "Click",
    //   buttonTitle: title,
    //   buttonType,
    //   buttonSize,
    //   testId: testID,
    //   affectedUserId,
    // })
    requestAnimationFrame(() => {
      if (onClick) {
        onClick()
      }
    })
  }

  const onPressIn = () => {
    startBackgroundColorAnimation(1)
  }

  const onPressOut = () => {
    startBackgroundColorAnimation(0)
  }

  const Touchable = Platform.OS === "android" ? TouchableNativeFeedback : TouchableWithoutFeedback

  return (
    <Flex direction="column">
      {header && (
        <Margin grow={1} direction="column" crossAxisDistribution={"flex-start"} marginBottomStep={2}>
          <Text style={textStyles} testID="myproject-ucl.button.title" accessibilityLabel="myproject-ucl.button.title">
            {header}
          </Text>
        </Margin>
      )}
      <Touchable
        accessibilityRole="button"
        nextFocusForward={nextFocusForward}
        nextFocusLeft={nextFocusLeft}
        nextFocusRight={nextFocusRight}
        nextFocusUp={nextFocusUp}
        nextFocusDown={nextFocusDown}
        testID={testID || "myproject-ucl.button"}
        accessibilityLabel={testID || "myproject-ucl.button"}
        disabled={disabled || loading}
        onPress={clickHandler}
        onLongPress={onLongClick}
        onPressIn={onPressIn}
        onPressOut={onPressOut}
        touchSoundDisabled={false}
      >
        <Animated.View style={buttonStyles} onLayout={onLayout}>
          {(loading && <ActivityIndicator size="small" tint={textColorName} />) || (
            <>
              {leftIcon && <Overlay insetLeftStep={0}>{renderLeftIcon()}</Overlay>}
              {placeholder && (
                <Overlay insetLeftStep={3}>
                  <Text
                    style={textStyles}
                    testID="myproject-ucl.button.title"
                    accessibilityLabel="myproject-ucl.button.title"
                  >
                    {placeholder}
                  </Text>
                </Overlay>
              )}
              {!_.isEmpty(title) && (
                <View style={styles.textContainer}>
                  {!_.isEmpty(title) && (
                    <Text
                      style={textStyles}
                      testID="myproject-ucl.button.title"
                      accessibilityLabel="myproject-ucl.button.title"
                    >
                      {title}
                    </Text>
                  )}
                  {subtitle && (
                    <Text
                      style={subtitleStyles}
                      testID="myproject-ucl.button.subtitle"
                      accessibilityLabel="myproject-ucl.button.subtitle"
                    >
                      {subtitle}
                    </Text>
                  )}
                </View>
              )}
              {rightIcon && <Overlay insetRightStep={2}>{renderRightIcon()}</Overlay>}
            </>
          )}
        </Animated.View>
      </Touchable>
    </Flex>
  )
}

const styles = StyleSheet.create({
  button: {
    flex: 0,
    borderRadius: 4,

    flexDirection: "row",
    flexWrap: "nowrap",

    justifyContent: "center",
    alignItems: "center",
    alignContent: "stretch",
  },
  icon: {
    marginRight: 8,
  },
  textContainer: {
    alignSelf: "auto",
    flex: 0,
    flexDirection: "column",
  },
  text: {
    textAlign: "center",
    alignContent: "center",
  },
  subtitle: {
    textAlign: "center",
  },
})
