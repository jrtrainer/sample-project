export interface FontTheme {
  identifier: string
  displayName: string
  baseMargin: number
  fonts: TextTypes
  baseBorder: {
    cornerRadius: { small: number; large: number }
    lineWeight: { light: number; heavy: number }
  }
}

export interface TextTypes {
  price: FontStyle
  headline1: FontStyle
  headline2: FontStyle
  headline3: FontStyle
  headline4: FontStyle
  headline5: FontStyle
  headline6: FontStyle
  primaryBody6: FontStyle
  primaryBody1: FontStyle
  primaryBody2: FontStyle
  primaryBody3: FontStyle
  primaryBody4: FontStyle
  primaryBody5: FontStyle
  secondaryBody1: FontStyle
  secondaryBody3: FontStyle
  secondaryBody2: FontStyle
  tertiaryBody1: FontStyle
  tertiaryBody2: FontStyle
  smallBody1: FontStyle
  smallBody2: FontStyle
  primaryBody7: FontStyle
  primaryBody9: FontStyle
  primaryBody8: FontStyle
  primaryBody6Italic: FontStyle
}

export type FontWeight = "normal" | "bold" | "100" | "200" | "300" | "400" | "500" | "600" | "700" | "800" | "900"

export interface FontStyle {
  fontFamily: string
  fontSize: number
  lineHeight: number
  fontWeight: FontWeight
}
