import { ViewStyle } from "react-native"

export const shadow = {
  /** Adds the standard drop shadow style */
  shadow: {
    elevation: 2,
    shadowColor: "rgba(0,0,0,0.2)",
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 1,
    shadowRadius: 3,
  } as ViewStyle,
}
