import { FontTheme, FontWeight, FontStyle } from "../../type-defs"
import { Platform } from "react-native"

const font900 = (size: { fontSize: number; lineHeight: number }): FontStyle => {
  return {
    ...size,
    fontFamily: "Lato-Bold",
    fontWeight: Platform.select<FontWeight>({
      ios: "900",
      android: "normal",
    }),
  }
}

const font700 = (size: { fontSize: number; lineHeight: number }): FontStyle => {
  return {
    ...size,
    fontFamily: "Lato-Bold",
    fontWeight: Platform.select<FontWeight>({
      ios: "700",
      android: "normal",
    }),
  }
}

const font600 = (size: { fontSize: number; lineHeight: number }): FontStyle => {
  return {
    ...size,
    fontFamily: "Lato-Bold",
    fontWeight: Platform.select<FontWeight>({
      ios: "600",
      android: "normal",
    }),
  }
}

const font400 = (size: { fontSize: number; lineHeight: number }): FontStyle => {
  return {
    ...size,
    fontFamily: "Lato-Regular",
    fontWeight: Platform.select<FontWeight>({
      ios: "400",
      android: "normal",
    }),
  }
}

const fontBold = (size: { fontSize: number; lineHeight: number }): FontStyle => {
  return {
    ...size,
    fontFamily: "Lato-Bold",
    fontWeight: Platform.select<FontWeight>({
      ios: "bold",
      android: "normal",
    }),
  }
}

const fontItalic = (size: { fontSize: number; lineHeight: number }): FontStyle => {
  return {
    ...size,
    fontFamily: "Lato-Italic",
    fontWeight: Platform.select<FontWeight>({
      ios: "100",
      android: "normal",
    }),
  }
}

const fontRegular = (size: { fontSize: number; lineHeight: number }): FontStyle => {
  return {
    ...size,
    fontFamily: "Lato-Regular",
    fontWeight: "normal",
  }
}

export const _MOBILE_FONT_THEME: FontTheme = {
  identifier: "standard",
  displayName: "Standard",
  baseMargin: 4,
  baseBorder: {
    cornerRadius: { small: 4, large: 8 },
    lineWeight: { light: 1, heavy: 4 },
  },
  fonts: {
    price: font900({
      fontSize: 40,
      lineHeight: 48,
    }),
    headline1: fontBold({
      fontSize: 26,
      lineHeight: 32,
    }),
    headline2: font900({
      fontSize: 24,
      lineHeight: 28,
    }),
    headline3: font900({
      fontSize: 20,
      lineHeight: 24,
    }),
    headline4: fontRegular({
      fontSize: 24,
      lineHeight: 21,
    }),
    headline5: font700({
      fontSize: 26,
      lineHeight: 28,
    }),
    headline6: fontRegular({
      fontSize: 20,
      lineHeight: 25,
    }),
    primaryBody1: fontBold({
      fontSize: 16,
      lineHeight: 20,
    }),
    primaryBody2: fontRegular({
      fontSize: 16,
      lineHeight: 20,
    }),
    primaryBody3: font400({
      fontSize: 12,
      lineHeight: 20,
    }),
    primaryBody4: font600({
      fontSize: 17,
      lineHeight: 28,
    }),
    primaryBody5: fontRegular({
      fontSize: 17,
      lineHeight: 28,
    }),
    primaryBody6: font400({
      fontSize: 18,
      lineHeight: 20,
    }),
    primaryBody6Italic: fontItalic({
      fontSize: 16,
      lineHeight: 20,
    }),
    primaryBody7: font600({
      fontSize: 18,
      lineHeight: 32,
    }),
    primaryBody8: font700({
      fontSize: 18,
      lineHeight: 32,
    }),
    primaryBody9: font400({
      fontSize: 18,
      lineHeight: 21,
    }),
    secondaryBody1: font600({
      fontSize: 14,
      lineHeight: 20,
    }),
    secondaryBody2: fontRegular({
      fontSize: 14,
      lineHeight: 21,
    }),
    secondaryBody3: font600({
      fontSize: 14,
      lineHeight: 28,
    }),
    tertiaryBody1: fontBold({
      fontSize: 12,
      lineHeight: 14,
    }),
    tertiaryBody2: fontRegular({
      fontSize: 12,
      lineHeight: 22,
    }),
    smallBody1: fontBold({
      fontSize: 10,
      lineHeight: 22,
    }),
    smallBody2: fontRegular({
      fontSize: 10,
      lineHeight: 18,
    }),
  },
}
