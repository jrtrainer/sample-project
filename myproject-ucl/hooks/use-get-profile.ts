import { UserProfile } from "myproject-shared/type-defs/generated-types/type-defs"
import { useState } from "react"

export const useGetUserProfile = () => {
  const [profile, setProfile] = useState<UserProfile | undefined>()

  return { profile }
}
