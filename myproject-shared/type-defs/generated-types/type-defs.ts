export type Maybe<T> = T | null
/** All built-in and custom scalars, mapped to their actual values */
export interface Scalars {
  ID: string
  String: string
  Boolean: boolean
  Int: number
  Float: number
  Upload: any
}

export interface Request {
  name: Scalars["String"]
  street1: Scalars["String"]
  street2?: Maybe<Scalars["String"]>
  city: Scalars["String"]
  state: Scalars["String"]
  zip: Scalars["String"]
  country: Scalars["String"]
  type?: Maybe<Scalars["String"]>
}
