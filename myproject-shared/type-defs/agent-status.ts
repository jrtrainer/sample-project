export enum AgentStatus {
  None,
  Created,
  Invited,
  Active,
  Archived
}