export enum FieldType {
  None,
  Email,
  PhoneNumber
}

export enum DataType {
  None,
  Number,
  String,
  List,
  MinMax,
  Object,
  ObjectList
}

export interface InfoField {
  FieldType: FieldType
  DataType: DataType
  FieldValue: string
  IsPublic: boolean
}