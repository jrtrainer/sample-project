import { AgentStatus } from "./agent-status";
import { InfoField } from "./info-field";

export interface Agent {
    Id: string
    ShortCode: string
    Status: AgentStatus
    FirstName: string
    LastName: string
    EmailAddress: string
    InfoFields: InfoField[]
    ProfileImageUri: string
    CreatedAt: string
    UpdatedAt: string
}