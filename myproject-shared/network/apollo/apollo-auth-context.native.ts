import { setContext } from "apollo-link-context"
import { getApiHeaders } from "../../utilities/api/api-helpers.native"

export const apolloAuthContextLink = setContext((_operation, _previousContext) => {
  const ouApiHeaders = getApiHeaders()
  return {
    headers: ouApiHeaders,
  }
})
