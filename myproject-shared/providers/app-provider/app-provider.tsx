import React, { createContext, FC } from "react"
import { Request } from "../../type-defs/generated-types/type-defs"
import { useFetchRequestData } from "../../hooks"

export interface AppContextProps {
  loading: boolean
  error: Error | undefined
  request: Request | undefined
  fetchRequest(variables?: any): Promise<any>
}
const initialValue: AppContextProps = {
  loading: true,
  error: undefined,
  request: undefined,
  fetchRequest: async () => {},
}

export const AppDataContext = createContext<AppContextProps>(initialValue)

export const AppDataProvider: FC = ({ children }) => {
  const { request, fetchRequest, loading, error } = useFetchRequestData()

  return (
    <AppDataContext.Provider
      value={{
        error,
        loading,
        request,
        fetchRequest,
      }}
    >
      {children}
    </AppDataContext.Provider>
  )
}
