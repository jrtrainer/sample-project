import _, { setWith } from "lodash"
import React, { createContext, FC, useContext, useEffect, useState } from "react"

import { StorageController, REQUEST_STORAGE_KEY, SETTINGS_STORAGE_KEY } from "../../utilities/storage"
import { useFetchRequestData } from "../../hooks"

export interface RequestContextProps {
  request: object
  requests: object
  requestLoading: boolean
  requestError: boolean
  loading: boolean
  error: boolean
  storeNewRequest: () => void
  fetchAllRequests: () => void
  fetchRequest: () => void
}

export const RequestContext = createContext<RequestContextProps>({
  request: {},
  requests: [],

  requestLoading: false,
  requestError: false,
  loading: true,
  error: false,
  storeNewRequest: async () => {},
  fetchAllRequests: async () => {},
  fetchRequest: async () => {},
})

interface RequestData {
  [requestId: string]: string
}
export const RequestProvider: FC = ({ children }) => {
  const [subscriptions, setAllSubscriptions] = useState<RequestData>([])
  const [userSettings, setAllSettings] = useState({})
  const [newSettings, setNewSetting] = useState({})
  const loadRequestsFromStorage = async () => {
    StorageController<RequestData>(REQUEST_STORAGE_KEY)
      .getItem()
      .then(requestFromStorage => {
        setAllSubscriptions(requestFromStorage || [])
      })
  }
  const loadSettingsFromStorage = async () => {
    StorageController<RequestData>(SETTINGS_STORAGE_KEY)
      .getItem()
      .then(settingsFromStorage => {
        setAllSettings(settingsFromStorage || {})
      })
  }
  useEffect(() => {
    loadRequestsFromStorage()
    loadSettingsFromStorage()
  }, [])

  useEffect(() => {
    StorageController<RequestData>(REQUEST_STORAGE_KEY).setItem(subscriptions)
  }, [subscriptions])

  useEffect(() => {
    StorageController<RequestData>(SETTINGS_STORAGE_KEY).setItem(newSettings)
  }, [newSettings])

  const storeNewSubscription = (request: object) => {
    // setRequest(request)
    setAllSubscriptions(request)
  }

  const storeNewSetting = (settings: object) => {
    setNewSetting(settings)
    setAllSettings(settings)
  }

  const { request, loading, fetchRequest, error, setData } = useFetchRequestData()

  return (
    <RequestContext.Provider
      value={{
        subscriptions,
        storeNewSubscription,
        request,
        loading,
        fetchRequest,
        setData,
        error,
        userSettings,
        storeNewSetting,
      }}
    >
      {children}
    </RequestContext.Provider>
  )
}
