// export const AUTH_CONFIG = {
//   domain: process.env.REACT_APP_AUTH0_DOMAIN || "myprojectdev.us.auth0.com",
//   clientID: process.env.REACT_APP_AUTH0_CLIENT_ID || "oBV4JxrwFuYoX1t6F88rBHraeAXC7qMJ",
//   callbackUrl: `${window.location.origin}/auth/callback`,
// }
export const JWT_TOKEN_KEY = "jwtToken"
export const REFRESH_TOKEN_KEY = "refreshtoken"
export const AGENT_ACCEPTANCE_KEY = "agentAcceptance"
export const ONBOARDED_KEY = "onboarded"
export const TOKEN_ID_KEY = "tokenId"
