import { useState, useEffect } from "react"
import { AuthProps } from "./auth-provider.d"
import { StorageControllerHelper } from "../../utilities/storage"
import { User } from "../../type-defs/generated-types/type-defs"

export const useProvideAuth = (authUser?: User, cleanUpForLogOut?: () => Promise<void>): AuthProps => {
  const [loading, setLoading] = useState<boolean>(true)
  const [user, setUser] = useState<null | User>(authUser || null)

  const handleSuccessfulAuth = async () => {
    setLoading(false)
  }

  const handleLogout = async (fallbackCall?: () => void) => {
    try {
      // await logout()
      StorageControllerHelper.clearAllButWhitelist()
      cleanUpForLogOut && (await cleanUpForLogOut())
      if (fallbackCall) {
        fallbackCall()
      } else {
        window.location.assign("/")
      }
    } catch (e) {
      console.log(e)
    }
  }

  return {
    loading,
    user,
    setUser,
    isAuthed: !!user,
    handleSuccessfulAuth,
    handleLogout,
  }
}
