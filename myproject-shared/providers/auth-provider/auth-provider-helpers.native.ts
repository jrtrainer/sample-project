import { SecureStorage } from "../../utilities/secure-storage/secure-storage.native"
import { AuthTokensProps } from "./auth-provider.d"
import {
  JWT_TOKEN_KEY,
  REFRESH_TOKEN_KEY,
  AGENT_ACCEPTANCE_KEY,
  ONBOARDED_KEY,
  TOKEN_ID_KEY,
} from "./auth-provider-constants"

const jwtTokenStorage = SecureStorage(JWT_TOKEN_KEY)
const tokenIdStorage = SecureStorage(TOKEN_ID_KEY)
const agentAcceptanceStorage = SecureStorage(AGENT_ACCEPTANCE_KEY)
const refreshTokenStorage = SecureStorage(REFRESH_TOKEN_KEY)
const agentOnboardedStorage = SecureStorage(ONBOARDED_KEY)

let sharedJwtToken: string | undefined
let sharedTokenId: string | undefined
let sharedRefreshToken: string | undefined
let sharedAcceptance: string | undefined
let sharedOnboarded: string | undefined

export const setAuthTokens = async (data: AuthTokensProps) => {
  const { jwtToken, refreshToken, tokenId } = data
  if (!jwtToken || !tokenId) {
    return
  }

  sharedJwtToken = jwtToken
  // sharedRefreshToken = refreshToken
  sharedTokenId = tokenId

  await jwtTokenStorage.secureSetItem(jwtToken)
  await tokenIdStorage.secureSetItem(tokenId)
  // await refreshTokenStorage.secureSetItem(refreshToken)
}
export const setAgentAcceptance = async (data: AuthTokensProps) => {
  const { agentAcceptance } = data
  if (!agentAcceptance) {
    return
  }
  sharedAcceptance = agentAcceptance

  await agentAcceptanceStorage.secureSetItem(agentAcceptance)
}
export const setOnboarded = async (data: AuthTokensProps) => {
  const { onboarded } = data
  if (!onboarded) {
    return
  }
  sharedOnboarded = onboarded

  await agentOnboardedStorage.secureSetItem(onboarded)
}
export const getAgentAcceptance = () => {
  return {
    agentAcceptance: sharedAcceptance,
  }
}
export const getOnboarded = () => {
  return {
    agentOnboarded: sharedOnboarded,
  }
}
export const getAuthTokens = () => {
  return {
    jwtToken: sharedJwtToken,
    // refreshToken: sharedRefreshToken,
    tokenId: sharedTokenId,
  }
}

export const removeAuthTokens = async () => {
  sharedJwtToken = undefined
  // sharedRefreshToken = undefined
  sharedTokenId = undefined

  await jwtTokenStorage.secureRemoveItem()
  // await refreshTokenStorage.secureRemoveItem()
  await tokenIdStorage.secureRemoveItem()
}

export const loadAuthTokens = async () => {
  sharedJwtToken = await jwtTokenStorage.secureGetItem()
  sharedTokenId = await tokenIdStorage.secureGetItem()
  // sharedRefreshToken = await refreshTokenStorage.secureGetItem()
  sharedAcceptance = await agentAcceptanceStorage.secureGetItem()
  sharedOnboarded = await agentOnboardedStorage.secureGetItem()
}
