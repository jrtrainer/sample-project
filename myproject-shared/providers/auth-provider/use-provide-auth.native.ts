import React, { useEffect, useState } from "react"
import { User } from "../../type-defs/generated-types/type-defs"
import { AuthProps } from "./auth-provider.d"
import {
  setAuthTokens,
  getAuthTokens,
  removeAuthTokens,
  setAgentAcceptance,
  getAgentAcceptance,
  getOnboarded,
} from "./auth-provider-helpers.native"
import { StorageControllerHelper } from "../../utilities/storage"
import { useAgentLogin, useAgentGet } from "../../hooks"
import jwt_decode from "jwt-decode"
import { createUser } from "../../hooks/parsers"
import axios from "axios"
import { Config } from "../../config"

// const HOST = Config.HOST
// const URL = Config.URL
export const authenticationChecker = () => {
  const { jwtToken } = getAuthTokens()
  if (!jwtToken) return false
  const decoded = jwt_decode(jwtToken)
  if (decoded.exp * 1000 < Date.now()) {
    return false
  }
  return true
}

export const useProvideAuth = (authUser?: User, cleanUpForLogOut?: () => Promise<void>): AuthProps => {
  const [loading, setLoading] = useState<boolean>(true)
  const [user, setUser] = useState<null | User>(authUser || null)
  const [error, setError] = useState(null)

  const checkInitAuth = async () => {
    setLoading(true)
    const isAuthenticated = authenticationChecker()
    if (!isAuthenticated) {
      setUser(null)
      setLoading(false)
      return
    }
    try {
      const { jwtToken, tokenId } = getAuthTokens()
      const decoded = jwt_decode(tokenId)
      const requestBody = { firstName: decoded.given_name, lastName: decoded.family_name, emailAddress: decoded.email }
      const config = {
        headers: {
          Authorization: `Bearer ${jwtToken}`,
        },
      }
      // axios
      //   .post(`${HOST}/${URL}/agents/login`, requestBody, config)
      //   .then(result => {
      //     setLoading(false)
      //     setUser(result.data)
      //     setLoading(false)
      //   })
      //   .catch(e => {
      //     setError(true)
      //     console.log("agent create fetch err", e)
      //   })
    } catch (e) {
      console.log("login err", e)
    }
  }

  useEffect(() => {
    checkInitAuth()
  }, [])

  const handleSuccessfulAuth = async (updatedUser: User) => {
    const { accessToken, tokenId } = updatedUser

    await setAuthTokens({
      jwtToken: accessToken,
      // refreshToken: refreshToken,
      tokenId: tokenId,
    })
    await checkInitAuth()
  }
  const handleLogout = async () => {
    setLoading(true)
    setUser(null)
    await StorageControllerHelper.clearAllButWhitelist()
    await removeAuthTokens()
    cleanUpForLogOut && (await cleanUpForLogOut())
    setLoading(false)
  }

  return {
    loading,
    user,
    setUser,
    isAuthed: !!user,
    handleSuccessfulAuth,
    handleLogout,
  }
}
