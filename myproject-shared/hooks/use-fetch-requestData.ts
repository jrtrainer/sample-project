import React, { useEffect, useState, useCallback } from "react"
import axios from "axios"
import { Config } from "../config"
import { Request } from "../type-defs/generated-types/type-defs"

export const useFetchRequestData = () => {
  const [error, setError] = useState(undefined)
  const [loading, setLoading] = useState(true)
  const [args, fetchRequest] = useState<Request | undefined>()
  const [request, setData] = useState([])
  const [tempData2, setTempData] = useState([])
  let tempData = []
  let updatedObj

  const execute = useCallback(() => {
    const baseAPIUrl = `${Config.baseUri}/emerald`
    const template = Config.defaultTemplate
    const config = {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }
    setTempData([])
    if (Object.keys(tempData2).length < 1) {
      args.ids.forEach(id => {
        const component = `${id}?template=${template}`
        const url = `${baseAPIUrl}/requests/${component}&transactionKey=${args.transactionKey}`
        setLoading(true)
        axios
          .get(url, config)
          .then(result => {
            setLoading(false)
            tempData.push(result.data)
            const mergedData = [...tempData2, ...tempData]

            updatedObj = { ...tempData[0], tickets: mergedData }
            setTempData(updatedObj)
            setData(updatedObj)
          })
          .catch(e => {
            setLoading(false)
            setError(true)
            console.log("fetch app data err", e)
          })
      })
    }
  }, [args])

  useEffect(() => {
    if (args) {
      execute()
      setTempData([])
    }
  }, [execute, fetchRequest])

  return { request, loading, fetchRequest, setData, error }
}
