export const Transform = (payload, data_type) => {
  const profileConfig = {
    emailAddress: "emailAddress",
    firstName: "firstName",
    homeAddress: "homeAddress",
    lastName: "lastName",
    personalPhoneNumber: "personalPhoneNumber",
    profileImageUri: "profileImageUri",
    agentStateLicenses: "agentStateLicenses",
    infoFields: "infoFields",
  }
  const portfolioConfig = {
    agentMlsId: "agentMlsId",
    isTrustOrBusiness: "isTrustOrBusiness",
    businessType: "businessType",
    legalName: "legalName",
    firstName: "firstName",
    lastName: "lastName",
    emailAddress: "emailAddress",
    phoneNumber: "phoneNumber",
    gender: "gender",
    age: "age",
    otherProperties: "otherProperties",
    notes: "notes",
  }

  const generateContent = (config, oldPayload) => {
    const createPayloadObj = payload => config => {
      let finalPayload = {}
      /**
       * TODO KEEP FOR DYNAMIC LICENSES IN FUTURE
       */
      // console.log("what is the payload", payload)
      // const createLicense = (licenses, newLicenses) => {
      //   console.log("what is the license", newLicenses)
      //   let tagObject
      //   const updatedTags = []
      //   for (let i = 0; i < licenses.length; i++) {
      //     tagObject = Object.assign(
      //       {},
      //       {
      //         state: "TX",
      //         license: licenses[i].license,
      //       },
      //     )
      //
      //     updatedTags.push(tagObject)
      //   }
      //   // console.log("what is the updated tags", updatedTags)
      //   for (let j = 0; j < newLicenses.length; j++) {
      //     updatedTags.push({
      //       state: newLicenses[j].state,
      //       license: newLicenses[j].license,
      //     })
      //   }
      //   return updatedTags
      //   // return [{ state: "TX", license: payload[key] }]
      // }
      Object.keys(config).map((key, idx, array) => {
        // if (key == "agentStateLicenses") {
        //   Object.keys(payload).map((_key, idx, array) => {
        //     if (_key == "agentStateLicenses") {
        //       finalPayload = Object.assign({}, finalPayload, {
        //         media_tags: createLicense(payload[_key], payload["agentStateLicenses"]),
        //       })
        //     }
        //   })
        //   return
        // }
        const newKey = config[array[idx]] ? config[array[idx]] : null
        if (newKey) {
          const newValue = payload[key] ? payload[key] : null
          finalPayload = Object.assign({}, finalPayload, { [config[array[idx]]]: newValue })
        }
      })
      return finalPayload
    }
    // 1. take config map keys use key to find oldPayload value use config value to create newpayload key,value pari
    const extractPayloads = (config, fn) => fn(config)
    return extractPayloads(config, createPayloadObj(oldPayload))
  }

  switch (data_type) {
    case "profile":
      return generateContent(profileConfig, payload)
    case "portfolio":
      return generateContent(portfolioConfig, payload)
  }
}
