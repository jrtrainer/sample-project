export const createUser = (args: any) => {
  const agents = {
    firstName: args.firstName,
    lastName: args.lastName,
    emailAddress: args.email,
  }
  return agents
}
export const parseDoc = (args: any) => {
  const doc = {
    documentType: args.documentType,
  }
  return doc
}
