import React, { useEffect, useState } from "react"
import { Request } from "../type-defs/generated-types/type-defs"
import axios from "axios"
import { Config } from "../config"
import { getAuthTokens } from "../providers/auth-provider/auth-provider-helpers.native"

// const HOST = Config.HOST
// const URL = Config.URL

export const useAgentAccept = () => {
  const [args, updateAccept] = useState<Request | undefined>()
  const [loading, setLoading] = useState<boolean>(true)
  const [error, setError] = useState("")
  const [acceptance, setAgentData] = useState<Request | undefined>()
  const { jwtToken } = getAuthTokens()

  const execute = React.useCallback(() => {
    setLoading(true)
    const config = {
      headers: {
        Authorization: `Bearer ${jwtToken}`,
      },
    }
    // console.log("what ist he args", args)
    // axios
    //   .post(`${HOST}/${URL}/agents/${args?.id}/acceptance/${args.documentId}`, config)
    //   .then(result => {
    //     setLoading(false)
    //     setAgentData(result.data)
    //   })
    //   .catch(e => {
    //     setError(true)
    //     // console.log("agent patch err", e)
    //   })
  }, [args])

  React.useEffect(() => {
    if (args) {
      execute()
    }
  }, [execute, updateAccept])

  return { acceptance, loading, updateAccept, error }
}
