import React, { useEffect, useState } from "react"
import { User, UserProfile } from "../type-defs/generated-types/type-defs"
import axios from "axios"
import Config from "./ConfigConstant"
import { createUser } from "./parsers"
import { getAuthTokens } from "../providers/auth-provider/auth-provider-helpers.native"

// const HOST = Config.HOST
// const URL = Config.URL

export const useAgentDashboard = () => {
  // static baseAPIUrl = `${Config.baseUri}/emerald`
  // static baseImgUrl = `${Config.baseUri}/image`
  const [args, setDashboard] = useState<UserProfile | undefined>()
  const [loading, setLoading] = useState<boolean>(true)
  const [error, setError] = useState("")
  const [dashboardData, setDashboardData] = useState<UserProfile | undefined>()
  const { jwtToken } = getAuthTokens()

  const execute = React.useCallback(() => {
    setLoading(true)
    const requestBody = createUser(args)
    const config = {
      headers: {
        Authorization: `Bearer ${jwtToken}`,
      },
    }
    // axios
    //   .post(`${HOST}/${URL}/agents`, requestBody, config)
    //   .then(result => {
    //     setLoading(false)
    //     setDashboardData(result.data)
    //   })
    //   .catch(e => {
    //     setError(true)
    //     console.log("dashboard fetch err", e)
    //   })
  }, [args])

  React.useEffect(() => {
    if (args) {
      execute()
    }
  }, [execute, setDashboard])

  return { dashboardData, loading, setDashboard, error }
}
