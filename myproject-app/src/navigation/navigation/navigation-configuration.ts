import {
  Navigation,
  _NAVIGATION_RTES_SCHEME,
  generatePathHandler,
  NavigationPathHandler,
  NavigationPayload,
} from "./navigation"
import { NavigationContainerRef } from "@react-navigation/native"
import { NavigableRoute, navigableRoutes } from "../navigator/navigableroute"
import { authenticationChecker } from "myproject-shared/providers/auth-provider/use-provide-auth.native"
import { LetGoOnboardingController } from "../../widgets/myproject-letgo-onboarding/myproject-letgo-onboarding-controller"

const RTES_THAT_DO_NOT_REQUIRE_AUTHENTICATION: string[] = [
  NavigableRoute.HomeTabs,
  NavigableRoute.HomeFeed,
  NavigableRoute.Search,
  NavigableRoute.SearchStack,
  NavigableRoute.AllCategories,
  NavigableRoute.AuthLanding,
  NavigableRoute.AuthEmailLanding,
  NavigableRoute.AuthLogin,
  NavigableRoute.AuthSignup,
  NavigableRoute.AuthGetUserEmail,
  NavigableRoute.AuthMultifactor,
  NavigableRoute.PasswordReset,
  NavigableRoute.ConfirmPhoneCode,
  NavigableRoute.SearchSuggestion,
  NavigableRoute.SearchFiltersStack,
  NavigableRoute.SearchFilters,
  NavigableRoute.SearchLocationFilters,
  NavigableRoute.SearchLocationPicker,
  NavigableRoute.SearchCategoryFilter,
  NavigableRoute.SearchSortModalScreen,
  NavigableRoute.SearchRangeFilter,
  NavigableRoute.SearchListSelectionFilter,
  NavigableRoute.ModalCard,
  NavigableRoute.WebViewScreen,
  NavigableRoute.PermissionsDialog,
  NavigableRoute.PublicProfile,
  NavigableRoute.PublicProfileAutosDealerInventory,
  NavigableRoute.ListingDetail,
  NavigableRoute.PhotoGalleryModal,
  NavigableRoute.LocationPicker,
  NavigableRoute.AffirmRejectDialog,
  NavigableRoute.ErrorDialog,
  NavigableRoute.LetGoWelcome,
]

export const APP_AUTHORITY: string[] = ["myproject.com", "myproject-stg.com", "myproject-int.com"]
const APP_SCHEME = ["https://", "myprojectapp://"]
const routeUrlsMap = new Map<string, string[]>()

export const initializeNavigation = (navigationRef: React.RefObject<NavigationContainerRef>) => {
  Navigation.start(navigationRef)

  for (const scheme of APP_SCHEME) {
    Navigation.addScheme(scheme)
  }
  Navigation.addScheme(_NAVIGATION_RTES_SCHEME)
  initializeActionPath()
  initializeGlobalSchemePaths()
  Navigation.addPaths(
    _NAVIGATION_RTES_SCHEME,
    [NavigableRoute.PostStackShadowForModalPostFlowTab],
    generateAuthPathHandler(NavigableRoute.PostStack),
  )
  Navigation.addPaths("ouapp", ["App.CloseWebView"], webNavigationHandler)
}

const initializeActionPath = () => {
  ///////// Discovery /////////
  addActionPathsNavigableRouteAssociation(NavigableRoute.Search, "/search")
  addActionPathsNavigableRouteAssociation(NavigableRoute.Search, "/itemfeed")
  addActionPathsNavigableRouteAssociation(NavigableRoute.SearchAlert, "/search/searchalerts/:aid")
  addActionPathsNavigableRouteAssociation(NavigableRoute.AccountSearchAlerts, "/manage/searchalerts")
  addActionPathsNavigableRouteAssociation(NavigableRoute.SavedLists, "/boards")
  // addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, '/board/:boardID/:linkID')
  // addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, '/board/:boardID')
  addActionPathsNavigableRouteAssociation(NavigableRoute.ListingDetail, "/item/detail/:listingId")
  addActionPathsNavigableRouteAssociation(NavigableRoute.CarBuyerProfile, "/autos/car_buyer_profile/:action/:listingId")
  addActionPathsNavigableRouteAssociation(NavigableRoute.PublicProfile, "/store/:vanityUserID")
  addActionPathsNavigableRouteAssociation(NavigableRoute.PublicProfile, "/profile/:vanityUserID")
  addActionPathsNavigableRouteAssociation(NavigableRoute.PublicProfile, "/p/:userID")
  // addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, '/user/:userid/relationships')
  // addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, '/user/:userid/item_relationships')

  ///////// Chat /////////
  addActionPathsNavigableRouteAssociation(NavigableRoute.Inbox, "/notifications")
  addActionPathsNavigableRouteAssociation(
    NavigableRoute.DiscussionStack,
    "/item/:listingId/discussions/:discussionId/messages",
  )
  addActionPathsNavigableRouteAssociation(
    NavigableRoute.DiscussionStack,
    "item/:listingId/discussions/:discussionId/messages",
  )
  addActionPathsNavigableRouteAssociation(NavigableRoute.Meetup, "/meetup/meetupspot_search")
  // addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, 'meetup/schedule')
  // addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, 'meetup/directions')
  addActionPathsNavigableRouteAssociation(NavigableRoute.RateUser, "/rate/transaction/:itemID")
  addActionPathsNavigableRouteAssociation(NavigableRoute.RateUser, "/item/:itemID/rate-transaction")
  addActionPathsNavigableRouteAssociation(NavigableRoute.RateUser, "/item/:listingId/buyers/")
  // addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, '/ratings/write-interaction-rating/item/:itemId/user/:userId')

  ///////// Account /////////
  addActionPathsNavigableRouteAssociation(NavigableRoute.AccountVanityUrl, "/accounts/settings/vanity")
  addActionPathsNavigableRouteAssociation(NavigableRoute.Account, "/accounts")
  addActionPathsNavigableRouteAssociation(NavigableRoute.ProfileImageSelect, "/accounts/profile_picture/update")
  addActionPathsNavigableRouteAssociation(NavigableRoute.VerifyEmail, "/accounts/email/verification/sent")
  addActionPathsNavigableRouteAssociation(NavigableRoute.VerifyPhone, "/accounts/verify/phone")
  addActionPathsNavigableRouteAssociation(NavigableRoute.AccountSettings, "/accounts/settings")
  addActionPathsNavigableRouteAssociation(NavigableRoute.ProfileImageStack, "/accounts/background/update")
  // addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, '/referral')
  // addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, '/get-the-app')
  // addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, '/accounts/facebook/connect')
  // addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, '/truyou')

  ///////// My Offers - Selling /////////
  addActionPathsNavigableRouteAssociation(NavigableRoute.PostStack, "/post")
  // addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, '/item/:itemID/edit/shipping') // TBD if still required
  addActionPathsNavigableRouteAssociation(NavigableRoute.Selling, "/selling")
  addActionPathsNavigableRouteAssociation(NavigableRoute.SellFaster, "/selling/promoteItem")
  addActionPathsNavigableRouteAssociation(NavigableRoute.SellFaster, "/selling/promoteItem/:listingId")
  addActionPathsNavigableRouteAssociation(NavigableRoute.SellingItemDashboard, "/item/:listingId/dashboard")
  addActionPathsNavigableRouteAssociation(NavigableRoute.SellingItemDashboard, "/item/:listingId/dashboard/:attemptedAction")
  addActionPathsNavigableRouteAssociation(NavigableRoute.SellingArchived, "/item/myoffers/archived")
  addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, "/prompted-actions/item")
  addActionPathsNavigableRouteAssociation(NavigableRoute.PromotionResults, "/selling/performanceDashboard")
  addActionPathsNavigableRouteAssociation(NavigableRoute.ItemPerformance, "/selling/itemPerformance/:listingId")

  ///////// My Offers - Buying /////////
  addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, "/buying")
  addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, "/item/myoffers/buying/archived")

  ///////// Commerce /////////
  addActionPathsNavigableRouteAssociation(NavigableRoute.AddDebitDepositMethod, "/payments/deposit-method")
  addActionPathsNavigableRouteAssociation(NavigableRoute.AddBankDepositMethod, "/payments/deposit-method/ach")
  addActionPathsNavigableRouteAssociation(NavigableRoute.KYC1, "/payments/identity/kyc1")
  addActionPathsNavigableRouteAssociation(NavigableRoute.KYC2, "/payments/identity/kyc2")
  addActionPathsNavigableRouteAssociation(NavigableRoute.KYC3, "/payments/identity/kyc3")
  addActionPathsNavigableRouteAssociation(NavigableRoute.SelectPaymentMethod, "/payments/payment-method")
  addActionPathsNavigableRouteAssociation(NavigableRoute.InspectListing, "/shipping/buyer_return/:returnId")
  addActionPathsNavigableRouteAssociation(NavigableRoute.BuyerRequestedRefund, "/shipping/seller_return/:returnId")
  addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, "/payments/buyer_discounts/shipping")
  addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, "/payments/buyer_discounts/free_shipping")
  addActionPathsNavigableRouteAssociation(NavigableRoute.FastDeposit, "/shipping/instant_payouts/promotion")
  addActionPathsNavigableRouteAssociation(NavigableRoute.PaymentDetail, "/shipping/payment/:paymentId/wimm")
  addActionPathsNavigableRouteAssociation(NavigableRoute.OrderReceipt, "/order/:orderId/receipt")
  addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, "/payments/receipt/item/:itemId/discussion/:discussionId")
  addActionPathsNavigableRouteAssociation(NavigableRoute.SellFasterSubscription, "/accounts/subscription/selection")
  addActionPathsNavigableRouteAssociation(NavigableRoute.ManagePromotions, "/accounts/subscription/manage")

  // These two Commerce Action Paths need logic to determine if we go to AcceptDeclineOffer or ShippingOffer
  // They use the same action path, but check if `buyerId` is equal to the currently logged in users's Id.
  addActionPathsNavigableRouteAssociation(
    NavigableRoute.AcceptDeclineOffer,
    "/shipping/view_offer/item/:itemId/discussion/:discussionId/buyer/:buyerId",
  )
  addActionPathsNavigableRouteAssociation(
    NavigableRoute.ShippingOffer,
    "/shipping/view_offer/item/:itemId/discussion/:discussionId/buyer/:buyerId",
  )

  ///////// Letgo Onboarding /////////
  if (LetGoOnboardingController.shouldRespondToLetGoDeeplinksByDate()) {
    addActionPathsNavigableRouteAssociation(NavigableRoute.LetGoWelcome, "/letgo")
  } else {
    addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, "/letgo")
  }

  ///////// Help Center Deeplinks /////////
  // addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, '/app/help/*')

  ///////// Webview Configuration Overrides /////////
  // Not sure if we need these two, or if they even belong here.
  // Legacy apps use these routes to disable back button on webviews when reporting a user or canceling a payment.
  addActionPathsNavigableRouteAssociation(NavigableRoute.HomeTabs, "/accounts/block")
}

const addActionPathsNavigableRouteAssociation = (route: NavigableRoute, path: string) => {
  const pathArray: string[] = routeUrlsMap.get(route) || []
  for (const scheme of APP_SCHEME) {
    for (const authority of APP_AUTHORITY) {
      // With authority. Example https://myproject.com/item/detail/:listingId
      pathArray.push(scheme.concat(authority.concat(path)))
    }
  }
  const OFFERUP_SCHEME = _NAVIGATION_RTES_SCHEME + "://"
  // Without authority . Example myproject://item/detail/:listingId
  pathArray.push(OFFERUP_SCHEME.concat(path))

  // Combination of scheme and route . Example myproject://listingdetails
  if (!pathArray.includes(route)) {
    pathArray.push(OFFERUP_SCHEME.concat(route))
  }

  routeUrlsMap.set(route, pathArray)
}

const addUrlsForRouteAndHandler = (route: string, handler: NavigationPathHandler) => {
  const urlsForRoute = routeUrlsMap.get(route)
  if (!urlsForRoute) {
    Navigation.addPaths(_NAVIGATION_RTES_SCHEME, [route], handler)
    return
  }
  urlsForRoute.forEach(urlString => {
    const urlParts = urlString.split("://")
    Navigation.addPaths(urlParts[0], [urlParts[1]], handler)
  })
}

interface NavOverridesMap {
  [key: string]: "push" | "navigate" | "replace" | undefined
}

const routeNavOverrides: NavOverridesMap = {
  [NavigableRoute.ListingDetail]: "push",
  [NavigableRoute.DiscussionStack]: "push",
  [NavigableRoute.AcceptDeclineOffer]: "push",
  [NavigableRoute.CarBuyerProfile]: "push",
  [NavigableRoute.AddBankDepositMethodReplace]: "replace",
  [NavigableRoute.AddDebitDepositMethodReplace]: "replace",
}

const initializeGlobalSchemePaths = () => {
  navigableRoutes.forEach(route => {
    let pathHandler: NavigationPathHandler
    if (route === NavigableRoute.CloseWebview) {
      pathHandler = webNavigationHandler
    } else if (RTES_THAT_DO_NOT_REQUIRE_AUTHENTICATION.includes(route)) {
      pathHandler = generatePathHandler(route, routeNavOverrides[route])
    } else {
      pathHandler = generateAuthPathHandler(route, routeNavOverrides[route])
    }
    addUrlsForRouteAndHandler(route, pathHandler)
  })
}

const generateAuthPathHandler = (route: string, navType?: "navigate" | "push" | "replace") => {
  return generatePathHandler(
    route,
    navType || "navigate",
    (): Promise<boolean> => {
      const authenticated = authenticationChecker()
      return new Promise<boolean>((resolve, _reject) => {
        resolve(authenticated)
      })
    },
    NavigableRoute.AuthLanding,
  )
}

export const webNavigationHandler: NavigationPathHandler = async (
  navigationRef: React.RefObject<NavigationContainerRef>,
  _payload: NavigationPayload<any>,
) => {
  // close the webview
  navigationRef.current?.goBack()
  return true
}
