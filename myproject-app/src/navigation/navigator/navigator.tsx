import { createStackNavigator } from "@react-navigation/stack"
import SplashScreen from "react-native-splash-screen"
import { WebViewProps } from "myproject-ucl/widgets/myproject-webview/index.native"
import React from "react"
import {
  SearchSortModalScreen,
  SearchSuggestionScreen,
  SearchScreen,
  SearchNavigationProps,
} from "../../../src/screens/feed"
import { LocationPicker, LocationPickerScreenProps } from "../../screens/location-picker/location-picker"
import { WebViewScreen } from "../../screens/WebView"
import { PermissionDialogProps } from "../../widgets/myproject-dialog"
import { PermissionDialog } from "../../widgets/myproject-dialog/myproject-permission-dialog"
import { StaticModalPopup } from "../../widgets/myproject-modal-card/myproject-modal-static"
import { PostFlowNavigator } from "../myproject-post-flow-navigator/myproject-post-flow-navigator"
import { PostFlowProps } from "../../screens/post-flow/commons"
import { NavigationPayload } from "../navigation"
import { TabNavigator } from "../tabnavigator/myproject-tab-navigator"
import { NavigableRoute } from "./navigableroute"
import {
  AffirmRejectDialogScreen,
  AffirmRejectDialogScreenProps,
  ErrorDialogScreenProps,
  ErrorDialogScreen,
} from "../../screens/dialog"
import { PublicProfileScreen } from "../../screens/public-profile/public-profile-screen"
import { ListingSavedListsModalScreen } from "../../screens/listing-detail/listing-saved-lists/listing-saved-lists-modal-screen"
import {
  KYC1,
  KYC2,
  KYC3,
  PaymentAccount,
  SelectPaymentMethod,
  AddBankDepositMethod,
  AddDebitDepositMethod,
  FastDeposit,
  AddPaymentMethod,
  ManagePromotions,
  SellFasterSubscription,
  MyOffersArchivedScreen,
  ListingDetailModal,
  SellFasterScreen,
} from "../../screens"
import { PhoneValidationNavigator } from "../myproject-phone-validation-navigator/myproject-phone-validation-navigator"
import {
  ModalDialogOverlayOptions,
  ModalCardOverlayOption,
  FullScreenModalOptions,
  FullScreenModalNoAnimateOptions,
  PushPopStackAnimationOptions,
  useMultiStepCardNavigatorOptions,
  ForcedFullScreenModalOptions,
} from "../common"
import { SearchStackNavigatorParamList } from "../ousearchnavigator"
import {
  BuyerSelfResolutionNavigator,
  SellerSelfResolutionNavigator,
  MerchantSelfResolutionNavigator,
} from "../myproject-self-resolution-navigator/"
import { ProfileImageSelectionStackNavigator } from "../myproject-profile-image-selection/myproject-profile-image-selection-navigator"
import { AuthStackNavigator } from "../myproject-auth-navigator/myproject-auth-navigator"
import { CustomizeThemeScreen } from "../../screens/account/settings/customize-theme-screen"
import { MeetupScreen } from "../../screens/meetup/meetup-screen"
import {
  ShippingOfferScreen,
  ShippingAddressScreen,
  EditOfferPriceScreen,
  AcceptDeclineOfferScreen,
  EditShippingOfferPriceScreenProps,
  ShippingOfferScreenProps,
  ShippingSellerConfirmScreen,
  ShippingAddressScreenProps,
  ShippingTimeToShip,
} from "../../screens/shipping-offer"
import { MakeOfferScreen } from "../../screens/make-offer"
import { SavedListsScreen } from "../../screens/saved-items"
import { CreateSavedListModalScreen } from "../../screens/saved-items/create-saved-list-modal-screen"
import { SavedListingsScreen } from "../../screens/saved-items/saved-listings-screen"
import { translate, Listing, AuthScreenContext } from "myproject-shared"
import { CarBuyerProfileEditScreen } from "../../screens/car-buyer-profile/car-buyer-profile-screen"
import { PublicProfileAutosDealerInventoryScreen } from "../../screens/public-profile/autos-dealer/autos-public-profile-inventory-screen"
import { IapSubscription, PaymentMethod, ShippingData } from "myproject-shared"
import { InitializeDeeplinking } from "../../third-party/branch-subscriber"
import { SellingItemDashboardScreen } from "../../screens/my-offers/my-offer-item-dashboard-screen"
import { PhotoGalleryModal } from "../../screens/photo-gallery/listing-photo-gallery-modal"
import { PhotoGalleryProps } from "myproject-app/src/screens/listing-detail/listing-details.props"
import { ReportUserScreen } from "../../screens/report/report-user-screen"
import { ReportItemScreen } from "../../screens/report/report-item-screen"
import { ItemPerformanceScreen } from "../../screens/item-performance/item-performance-screen"
import { DiscussionStackNavigator } from "../myproject-discussion/myproject-discussion-navigator"
import { RateUserScreen } from "../../screens/rate-user/rate-user-screen"
import { PushManager } from "../../pushnotifications"
import { createSavedListScreen } from "../../screens/saved-items/create-saved-list-screen"
import { PromotionResultsScreen } from "../../screens/item-performance/promotion-results-screen"
import { TruYouVerifyScreen } from "../../screens/account/settings/tru-ymyproject-verify-screen"
import { AccountPasswordChangeScreen } from "../../screens/account/settings/account-password-change"
import { AccountVanityUrlScreen } from "myproject-app/src/screens/account/account-vanity-url-screen"
import { VerifyEmailScreen } from "myproject-app/src/screens/account/settings/verify-email-screen"
import { AccountSearchAlertsScreen } from "../../screens/account/search-alert/myproject-search-alerts-screen"
import { NotificationPreferenceType, NotificationPreferencesScreen } from "../../screens/account/settings"
import { UpdateNameScreen } from "../../screens/account/settings/update-name-screen"
import { MerchantProfileScreen } from "../../screens/public-profile/merchant/merchant-profile-screen"
import { PaymentDetailScreen } from "../../screens/payments/payment-detail-screen"
import { YearMakeModelSelectionNavigator } from "../../screens/post-flow/step-2/details/year-make-model-selection/year-make-model-navigator"
import { ForcedAppUpgrade } from "../../screens/app-upgrade/forced-app-upgrade-screen"
import { AccountMFANavigator } from "../myproject-account-mfa-navigator/myproject-acount-mfa-navigator"
import { OrderReceiptScreen } from "../../screens/order/order-receipt-screen"
import { SellFasterFTUENavigator } from "../myproject-sell-faster-navigator/myproject-sell-faster-navigator"
import { DiscussionScreenProps } from "../../screens/messages/discussion-screen-props"
import { OrderConfirmationScreen, OrderConfirmationScreenProps } from "../../screens/order/order-confirmation-screen"
import { LetGoWelcomeScreen } from "../../screens/letgo-welcome/letgo-welcome-screen"
import { ServerSettingScreen } from "../../screens/account/settings/server-setting-screen"
import { SearchFilterNavigator } from "../../screens/feed/myproject-search-filters/modals/search-filters-navigator"

// tslint:disable-next-line: interface-over-type-literal
export type NavigatorParamList = {
  [NavigableRoute.UpdateApp]: NavigationPayload<{ iosAppStoreUrl: string; androidPlayStoreUrl: string }>
  [NavigableRoute.ListingDetail]: NavigationPayload<{
    listingId: string
    originScreenName: string | undefined
    sourceEventId: string | undefined
    tileType: string | undefined
  }>
  [NavigableRoute.HomeTabs]: NavigationPayload<undefined>
  [NavigableRoute.PostStack]: NavigationPayload<PostFlowProps>
  [NavigableRoute.ModalCard]: NavigationPayload<{ modalId: string }>
  [NavigableRoute.SearchAlert]: NavigationPayload<SearchNavigationProps>
  [NavigableRoute.SearchSuggestion]: NavigationPayload<{ q?: string; cid?: string }>
  [NavigableRoute.SearchFeed]: NavigationPayload<SearchStackNavigatorParamList>
  [NavigableRoute.SearchFiltersStack]: NavigationPayload<undefined>
  [NavigableRoute.SearchSortModalScreen]: NavigationPayload<undefined>
  [NavigableRoute.LocationPicker]: NavigationPayload<LocationPickerScreenProps>
  [NavigableRoute.PermissionsDialog]: NavigationPayload<PermissionDialogProps>
  [NavigableRoute.WebViewScreen]: NavigationPayload<WebViewProps>
  [NavigableRoute.AffirmRejectDialog]: NavigationPayload<AffirmRejectDialogScreenProps>
  [NavigableRoute.ErrorDialog]: NavigationPayload<ErrorDialogScreenProps>
  [NavigableRoute.RateUser]: NavigationPayload<{ listingId: string }>
  [NavigableRoute.KYC1]: NavigationPayload<undefined>
  [NavigableRoute.KYC2]: NavigationPayload<undefined>
  [NavigableRoute.KYC3]: NavigationPayload<undefined>
  [NavigableRoute.BuyerSelfResolutionStack]: NavigationPayload<undefined>
  [NavigableRoute.SellerSelfResolutionStack]: NavigationPayload<undefined>
  [NavigableRoute.MarchantSelfResolutionStack]: NavigationPayload<undefined>
  [NavigableRoute.DiscussionStack]: NavigationPayload<DiscussionScreenProps>
  [NavigableRoute.PaymentAccount]: NavigationPayload<{ selectedTab?: "account" | "transactions" }>
  [NavigableRoute.PaymentDetail]: NavigationPayload<{ paymentId: number; originScreenName: string }>
  [NavigableRoute.SelectPaymentMethod]: NavigationPayload<{ onGoBack: (paymentMethodId: number) => void }>
  [NavigableRoute.AddPaymentMethod]: NavigationPayload<{ onGoBack: (paymentMethod: PaymentMethod) => void }>
  [NavigableRoute.AddDebitDepositMethod]: NavigationPayload<undefined>
  [NavigableRoute.AddDebitDepositMethodReplace]: NavigationPayload<undefined>
  [NavigableRoute.AddBankDepositMethod]: NavigationPayload<undefined>
  [NavigableRoute.AddBankDepositMethodReplace]: NavigationPayload<undefined>
  [NavigableRoute.FastDeposit]: NavigationPayload<undefined>
  [NavigableRoute.PromotionFTUEScreenOne]: NavigationPayload<undefined>
  [NavigableRoute.PromotionFTUEScreenTwo]: NavigationPayload<undefined>
  [NavigableRoute.SellFasterSubscription]: NavigationPayload<{ subscription?: IapSubscription; itemId?: number }>
  [NavigableRoute.CustomizeTheme]: NavigationPayload<undefined>
  [NavigableRoute.Meetup]: NavigationPayload<{ discussionId: string; refetch: () => void }>
  [NavigableRoute.ShippingOffer]: NavigationPayload<ShippingOfferScreenProps>
  [NavigableRoute.ShippingSellerConfirm]: NavigationPayload<{ shippingData: ShippingData }>
  [NavigableRoute.ShippingTimeToShip]: NavigationPayload<undefined>
  [NavigableRoute.ShippingAddress]: NavigationPayload<ShippingAddressScreenProps>
  [NavigableRoute.MakeOffer]: NavigationPayload<{ listing: Listing; originScreenName: string }>
  [NavigableRoute.EditOfferPrice]: NavigationPayload<EditShippingOfferPriceScreenProps>
  [NavigableRoute.AcceptDeclineOffer]: NavigationPayload<{ buyerId: string; itemId: string }>
  [NavigableRoute.OrderReceipt]: NavigationPayload<{ orderId: string }>
  [NavigableRoute.OrderConfirmation]: NavigationPayload<OrderConfirmationScreenProps>
  [NavigableRoute.SavedLists]: NavigationPayload<undefined>
  [NavigableRoute.SavedListings]: NavigationPayload<{
    savedList: { savedListId: string; savedListName: string; isQuickSave: boolean }
  }>
  [NavigableRoute.CreateSavedListScreen]: NavigationPayload<undefined>
  [NavigableRoute.ManagePromotions]: NavigationPayload<undefined>
  [NavigableRoute.SellingArchived]: NavigationPayload<undefined>
  [NavigableRoute.ItemPerformance]: NavigationPayload<{ listing: Listing; listingId: string }>
  [NavigableRoute.PromotionResults]: NavigationPayload<{ listing: Listing[] }>
  [NavigableRoute.SellingItemDashboard]: NavigationPayload<{
    listingId: string
    attemptedAction: string
    allowRating: boolean
  }>
  [NavigableRoute.SellFaster]: NavigationPayload<{ listing: Listing; listingId?: string }>
  [NavigableRoute.SellFasterStack]: NavigationPayload<undefined>
  [NavigableRoute.PhotoGalleryModal]: NavigationPayload<PhotoGalleryProps>
  [NavigableRoute.ReportUser]: NavigationPayload<{ userId: string }>
  [NavigableRoute.ReportItem]: NavigationPayload<{ listingId: string }>
  /**
   * Profiles
   */
  [NavigableRoute.PublicProfile]: NavigationPayload<{ userId: number }>
  [NavigableRoute.MerchantProfile]: NavigationPayload<{ uuid: string }>
  [NavigableRoute.PublicProfileAutosDealerInventory]: NavigationPayload<{ userId: number; title: string }>
  /**
   * Account
   */
  [NavigableRoute.AuthStack]: NavigationPayload<undefined>
  [NavigableRoute.CarBuyerProfile]: NavigationPayload<{ listingId: string; sendToDealer?: boolean }>
  [NavigableRoute.ProfileImageStack]: NavigationPayload<{}>
  [NavigableRoute.VerifyPhoneStack]: NavigationPayload<undefined>
  [NavigableRoute.AccountMFAStack]: NavigationPayload<undefined>
  [NavigableRoute.AccountVanityUrl]: NavigationPayload<undefined>
  [NavigableRoute.AccountSearchAlerts]: NavigationPayload<undefined>
  [NavigableRoute.NotificationPreferences]: NavigationPayload<{ notificationType: NotificationPreferenceType }>
  [NavigableRoute.VerifyEmail]: NavigationPayload<{ email: string; verified: boolean; context?: AuthScreenContext }>
  [NavigableRoute.UpdateName]: NavigationPayload<{ name: string }>
  [NavigableRoute.AccountPasswordChange]: NavigationPayload<undefined>
  [NavigableRoute.TruYouVerify]: NavigationPayload<undefined>
  [NavigableRoute.YMMSelectionModalCardStack]: NavigationPayload<undefined>
  [NavigableRoute.ListingSavedListsModel]: NavigationPayload<undefined>
  [NavigableRoute.CreateSavedListModel]: NavigationPayload<undefined>
  [NavigableRoute.LetGoWelcome]: NavigationPayload<undefined>
  [NavigableRoute.DevServerSetting]: NavigationPayload<undefined>
}

const Stack = createStackNavigator<NavigatorParamList>()

export const Navigator = () => {
  React.useEffect(() => {
    SplashScreen.hide()

    InitializeDeeplinking()

    PushManager.getInitialNotification(notificationPayload => {
      PushManager.navigateUsingNotification(notificationPayload)
    })
    const newPushNotificationSubscription = PushManager.startListeningForNotifications(notificationPayload => {
      PushManager.navigateUsingNotification(notificationPayload)
    })

    return () => {
      newPushNotificationSubscription.remove()
    }
  }, [])

  const { MultiStepModalCardNavigatorOptions } = useMultiStepCardNavigatorOptions()

  return (
    <Stack.Navigator
      initialRouteName={NavigableRoute.HomeTabs}
      headerMode="none"
      screenOptions={PushPopStackAnimationOptions}>
      <Stack.Screen name={NavigableRoute.HomeTabs} component={TabNavigator} />
      <Stack.Screen name={NavigableRoute.SearchAlert} component={SearchScreen} />
      <Stack.Screen name={NavigableRoute.SearchFeed} component={SearchScreen} />
      <Stack.Screen
        name={NavigableRoute.SearchFiltersStack}
        component={SearchFilterNavigator}
        options={MultiStepModalCardNavigatorOptions}
      />
      <Stack.Screen name={NavigableRoute.AuthStack} component={AuthStackNavigator} options={ForcedFullScreenModalOptions} />
      <Stack.Screen name={NavigableRoute.DiscussionStack} component={DiscussionStackNavigator} />
      <Stack.Screen name={NavigableRoute.ListingDetail} component={ListingDetailModal} options={FullScreenModalOptions} />
      <Stack.Screen name={NavigableRoute.PhotoGalleryModal} component={PhotoGalleryModal} options={FullScreenModalOptions} />
      <Stack.Screen
        name={NavigableRoute.PostStack}
        component={PostFlowNavigator}
        options={{ ...FullScreenModalOptions, gestureEnabled: false }}
      />
      <Stack.Screen name={NavigableRoute.RateUser} component={RateUserScreen} options={FullScreenModalOptions} />
      <Stack.Screen name={NavigableRoute.ModalCard} component={StaticModalPopup} options={ModalCardOverlayOption} />
      <Stack.Screen
        name={NavigableRoute.SearchSuggestion}
        component={SearchSuggestionScreen}
        options={FullScreenModalNoAnimateOptions}
      />
      <Stack.Screen
        name={NavigableRoute.SearchSortModalScreen}
        component={SearchSortModalScreen}
        options={ModalDialogOverlayOptions}
      />
      <Stack.Screen name={NavigableRoute.WebViewScreen} component={WebViewScreen} options={FullScreenModalOptions} />
      <Stack.Screen name={NavigableRoute.LocationPicker} component={LocationPicker} options={ModalCardOverlayOption} />
      <Stack.Screen
        name={NavigableRoute.PermissionsDialog}
        component={PermissionDialog}
        options={ModalDialogOverlayOptions}
      />
      <Stack.Screen
        name={NavigableRoute.AffirmRejectDialog}
        component={AffirmRejectDialogScreen}
        options={ModalDialogOverlayOptions}
      />
      <Stack.Screen name={NavigableRoute.ErrorDialog} component={ErrorDialogScreen} options={ModalDialogOverlayOptions} />
      <Stack.Screen
        name={NavigableRoute.VerifyPhoneStack}
        component={PhoneValidationNavigator}
        options={FullScreenModalOptions}
      />
      <Stack.Screen
        name={NavigableRoute.AccountMFAStack}
        component={AccountMFANavigator}
        options={ModalDialogOverlayOptions}
      />
      <Stack.Screen name={NavigableRoute.KYC1} component={KYC1} />
      <Stack.Screen name={NavigableRoute.KYC2} component={KYC2} />
      <Stack.Screen name={NavigableRoute.KYC3} component={KYC3} />
      <Stack.Screen
        name={NavigableRoute.ProfileImageStack}
        component={ProfileImageSelectionStackNavigator}
        options={FullScreenModalOptions}
      />
      <Stack.Screen name={NavigableRoute.SelectPaymentMethod} component={SelectPaymentMethod} />
      <Stack.Screen
        name={NavigableRoute.BuyerSelfResolutionStack}
        component={BuyerSelfResolutionNavigator}
        options={FullScreenModalOptions}
      />
      <Stack.Screen
        name={NavigableRoute.SellerSelfResolutionStack}
        component={SellerSelfResolutionNavigator}
        options={FullScreenModalOptions}
      />
      <Stack.Screen
        name={NavigableRoute.MarchantSelfResolutionStack}
        component={MerchantSelfResolutionNavigator}
        options={FullScreenModalOptions}
      />
      <Stack.Screen name={NavigableRoute.PaymentAccount} component={PaymentAccount} />
      <Stack.Screen name={NavigableRoute.PaymentDetail} component={PaymentDetailScreen} />
      <Stack.Screen name={NavigableRoute.AddPaymentMethod} component={AddPaymentMethod} options={FullScreenModalOptions} />
      <Stack.Screen
        name={NavigableRoute.AddDebitDepositMethod}
        component={AddDebitDepositMethod}
        options={FullScreenModalOptions}
      />
      <Stack.Screen
        name={NavigableRoute.AddDebitDepositMethodReplace}
        component={AddDebitDepositMethod}
        options={FullScreenModalOptions}
      />
      <Stack.Screen name={NavigableRoute.FastDeposit} component={FastDeposit} />
      <Stack.Screen
        name={NavigableRoute.AddBankDepositMethod}
        component={AddBankDepositMethod}
        options={FullScreenModalOptions}
      />
      <Stack.Screen
        name={NavigableRoute.AddBankDepositMethodReplace}
        component={AddBankDepositMethod}
        options={FullScreenModalOptions}
      />
      <Stack.Screen name={NavigableRoute.ManagePromotions} component={ManagePromotions} />
      <Stack.Screen name={NavigableRoute.SellFasterSubscription} component={SellFasterSubscription} />
      <Stack.Screen name={NavigableRoute.CustomizeTheme} component={CustomizeThemeScreen} />
      <Stack.Screen name={NavigableRoute.Meetup} component={MeetupScreen} options={{ headerShown: false }} />
      <Stack.Screen
        name={NavigableRoute.ShippingOffer}
        component={ShippingOfferScreen}
        options={{ ...FullScreenModalOptions, headerShown: false }}
      />
      <Stack.Screen name={NavigableRoute.ShippingSellerConfirm} component={ShippingSellerConfirmScreen} />
      <Stack.Screen name={NavigableRoute.ShippingTimeToShip} component={ShippingTimeToShip} />
      <Stack.Screen name={NavigableRoute.UpdateApp} component={ForcedAppUpgrade} options={ForcedFullScreenModalOptions} />
      <Stack.Screen
        name={NavigableRoute.ShippingAddress}
        component={ShippingAddressScreen}
        options={{ headerTitle: translate("shipping_address_screen.screen_title") }}
      />
      <Stack.Screen
        name={NavigableRoute.MakeOffer}
        component={MakeOfferScreen}
        options={{ ...FullScreenModalOptions, headerShown: false }}
      />
      <Stack.Screen
        name={NavigableRoute.EditOfferPrice}
        component={EditOfferPriceScreen}
        options={{ headerTitle: translate("edit_price_screen.screen_title") }}
      />
      <Stack.Screen
        name={NavigableRoute.AcceptDeclineOffer}
        component={AcceptDeclineOfferScreen}
        options={FullScreenModalOptions}
      />
      <Stack.Screen name={NavigableRoute.OrderReceipt} component={OrderReceiptScreen} options={{ headerShown: false }} />
      <Stack.Screen
        name={NavigableRoute.OrderConfirmation}
        component={OrderConfirmationScreen}
        options={FullScreenModalOptions}
      />
      <Stack.Screen name={NavigableRoute.SavedLists} component={SavedListsScreen} />
      <Stack.Screen
        name={NavigableRoute.ListingSavedListsModel}
        component={ListingSavedListsModalScreen}
        options={ModalDialogOverlayOptions}
      />
      <Stack.Screen
        name={NavigableRoute.SellingArchived}
        component={MyOffersArchivedScreen}
        options={FullScreenModalOptions}
      />
      <Stack.Screen
        name={NavigableRoute.CreateSavedListModel}
        component={CreateSavedListModalScreen}
        options={ModalDialogOverlayOptions}
      />
      <Stack.Screen name={NavigableRoute.CreateSavedListScreen} component={createSavedListScreen} />
      <Stack.Screen name={NavigableRoute.SavedListings} component={SavedListingsScreen} />
      <Stack.Screen name={NavigableRoute.SellingItemDashboard} component={SellingItemDashboardScreen} />
      <Stack.Screen name={NavigableRoute.ItemPerformance} component={ItemPerformanceScreen} />
      <Stack.Screen name={NavigableRoute.PromotionResults} component={PromotionResultsScreen} />
      <Stack.Screen name={NavigableRoute.SellFaster} component={SellFasterScreen} options={FullScreenModalOptions} />
      <Stack.Screen
        name={NavigableRoute.SellFasterStack}
        component={SellFasterFTUENavigator}
        options={FullScreenModalOptions}
      />
      <Stack.Screen name={NavigableRoute.ReportUser} component={ReportUserScreen} options={ForcedFullScreenModalOptions} />
      <Stack.Screen name={NavigableRoute.ReportItem} component={ReportItemScreen} options={ForcedFullScreenModalOptions} />
      {/* Profiles */}
      <Stack.Screen name={NavigableRoute.MerchantProfile} component={MerchantProfileScreen} />
      <Stack.Screen name={NavigableRoute.PublicProfile} component={PublicProfileScreen} />
      <Stack.Screen
        name={NavigableRoute.PublicProfileAutosDealerInventory}
        component={PublicProfileAutosDealerInventoryScreen}
      />
      {/* Account */}
      <Stack.Screen
        name={NavigableRoute.AccountVanityUrl}
        component={AccountVanityUrlScreen}
        options={FullScreenModalOptions}
      />
      <Stack.Screen
        name={NavigableRoute.AccountSearchAlerts}
        component={AccountSearchAlertsScreen}
        options={FullScreenModalOptions}
      />
      <Stack.Screen
        name={NavigableRoute.CarBuyerProfile}
        component={CarBuyerProfileEditScreen}
        options={ModalCardOverlayOption}
      />
      <Stack.Screen
        name={NavigableRoute.NotificationPreferences}
        component={NotificationPreferencesScreen}
        options={FullScreenModalOptions}
      />
      <Stack.Screen name={NavigableRoute.VerifyEmail} component={VerifyEmailScreen} options={FullScreenModalOptions} />
      <Stack.Screen name={NavigableRoute.UpdateName} component={UpdateNameScreen} options={ModalCardOverlayOption} />
      <Stack.Screen
        name={NavigableRoute.AccountPasswordChange}
        component={AccountPasswordChangeScreen}
        options={FullScreenModalOptions}
      />
      <Stack.Screen name={NavigableRoute.TruYouVerify} component={TruYouVerifyScreen} options={FullScreenModalOptions} />
      <Stack.Screen
        name={NavigableRoute.YMMSelectionModalCardStack}
        component={YearMakeModelSelectionNavigator}
        options={MultiStepModalCardNavigatorOptions}
      />
      <Stack.Screen name={NavigableRoute.LetGoWelcome} component={LetGoWelcomeScreen} options={FullScreenModalOptions} />
      <Stack.Screen
        name={NavigableRoute.DevServerSetting}
        component={ServerSettingScreen}
        options={FullScreenModalOptions}
      />
    </Stack.Navigator>
  )
}
