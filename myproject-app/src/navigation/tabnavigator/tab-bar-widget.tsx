import React, { memo } from "react"
import { View, StyleSheet } from "react-native"
import { useColor, useColorForBackgroundColor } from "myproject-ucl"
import { useAuth } from "myproject-shared"
import { TabBarButton } from "./myproject-tab-bar-button"
import { TabBarIndicator } from "./myproject-tab-bar-indicator"
import { Navigation } from "../navigation/navigation"
import { NavigableRoute } from "../navigator/navigableroute"
import { BottomTabBarProps } from "@react-navigation/bottom-tabs"
import { useNavigationState, NavigationState } from "@react-navigation/native"
import {
  AnalyticsNavigation,
  ScreenName,
  TabNavigatorElementName,
  AnalyticsDebug,
  BottomNavEventInput,
  RealtimeConnectionContext,
} from "myproject-shared"
import { InboxDataContext } from "myproject-shared/providers/chat"

export const TabBarHeight = 52
const TabStyles = StyleSheet.create({
  tabBar: { flexDirection: "column", height: TabBarHeight },
  tabContainer: { flexDirection: "row", height: TabBarHeight },
  line: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    height: StyleSheet.hairlineWidth,
  },
})

const canPopTabStack = (state: NavigationState, stackRoute: string): boolean => {
  const homeTabInfo = state.routes.find(route => route.name === NavigableRoute.HomeTabs)
  const homeTabInfoState = homeTabInfo?.state as NavigationState
  if (!homeTabInfoState) {
    return false
  }

  const stackInfo = homeTabInfoState.routes.find(route => route.name === stackRoute)
  const stackInfoState = stackInfo?.state as NavigationState
  if (!stackInfoState) {
    return false
  }

  return stackInfoState.index > 0
}

const TabBarWidget = memo((props: BottomTabBarProps) => {
  const navState = useNavigationState(state => state)
  const { subscribe } = React.useContext(RealtimeConnectionContext)
  const { unseenAlertCount, refetchUnseenAlertCount } = React.useContext(InboxDataContext)
  const { isAuthed } = useAuth()
  const { colors } = useColor()
  const [inboxBadgeAmount, setInboxBadgeAmount] = React.useState<number>()
  const activeTintColor = colors.palouseGreen

  React.useEffect(() => {
    let subscription
    if (isAuthed) {
      subscription = subscribe(() => {
        refetchUnseenAlertCount()
      })
    }
    return () => subscription && subscription.unsubscribe()
  }, [isAuthed, refetchUnseenAlertCount])

  React.useEffect(() => {
    if (!isAuthed || !unseenAlertCount) {
      setInboxBadgeAmount(undefined)
      return
    }

    setInboxBadgeAmount(unseenAlertCount.total > 0 ? unseenAlertCount.total : undefined)
  }, [isAuthed, unseenAlertCount])

  return (
    // The container for the whole tab bar
    <View style={TabStyles.tabBar} testID="tab-navigator.tab-bar-widget" accessibilityLabel="tab-navigator.tab-bar-widget">
      {/* The bar under the slider for the actual tabs */}
      <View style={TabStyles.tabContainer}>
        {/* Build the tab buttons from the routes */}
        {props.state.routes.map((route, index: number) => {
          const { options } = props.descriptors[route.key]
          const label = options.title || route.name
          const isFocused = props.state.index === index
          const icon = options.tabBarIcon! as (props: { focused: boolean }) => React.ReactNode
          const badgeAmount = route.name === NavigableRoute.Inbox ? inboxBadgeAmount : undefined
          const onPress = () => {
            const eventInput: BottomNavEventInput = {
              screenName: ScreenName[props.state.routeNames[props.state.index]],
              elementName: TabNavigatorElementName[route.name],
            }
            AnalyticsNavigation.trackBottomNavEventButtonClick(eventInput)
            requestAnimationFrame(() => {
              if (!isFocused) {
                Navigation.navigateToRoute(route.name)
                  .then(() => {
                    AnalyticsNavigation.trackBottomNavEventScreenShow(eventInput)
                  })
                  .catch(error => {
                    AnalyticsDebug.logError(error)
                  })
              } else {
                Navigation.performWhenAvailable(() => {
                  if (canPopTabStack(navState, route.name)) {
                    Navigation.popToTop()
                  }
                })
              }
            })
          }
          return (
            <TabBarButton
              key={index}
              index={index}
              onPress={onPress}
              active={isFocused}
              labelText={label}
              renderIcon={icon({ focused: isFocused })}
              badgeAmount={badgeAmount}
              testID={label}
            />
          )
        })}
      </View>

      {/* Line at the top of the tab bar */}
      <View style={[TabStyles.line, { backgroundColor: useColorForBackgroundColor("secondary") }]} />

      <TabBarIndicator tabCount={props.state.routes.length} tintColor={activeTintColor} activeIndex={props.state.index} />
    </View>
  )
})

export { TabBarWidget }
