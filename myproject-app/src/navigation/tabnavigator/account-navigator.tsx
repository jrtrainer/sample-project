import { createStackNavigator } from "@react-navigation/stack"
import React from "react"
import { AccountScreen } from "../../screens/account"
import { AccountSettingScreen } from "../../screens/account/settings"
import { PushPopStackAnimationOptions } from "../common"
import { NavigationPayload } from "../navigation"
import { NavigableRoute } from "../navigator/navigableroute"

// tslint:disable-next-line: interface-over-type-literal
export type AccountParamList = {
  [NavigableRoute.Account]: NavigationPayload<undefined>
  [NavigableRoute.AccountSettings]: NavigationPayload<undefined>
}

const Stack = createStackNavigator<AccountParamList>()

export const AccountNavigator: React.FC = () => {
  return (
    <Stack.Navigator
      initialRouteName={NavigableRoute.Account}
      headerMode="none"
      screenOptions={PushPopStackAnimationOptions}>
      <Stack.Screen name={NavigableRoute.Account} component={AccountScreen} />
      <Stack.Screen
        name={NavigableRoute.AccountSettings}
        component={AccountSettingScreen}
        options={PushPopStackAnimationOptions}
      />
    </Stack.Navigator>
  )
}
