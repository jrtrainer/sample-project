import { BottomTabBarProps, createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import {
  LocalSVGSource,
  LogoCircle,
  LogoCircleSecondary,
  SVG,
  TabBarMyOffersFill,
  TabBarMyOffersOutline,
  TabBarNotificationsFill,
  TabBarNotificationsOutline,
  TabBarPostFill,
  TabBarPostOutline,
  TabBarProfileFill,
  TabBarProfileOutline,
  useColorForBackgroundColor,
} from "myproject-ucl"
import React from "react"
import { View } from "react-native"
import { useSafeAreaInsets } from "react-native-safe-area-context"
import { MyOffersSellingScreen } from "../../screens/my-offers/"
import { InboxStackNavigator } from "../ouinboxnavigator/myproject-inbox-navigator"
import { NavigableRoute } from "../navigator/navigableroute"
import { SearchStackNavigator } from "../ousearchnavigator/myproject-search-navigator"
import { AccountNavigator } from "./myproject-account-navigator"
import { TabBarWidget } from "./myproject-tab-bar-widget"

const createNavigationOptions = (title: string, fillSVG: LocalSVGSource, outlineSVG: LocalSVGSource) => {
  return () => {
    return {
      tabBarIcon: (props: { focused: boolean }) => {
        return (
          <SVG
            localSVG={{ SVG: props.focused ? fillSVG.SVG : outlineSVG.SVG, size: { width: 24, height: 24 } }}
            tint={props.focused ? "link" : "secondary"}
          />
        )
      },
      title,
    }
  }
}

const HomeNavigationOptions = createNavigationOptions("myproject-tab-navigator.home", LogoCircle, LogoCircleSecondary)
const InboxNavigationOptions = createNavigationOptions(
  "myproject-tab-navigator.inbox",
  TabBarNotificationsFill,
  TabBarNotificationsOutline,
)
const PostNavigationOptions = createNavigationOptions("myproject-tab-navigator.post", TabBarPostFill, TabBarPostOutline)
const SellingNavigationOptions = createNavigationOptions(
  "myproject-tab-navigator.selling",
  TabBarMyOffersFill,
  TabBarMyOffersOutline,
)
const AccountNavigationOptions = createNavigationOptions(
  "myproject-tab-navigator.account",
  TabBarProfileFill,
  TabBarProfileOutline,
)

const Tabs = createBottomTabNavigator()

const tabBarFunc = (props: BottomTabBarProps) => {
  return <TabBarWidget {...props} />
}

const ShadowPostFlow = () => {
  // This route is redirected by Navigation to PostFlowNavigator as a modal on the root navigator.
  return <></>
}

export const TabNavigator = () => {
  const insets = useSafeAreaInsets()
  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        paddingBottom: insets.bottom,
        backgroundColor: useColorForBackgroundColor("primary"),
      }}
      testID="tab-navigator"
      accessibilityLabel="tab-navigator">
      <Tabs.Navigator tabBar={tabBarFunc}>
        <Tabs.Screen name={NavigableRoute.SearchStack} component={SearchStackNavigator} options={HomeNavigationOptions} />
        <Tabs.Screen name={NavigableRoute.Inbox} component={InboxStackNavigator} options={InboxNavigationOptions} />
        <Tabs.Screen
          name={NavigableRoute.PostStackShadowForModalPostFlowTab}
          component={ShadowPostFlow}
          options={PostNavigationOptions}
        />
        <Tabs.Screen name={NavigableRoute.Selling} component={MyOffersSellingScreen} options={SellingNavigationOptions} />
        <Tabs.Screen name={NavigableRoute.AccountStack} component={AccountNavigator} options={AccountNavigationOptions} />
      </Tabs.Navigator>
    </View>
  )
}
