import { ActionSheetIOSOptions } from 'react-native'
import { LocalSVGSource } from 'myproject-ucl'

export type ActionSheetCallback = (buttonIndex: number) => void

export interface ActionSheetParams extends ActionSheetIOSOptions {
  icons?: LocalSVGSource[]
}
