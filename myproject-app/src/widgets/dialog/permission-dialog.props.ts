import { LocalSVGSource } from 'myproject-ucl'

export interface PermissionDialogProps {
  title: string
  prompt: string
  localSVGSource?: LocalSVGSource
  onGranted(): void
  onDenied(): void
}
