import { ModalCardProps } from '../myproject-modal-card.props'

export interface ModalHostContextProps {
  children?: React.ReactNode
  modalProps: ModalCardProps
}
