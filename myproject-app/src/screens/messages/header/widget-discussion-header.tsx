import _ from "lodash"
import {
  ChatBuyerShippingBanner,
  ChatSellerShippingBanner,
  translate,
  AnalyticsElementType,
  AnalyticsActionType,
  ChatUserRole,
  ChatScreenElement,
  AccountDataContext,
  ChatScreenName,
  UserProfile,
  Listing,
  VisualTag,
} from "myproject-shared"
import { ShippingOfferType } from "myproject-shared/constants"
import {
  NAVIGATION_BAR_HEIGHT,
  ActionEllipsisFill,
  Flex,
  ItemPreview,
  Margin,
  NavigationBar,
  NavigationBarItem,
  PublicUserProfile,
  Separator,
  Touchable,
  Background,
  DiscussionVisualTagsRow,
} from "myproject-ucl"
import React, { FC, useContext } from "react"
import { Navigation, CommonNavs } from "../../../navigation/navigation"
import { NavigableRoute } from "../../../navigation/navigator"
import { ChatAnalyticsController } from "myproject-shared/analytics/myproject-analytics-chat"
import { useActionSheet, ActionSheetParams } from "../../../widgets"
import { getNavigationBackButton } from "../../../navigation/common"
import { useSettingsWebViewLinks } from "../../../providers/myproject-settings-context"

const DISCUSSION_HEADER_HEIGHT = 74

export const DiscussionHeader: FC<{
  discussionId?: string
  itemId?: number
  sellerId?: string
  buyerId?: string
  chatError?: any
  profile?: UserProfile
  listing?: Listing
  visualTags?: VisualTag[]
}> = ({ itemId, sellerId, discussionId, buyerId, chatError, listing, profile, visualTags }) => {
  const { data: me } = useContext(AccountDataContext)
  const myId = _.property("id")(me) + ""
  const isMySale = myId === sellerId
  const analyticsUserRole: ChatUserRole = isMySale ? ChatUserRole.Seller : ChatUserRole.Buyer
  const otherUserId = !isMySale ? sellerId : buyerId
  const webViewLinks = useSettingsWebViewLinks()

  const onBackButton = () => {
    ChatAnalyticsController.trackChatScreenUIEvent(
      ChatScreenElement.Back,
      AnalyticsElementType.Button,
      AnalyticsActionType.Click,
      analyticsUserRole,
    )
  }

  const { show: showActionSheet } = useActionSheet()
  const onLinkPressed = () => {
    const ouActionSheetOptions: ActionSheetParams = {
      options: [
        translate("discussion-screen.help-title"),
        translate("common-actions.report"),
        translate("common-actions.cancel"),
      ],
      cancelButtonIndex: 2,
      destructiveButtonIndex: 1,
    }
    showActionSheet(ouActionSheetOptions, (pressed: number) => {
      if (pressed === 0) {
        Navigation.performWhenAvailable(() => {
          CommonNavs.presentWebView(webViewLinks.MessagingTips)
        })
      }
      if (pressed === 1) {
        ChatAnalyticsController.trackChatScreenUIEvent(
          ChatScreenElement.ReportHelp,
          AnalyticsElementType.Button,
          AnalyticsActionType.Click,
          analyticsUserRole,
        )
        Navigation.performWhenAvailable(() => {
          Navigation.navigateToRoute(NavigableRoute.ReportUser, { userId: _.toNumber(otherUserId) })
        })
      }
    })
  }
  const rightMenuActions: NavigationBarItem = {
    pressHandler: onLinkPressed,
    icon: ActionEllipsisFill,
    testID: "discussion-screen.navigation-bar.ellipsis",
  }

  const openUserProfile = () => {
    ChatAnalyticsController.trackChatScreenUIEvent(
      ChatScreenElement.UserProfile,
      AnalyticsElementType.Button,
      AnalyticsActionType.Click,
      analyticsUserRole,
    )
    otherUserId && Navigation.navigateToRoute(NavigableRoute.PublicProfile, { userId: _.toInteger(otherUserId) })
  }

  const onShippingBuyNowPressed = () => {
    Navigation.navigateToRoute(NavigableRoute.ShippingOffer, { listing, offerType: ShippingOfferType.BuyNow })
  }

  const onShippingMakeOfferPressed = (offerType: ShippingOfferType) => {
    Navigation.navigateToRoute(NavigableRoute.ShippingOffer, { listing, offerType })
  }

  const itemImagePreview: any = _.head(listing?.photos)?.detail?.url || ""
  const price: any = listing?.price
  const goToListingDetail = () => {
    if (!itemId) {
      return
    }
    ChatAnalyticsController.trackChatScreenUIEvent(
      ChatScreenElement.ItemDetail,
      AnalyticsElementType.Button,
      AnalyticsActionType.Click,
      analyticsUserRole,
    )
    Navigation.performWhenAvailable(() => {
      Navigation.navigateToRoute(NavigableRoute.ListingDetail, {
        listingId: _.toString(itemId),
        originScreenName: ChatScreenName.Chat,
      })
    })
  }
  return (
    <Flex direction="column" grow={0} shrink={0}>
      <Flex direction="column" grow={0} height={NAVIGATION_BAR_HEIGHT}>
        <NavigationBar
          title={translate(discussionId ? "discussion-screen.title" : "discussion-screen.first-message-web-title")}
          leftItems={[getNavigationBackButton("discussion-screen.navigation-bar", onBackButton)]}
          rightItems={chatError ? [] : [rightMenuActions]}
          testID="discussion-screen.navigation-bar"
        />
      </Flex>
      {chatError ? null : (
        <>
          <Margin
            marginRightStep={4}
            marginLeftStep={4}
            marginTopStep={4}
            marginBottomStep={0}
            grow={0}
            direction="column"
            axisDistribution="center"
            crossAxisDistribution="center">
            <Flex direction="row" height={DISCUSSION_HEADER_HEIGHT}>
              <Background />
              <Touchable onPress={openUserProfile} testID="discussion-screen.user-profile">
                {profile && <PublicUserProfile {...(profile as any)} />}
              </Touchable>

              <Flex direction="column" grow={1} crossAxisDistribution="flex-end" axisDistribution="center">
                <Touchable onPress={goToListingDetail} testID="discussion-screen.item-image">
                  <ItemPreview image={itemImagePreview} price={price} />
                </Touchable>
              </Flex>
            </Flex>
            <Separator />
          </Margin>
          {visualTags && visualTags.length > 0 ? (
            <Margin marginStep={4}>
              <DiscussionVisualTagsRow tags={visualTags} axisDistribution="center" crossAxisDistribution="center" grow={1} />
            </Margin>
          ) : isMySale && buyerId && itemId && discussionId ? (
            <ChatSellerShippingBanner itemId={itemId} buyerId={_.toInteger(buyerId)} />
          ) : itemId && discussionId ? (
            <ChatBuyerShippingBanner
              itemId={itemId}
              onBuyNowClicked={onShippingBuyNowPressed}
              onMakeOfferClicked={onShippingMakeOfferPressed}
            />
          ) : null}
        </>
      )}
    </Flex>
  )
}
