import { ItemDetailSellerType, ItemDetailType } from 'myproject-shared/analytics/constants/item-detail-constants'

export interface DiscussionScreenProps {
  discussionId?: string
  itemId?: string
  buyerId?: string
  sellerId?: string
  sellerType?: ItemDetailSellerType
  type?: ItemDetailType
  fullPrice?: boolean
  buyNowEnabled?: boolean
  price?: string
  offerPrice?: string
  categoryL1?: string
  categoryL2?: string
}
