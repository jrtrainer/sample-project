import _ from "lodash"
import { translate, ChatScreenElement, AnalyticsElementType, AnalyticsActionType } from "myproject-shared"
import { InboxDataContext } from "myproject-shared/providers/chat"
import {
  ActivityIndicator,
  Background,
  CameraCapture,
  CameraFlashLightAuto,
  CameraFlashLightOff,
  CameraFlashLightOn,
  CameraFlip,
  Center,
  Flex,
  Margin,
  Overlay,
  Stack,
  SVG,
  Text,
  Touchable,
} from "myproject-ucl"
import { ListRef } from "myproject-ucl/controls/myproject-list"
import React, { FC, useContext, useRef, useState } from "react"
import { RNCamera } from "react-native-camera"
import { TouchableOpacity, TouchableWithoutFeedback } from "react-native-gesture-handler"
import { DiscussionStackNavigatorParamList } from "../../navigation/myproject-discussion/myproject-discussion-navigator"
import { ScreenRoute } from "../../navigation/myproject-route"
import { Navigation } from "../../navigation/navigation"
import { NavigableRoute } from "../../navigation/navigator"
import { Screen, saveToCameraRoll, deletePhotosAt } from "../../widgets"
import { DraftPhotoList } from "../post-flow/camera/draft-photo-list"
import { GalleryPreview } from "../post-flow/camera/gallery-preview"
import { flashLightModes } from "../post-flow/camera/top-bar"
import { usePhotoPicker } from "../post-flow/image-picker/usePhotoPicker"
import { usePhotoSelection } from "../post-flow/photo-selection-provider"
import { ChatAnalyticsController } from "myproject-shared/analytics/myproject-analytics-chat"
import { CAMERA_CHAT_IMAGE_SAVE_FOLDER } from "./discussion-image-common"
const CAMERA_OPTIONS = {
  quality: 1,
  base64: true,
}

export const DiscussionCameraScreen: FC<ScreenRoute<DiscussionStackNavigatorParamList, NavigableRoute.DiscussionCamera>> = ({
  route,
}) => {
  const { discussionId, refetch } = route.params.props
  const [loading, setLoading] = useState(false)
  const camera = useRef<RNCamera>(null)
  const { resetDraftSelectedPhotos } = usePhotoPicker()
  const { draftSelectedPhotos, maxNumberOfPhotos, setDraftSelectedPhotos, savePhotosToLocalSandBox } = usePhotoSelection()
  const maxNumberOfPhotosTaken = maxNumberOfPhotos > draftSelectedPhotos.length
  const [isBackCamera, setIsBackCamera] = useState(true)
  const [flashLightMode, setFlashLightMode] = useState(flashLightModes[0])
  const [photoCaptureLoading, setPhotoCaptureLoading] = useState(false)
  const photoListRef = useRef<ListRef>(null)
  const takePicture = async () => {
    ChatAnalyticsController.trackChatTakePhotoUIEvent(
      ChatScreenElement.TakePhoto,
      AnalyticsElementType.Button,
      AnalyticsActionType.Click,
    )
    if (camera.current) {
      setPhotoCaptureLoading(true)
      const { uri } = await camera.current.takePictureAsync(CAMERA_OPTIONS)
      const photoSavedFromCamera = await saveToCameraRoll({
        uri,
        directoryPath: CAMERA_CHAT_IMAGE_SAVE_FOLDER,
      })
      setDraftSelectedPhotos([...draftSelectedPhotos, photoSavedFromCamera])
      setPhotoCaptureLoading(false)
    }
    if (photoListRef.current) {
      photoListRef.current.scrollToEnd()
    }
  }

  const cancel = () => {
    ChatAnalyticsController.trackChatTakePhotoUIEvent(
      ChatScreenElement.Cancel,
      AnalyticsElementType.Button,
      AnalyticsActionType.Click,
    )
    Navigation.goBack()
  }

  const goToImagePicker = () => {
    ChatAnalyticsController.trackChatTakePhotoUIEvent(
      ChatScreenElement.CameraRoll,
      AnalyticsElementType.Button,
      AnalyticsActionType.Click,
    )
    Navigation.goBack()
  }
  const onFlashLightChange = () => {
    const currentFlashLightMode = flashLightModes.findIndex(f => f === flashLightMode)
    const newFlashLightMode = flashLightModes[(currentFlashLightMode + 1) % flashLightModes.length]
    const flashlightModeElement: ChatScreenElement = flashLightElementNames[newFlashLightMode]
    ChatAnalyticsController.trackChatTakePhotoUIEvent(
      flashlightModeElement,
      AnalyticsElementType.Button,
      AnalyticsActionType.Click,
    )
    setFlashLightMode(newFlashLightMode)
  }

  const toggleCameraType = () => {
    ChatAnalyticsController.trackChatTakePhotoUIEvent(
      ChatScreenElement.Rotate,
      AnalyticsElementType.Button,
      AnalyticsActionType.Click,
    )
    setIsBackCamera(!isBackCamera)
  }
  const flashLightIcons = {
    [RNCamera.Constants.FlashMode.auto]: (
      <SVG onPress={onFlashLightChange} localSVG={CameraFlashLightAuto} tint="alwaysLight" />
    ),
    [RNCamera.Constants.FlashMode.on]: <SVG onPress={onFlashLightChange} localSVG={CameraFlashLightOn} />,
    [RNCamera.Constants.FlashMode.off]: (
      <SVG onPress={onFlashLightChange} localSVG={CameraFlashLightOff} tint="alwaysLight" />
    ),
  }
  const flashLightElementNames = {
    [RNCamera.Constants.FlashMode.auto]: ChatScreenElement.FlashAuto,
    [RNCamera.Constants.FlashMode.off]: ChatScreenElement.FlashOff,
    [RNCamera.Constants.FlashMode.on]: ChatScreenElement.FlashOn,
  }
  const { sendImageMessage } = useContext(InboxDataContext)

  const onSend = async () => {
    setLoading(true)
    ChatAnalyticsController.trackChatTakePhotoUIEvent(
      ChatScreenElement.Done,
      AnalyticsElementType.Button,
      AnalyticsActionType.Click,
      draftSelectedPhotos.length,
    )
    try {
      const photosToSend = await savePhotosToLocalSandBox(draftSelectedPhotos, CAMERA_CHAT_IMAGE_SAVE_FOLDER)
      await Promise.all(photosToSend.map(image => sendImageMessage(discussionId, image.uri)))
      await refetch()
      resetDraftSelectedPhotos()
      Navigation.popToTop()
    } catch (error) {
      Navigation.navigateToRoute(NavigableRoute.ErrorDialog, { error })
    } finally {
      await deletePhotosAt(CAMERA_CHAT_IMAGE_SAVE_FOLDER)
    }
    setLoading(false)
  }
  return (
    <Screen safeAreaMode="top">
      <Background type="alwaysDark" />
      <Stack height="100%" direction="column">
        <Stack direction="row" crossAxisDistribution="flex-end">
          <Margin marginStep={4}>
            <Stack direction="column">
              <TouchableWithoutFeedback onPress={cancel}>
                <Text color="alwaysLight" textType="primaryBody1">
                  {translate("camera.cancel-btn")}
                </Text>
              </TouchableWithoutFeedback>
            </Stack>
            <Flex grow={1}>
              <></>
            </Flex>
            <Stack childSeparationStep={2} crossAxisDistribution="center" direction="row" axisDistribution="flex-end">
              <TouchableWithoutFeedback onPress={toggleCameraType}>
                <SVG localSVG={CameraFlip} tint="alwaysLight" />
              </TouchableWithoutFeedback>
              {flashLightIcons[flashLightMode]}
            </Stack>
          </Margin>
        </Stack>

        <Flex grow={1}>
          <RNCamera
            ref={camera}
            captureAudio={false}
            style={{ flexGrow: 1 }}
            type={isBackCamera ? RNCamera.Constants.Type.back : RNCamera.Constants.Type.front}
            flashMode={RNCamera.Constants.FlashMode.on}
          />
          {photoCaptureLoading && (
            <Overlay width="100%" height="100%">
              <Background type="overlay" />
              <Center>
                <ActivityIndicator size="large" />
              </Center>
            </Overlay>
          )}
        </Flex>

        <Margin marginLeftStep={2} marginRightStep={2} marginBottomStep={8}>
          <Stack direction="column" grow={1} childSeparationStep={1}>
            <Margin marginTopStep={4} marginBottomStep={4}>
              <DraftPhotoList ref={photoListRef} photos={draftSelectedPhotos} photoIsBeingCaptured={photoCaptureLoading} />
            </Margin>
            <Stack grow={1} direction="row" crossAxisDistribution="center" axisDistribution="center">
              <Flex
                grow={1}
                crossAxisDistribution="center"
                axisDistribution="flex-end"
                touchUpInsideHandler={goToImagePicker}>
                <GalleryPreview />
              </Flex>
              <Margin grow={1} crossAxisDistribution="center" direction="column">
                <TouchableOpacity onPress={takePicture} disabled={!maxNumberOfPhotosTaken}>
                  <SVG localSVG={CameraCapture} />
                </TouchableOpacity>
              </Margin>
              <Stack grow={1} direction="column" crossAxisDistribution="flex-start">
                {!loading && _.first(draftSelectedPhotos) && (
                  <Touchable onPress={onSend}>
                    <Text textType="headline3" color="alwaysLight">
                      {translate("discussion-screen.send")}
                    </Text>
                  </Touchable>
                )}
                {loading && <ActivityIndicator />}
              </Stack>
            </Stack>
          </Stack>
        </Margin>
      </Stack>
    </Screen>
  )
}
