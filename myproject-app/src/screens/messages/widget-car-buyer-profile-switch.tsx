import { useIsFocused } from '@react-navigation/native'
import _ from 'lodash'
import { NavigableRoute, Navigation } from 'myproject-app/src'
import { translate } from 'myproject-shared/utilities'
import { ActivityIndicator, Button, Separator, Spacer, SpacerFlex, Stack, Text, Toggle } from 'myproject-ucl/controls'
import React, { useEffect, useLayoutEffect, useState } from 'react'
import { CarBuyerProfileController, SharingCBPState } from '../car-buyer-profile/car-buyer-controller'
import { useGetAutoBuyerProfile } from '../car-buyer-profile/hooks/use-get-autos-buyer-profile'

export const FirstMessageCarBuyerProfileSwitch: React.FC<{ inputFocused: boolean; listingId: string }> = ({ inputFocused, listingId }) => {
  const { loading, profile, refetch } = useGetAutoBuyerProfile()

  const focused = useIsFocused()

  useLayoutEffect(() => {
    if (focused) {
      refetch && refetch()
    }
  }, [focused])

  const [toggleChecked, setToggleChecked] = useState(true)

  // initial state
  useEffect(() => {
    // has user seen the modal in the past 2 weeks?
    CarBuyerProfileController.getSeenMoreThanTwoWeeksAgo().then(seenMoreThanTwoWeeksAgo => {
      if (seenMoreThanTwoWeeksAgo) {
        CarBuyerProfileController.getSharingEnabledStorage().setItem(SharingCBPState.ENABLED)
        setToggleChecked(true)
      }
      restoreSavedState()
    })
  }, [])

  function restoreSavedState() {
    CarBuyerProfileController.getSharingEnabledStorage()
      .getItem()
      .then(checked => {
        setToggleChecked(checked === SharingCBPState.ENABLED)
      })
  }

  const onToggleChange = (state: boolean) => {
    setToggleChecked(state)
    CarBuyerProfileController.getSharingEnabledStorage().setItem(state ? SharingCBPState.ENABLED : SharingCBPState.DISABLED)
  }

  const openEditCarBuyerProfile = () => {
    Navigation.navigateToRoute(NavigableRoute.CarBuyerProfile, { listingId: _.toString(listingId), sendToDealer: false })
  }
  if (loading) {
    return <ActivityIndicator />
  }

  return (
    <>
      <Stack direction='column' childSeparationStep={2}>
        <Separator />
        <Stack direction='row' crossAxisDistribution='center'>
          <Text>{translate('discussion-screen.autos-dealer-i-want-a-faster-response')}</Text>
          <SpacerFlex />
          <Toggle onChange={onToggleChange} state={toggleChecked} />
        </Stack>
        <Separator />

        {toggleChecked && !inputFocused && (
          <>
            <Stack direction='row' crossAxisDistribution='center'>
              <Text textType='primaryBody2'>{translate('discussion-screen.autos-dealer-include-my-info')}</Text>
              <SpacerFlex />
              <Button buttonSize='small' buttonType='flat' title={translate('common-actions.edit')} onClick={openEditCarBuyerProfile} />
            </Stack>
            <Stack direction='column'>
              <Text textType='primaryBody2' color={profile?.phoneNumber ? 'primary' : 'secondary'}>
                • {profile?.phoneNumber || translate('discussion-screen.phone-number')}
              </Text>

              <Text textType='primaryBody2' color={profile?.email ? 'primary' : 'secondary'}>
                • {profile?.email || translate('account-stack.settings-screen.email')}
              </Text>

              <Text textType='primaryBody2' color={profile?.name ? 'primary' : 'secondary'}>
                • {profile?.name || translate('car-buyer-profile.name')}
              </Text>
            </Stack>
            <Spacer direction='column' sizeStep={2} />
            <Separator />
          </>
        )}
      </Stack>
    </>
  )
}
