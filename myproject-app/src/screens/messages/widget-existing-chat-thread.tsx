import Instabug from "instabug-reactnative"
import _ from "lodash"
import {
  AccountDataContext,
  AnalyticsActionType,
  AnalyticsElementType,
  ChatScreenElement,
  ChatUserRole,
  CHAT_MESSAGES,
  Discussion,
  GraphGQLErrorParser,
  Message,
  Exception,
  Query,
  RealtimeConnectionContext,
  SEND_MESSAGE,
  SHIPPING_TRANSACTIONS,
  SystemMessageAction,
  translate,
  UPDATE_MESSAGE_READ_DATE,
  Urls,
  ChatReadIndicatorStatus,
  recieverReadDate,
  isSystemMessage,
  LISTING_DETAIL,
  PUBLIC_PROFILE_QUERY,
} from "myproject-shared"
import { useApolloClient, useLazyQuery, useMutation } from "@apollo/react-hooks"
import { ChatAnalyticsController } from "myproject-shared/analytics/myproject-analytics-chat"
import { AnalyticsDebug } from "myproject-shared/analytics/myproject-analytics-debug.native"
import { InboxDataContext } from "myproject-shared/providers/chat"
import { isOfferAccepted } from "myproject-shared/utilities/transaction"
import {
  Center,
  ChatInput,
  ChatPhotoSelect,
  ChatReceipt,
  CommonChatMessage,
  EmptyState,
  ErrorBearFailure,
  Flex,
  ListRef,
  LocationPinOutline,
  Margin,
  POST_FIRST_MESSAGE_TYPE,
  isValidUserId,
  MessageReadStatusInfo,
} from "myproject-ucl"
import { ChatInputActionButton } from "myproject-ucl/widgets/myproject-chat/myproject-chat-input-action-button"
import React, { FC, useContext, useEffect, useLayoutEffect, useRef, useState, useCallback } from "react"
import { Keyboard, KeyboardEvent, Platform } from "react-native"
import { useIsFocused } from "@react-navigation/native"
import { KeyboardAvoidancePaddingContainer } from "../../keyboardavoidance"
import { CommonNavs, Navigation } from "../../navigation/navigation"
import { NavigableRoute } from "../../navigation/navigator"
import { OpenURL } from "../../utilities"
import { DiscussionHeader } from "./header/widget-discussion-header"
import { DiscussionFullScreenLoadingView } from "./widget-discussion-loading-view"
import { StockPTRList } from "../../widgets"

export const ExistingChatThread: FC<{ discussionId: string }> = ({ discussionId }) => {
  const [updateReadDate] = useMutation(UPDATE_MESSAGE_READ_DATE)
  const { data: me } = useContext(AccountDataContext)
  const { draftMessage, setDraftMessage } = useContext(InboxDataContext)
  const [messages, setMessages] = useState<Message[]>([])
  const [loading, setLoading] = useState<boolean>(true)
  const [scrollToBottom, setScrollToBottom] = useState<boolean>(true)
  const [error, setError] = useState<Exception | undefined>()
  const [discussion, setDiscussion] = useState<Discussion | undefined | null>(null)
  const listRef = useRef<ListRef>(null)
  const apolloClient = useApolloClient()
  const isFocused = useIsFocused()

  const [
    fetchListingDetail,
    { data: listingResponse, loading: listingResponseLoading, called: listingDetailCalled },
  ] = useLazyQuery<Query>(LISTING_DETAIL, {
    variables: {
      listingId: _.toInteger(discussion?.itemId),
    },
    fetchPolicy: "network-only",
  })

  const myId = me?.id || 0
  const isMySale = myId === discussion?.sellerId
  const otherUserId = !isMySale ? discussion?.sellerId : discussion?.buyerId
  const [
    fetchProfile,
    { data: profileResponse, loading: profileResponseLoading, called: publicProfileQueryCalled },
  ] = useLazyQuery<Query>(PUBLIC_PROFILE_QUERY, {
    variables: {
      userId: _.toInteger(otherUserId),
    },
  })

  const fetchDataAfterDiscussion = () => {
    if (!listingResponse) {
      fetchListingDetail()
    }

    if (isValidUserId(otherUserId)) {
      fetchProfile()
    }

    if (!shippingDataResponse) {
      fetchShippingData()
    }
  }

  const fetchLatestMessages = async (): Promise<void> => {
    return new Promise<void>((resolve, reject) => {
      fetch()
        .then(() => resolve())
        .catch(() => reject())
    })
  }
  const fetchOlderMessages = async (): Promise<void> => {
    return new Promise<void>((resolve, reject) => {
      const oldestPost = _.minBy(messages, "sendDate")
      fetch(`${oldestPost?.sendDate}`)
        .then(() => resolve())
        .catch(() => reject())
    })
  }
  const lastPostDate = discussion?.lastPostDate

  useEffect(() => {
    if (!discussionId || !lastPostDate) {
      return
    }
    updateReadDate({
      variables: {
        userId: myId,
        discussionId,
        lastPostDate,
      },
    }).catch(error => {
      AnalyticsDebug.logError(error)
      const ouError = GraphGQLErrorParser(error)
      Instabug.logError(`Error marking as read: ${ouError.message || error}`)
    })
  }, [discussionId, lastPostDate])

  const mergeMessages = (data?: Message[]) => {
    setMessages((old: Message[]) => {
      const list = old.concat(data || [])
      return _.chain(list).uniqBy("id").orderBy("sendDate").value()
    })
  }
  const [sendMessage] = useMutation(SEND_MESSAGE)
  const fetch = async (before?: string) => {
    setError(undefined)
    try {
      const { data } = await apolloClient.query<Query>({
        query: CHAT_MESSAGES,
        fetchPolicy: "network-only",
        variables: {
          discussionId,
          before,
        },
      })
      // console.log('what are the messages', data?.discussion?.messages)
      setDiscussion(data?.discussion)
      mergeMessages(data?.discussion?.messages)
      fetchDataAfterDiscussion()
      if (before) {
        setScrollToBottom(false)
      }
    } catch (error) {
      AnalyticsDebug.logError(error)

      const ouError = GraphGQLErrorParser(error)
      Instabug.logError(`Error loading chat: ${ouError.message || error}`)

      setError(ouError)
    }
  }
  const fetchLatest = async () => {
    await fetch()
    setLoading(false)
  }

  /**
   * Get the last read date of the message you last sent to the other user
   */
  const { lastReadDate: lastReceiverReadDate } = recieverReadDate(discussion?.readStatus || [], _.toString(myId)) || {}

  /**
   * Helper function to determine if a message has been seen or not by the receipient
   */
  const lastSeen = (message: Message): boolean => {
    return (lastReceiverReadDate && message?.sendDate && message?.sendDate <= _.toNumber(lastReceiverReadDate)) || false
  }
  /**
   * Helper function to determine if a message was sent by the current user
   */
  const mine = (message: Message): boolean => {
    return message.senderId === myId
  }

  const [readIndicator, setReadIndicator] = useState<ChatReadIndicatorStatus>({})
  /**
   * Update read status indicators when we get new messages
   */
  useEffect(() => {
    const idOfLastMessageSeen =
      _.chain(messages)
        .orderBy("sendDate")
        .findLast(message => lastSeen(message) && mine(message) && !isSystemMessage(message))
        .value()?.id || undefined

    const idOfLastMessageSent =
      _.chain(messages)
        .orderBy("sendDate")
        .findLast(message => mine(message) && !isSystemMessage(message))
        .value()?.id || undefined

    setReadIndicator({ idOfLastMessageSeen, idOfLastMessageSent, lastReceiverReadDate: _.toNumber(lastReceiverReadDate) })
  }, [messages, lastReceiverReadDate])

  useLayoutEffect(() => {
    if (scrollToBottom) {
      listRef.current?.scrollToEnd(true)
    }
  }, [scrollToBottom, listRef.current])

  useEffect(() => {
    isFocused && !loading && fetchLatestMessages()
  }, [isFocused, loading])

  const { subscribe } = useContext(RealtimeConnectionContext)
  useEffect(() => {
    fetchLatest()
    const subscription = subscribe(async () => {
      await fetchLatest()
      setScrollToBottom(true)
    })
    return () => subscription.unsubscribe()
  }, [discussionId, otherUserId])
  const onSendMessage = async (text: string) => {
    ChatAnalyticsController.trackChatScreenUIEvent(
      ChatScreenElement.SendMessage,
      AnalyticsElementType.Button,
      AnalyticsActionType.Click,
      getUserAnalyticsRole(),
    )
    try {
      await sendMessage({
        variables: {
          text,
          discussionId,
        },
      })
      await fetchLatest()
      return new Promise<void>((resolve, _reject) => resolve())
    } catch (error) {
      return new Promise<void>((_resolve, reject) => reject())
    }
  }

  const getUserAnalyticsRole = (): ChatUserRole => {
    return discussion?.sellerId === me?.id ? ChatUserRole.Seller : ChatUserRole.Buyer
  }

  const [fetchShippingData, { data: shippingDataResponse }] = useLazyQuery<Query>(SHIPPING_TRANSACTIONS, {
    fetchPolicy: "network-only",
    variables: {
      itemId: discussion?.itemId,
      buyerId: discussion?.buyerId,
    },
  })
  const renderRecieptAction = useCallback(() => {
    if (!isOfferAccepted(shippingDataResponse?.getShippingTransaction?.transactionState)) {
      return null
    }

    const goToRecieptScreen = () => {
      ChatAnalyticsController.trackChatScreenUIEvent(
        ChatScreenElement.Receipt,
        AnalyticsElementType.Button,
        AnalyticsActionType.Click,
        getUserAnalyticsRole(),
      )
      Navigation.navigateToActionPath(`${Urls.receiptByItemId(_.toString(discussion?.itemId))}?flavour=app`)
    }

    return <ChatInputActionButton key={"show-reciept"} icon={ChatReceipt} onPress={goToRecieptScreen} />
  }, [shippingDataResponse?.getShippingTransaction?.transactionState, discussion?.itemId])

  const openMeetup = useCallback(() => {
    ChatAnalyticsController.trackChatScreenUIEvent(
      ChatScreenElement.SuggestMeetup,
      AnalyticsElementType.Button,
      AnalyticsActionType.Click,
      getUserAnalyticsRole(),
    )
    const payload = {
      discussionId,
      refetch: fetchLatest,
    }
    Navigation.navigateToRoute(NavigableRoute.Meetup, payload)
  }, [discussionId])

  const canSendPhotos = me?.profile?.isAutosDealer || shippingDataResponse?.getShippingTransaction?.canSendPhotos || false
  const renderImageSelectAction = useCallback(() => {
    if (!canSendPhotos) {
      return null
    }

    const goToImagePicker = () => {
      ChatAnalyticsController.trackChatScreenUIEvent(
        ChatScreenElement.SendPic,
        AnalyticsElementType.Button,
        AnalyticsActionType.Click,
        getUserAnalyticsRole(),
      )
      Navigation.navigateToRoute(NavigableRoute.DiscussionImagePicker, { discussionId, refetch: fetchLatest })
    }

    return <ChatInputActionButton key={"send-image"} icon={ChatPhotoSelect} onPress={goToImagePicker} />
  }, [canSendPhotos, discussionId])

  const onActionSelect = useCallback((action: SystemMessageAction, id) => {
    const { actionPath, externalURL } = action
    if (actionPath) {
      handleActionPath(actionPath, id)
    } else if (externalURL) {
      CommonNavs.openExternalUrl(externalURL)
    }
  }, [])

  const onHelpPressed = useCallback((helpURL: string) => {
    if (helpURL) {
      CommonNavs.presentWebView(helpURL)
    }
  }, [])

  const handleActionPath = useCallback((actionPath: string, id: string) => {
    Navigation.performWhenAvailable(() => {
      Navigation.navigateToActionPath(actionPath, { id })
    })
  }, [])

  const onMessageClick = useCallback((message: Message) => {
    const { metadataType } = message
    if (metadataType === POST_FIRST_MESSAGE_TYPE.MEETUP || metadataType === POST_FIRST_MESSAGE_TYPE.MEETUP_SPOT) {
      const { latitude, longitude, name } = message?.metadata?.place || {}
      OpenURL.openLocationInMaps(_.toNumber(latitude), _.toNumber(longitude), `${name}`)
    }
  }, [])
  const renderMessageRow = (message: any, index: number) => {
    let messageReadStatus: MessageReadStatusInfo
    if (readIndicator.idOfLastMessageSeen === message.id) {
      messageReadStatus = { readStatusType: "seen", seenDate: _.toNumber(readIndicator.lastReceiverReadDate) }
    } else if (readIndicator.idOfLastMessageSent === message.id) {
      messageReadStatus = { readStatusType: "sent" }
    }
    // console.log('what is the message', message)
    return (
      <CommonChatMessage
        myId={myId}
        {...message}
        readStatus={messageReadStatus}
        onActionSelect={onActionSelect}
        onHelpPressed={onHelpPressed}
        onClick={onMessageClick}
        testID={"discussion-screen.chat.message." + index}
      />
    )
  }

  const keyExtractor = (message: any, _index: number): string => {
    return _.property("id")(message) as string
  }

  const onContentSizeChange = useCallback((w: number, h: number) => {
    requestAnimationFrame(() => {
      listRef.current?.scrollToEnd(true)
    })
  }, [])

  const onKeyboardEvent = (event: KeyboardEvent) => {
    setTimeout(
      () => {
        requestAnimationFrame(() => {
          listRef.current?.scrollToEnd()
        })
      },
      Platform.OS === "ios" ? event.duration : 5,
    )
  }

  useEffect(() => {
    const showListener = Platform.OS === "android" ? "keyboardDidShow" : "keyboardWillShow"
    Keyboard.addListener(showListener, onKeyboardEvent)
    return () => {
      Keyboard.removeListener(showListener, onKeyboardEvent)
    }
  }, [])

  const BoundingContainer = Platform.OS === "android" ? Flex : KeyboardAvoidancePaddingContainer

  if (loading || listingResponseLoading || !listingDetailCalled || profileResponseLoading || !publicProfileQueryCalled) {
    return <DiscussionFullScreenLoadingView />
  }
  return (
    <>
      <DiscussionHeader
        profile={profileResponse?.publicProfile}
        listing={listingResponse?.listing}
        itemId={discussion?.itemId}
        discussionId={discussionId}
        buyerId={discussion?.buyerId?.toString()}
        sellerId={discussion?.sellerId?.toString()}
        chatError={error}
        visualTags={discussion?.visualTags}
      />
      {error ? (
        <Center>
          <EmptyState
            icon={ErrorBearFailure}
            title={error.title}
            subtitle={error.message}
            buttonTitle={translate("common-errors.server-error.button-title")}
            buttonHandler={fetchLatest}
            testID="existing-chat-thread.empty-state"
          />
        </Center>
      ) : (
        <>
          <BoundingContainer grow={1} direction="column">
            <Flex direction="column" grow={1}>
              <Margin direction="column" grow={1} marginLeftStep={3} marginRightStep={3} marginBottomStep={3}>
                <StockPTRList
                  listRef={listRef}
                  keyExtractor={keyExtractor}
                  horizontal={false}
                  data={messages}
                  onContentSizeChange={onContentSizeChange}
                  renderItem={renderMessageRow}
                  inverted={false}
                  dismissKeyboardOnDrag={false}
                  leadingRefreshHandler={fetchOlderMessages}
                  trailingRefreshHandler={fetchLatestMessages}
                  testID="discussion-screen.chat-container"
                />
              </Margin>

              <Flex direction="column" grow={0} shrink={0}>
                <ChatInput
                  actions={[
                    <ChatInputActionButton key={"meetup-spot"} icon={LocationPinOutline} onPress={openMeetup} />,
                    renderRecieptAction(),
                    renderImageSelectAction(),
                  ]}
                  onSend={onSendMessage}
                  value={draftMessage[discussionId]}
                  onChange={setDraftMessage(discussionId)}
                  onMeetup={openMeetup}
                />
              </Flex>
            </Flex>
          </BoundingContainer>
        </>
      )}
    </>
  )
}
