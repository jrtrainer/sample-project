import _ from 'lodash'
import { KeyboardAvoidanceOverlayContainer, KeyboardAvoidanceRollawayContainer, KeyboardAvoidanceView } from 'myproject-app/src/keyboardavoidance'
import {
  LISTING_DETAIL,
  Mutation,
  PostFirstMessageInput,
  POST_FIRST_MESSAGE,
  Query,
  SuggestedMessage,
  translate,
  PUBLIC_PROFILE_QUERY
} from 'myproject-shared'
import { useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks'
import { Button, Flex, Input, Margin, ScrollView, Stack, Text, TextEntryRef } from 'myproject-ucl/controls'
import React, { FC, useLayoutEffect, useRef, useState, useEffect } from 'react'
import { CommonNavs } from '../../navigation/navigation'
import { DiscussionHeader } from './header/widget-discussion-header'
import { BuyerEngagementType } from 'myproject-shared/analytics/constants/item-detail-constants'
import { DiscussionFullScreenLoadingView } from './widget-discussion-loading-view'
import { useDiscussionProps } from './discussion-props-provider'
import { isValidUserId } from 'myproject-ucl'

const CUSTOM_INPUT_LINES = 1
const CUSTOM_INPUT_LINES_WHEN_FOCUSED = 3

const DEFAULT_MARGIN_STEP = 4

export const SendFirstMessage: FC<{
  listingId: string
  sendAskEvent(type: BuyerEngagementType): void
  suggestedMessages?: SuggestedMessage[] | null | undefined
  testID?: string
}> = ({ listingId, sendAskEvent, suggestedMessages, testID }) => {
  const { replaceDiscussionProps } = useDiscussionProps()

  const [postFirstMessage] = useMutation<Mutation, PostFirstMessageInput>(POST_FIRST_MESSAGE)

  const [userInput, setUserInput] = useState('')
  const [loading, setLoading] = useState(false)
  const [inputFocused, setInputFocused] = useState(false)

  const textRef = useRef<TextEntryRef>(null)

  const { data: listingResponse, loading: listingLoading } = useQuery<Query>(LISTING_DETAIL, {
    variables: {
      listingId: _.toInteger(listingId)
    }
  })

  useEffect(() => {
    const otherUserId = listingResponse?.listing?.owner?.id
    if (!listingResponse?.listing || !isValidUserId(otherUserId)) {
      return
    }
    queryPublicProfile({
      variables: {
        userId: _.toInteger(otherUserId)
      }
    })
  }, [listingResponse?.listing])

  const [queryPublicProfile, { data: profileResponse, loading: profileResponseLoading, called: profileResponseCalled }] = useLazyQuery<Query>(
    PUBLIC_PROFILE_QUERY
  )

  const sendMessage = async (text: string, type: BuyerEngagementType, suggestedMessageUuid?: string) => {
    setLoading(true)
    sendAskEvent(type)
    const data = await postFirstMessage({ variables: { itemId: _.toString(listingId), text, suggestedMessageUuid } })
    const discussionId = data.data?.postFirstMessage.discussionId

    replaceDiscussionProps({ itemId: _.toString(listingId), discussionId: _.toString(discussionId) })
  }

  const handleFocus = () => {
    setInputFocused(true)
  }

  useLayoutEffect(() => {
    if (inputFocused) {
      textRef.current?.focus()
    }
  }, [inputFocused])

  const handleTextChanged = (text?: string) => {
    setUserInput(text || '')
  }

  const sendCustomMessage = () => {
    if (userInput) {
      sendMessage(userInput, BuyerEngagementType.AskFreeform)
    } else {
      CommonNavs.presentError({
        ouException: {
          title: translate('discussion-screen.first-message-empty-error-title'),
          message: translate('discussion-screen.first-message-empty-error-message')
        }
      })
    }
  }

  const handleEndEditing = () => {
    setInputFocused(false)
    textRef.current?.blur()
  }

  const showSendButton: boolean = inputFocused || userInput.length > 0
  const buttonVisuallyDisabled = _.isEmpty(userInput) || loading
  const buttonDisabled = loading
  const localTestID = (testID || 'discussion') + '.first-message'
  const clickAction = (item: SuggestedMessage) => () => sendMessage(item?.text!, BuyerEngagementType.AskSuggested, item.id!)

  const SuggestedMessageRow: FC<{ item: SuggestedMessage; index: number }> = ({ item, index }) => {
    return (
      <Flex grow={0} direction='row'>
        <Button
          buttonSize='small'
          title={item?.text!}
          buttonType={loading ? 'disabled' : 'secondary'}
          onClick={clickAction(item)}
          disabled={loading}
          testID={localTestID + '.suggested.' + index}
        />
      </Flex>
    )
  }
  if (listingLoading || profileResponseLoading || !profileResponseCalled) {
    return <DiscussionFullScreenLoadingView />
  }
  return (
    <Flex grow={1} direction='column'>
      <DiscussionHeader
        itemId={_.toNumber(listingId)}
        discussionId={undefined}
        sellerId={_.toString(listingResponse?.listing?.owner?.id)}
        listing={listingResponse?.listing}
        profile={profileResponse?.publicProfile}
      />
      {suggestedMessages && (
        <>
          <ScrollView onlyScrollsWhenNeeded={true}>
            <KeyboardAvoidanceRollawayContainer direction='column' grow={1}>
              <Margin marginStep={DEFAULT_MARGIN_STEP} grow={1} direction='column'>
                <Stack direction='column' childSeparationStep={DEFAULT_MARGIN_STEP}>
                  {!inputFocused && <Text textType='primaryBody1'>{translate('discussion-screen.first-message-instruction')}</Text>}
                  {!inputFocused && suggestedMessages.map((item, idx) => <SuggestedMessageRow item={item} key={idx} index={idx} />)}
                  <KeyboardAvoidanceView stackOrder={1} direction='column'>
                    <Flex grow={1} direction='row'>
                      <Input
                        autoCapitalize='sentences'
                        ref={textRef}
                        title={translate('discussion-screen.first-message-new-message')}
                        textType='primaryBody2'
                        text={userInput}
                        linesMinimum={inputFocused ? CUSTOM_INPUT_LINES_WHEN_FOCUSED : CUSTOM_INPUT_LINES}
                        linesMaximum={inputFocused ? CUSTOM_INPUT_LINES_WHEN_FOCUSED : CUSTOM_INPUT_LINES}
                        focusHandler={handleFocus}
                        textChangeHandler={handleTextChanged}
                        endEditingHandler={handleEndEditing}
                        focusState={inputFocused ? 'focused' : 'unfocused'}
                        testID={localTestID + '.message'}
                      />
                    </Flex>
                  </KeyboardAvoidanceView>
                </Stack>
              </Margin>
            </KeyboardAvoidanceRollawayContainer>
          </ScrollView>

          <KeyboardAvoidanceOverlayContainer absoluteBottom={0} direction='row'>
            <KeyboardAvoidanceView stackOrder={0} activeInGroups={[]} direction='column' grow={1}>
              <Margin marginStep={DEFAULT_MARGIN_STEP} direction='column' grow={1}>
                {showSendButton && (
                  <Button
                    title={translate('discussion-screen.first-message-send')}
                    buttonSize='large'
                    buttonType={buttonVisuallyDisabled ? 'disabled' : 'primary'}
                    onClick={sendCustomMessage}
                    disabled={buttonDisabled}
                    testID={localTestID + '.send'}
                  />
                )}
              </Margin>
            </KeyboardAvoidanceView>
          </KeyboardAvoidanceOverlayContainer>
        </>
      )}
    </Flex>
  )
}
