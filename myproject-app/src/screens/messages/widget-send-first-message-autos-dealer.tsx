import React, { FC, useEffect, useLayoutEffect, useRef, useState } from "react"
import _ from "lodash"
import {
  KeyboardAvoidanceOverlayContainer,
  KeyboardAvoidanceRollawayContainer,
  KeyboardAvoidanceView,
} from "../../keyboardavoidance"
import { Navigation, CommonNavs } from "../../navigation/navigation"
import { NavigableRoute } from "../../navigation/navigator"
import {
  LISTING_DETAIL,
  Mutation,
  PostFirstMessageInput,
  POST_FIRST_MESSAGE,
  Query,
  PUBLIC_PROFILE_QUERY,
  ItemIdInput,
} from "myproject-shared/gql-tags"
import { useLazyQuery, useMutation, useQuery } from "@apollo/react-hooks"
import { translate, useCallAutosDealer } from "myproject-shared/utilities"
import { PhoneIcon } from "myproject-ucl/assets"
import { isValidUserId } from "myproject-ucl"
import { Button, Flex, Input, Margin, ScrollView, Stack, TextEntryRef } from "myproject-ucl/controls"
import { OpenURL } from "../../utilities"
import { NUDGE_CAR_BUYER_PROFILE, SEND_EXISTING_CAR_BUYER_PROFILE } from "../car-buyer-profile/car-buyer-profile-queries"
import { useGetAutoBuyerProfile } from "../car-buyer-profile/hooks/use-get-autos-buyer-profile"
import { DiscussionHeader } from "./header/widget-discussion-header"
import { FirstMessageCarBuyerProfileSwitch } from "./widget-car-buyer-profile-switch"
import { DiscussionFullScreenLoadingView } from "./widget-discussion-loading-view"
import { useDiscussionProps } from "./discussion-props-provider"
import { useHasUserSeenCarBuyerProfile } from "../car-buyer-profile/hooks/use-has-user-seen-cbp"
import { CarBuyerProfileController, SharingCBPState } from "../car-buyer-profile/car-buyer-controller"

const DEFAULT_MARGIN_STEP = 4

export const SendFirstMessageAutosDealer: FC<{ itemId: number }> = ({ itemId }) => {
  const { replaceDiscussionProps } = useDiscussionProps()

  const [postFirstMessage] = useMutation<Mutation, PostFirstMessageInput>(POST_FIRST_MESSAGE)

  const { data: listingResponse, loading: listingLoading } = useQuery<Query>(LISTING_DETAIL, {
    variables: {
      listingId: _.toInteger(itemId),
    },
  })

  useEffect(() => {
    const otherUserId = listingResponse?.listing?.owner?.id
    if (isValidUserId(otherUserId)) {
      queryPublicProfile({
        variables: {
          userId: _.toInteger(otherUserId),
        },
      })
    }
  }, [listingResponse?.listing])

  const [
    queryPublicProfile,
    { data: profileResponse, loading: profileResponseLoading, called: profileResponseCalled },
  ] = useLazyQuery<Query>(PUBLIC_PROFILE_QUERY)

  const [userInput, setUserInput] = useState("")
  const [loading, setLoading] = useState(false)
  const [inputFocused, setInputFocused] = useState(false)

  const textRef = useRef<TextEntryRef | null>(null)

  const { loading: cbpLoading, profile, profileSufficientlyFilledOut } = useGetAutoBuyerProfile()

  const { seen: hasUserSeenCarBuyerProfile, userSeenMoreThanTwoWeeksAgo } = useHasUserSeenCarBuyerProfile()

  const [sendExistingCarBuyerProfile] = useMutation<Mutation, { itemId: ItemIdInput }>(SEND_EXISTING_CAR_BUYER_PROFILE)

  const [sendCarBuyerProfileNudge] = useMutation<Mutation, { itemId: ItemIdInput }>(NUDGE_CAR_BUYER_PROFILE)

  const sendMessage = async (text: string, suggestedMessageUuid?: string) => {
    setLoading(true)
    const data = await postFirstMessage({ variables: { itemId: _.toString(itemId), text, suggestedMessageUuid } })
    const discussionId = data.data?.postFirstMessage.discussionId
    replaceDiscussionProps({
      itemId: _.toString(itemId),
      discussionId: _.toString(discussionId),
    })

    // we have posted the message, we now determine whether to show the car buyer modal
    const showModal = (!!!profile && !hasUserSeenCarBuyerProfile) || userSeenMoreThanTwoWeeksAgo

    const userShareInformation: boolean =
      (await CarBuyerProfileController.getSharingEnabledStorage().getItem()) === SharingCBPState.ENABLED
    const sendCarBuyerProfile: boolean = userShareInformation && !!profile && profileSufficientlyFilledOut

    if (showModal) {
      Navigation.performWhenAvailable(() => {
        Navigation.navigateToRoute(NavigableRoute.CarBuyerProfile, { listingId: _.toString(itemId), sendToDealer: true })
      })
    } else if (sendCarBuyerProfile) {
      // share existing profile with dealer
      sendExistingCarBuyerProfile({
        variables: {
          itemId: {
            itemId: _.toString(itemId),
          },
        },
      })
    } else {
      // send nudge
      sendCarBuyerProfileNudge({
        variables: {
          itemId: {
            itemId: _.toString(itemId),
          },
        },
      })
    }
  }

  const handleFocus = () => {
    setInputFocused(true)
  }

  useLayoutEffect(() => {
    if (inputFocused) {
      textRef.current?.focus()
    }
  }, [inputFocused])

  const handleTextChanged = (text?: string) => {
    setUserInput(text || "")
  }

  const sendCustomMessage = () => {
    if (userInput) {
      sendMessage(userInput)
    } else {
      CommonNavs.presentError({
        ouException: {
          title: translate("discussion-screen.first-message-empty-error-title"),
          message: translate("discussion-screen.first-message-empty-error-message"),
        },
      })
    }
  }

  const handleEndEditing = () => {
    setInputFocused(false)
    textRef.current?.blur()
  }

  const { getProxiedNumber } = useCallAutosDealer(_.toString(itemId))

  const callDealer = () => {
    const sellerId = listingResponse?.listing?.owner?.id

    if (sellerId) {
      getProxiedNumber(sellerId).then(c2cNumber => {
        if (c2cNumber) {
          OpenURL.openDialerForNumer(c2cNumber)
        }
      })
    }
  }

  const showSendButton: boolean = inputFocused || userInput.length > 0

  const showCarBuyerProfileSwitch = profile

  const buttonVisuallyDisabled = _.isEmpty(userInput) || loading
  const buttonDisabled = loading
  if (listingLoading || profileResponseLoading || !profileResponseCalled || cbpLoading) {
    return <DiscussionFullScreenLoadingView />
  }
  return (
    <Flex grow={1} direction="column">
      <DiscussionHeader
        itemId={itemId}
        discussionId={undefined}
        sellerId={_.toString(listingResponse?.listing?.owner?.id)}
        listing={listingResponse?.listing}
        profile={profileResponse?.publicProfile}
      />
      <>
        <ScrollView onlyScrollsWhenNeeded={true}>
          <KeyboardAvoidanceRollawayContainer direction="column" grow={1}>
            <Margin marginStep={DEFAULT_MARGIN_STEP} grow={0} direction="column">
              <Stack direction="column" childSeparationStep={DEFAULT_MARGIN_STEP}>
                <KeyboardAvoidanceView stackOrder={1} direction="column">
                  <Flex grow={1} direction="row">
                    <Input
                      ref={textRef}
                      title={translate("discussion-screen.first-message-new-message")}
                      autoCapitalize="sentences"
                      textType="primaryBody2"
                      hint={translate("discussion-screen.first-message-help-text")}
                      text={userInput}
                      linesMinimum={4}
                      focusHandler={handleFocus}
                      textChangeHandler={handleTextChanged}
                      endEditingHandler={handleEndEditing}
                      focusState={inputFocused ? "focused" : "unfocused"}
                    />
                  </Flex>
                </KeyboardAvoidanceView>
                {showCarBuyerProfileSwitch && (
                  <FirstMessageCarBuyerProfileSwitch inputFocused={inputFocused} listingId={_.toString(itemId)} />
                )}
                <Flex direction="row">
                  <Button
                    onClick={callDealer}
                    buttonSize="large"
                    buttonType="flat"
                    doNotApplySidePadding={true}
                    title={translate("discussion-screen.call-autos-dealer-button")}
                    icon={PhoneIcon}
                  />
                </Flex>
              </Stack>
            </Margin>
          </KeyboardAvoidanceRollawayContainer>
        </ScrollView>

        <KeyboardAvoidanceOverlayContainer absoluteBottom={0} direction="row">
          <KeyboardAvoidanceView stackOrder={0} activeInGroups={[]} direction="column" grow={1}>
            <Margin marginStep={DEFAULT_MARGIN_STEP} direction="column" grow={1}>
              {showSendButton && (
                <Button
                  title={translate("discussion-screen.first-message-send")}
                  buttonSize="large"
                  buttonType={buttonVisuallyDisabled ? "disabled" : "primary"}
                  onClick={sendCustomMessage}
                  disabled={buttonDisabled}
                />
              )}
            </Margin>
          </KeyboardAvoidanceView>
        </KeyboardAvoidanceOverlayContainer>
      </>
    </Flex>
  )
}
