import React from 'react'
import { DiscussionScreenProps } from './discussion-screen-props'

/**
 * A context to share the props handed to the DiscussionStack navigator to its child screens
 */
interface DiscussionContextProps {
  /**
   * The props passed into the DiscussionStack navigator
   */
  discussionProps: DiscussionScreenProps
  /**
   * A setter to update the current discussion props.
   */
  replaceDiscussionProps: (newProps: DiscussionScreenProps) => void
}

const DiscussionPropsContext = React.createContext<DiscussionContextProps>({
  discussionProps: {},
  replaceDiscussionProps: _ => {}
})

/**
 * Exposes the context object that drives the DiscussionStack navigator's child screens.
 */
export const useDiscussionProps = () => {
  return React.useContext(DiscussionPropsContext)
}

export const DiscussionPropsProvider: React.FC<DiscussionScreenProps> = props => {
  const { children, ...rest } = props
  const [discussionProps, setDiscussionProps] = React.useState<DiscussionScreenProps>(rest)
  const contextValue: DiscussionContextProps = {
    discussionProps,
    replaceDiscussionProps: setDiscussionProps
  }
  return <DiscussionPropsContext.Provider value={contextValue}>{children}</DiscussionPropsContext.Provider>
}
