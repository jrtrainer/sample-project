import { translate, ChatScreenElement, AnalyticsElementType, AnalyticsActionType, ChatScreenName } from "myproject-shared"
import { InboxDataContext } from "myproject-shared/providers/chat"
import { Button, Margin, Stack, Text, ErrorIcon } from "myproject-ucl"
import { ListRef } from "myproject-ucl/controls/myproject-list"
import React, { FC, useContext, useRef, useState } from "react"
import { DiscussionStackNavigatorParamList } from "../../navigation/myproject-discussion/myproject-discussion-navigator"
import { ScreenRoute } from "../../navigation/myproject-route"
import { Navigation } from "../../navigation/navigation"
import { NavigableRoute } from "../../navigation/navigator"
import { ImagePicker, ImagePickerGridItemPress, Screen, deletePhotosAt } from "../../widgets"
import { PHOTO_GRID_PAGE_SIZE } from "../post-flow/commons"
import { ImagePickerSelectedPhotos } from "../post-flow/image-picker/selected-image-list"
import _ from "lodash"
import { usePhotoSelection } from "../post-flow/photo-selection-provider"
import { ChatAnalyticsController } from "myproject-shared/analytics/myproject-analytics-chat"
import { AffirmRejectDialogScreenProps } from "../dialog"
import { CAMERA_CHAT_IMAGE_SAVE_FOLDER } from "./discussion-image-common"

export const DiscussionImagePickerScreen: FC<ScreenRoute<
  DiscussionStackNavigatorParamList,
  NavigableRoute.DiscussionImagePicker
>> = ({ route }) => {
  const { discussionId, refetch } = route.params.props
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState<string | undefined>()
  const { draftSelectedPhotos, setDraftSelectedPhotos, savePhotosToLocalSandBox } = usePhotoSelection()
  const { sendImageMessage } = useContext(InboxDataContext)
  const selectdPhotosListRef = useRef<ListRef>(null)
  const onCancel = () => {
    ChatAnalyticsController.trackChatCameraRollUIEvent(
      ChatScreenElement.Cancel,
      AnalyticsElementType.Button,
      AnalyticsActionType.Click,
    )
    setDraftSelectedPhotos([])
    Navigation.popToTop()
  }

  const onImagePickerItemPressed: ImagePickerGridItemPress = photo => {
    const found = _.find(draftSelectedPhotos, photo)
    if (found) {
      setDraftSelectedPhotos(_.without(draftSelectedPhotos, photo))
    } else {
      setDraftSelectedPhotos([...draftSelectedPhotos, photo])
    }
    if (selectdPhotosListRef && selectdPhotosListRef.current) {
      selectdPhotosListRef.current.scrollToEnd()
    }
  }

  const goToCameraScreen = () => {
    ChatAnalyticsController.trackChatCameraRollUIEvent(
      ChatScreenElement.TakePhoto,
      AnalyticsElementType.Button,
      AnalyticsActionType.Click,
    )
    Navigation.navigateToRoute(NavigableRoute.DiscussionCamera, { discussionId, refetch })
  }
  const onSend = async () => {
    setLoading(true)
    ChatAnalyticsController.trackChatCameraRollUIEvent(
      ChatScreenElement.Send,
      AnalyticsElementType.Button,
      AnalyticsActionType.Click,
    )
    let sent = 0
    const photosToSend = await savePhotosToLocalSandBox(draftSelectedPhotos, CAMERA_CHAT_IMAGE_SAVE_FOLDER)
    await Promise.all(
      photosToSend.map(image =>
        sendImageMessage(discussionId, image.uri)
          .then(() => {
            sent += 1
            return new Promise<void>((resolve, _reject) => resolve())
          })
          .catch(
            () => new Promise<void>((resolve, _reject) => resolve()),
          ),
      ),
    )
    await deletePhotosAt(CAMERA_CHAT_IMAGE_SAVE_FOLDER)
    if (sent > 0) {
      await refetch()
      setDraftSelectedPhotos([])
      Navigation.popToTop()
    } else {
      const dialogProps: AffirmRejectDialogScreenProps = {
        onAffirm: () => {},
        affirmText: translate("common-actions.got-it"),
        title: translate("discussion-image-picker.send-error-title"),
        body: translate("discussion-image-picker.send-error-body"),
        icon: ErrorIcon,
      }
      Navigation.navigateToRoute(NavigableRoute.AffirmRejectDialog, dialogProps)
    }

    setLoading(false)
  }
  return (
    <Screen safeAreaMode="all" screenName={ChatScreenName.ChatCameraRoll}>
      <Margin>
        <Stack height="100%" direction="column" grow={1}>
          <ImagePicker
            pageSize={PHOTO_GRID_PAGE_SIZE}
            selectedPhotos={draftSelectedPhotos}
            onImagePickerItemPressed={onImagePickerItemPressed}
            onCancel={onCancel}
            onTakePhotoClicked={goToCameraScreen}
          />
          <Margin basis={80} marginTopStep={4}>
            <ImagePickerSelectedPhotos ref={selectdPhotosListRef} selectedPhotos={draftSelectedPhotos} />
          </Margin>
          <Margin basis={76} marginLeftStep={4} marginRightStep={4} marginTopStep={2} marginBottomStep={4}>
            <Stack grow={1} direction="column">
              <Button
                buttonSize="large"
                buttonType={loading ? "disabled" : "primary"}
                title={translate("discussion-screen.send")}
                loading={loading}
                onClick={onSend}
              />
              {error && (
                <Text color="error" textType="tertiaryBody2" testID="discussion-screen.error">
                  {error}
                </Text>
              )}
            </Stack>
          </Margin>
        </Stack>
      </Margin>
    </Screen>
  )
}
