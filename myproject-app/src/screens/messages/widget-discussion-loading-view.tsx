import { NAVIGATION_BAR_HEIGHT, Flex, NavigationBar, Stack } from 'myproject-ucl'
import { ActivityIndicator } from 'myproject-ucl/controls/myproject-activity-indicator'
import React, { FC, memo } from 'react'
import { getNavigationBackButton } from '../../navigation/common'

export const DiscussionFullScreenLoadingView: FC = memo(() => {
  return (
    <Stack direction='column' grow={1}>
      <Flex direction='column' grow={0} height={NAVIGATION_BAR_HEIGHT}>
        <NavigationBar
          title=''
          leftItems={[getNavigationBackButton('discussion-loading-screen.navigation-bar')]}
          testID='discussion-loading-screen.navigation-bar'
        />
      </Flex>
      <Flex grow={1} axisDistribution='center' crossAxisDistribution='center'>
        <ActivityIndicator size='large' />
      </Flex>
    </Stack>
  )
})
