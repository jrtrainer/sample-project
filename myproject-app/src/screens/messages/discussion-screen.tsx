import _ from "lodash"
import { CommonNavs, Navigation } from "myproject-app/src/navigation/navigation"
import {
  GET_SUGGESTED_MESSAGES,
  PUBLIC_PROFILE_QUERY,
  Query,
  SuggestedMessage,
  translate,
  AccountDataContext,
  AnalyticsActionType,
  AnalyticsElementType,
  useGetItemOwnerByItemId,
  PUBLIC_PROFILE_USER_RELATIONSHIP_QUERY,
  UNBLOCK_USER,
} from "myproject-shared"
import { useLazyQuery, useMutation, useQuery } from "@apollo/react-hooks"
import { useDoesItemIdBelongToAutosDealer } from "myproject-shared/utilities/hooks/account/use-does-item-id-belong-to-autos-dealer"
import React, { useLayoutEffect, useState, useEffect, useContext } from "react"
import { DiscussionStackNavigatorParamList } from "../../navigation/myproject-discussion/myproject-discussion-navigator"
import { ScreenRoute } from "../../navigation/myproject-route"
import { NavigableRoute } from "../../navigation/navigator"
import { Screen } from "../../widgets"
import { DiscussionFullScreenLoadingView } from "./widget-discussion-loading-view"
import { ExistingChatThread } from "./widget-existing-chat-thread"
import { SendFirstMessage } from "./widget-send-first-message"
import { SendFirstMessageAutosDealer } from "./widget-send-first-message-autos-dealer"
import { NavigationBar, Center, EmptyState, ErrorBearFailure, isValidUserId } from "myproject-ucl"
import { getNavigationBackButton } from "../../navigation/common"
import Instabug from "instabug-reactnative"
import {
  BuyerEngagementType,
  ItemDetailElementName,
  ItemDetailSellerType,
  ItemDetailType,
} from "myproject-shared/analytics/constants/item-detail-constants"
import {
  ItemDetailAnalyticsController,
  BuyerEventParams,
} from "myproject-shared/analytics/item-detail/myproject-analytics-item-detail"
import {
  CheckoutFlowElementName,
  CheckoutFlowEventName,
  CheckoutFlowScreenName,
} from "myproject-shared/analytics/constants/checkout-flow-constants"
import {
  CheckoutFlowAnalyticsInput,
  AnalyticsBuyerFlow,
} from "myproject-shared/analytics/checkout-flow/myproject-analytics-checkout"
import { AffirmRejectDialogScreenProps } from "../dialog"
import { useDiscussionProps } from "./discussion-props-provider"

export const DiscussionScreen: React.FC<ScreenRoute<DiscussionStackNavigatorParamList, NavigableRoute.Discussion>> = () => {
  const {
    discussionProps: {
      discussionId: inputDiscussionId,
      itemId,
      buyerId,
      sellerId,
      sellerType,
      type,
      fullPrice,
      buyNowEnabled,
      price,
      offerPrice,
      categoryL1,
      categoryL2,
    },
  } = useDiscussionProps()

  const { loading: accountLoading } = useContext(AccountDataContext)

  const [lookupThreadId, { data, called, loading, error }] = useLazyQuery<Query>(GET_SUGGESTED_MESSAGES, {
    variables: {
      itemId: _.toString(itemId),
    },
    onCompleted: response => {
      setDiscussionId(response.suggestedMessageForItem.threadInfo?.id)
    },
    onError: err => CommonNavs.presentError({ error: err }),
    fetchPolicy: "network-only",
  })

  const [discussionId, setDiscussionId] = useState(inputDiscussionId)

  /**
   * Listen for changes to the input parameter discussion ID.
   *
   * If the user opens this screen from the "Ask" button on item listing, there is no discussion ID.
   * They proceed to create a thread by clicking one of the suggested responses, then the navigation
   *  doesn't actually relaunch the screen, but updates the parameters. So we're listening to those here.
   */
  useLayoutEffect(() => {
    if (!inputDiscussionId) {
      // need to look up based on the listing
      lookupThreadId()
    } else if (!discussionId) {
      setDiscussionId(inputDiscussionId)
    }
  }, [inputDiscussionId])

  useEffect(() => {
    Instabug.logDebug(`Viewing discussion, param discussionId: ${inputDiscussionId}, param itemId: ${itemId}`)
    logUIElementShowEvents()
  }, [])

  const sendFirstTimeAskEvent = (engagementType: BuyerEngagementType) => {
    const buyerParams: BuyerEventParams = {
      itemId: itemId as string,
      buyerId: buyerId as string,
      sellerId: sellerId as string,
      sellerType: sellerType as ItemDetailSellerType,
      type: type as ItemDetailType,
      engagementType,
      fullPrice: fullPrice as boolean,
      buynowEnabled: buyNowEnabled as boolean,
      price: price as string,
      offerPrice: offerPrice as string,
      categoryL1: categoryL1 as string,
      categoryL2: categoryL2 as string,
    }
    ItemDetailAnalyticsController.trackBuyerEngagementMade(ItemDetailElementName.Ask, buyerParams)
    logUIEvent(AnalyticsActionType.Click, AnalyticsElementType.Button, CheckoutFlowElementName.Send)
  }

  const logUIElementShowEvents = () => {
    logUIEvent(AnalyticsActionType.Show, AnalyticsElementType.Icon, CheckoutFlowElementName.Back)
    logUIEvent(AnalyticsActionType.Show, AnalyticsElementType.Clickable, CheckoutFlowElementName.UserProfile)
    logUIEvent(AnalyticsActionType.Show, AnalyticsElementType.Clickable, CheckoutFlowElementName.ItemDetail)
    logUIEvent(AnalyticsActionType.Show, AnalyticsElementType.Link, CheckoutFlowElementName.Help)
    logUIEvent(AnalyticsActionType.Show, AnalyticsElementType.Button, CheckoutFlowElementName.MessagePill)
  }

  const logUIEvent = (
    actionType: AnalyticsActionType,
    elementType: AnalyticsElementType,
    elementName: CheckoutFlowElementName,
  ) => {
    const eventInput: CheckoutFlowAnalyticsInput = {
      eventName: CheckoutFlowEventName.BuyerAskFlowUIEvent,
      screenName: CheckoutFlowScreenName.BuyerAsk,
      actionType,
      elementType,
      elementName,
      itemId: _.toString(itemId),
      sellerId: _.toString(sellerId),
    }
    AnalyticsBuyerFlow.logUIEvent(eventInput)
  }

  return (
    <Screen safeAreaMode="all" androidSoftInputMode="adjustResize">
      {(!called && !discussionId) || loading || accountLoading ? (
        <DiscussionFullScreenLoadingView />
      ) : error ? (
        <>
          <NavigationBar title={translate("discussion-screen.title")} leftItems={[getNavigationBackButton()]} />
          <Center>
            <EmptyState
              icon={ErrorBearFailure}
              title={translate("common-errors.server-error.title")}
              subtitle={translate("common-errors.server-error.subtitle")}
              buttonTitle={translate("common-errors.server-error.button-title")}
              buttonHandler={lookupThreadId}
              testID="discussion-screen.empty-state"
            />
          </Center>
        </>
      ) : discussionId ? (
        <ExistingChatThread discussionId={discussionId} />
      ) : itemId ? (
        <FirstMessageDealerOrRegular
          suggestedMessages={data?.suggestedMessageForItem.suggestions}
          itemId={itemId}
          sendAskEvent={sendFirstTimeAskEvent}
        />
      ) : null}
    </Screen>
  )
}

const FirstMessageDealerOrRegular: React.FC<{
  itemId: string
  sendAskEvent(type: BuyerEngagementType): void
  suggestedMessages?: SuggestedMessage[] | null | undefined
}> = ({ itemId, suggestedMessages, sendAskEvent }) => {
  const { isAutosDealer, loading: isAutosDealerLoading } = useDoesItemIdBelongToAutosDealer(_.toNumber(itemId))
  const ownerId = useGetItemOwnerByItemId(_.toNumber(itemId))
  const { data: seller } = useQuery<Query>(PUBLIC_PROFILE_QUERY, {
    variables: { userId: _.toNumber(ownerId) },
    skip: !isValidUserId(ownerId),
  })
  const { data: relationship, loading: loadingRelationship } = useQuery<Query>(PUBLIC_PROFILE_USER_RELATIONSHIP_QUERY, {
    variables: { userId: _.toNumber(ownerId) },
  })
  const [unblockUser] = useMutation(UNBLOCK_USER)
  useEffect(() => {
    if (!ownerId) {
      return
    }
    if (!relationship?.userRelationship?.blocked) {
      return
    }
    const modalProps: AffirmRejectDialogScreenProps = {
      onAffirm: async () => {
        try {
          await unblockUser({ variables: { userId: _.toString(ownerId) } })
        } catch (error) {
          CommonNavs.presentError({ error })
        }
      },
      onReject: () => {
        Navigation.performWhenAvailable(() => {
          Navigation.popRootNavigator()
        })
      },
      dismissOnReject: true,
      affirmText: translate("public-profile.unblock"),
      rejectText: translate("common-actions.cancel"),
      title: translate("public-profile.unblock-dialog-title"),
      body: translate("public-profile.unblock-dialog-description", { username: seller?.publicProfile?.name }),
    }
    Navigation.navigateToRoute(NavigableRoute.AffirmRejectDialog, modalProps)
  }, [ownerId])
  if (isAutosDealerLoading || loadingRelationship) {
    return <DiscussionFullScreenLoadingView />
  }

  if (isAutosDealer) {
    return <SendFirstMessageAutosDealer itemId={_.toNumber(itemId)} />
  } else {
    return (
      <SendFirstMessage
        suggestedMessages={suggestedMessages}
        listingId={itemId}
        sendAskEvent={sendAskEvent}
        testID="discussion-screen"
      />
    )
  }
}
