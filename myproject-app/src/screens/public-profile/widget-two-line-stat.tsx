import React from 'react'
import { Stack, Text } from 'myproject-ucl/controls'

export const TwoLineStat: React.FC<{ displayNumber: string; text: string; testID?: string }> = ({ displayNumber, text, testID = 'ou.twoline.' }) => (
  <Stack direction='column' childSeparationStep={0} grow={1} crossAxisDistribution='center' axisDistribution='center'>
    <Text whiteSpace='pre-wrap' textAlign='center' textType='primaryBody1' testID={testID + '.line.1'}>
      {displayNumber}
    </Text>
    <Text whiteSpace='pre-wrap' textAlign='center' textType='primaryBody2' testID={testID + '.line.2'}>
      {text}
    </Text>
  </Stack>
)
