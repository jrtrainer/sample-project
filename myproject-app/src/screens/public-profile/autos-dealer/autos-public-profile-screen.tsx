import _ from "lodash"
import { Navigation, CommonNavs } from "myproject-app/src"
import { NavigableRoute } from "myproject-app/src/navigation/navigator"
import {
  MARGIN_SEPARATION_GAP,
  SeparatorWithPageMargin,
  useShareUser,
} from "myproject-app/src/screens/public-profile/public-profile-screen"
import { ActionSheetParams } from "myproject-app/src/widgets/myproject-action-sheet"
import { useActionSheet } from "myproject-app/src/widgets/myproject-action-sheet/myproject-action-sheet"
import { Screen } from "myproject-app/src/widgets/myproject-screen"
import {
  Listing,
  OpeningHour,
  AnalyticsDebug,
  PublicLocation,
  PUBLIC_PROFILE_QUERY,
  PUBLIC_PROFILE_USER_ITEMS_QUERY,
  Query,
  translate,
  PublicProfileAutosReviewsSection,
  Review,
} from "myproject-shared"
import { useQuery } from "@apollo/react-hooks"
import { AccountAnalyticsController } from "myproject-shared/analytics/account/myproject-analytics-account"
import { AccountScreenNames, PublicProfileScreenElement } from "myproject-shared/analytics/constants/account-constants"
import {
  NAVIGATION_BAR_HEIGHT,
  ActionClose,
  ActionEllipsisFill,
  ActionReportLine,
  Button,
  Center,
  Flex,
  Margin,
  NavigationBar,
  NavigationBarItem,
  ShareLine,
  Stack,
  Text,
  isValidUserId,
} from "myproject-ucl"
import { AccountLoadingScreen } from "myproject-ucl/widgets/account"
import React, { useEffect, useState } from "react"
import { ScrollView } from "react-native-gesture-handler"
import { useSafeAreaInsets } from "react-native-safe-area-context"
import { AutosDealerInfo } from "myproject-ucl/widgets/myproject-autos-dealer-info"
import { UsersItemsForSale } from "../section-user-items"
import { PublicAutosDealerAboutSection } from "./section-autos-dealer-about"
import { PublicDealerInfo } from "./section-autos-dealer-header"
import { getNavigationBackButton } from "myproject-app/src/navigation/common"
import { OpenURL } from "../../../utilities/open-url"
import { useSettingsWebViewLinks } from "../../../providers"

const LOAD_PAGE_SIZE = 6

const CHILD_SEPARATION_STEP = 2

export const PublicProfileAutosDealerScreen: React.FC<{ userId: number }> = ({ userId }) => {
  const insets = useSafeAreaInsets()
  const webViewLinks = useSettingsWebViewLinks()

  const { error, loading, data } = useQuery<Query>(PUBLIC_PROFILE_QUERY, {
    variables: { userId },
    skip: !isValidUserId(userId),
  })

  const { show: showActionSheet } = useActionSheet()
  const { shareUser } = useShareUser()

  const onThreeDotMenuPressed = () => {
    const ouActionSheetOptions: ActionSheetParams = {
      icons: [ShareLine, ActionReportLine, ActionClose],
      options: [translate("common-actions.share"), translate("common-actions.report"), translate("common-actions.cancel")],
      cancelButtonIndex: 2,
      destructiveButtonIndex: 1,
    }
    AccountAnalyticsController.trackPublicProfileShareDialogueShow(AccountScreenNames.PublicProfileDealer)
    showActionSheet(ouActionSheetOptions, (pressed: number) => {
      switch (pressed) {
        case 0:
          AccountAnalyticsController.trackPublicProfileShareDialogueActionClick(
            PublicProfileScreenElement.Share,
            AccountScreenNames.PublicProfileDealer,
          )
          shareUser(userId)
          break
        case 1:
          AccountAnalyticsController.trackPublicProfileShareDialogueActionClick(
            PublicProfileScreenElement.Report,
            AccountScreenNames.PublicProfileDealer,
          )
          // report
          Navigation.navigateToRoute(NavigableRoute.ReportUser, { userId })
          break
        default:
        case 2:
          AccountAnalyticsController.trackPublicProfileShareDialogueActionClick(
            PublicProfileScreenElement.Cancel,
            AccountScreenNames.PublicProfileDealer,
          )
          // do nothing
          break
      }
    })
  }

  const ellpisis: NavigationBarItem = {
    pressHandler: onThreeDotMenuPressed,
    icon: ActionEllipsisFill,
    testID: "autos-public-profile-screen.navigation-bar.ellipsis",
  }

  useEffect(() => {
    // log intial account loading errors
    if (error) {
      AnalyticsDebug.logError(error)
    }
  }, [error])

  /**
   * Initial items query
   */
  const { called: initialItemsLoadCalled, loading: initialItemsLoading } = useQuery<Query>(PUBLIC_PROFILE_USER_ITEMS_QUERY, {
    variables: { userId, pageSize: LOAD_PAGE_SIZE },
    onCompleted: response => {
      setItems(response?.userItems || [])
    },
  })

  const [items, setItems] = useState<Listing[]>([])

  const openDealerInventory = () => {
    Navigation.navigateToRoute(NavigableRoute.PublicProfileAutosDealerInventory, { userId, title: userProfile.name })
  }

  if (loading || !data || error) {
    return (
      <Screen safeAreaMode="top">
        <Flex direction="column" grow={0} height={NAVIGATION_BAR_HEIGHT}>
          <NavigationBar
            title={translate("public-profile.profile")}
            leftItems={[getNavigationBackButton("autos-public-profile-screen.loading.navigation-bar")]}
            testID="autos-public-profile-screen.loading.navigation-bar"
          />
        </Flex>
        {loading ? (
          <AccountLoadingScreen />
        ) : error ? (
          <Center>
            <Text>{translate("public-profile.error-loading")}</Text>
          </Center>
        ) : null}
      </Screen>
    )
  }

  const userProfile = data?.publicProfile
  const websiteLink = userProfile.websiteLink as string
  const openingHours = userProfile.openingHours as [OpeningHour]
  const publicLocation = userProfile.publicLocation as PublicLocation
  const reviewSection = _.first(userProfile.reviews)
  const bio = userProfile.bio || ""

  const onSeeMoreReviewsClicked = (review: Review) => {
    CommonNavs.presentWebView(webViewLinks.MoreReviewsLink(review.readMoreUrl || ""), review.title || "")
  }

  const navigateToWebsite = () => {
    if (!websiteLink) {
      return
    }

    OpenURL.openInExternalBrowser(websiteLink)
  }

  return (
    <Screen safeAreaMode="top">
      <Flex direction="column" grow={0} height={NAVIGATION_BAR_HEIGHT}>
        <NavigationBar
          title={translate("public-profile.profile")}
          leftItems={[getNavigationBackButton("autos-public-profile-screen.navigation-bar")]}
          rightItems={[ellpisis]}
          testID="autos-public-profile-screen.navigation-bar"
        />
      </Flex>
      <ScrollView style={{ flexGrow: 1, flex: 1 }}>
        <Stack direction="column" grow={1} childSeparationStep={MARGIN_SEPARATION_GAP}>
          <PublicDealerInfo userId={userId} profile={userProfile} />

          {(websiteLink || publicLocation || openingHours) && (
            <Margin marginLeftStep={MARGIN_SEPARATION_GAP} marginRightStep={MARGIN_SEPARATION_GAP} direction="column">
              <AutosDealerInfo
                website={websiteLink}
                location={publicLocation}
                hours={openingHours}
                navigateToWebsite={navigateToWebsite}
              />
            </Margin>
          )}
          <SeparatorWithPageMargin />

          <Flex direction="column">
            <UsersItemsForSale
              sectionTitle={translate("public-profile.vehicles-from-this-dealer")}
              items={items}
              initialLoad={!initialItemsLoadCalled || initialItemsLoading}
            />
            <Margin marginLeftStep={2} marginTopStep={CHILD_SEPARATION_STEP} marginBottomStep={CHILD_SEPARATION_STEP}>
              <Button
                title={translate("public-profile.view-all-inventory")}
                buttonSize="large"
                buttonType="flat"
                onClick={openDealerInventory}
              />
            </Margin>
            <SeparatorWithPageMargin />
          </Flex>

          <PublicAutosDealerAboutSection bio={bio} />
          <SeparatorWithPageMargin />

          {reviewSection && (
            <Margin marginLeftStep={MARGIN_SEPARATION_GAP} marginRightStep={MARGIN_SEPARATION_GAP} direction="column">
              <PublicProfileAutosReviewsSection review={reviewSection} onSeeMoreClicked={onSeeMoreReviewsClicked} />
            </Margin>
          )}

          <Flex height={insets.bottom} />
        </Stack>
      </ScrollView>
    </Screen>
  )
}
