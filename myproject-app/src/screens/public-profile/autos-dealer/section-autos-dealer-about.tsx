import { translate } from 'myproject-shared/utilities'
import { Margin, Stack, Text } from 'myproject-ucl/controls'
import { ExpandingTextSection } from 'myproject-ucl/widgets/index.native'
import React from 'react'
import { MARGIN_SEPARATION_GAP } from '../public-profile-screen'

export const DESCRIPTION_TRUNCATE_AFTER = 7

export const PublicAutosDealerAboutSection: React.FC<{ bio: string }> = ({ bio }) => {
  const reportTextExpanded = () => {}
  return (
    <Margin marginLeftStep={2} marginRightStep={MARGIN_SEPARATION_GAP} direction='column'>
      <Stack direction='column' childSeparationStep={MARGIN_SEPARATION_GAP}>
        <Margin marginLeftStep={2}>
          <Text textType='headline3'>{translate('public-profile.about')}</Text>
        </Margin>
        <ExpandingTextSection
          truncateThreshold={DESCRIPTION_TRUNCATE_AFTER}
          textType='secondaryBody2'
          collapseButtonTitle={translate('listing-detail.see_less')}
          expandButtonTitle={translate('listing-detail.see_more')}
          onTextExpand={reportTextExpanded}
        >
          {bio}
        </ExpandingTextSection>
      </Stack>
    </Margin>
  )
}
