import { OpenURL } from 'myproject-app/src/utilities'
import { formatPhoneNumber } from 'myproject-shared/utilities/strings/string-utils'
import { translate, UserProfile } from 'myproject-shared'
import { AccountAnalyticsController } from 'myproject-shared/analytics/account/myproject-analytics-account'
import { Button, Margin, PhoneIcon, Spacer, Stack, StarRating, Text } from 'myproject-ucl'
import { AccountProfileAvatar } from 'myproject-ucl/widgets/account/account-profile-widget-avatar'
import React from 'react'
import { ActionSheetParams } from 'myproject-app/src/widgets/myproject-action-sheet'
import { useActionSheet } from 'myproject-app/src/widgets/myproject-action-sheet/myproject-action-sheet'
import { PublicProfileScreenElement } from 'myproject-shared/analytics/constants/account-constants'
import _ from 'lodash'

const MARGIN_SEPARATION_GAP = 4

const SPACER_STEP = 2

export const PublicDealerInfo: React.FC<{ profile: UserProfile; userId: number }> = props => {
  const profile = props.profile

  const avatar = profile.avatars.squareImage
  const name = profile.name
  const location = profile.publicLocationName
  const ratingAverage = profile.ratingSummary?.average || _.first(profile.reviews)?.average || 0
  const ratingCount = profile.ratingSummary?.count || 0
  const isTruyouVerified = profile.isTruyouVerified || false
  const isAutosDealer = profile.isAutosDealer || true
  const isSubPrimeAutosDealer = profile.isSubPrimeDealer || false
  const phoneNumber = profile.c2cPhoneNumber?.nationalNumber

  const formattedNumber: string = formatPhoneNumber(phoneNumber) || ''

  const { show: showActionSheet } = useActionSheet()

  const callDealer = () => {
    const ouActionSheetOptions: ActionSheetParams = {
      options: [translate('public-profile.call-number', { number: formattedNumber }), translate('common-actions.cancel')],
      cancelButtonIndex: 2,
      destructiveButtonIndex: 1
    }
    AccountAnalyticsController.trackDealerCallDialogueShareShow()
    showActionSheet(ouActionSheetOptions, (pressed: number) => {
      switch (pressed) {
        case 0:
          // call
          AccountAnalyticsController.trackDealerCallDialogueActionClick(PublicProfileScreenElement.Call)
          phoneNumber && OpenURL.openDialerForNumer(phoneNumber)
          break
        default:
        case 1:
          AccountAnalyticsController.trackDealerCallDialogueActionClick(PublicProfileScreenElement.Cancel)
          // cancel
          break
      }
    })
  }

  const CallDealerButton = () => {
    return phoneNumber ? <Button buttonSize='large' buttonType='primary' title={formattedNumber} onClick={callDealer} icon={PhoneIcon} /> : null
  }

  const NameAndRating = () => (
    <>
      <Spacer direction='row' sizeStep={MARGIN_SEPARATION_GAP} />
      <Stack direction='column' childSeparationStep={1}>
        <Text textType='primaryBody1'>{name}</Text>
        {location && (
          <Text textType='primaryBody2' color='primary'>
            {location}
          </Text>
        )}
        {ratingCount > 0 && (
          <Stack direction='row' crossAxisDistribution='center'>
            <Text textType='primaryBody1'>{ratingAverage}</Text>
            <Spacer direction='row' sizeStep={SPACER_STEP} />
            <StarRating rating={_.toNumber(ratingAverage)} halfStarRenderEnabled={true} disabled={true} />
          </Stack>
        )}
        <Text textType='tertiaryBody2'>
          {translate(isSubPrimeAutosDealer ? 'public-profile.subprime-autos-dealer' : 'public-profile.verified-autos-dealer')}
        </Text>
      </Stack>
    </>
  )

  return (
    <Margin marginLeftStep={MARGIN_SEPARATION_GAP} marginRightStep={MARGIN_SEPARATION_GAP} marginTopStep={MARGIN_SEPARATION_GAP} direction='column'>
      <Stack direction='column' childSeparationStep={MARGIN_SEPARATION_GAP}>
        <Stack direction='row' axisDistribution='flex-start' crossAxisDistribution='flex-end'>
          <AccountProfileAvatar
            avatarUri={avatar}
            isTruyouVerified={isTruyouVerified}
            isAutosDealer={isAutosDealer}
            isSubPrimeAutosDealer={isSubPrimeAutosDealer}
          />
          <NameAndRating />
        </Stack>

        <Spacer direction='column' sizeStep={SPACER_STEP} />

        <CallDealerButton />
      </Stack>
    </Margin>
  )
}
