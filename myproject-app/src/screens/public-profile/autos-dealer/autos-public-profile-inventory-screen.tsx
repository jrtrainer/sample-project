import { NavigableRoute, Navigation, NavigatorParamList, Screen } from 'myproject-app/src'
import { ScreenRouteAndStackNavigation } from 'myproject-app/src/navigation/myproject-route'
import { FeedItem } from 'myproject-app/src/widgets/myproject-feed-item-grid/myproject-feed-item'
import { FeedItemWrapper } from 'myproject-app/src/widgets/myproject-feed-item-grid/myproject-feed-item-grid.d'
import { useLazyQuery, useQuery } from '@apollo/react-hooks'
import { Listing, PUBLIC_PROFILE_AUTOS_DEALER_ITEMS_QUERY, Query } from 'myproject-shared/gql-tags'
import { Flex, Margin, Spacer, Stack, Text } from 'myproject-ucl/controls'
import { NAVIGATION_BAR_HEIGHT, NavigationBar } from 'myproject-ucl/widgets'
import React, { useState } from 'react'
import { formatMoney, formatMileage, MoneyTrimType } from 'myproject-ucl/formatters/number-formatters'
import { getNavigationBackButton } from '../../../navigation/common'
import { StockPTRGrid } from '../../../widgets'

const LOAD_PAGE_SIZE = 20

const TITLE_HEIGHT = 20

const TOP_BOTTOM_MARGIN_STEP = 2

export const PublicProfileAutosDealerInventoryScreen: React.FC<ScreenRouteAndStackNavigation<
  NavigatorParamList,
  NavigableRoute.PublicProfileAutosDealerInventory
>> = props => {
  const { userId, title } = props.route.params.props

  /**
   * Initial items query
   */
  useQuery<Query>(PUBLIC_PROFILE_AUTOS_DEALER_ITEMS_QUERY, {
    variables: { userId, pageSize: LOAD_PAGE_SIZE },
    onCompleted: response => {
      setItems(response?.userItems || [])
    }
  })

  /**
   * Loading more items
   */
  const [items, setItems] = useState<Listing[]>([])
  const [page, setPage] = useState(1)
  const [loadMoreItems, { refetch }] = useLazyQuery<Query>(PUBLIC_PROFILE_AUTOS_DEALER_ITEMS_QUERY, {
    onCompleted: response => {
      setPage(page + 1)
      const newItems = response.userItems || []
      setItems(items.concat(newItems))
    }
  })

  /**
   * Kicks off request to load next page of items
   */
  const loadMore = () => {
    loadMoreItems({ variables: { userId, page: page + 1, pageSize: LOAD_PAGE_SIZE } })
  }

  const refetchListings = async (): Promise<void> => {
    return new Promise<void>((resolve, reject) => {
      refetch()
        .then(() => resolve())
        .catch(() => reject())
    })
  }

  const handleClickFeedItem = (listing: object) => {
    const listingId = (listing as FeedItemWrapper).tile?.id
    Navigation.navigateToRoute(NavigableRoute.ListingDetail, { listingId, tileType: (listing as FeedItemWrapper).tile?.type })
  }

  const renderDealerItem = (item: Listing) => {
    const feedItem: FeedItemWrapper = {
      tile: item,
      isDummyItem: false,
      needsTopMargin: false,
      type: 'item'
    }

    return (
      <Stack direction='column' grow={1} crossAxisDistribution='flex-start'>
        <FeedItem feedItem={feedItem} onClick={handleClickFeedItem} />
        <Margin direction='column' marginStep={1} marginTopStep={TOP_BOTTOM_MARGIN_STEP} marginBottomStep={TOP_BOTTOM_MARGIN_STEP} grow={0}>
          <Flex direction='row' height={TITLE_HEIGHT} grow={0} axisDistribution='flex-start'>
            <Text whiteSpace='nowrap' textAlign='left'>
              {item.title}
            </Text>
          </Flex>
          <Stack direction='row' grow={0}>
            <Flex grow={1}>
              <Text textType='secondaryBody2'>{formatMoney(item.price, MoneyTrimType.NoDecimals)}</Text>
            </Flex>
            <Flex axisDistribution='flex-end'>
              <Text textType='secondaryBody2'>{formatMileage(item.vehicleAttributes?.vehicleMiles || undefined)}</Text>
            </Flex>
          </Stack>
        </Margin>
      </Stack>
    )
  }

  return (
    <Screen safeAreaMode='top'>
      <Stack direction='column' grow={1}>
        <Flex direction='column' grow={0} height={NAVIGATION_BAR_HEIGHT}>
          <NavigationBar
            title={title}
            leftItems={[getNavigationBackButton('autos-public-profile-inventory-screen.navigation-bar')]}
            testID='autos-public-profile-inventory-screen.navigation-bar'
          />
        </Flex>
        <Spacer direction={'column'} sizeStep={4} />
        <StockPTRGrid data={items} columns={2} renderItem={renderDealerItem} onEndReached={loadMore} leadingRefreshHandler={refetchListings} />
      </Stack>
    </Screen>
  )
}
