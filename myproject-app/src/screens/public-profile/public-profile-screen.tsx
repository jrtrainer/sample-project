import _ from "lodash"
import { NavigableRoute } from "myproject-app/src"
import { getNavigationBackButton } from "myproject-app/src/navigation/common"
import { ScreenRouteAndStackNavigation } from "myproject-app/src/navigation/myproject-route"
import { NavigatorParamList } from "myproject-app/src/navigation/navigator"
import { Screen } from "myproject-app/src/widgets/myproject-screen"
import { ACCNT_VANITY_URL_QUERY, AnalyticsDebug, PUBLIC_PROFILE_QUERY, Query, translate } from "myproject-shared"
import { useLazyQuery, useQuery } from "@apollo/react-hooks"
import { Share } from "myproject-shared/utilities/share/index.native"
import {
  NAVIGATION_BAR_HEIGHT,
  Center,
  Flex,
  Margin,
  NavigationBar,
  Separator,
  EmptyState,
  ErrorBearFailure,
} from "myproject-ucl"
import { AccountLoadingScreen } from "myproject-ucl/widgets/account"
import React, { useEffect } from "react"
import { PublicProfileAutosDealerScreen } from "./autos-dealer/autos-public-profile-screen"
import { PublicProfileUserScreen } from "./user/user-public-profile-screen"
import { useSettingsWebViewLinks } from "../../providers"

import { isValidUserId } from "myproject-ucl"
export const MARGIN_SEPARATION_GAP = 4

export const PublicProfileScreen: React.FC<ScreenRouteAndStackNavigation<
  NavigatorParamList,
  NavigableRoute.PublicProfile
>> = ({ route }) => {
  const userId = route.params.props.userId

  const { error, loading, data, refetch } = useQuery<Query>(PUBLIC_PROFILE_QUERY, {
    variables: { userId },
    skip: !isValidUserId(userId),
  })

  useEffect(() => {
    // log intial account loading errors
    if (error) {
      AnalyticsDebug.logError(error)
    }
  }, [error])

  if (loading || error || !data) {
    return (
      <Screen safeAreaMode="top">
        <Flex direction="column" grow={0} height={NAVIGATION_BAR_HEIGHT}>
          <NavigationBar
            title={translate("public-profile.profile")}
            leftItems={[getNavigationBackButton("public-profile-screen.loading.navigation-bar")]}
            testID="public-profile-screen.loading.navigation-bar"
          />
        </Flex>
        {loading ? (
          <AccountLoadingScreen />
        ) : error ? (
          <Center>
            <EmptyState
              icon={ErrorBearFailure}
              title={translate("common-errors.server-error.title")}
              subtitle={translate("common-errors.server-error.subtitle")}
              buttonTitle={translate("common-errors.server-error.button-title")}
              buttonHandler={refetch}
              testID="account-screen.empty-state"
            />
          </Center>
        ) : null}
      </Screen>
    )
  }

  const userProfile = data?.publicProfile

  return (
    <>
      {userProfile.isAutosDealer ? (
        <PublicProfileAutosDealerScreen userId={userId} />
      ) : (
        <PublicProfileUserScreen userId={userId} />
      )}
    </>
  )
}

export const SeparatorWithPageMargin = () => (
  <Margin marginLeftStep={MARGIN_SEPARATION_GAP} marginRightStep={MARGIN_SEPARATION_GAP}>
    <Separator />
  </Margin>
)

export function useShareUser() {
  const [lookupUserVanityUrl, { data }] = useLazyQuery<Query>(ACCNT_VANITY_URL_QUERY)
  const { PublicProfileByUserId } = useSettingsWebViewLinks()

  const shareUser = async (userId: number) => {
    await lookupUserVanityUrl({ variables: { userId: _.toInteger(userId) } })
    const url = data?.vanityUrl.url || PublicProfileByUserId(userId)
    if (url) {
      Share.share({
        title: translate("common-actions.share"),
        url,
        message: "",
      })
    }
  }

  return { shareUser }
}
