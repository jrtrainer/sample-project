import React, { useState, useCallback } from "react"
import { Screen, ActionSheetParams, useActionSheet } from "myproject-app/src/widgets"
import { getNavigationBackButton } from "myproject-app/src/navigation/common"
import { ScreenRouteAndStackNavigation } from "myproject-app/src/navigation/myproject-route"
import { NavigableRoute, NavigatorParamList } from "myproject-app/src/navigation/navigator"
import { Query, Listing, MerchantProfile } from "myproject-shared/gql-tags"
import { MERCHANT_PROFILE_QUERY, MERCHANT_ITEMS_QUERY } from "myproject-shared/gql-tags/public-profile-merchant"
import { useLazyQuery, useQuery } from "@apollo/react-hooks"
import { translate } from "myproject-shared/utilities"
import {
  NAVIGATION_BAR_HEIGHT,
  ActionEllipsisFill,
  Center,
  Flex,
  NavigationBar,
  NavigationBarItem,
  Stack,
  Overlay,
  ActivityIndicator,
  ActionReportLine,
  ShareLine,
  ActionClose,
  EmptyState,
  ErrorBearFailure,
} from "myproject-ucl"
import { AccountLoadingScreen } from "myproject-ucl/widgets/account/account-loading-screen"
import { ScrollView, NativeSyntheticEvent, NativeScrollEvent, Dimensions } from "react-native"
import { SeparatorWithPageMargin } from ".."
import { MerchantInfo } from "./section-merchant-header"
import { MerchantContact } from "./section-merchant-contact"
import { UsersItemsForSale } from "../section-user-items"
import _ from "lodash"
import { useSafeArea } from "react-native-safe-area-context"
import { MerchantAboutSection } from "./section-merchant-about"
import { AccountScreenNames, PublicProfileScreenElement } from "myproject-shared/analytics/constants/account-constants"
import { AccountAnalyticsController } from "myproject-shared/analytics/account/myproject-analytics-account"
import { Navigation } from "myproject-app/src"
import { useShareMerchantUser } from "./use-share-merchant-user"

const MARGIN_SEPARATION_GAP = 4
/**
 * How many pixels to allow below users viewport before we begin another load of user pages.
 * Higher the value, earlier we load next request.
 */
const BOTTOM_THRESHOLD_PX = 200

/**
 * Controls how long before user can request next page by scrolling down
 */
const LOAD_NEXT_PAGE_DEBNCE_TIME = 500

const LOAD_PAGE_SIZE = 20

const SCROLL_EVENT_THROTTLE = 5

export const MerchantProfileScreen: React.FC<ScreenRouteAndStackNavigation<
  NavigatorParamList,
  NavigableRoute.MerchantProfile
>> = ({ route }) => {
  const uuid = route.params.props.uuid

  const windowHeight = Dimensions.get("window").height
  const insets = useSafeArea()
  const { show: showActionSheet } = useActionSheet()

  const { shareUser } = useShareMerchantUser()

  const { error, loading, data, refetch } = useQuery<Query>(MERCHANT_PROFILE_QUERY, {
    variables: { uuid },
    onCompleted: () => {
      if (userId) {
        fetchItems()
      }
    },
  })

  /**
   * Profile props
   */
  const merchantProfile = data?.publicMerchantProfile as MerchantProfile
  const email = merchantProfile?.emailAddress as string
  const userId = merchantProfile?.legacyUserOwner as number
  const bio = merchantProfile?.description as string

  const onThreeDotMenuPressed = () => {
    const ouActionSheetOptions: ActionSheetParams = {
      icons: [ShareLine, ActionReportLine, ActionClose],
      options: [translate("common-actions.share"), translate("common-actions.report"), translate("common-actions.cancel")],
      cancelButtonIndex: 2,
      destructiveButtonIndex: 1,
    }
    AccountAnalyticsController.trackPublicProfileShareDialogueShow(AccountScreenNames.PublicProfileMerchant)
    showActionSheet(ouActionSheetOptions, (pressed: number) => {
      switch (pressed) {
        case 0:
          AccountAnalyticsController.trackPublicProfileShareDialogueActionClick(
            PublicProfileScreenElement.Share,
            AccountScreenNames.PublicProfileMerchant,
          )
          shareUser(uuid)
          break
        case 1:
          AccountAnalyticsController.trackPublicProfileShareDialogueActionClick(
            PublicProfileScreenElement.Report,
            AccountScreenNames.PublicProfileMerchant,
          )
          // report
          userId && Navigation.navigateToRoute(NavigableRoute.ReportUser, { userId })
          break
        default:
        case 2:
          AccountAnalyticsController.trackPublicProfileShareDialogueActionClick(
            PublicProfileScreenElement.Cancel,
            AccountScreenNames.PublicProfileMerchant,
          )
          // do nothing
          break
      }
    })
  }
  const ellpisis: NavigationBarItem = {
    pressHandler: onThreeDotMenuPressed,
    icon: ActionEllipsisFill,
    testID: "merchant-profile-screen.navigation-bar.ellpisis",
  }

  /**
   * Initial items query
   */
  const [fetchItems, { called: initialItemsLoadCalled, loading: initialItemsLoading }] = useLazyQuery<Query>(
    MERCHANT_ITEMS_QUERY,
    {
      variables: { uuid, pageSize: LOAD_PAGE_SIZE },
      onError: error => {
        setLoadingMore(false)
      },
      onCompleted: response => {
        const initialItems = response?.merchantItems?.items || []
        setItems(initialItems)
        setCanLoadMore(initialItems.length > 0)
        setContinuationToken(response?.merchantItems?.continuationToken || "")
      },
    },
  )

  /**
   * Loading more items
   */
  const [canLoadMore, setCanLoadMore] = useState(false)
  const [loadingMore, setLoadingMore] = useState(false)
  const [items, setItems] = useState<Listing[]>([])
  const [continuationToken, setContinuationToken] = useState<string>("")
  const [page, setPage] = useState(1)
  const [loadMoreItems] = useLazyQuery<Query>(MERCHANT_ITEMS_QUERY, {
    onError: () => {
      setLoadingMore(false)
    },
    onCompleted: response => {
      setPage(page + 1)
      const newItems = response?.merchantItems?.items || []
      setItems(items.concat(newItems))
      setCanLoadMore(newItems.length > 0)
      setLoadingMore(false)
      setContinuationToken(response?.merchantItems?.continuationToken || "")
    },
  })

  /**
   * Makes sure we're not loading an item before loading more
   */
  const onRequestMoreUserItems = () => {
    if (!loadingMore) {
      loadMore()
    }
  }

  /**
   * Kicks off request to load next page of items
   */
  const loadMore = () => {
    if (canLoadMore && !loadingMore) {
      setLoadingMore(true)
      userId && loadMoreItems({ variables: { uuid, pageSize: LOAD_PAGE_SIZE, continuationToken } })
    }
  }

  /**
   * Handles infinite scroll, basic idea is that when the viewport of the scrollview visible to
   * the user is near the bottom of the available y space in the scrollview, kick off a
   * new request and append the items
   * @param event
   */
  const onScrollInternal = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    const { y } = event.nativeEvent.contentOffset
    const { height } = event.nativeEvent.contentSize

    const bottomScrollViewPort = y + windowHeight - insets.top - NAVIGATION_BAR_HEIGHT + BOTTOM_THRESHOLD_PX

    if (bottomScrollViewPort >= height) {
      onBottomHit()
    }
  }

  const onBottomHit = useCallback(
    _.debounce(onRequestMoreUserItems, LOAD_NEXT_PAGE_DEBNCE_TIME, { leading: true, trailing: false }),
    [items],
  )

  if (loading || error || !data) {
    return (
      <Screen safeAreaMode="top">
        <Flex direction="column" grow={0} height={NAVIGATION_BAR_HEIGHT}>
          <NavigationBar
            title={translate("public-profile.profile")}
            leftItems={[getNavigationBackButton("merchant-profile-screen.loading.navigation-bar")]}
            testID="merchant-profile-screen.loading.navigation-bar"
          />
        </Flex>
        {loading ? (
          <AccountLoadingScreen />
        ) : error ? (
          <Center>
            <EmptyState
              icon={ErrorBearFailure}
              title={translate("common-errors.server-error.title")}
              subtitle={translate("common-errors.server-error.subtitle")}
              buttonTitle={translate("common-errors.server-error.button-title")}
              buttonHandler={refetch}
              testID="account-screen.empty-state"
            />
          </Center>
        ) : null}
      </Screen>
    )
  }

  return (
    <Screen safeAreaMode="top">
      <Flex direction="column" grow={0} height={NAVIGATION_BAR_HEIGHT}>
        <NavigationBar
          title={translate("public-profile.profile")}
          leftItems={[getNavigationBackButton("merchant-profile-screen.navigation-bar")]}
          rightItems={[ellpisis]}
          testID="merchant-profile-screen.navigation-bar"
        />
      </Flex>
      <ScrollView style={{ flexGrow: 1, flex: 1 }} scrollEventThrottle={SCROLL_EVENT_THROTTLE} onScroll={onScrollInternal}>
        <Stack direction="column" grow={1} childSeparationStep={MARGIN_SEPARATION_GAP}>
          <MerchantInfo profile={merchantProfile} />
          <SeparatorWithPageMargin />
          {email && <MerchantContact profile={merchantProfile} />}
          {email && <SeparatorWithPageMargin />}

          {bio && <MerchantAboutSection bio={bio} />}
          {bio && <SeparatorWithPageMargin />}

          <UsersItemsForSale
            sectionTitle={translate("public-profile.items-from-this-seller")}
            items={items}
            initialLoad={initialItemsLoading}
          />
          {/* <Flex height={insets.bottom} /> */}
        </Stack>
      </ScrollView>
      {loadingMore ? (
        <Overlay insetBottomStep={MARGIN_SEPARATION_GAP}>
          <Center>
            <ActivityIndicator size="large" />
          </Center>
        </Overlay>
      ) : null}
    </Screen>
  )
}
