import { ACCNT_VANITY_URL_QUERY, Query, translate } from 'myproject-shared'
import { useLazyQuery } from '@apollo/react-hooks'
import { Share } from 'myproject-shared/utilities/share/index.native'

export function useShareMerchantUser() {
  const [lookupUserVanityUrl, { data }] = useLazyQuery<Query>(ACCNT_VANITY_URL_QUERY)

  const shareUser = async (uuid: string) => {
    // await lookupUserVanityUrl({ variables: { userId: _.toInteger(userId) } })
    const url = data?.vanityUrl.url || 'myproject.com' // TODO CLIENT-969: build proper profile URL
    if (url) {
      Share.share({
        title: translate('common-actions.share'),
        url,
        message: ''
      })
    }
  }

  return { shareUser }
}
