import React from 'react'
import { MerchantProfile, translate } from 'myproject-shared'
import { Stack, SVG, EmailEnvelope, Text, Margin, Spacer } from 'myproject-ucl'

export const MerchantContact: React.FC<{ profile: MerchantProfile }> = () => {
  return (
    <Margin marginLeftStep={4} marginRightStep={4}>
      <Stack direction='row' height={24} axisDistribution='center' childSeparationStep={2}>
        <SVG localSVG={{ SVG: EmailEnvelope.SVG, size: { width: 24, height: 24 } }} tint='link' />
        <Text textType='primaryBody1' color='link'>
          {translate('merchant-profile.contact-seller')}
        </Text>
      </Stack>
    </Margin>
  )
}
