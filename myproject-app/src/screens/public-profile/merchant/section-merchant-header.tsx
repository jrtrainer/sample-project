import { useFocusEffect } from "@react-navigation/native"
import _ from "lodash"
import { MerchantProfile, PUBLIC_PROFILE_USER_RELATIONSHIP_QUERY, Query, translate, UNBLOCK_USER } from "myproject-shared"
import { useMutation, useQuery } from "@apollo/react-hooks"
import { Button, Margin, Spacer, Stack, StarRating, Text } from "myproject-ucl"
import { AccountProfileAvatar } from "myproject-ucl/widgets/account/account-profile-widget-avatar"
import React, { useState } from "react"
import { Navigation } from "../../../navigation/navigation"
import { NavigableRoute } from "../../../navigation/navigator"
import { AffirmRejectDialogScreenProps } from "../../dialog"

const MARGIN_SEPARATION_GAP = 4

export const MerchantInfo: React.FC<{ profile: MerchantProfile }> = props => {
  const profile = props.profile
  const id = props.profile.id

  const avatar = profile.avatar?.large?.url || ""
  const name = profile.storeName || ""
  const location = profile.publicLocationName || ""
  const ratingAverage = profile.ratingSummary?.average || 0
  const ratingCount = profile.ratingSummary?.count || 0

  const { data, loading, refetch } = useQuery<Query>(PUBLIC_PROFILE_USER_RELATIONSHIP_QUERY, {
    variables: { userId: profile.legacyUserOwner },
    fetchPolicy: "network-only",
  })

  useFocusEffect(() => {
    refetch && refetch()
  })

  const [unblockUser] = useMutation(UNBLOCK_USER)
  const [waitingForUnblock, setWaitingForUnBlock] = useState(false)
  const onUnblockUserClicked = async () => {
    const modalProps: AffirmRejectDialogScreenProps = {
      onAffirm: async () => {
        setWaitingForUnBlock(true)
        try {
          await unblockUser({ variables: { userId: _.toString(id) } })
        } catch (error) {
          Navigation.navigateToRoute(NavigableRoute.ErrorDialog, { error })
        }
        setWaitingForUnBlock(false)
      },
      onReject: () => {
        Navigation.goBack()
      },
      dismissOnReject: false,
      affirmText: translate("public-profile.unblock"),
      rejectText: translate("common-actions.cancel"),
      title: translate("public-profile.unblock-dialog-title"),
      body: translate("public-profile.unblock-dialog-description", { username: name }),
    }
    Navigation.navigateToRoute(NavigableRoute.AffirmRejectDialog, modalProps)
  }

  const BlockedButton = () => {
    if (loading || !data) {
      return null
    }
    if (data.userRelationship.blocked) {
      return (
        <Button
          buttonSize="large"
          buttonType={waitingForUnblock ? "disabled" : "primary"}
          title={translate("public-profile.unblock-user")}
          onClick={onUnblockUserClicked}
        />
      )
    }
    return null
  }

  const NameAndRating = () => (
    <>
      <Spacer direction="row" sizeStep={MARGIN_SEPARATION_GAP} />
      <Stack direction="column" childSeparationStep={1}>
        <Text textType="primaryBody1">{name}</Text>
        {location && (
          <Text textType="primaryBody2" color="primary">
            {location}
          </Text>
        )}
        {ratingCount > 0 && (
          <Stack direction="row" crossAxisDistribution="stretch">
            <StarRating rating={ratingAverage} halfStarRenderEnabled={true} disabled={true} />
            <Spacer direction="row" sizeStep={2} />
            <Text textType="tertiaryBody2" color="secondary">
              {translate("account-stack.account-screen.rating-count-formatted", { ratings: ratingCount })}
            </Text>
          </Stack>
        )}
        <Text textType="tertiaryBody2">{translate("merchant-profile.verified-pro-seller")}</Text>
      </Stack>
    </>
  )
  const soldFormatted = "0"

  return (
    <Margin marginLeftStep={MARGIN_SEPARATION_GAP} marginRightStep={MARGIN_SEPARATION_GAP} direction="column">
      <Stack direction="column" childSeparationStep={MARGIN_SEPARATION_GAP}>
        <Stack direction="row" axisDistribution="flex-start" crossAxisDistribution="flex-end">
          <AccountProfileAvatar avatarUri={avatar} isMerchant={true} />
          <NameAndRating />
        </Stack>
        <Stack direction="row" grow={1} crossAxisDistribution="center">
          <Text textType="primaryBody1">{soldFormatted}</Text>
          <Spacer sizeStep={1} direction="row" />
          <Text textType="primaryBody2">{translate("merchant-profile.items-sold-on-myproject")}</Text>
        </Stack>
      </Stack>
      <BlockedButton />
    </Margin>
  )
}
