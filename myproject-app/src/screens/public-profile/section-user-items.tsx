import React from "react"
import { View } from "react-native"
import { FeedItemWrapper } from "../../widgets/myproject-feed-item-grid/myproject-feed-item-grid.d"
import { Navigation } from "../../navigation/navigation"
import { NavigableRoute } from "../../navigation/navigator"
import { FeedItem } from "../../widgets/myproject-feed-item-grid/myproject-feed-item"
import { Listing } from "myproject-shared/gql-tags"
import { ActivityIndicator, Flex, Margin, Stack, Text, useMargin } from "myproject-ucl"
import { AccountAnalyticsController } from "myproject-shared/analytics/account/myproject-analytics-account"

const NUMBER_OF_USER_IMAGES_COLUMNS = 3

const HUNDRED_PERCENT = 100

const cellWidth = `${HUNDRED_PERCENT / NUMBER_OF_USER_IMAGES_COLUMNS}%`

const MARGIN_STEP = 4

export const UsersItemsForSale: React.FC<{ sectionTitle: string; items: Listing[]; initialLoad: boolean }> = ({
  sectionTitle,
  items,
  initialLoad,
}) => {
  const { baseMargin } = useMargin()

  const handleClickFeedItem = (listing: object) => {
    const listingId = (listing as FeedItemWrapper).tile?.id

    listingId && AccountAnalyticsController.trackUserClickedItemFromUserProfile(listingId)
    listingId &&
      Navigation.navigateToRoute(NavigableRoute.ListingDetail, {
        listingId,
        tileType: (listing as FeedItemWrapper).tile?.type,
      })
  }

  const renderItems = () => {
    return (
      <Flex wrap={"wrap"} direction="row">
        {items.map((value, index) => {
          // tslint:disable-next-line: no-magic-numbers
          const margin = baseMargin / 2

          const feedItem: FeedItemWrapper = {
            tile: value,
            isDummyItem: false,
            needsTopMargin: false,
            type: "item",
          }

          return (
            <Flex direction="row" basis={cellWidth} grow={0} key={index} crossAxisDistribution="center">
              <View style={{ margin }}>
                <FeedItem feedItem={feedItem} onClick={handleClickFeedItem} testID={"public-profile.items." + index} />
              </View>
            </Flex>
          )
        })}
      </Flex>
    )
  }

  return (
    <Stack direction="column" childSeparationStep={MARGIN_STEP} testID="public-profile.container">
      <Margin marginLeftStep={MARGIN_STEP}>
        <Text textType="headline3" testID="public-profile.item-from-seller">
          {sectionTitle}
        </Text>
      </Margin>
      {initialLoad ? <ActivityIndicator /> : items.length > 0 ? renderItems() : null}
    </Stack>
  )
}
