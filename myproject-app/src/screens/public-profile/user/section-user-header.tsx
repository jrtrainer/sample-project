import { useFocusEffect } from "@react-navigation/native"
import _ from "lodash"
import {
  DateFormatter,
  PUBLIC_PROFILE_USER_RELATIONSHIP_QUERY,
  Query,
  translate,
  UNBLOCK_USER,
  UserProfile,
} from "myproject-shared"
import { useMutation, useQuery } from "@apollo/react-hooks"
import { Button, Margin, Spacer, Stack, StarRating, Text } from "myproject-ucl"
import { AccountProfileAvatar } from "myproject-ucl/widgets/account/account-profile-widget-avatar"
import React, { useState } from "react"
import { TwoLineStat } from "../widget-two-line-stat"
import { AffirmRejectDialogScreenProps } from "../../dialog"
import { Navigation } from "../../../navigation/navigation"
import { NavigableRoute } from "../../../navigation/navigator"
const MARGIN_SEPARATION_GAP = 4

export const PublicUserInfo: React.FC<{ profile: UserProfile; userId: number }> = props => {
  const profile = props.profile
  const id = props.userId

  const avatar = profile.avatars.squareImage
  const name = profile.name
  const location = profile.publicLocationName
  const ratingAverage = profile.ratingSummary?.average || 0
  const ratingCount = profile.ratingSummary?.count || 0
  const truYouVerified = profile.isTruyouVerified || false

  const { data, loading, refetch } = useQuery<Query>(PUBLIC_PROFILE_USER_RELATIONSHIP_QUERY, {
    variables: { userId: id },
    fetchPolicy: "network-only",
  })

  useFocusEffect(() => {
    refetch()
  })

  const [unblockUser] = useMutation(UNBLOCK_USER)
  const [waitingForUnblock, setWaitingForUnBlock] = useState(false)
  const onUnblockUserClicked = async () => {
    const modalProps: AffirmRejectDialogScreenProps = {
      onAffirm: async () => {
        setWaitingForUnBlock(true)
        try {
          await unblockUser({ variables: { userId: _.toString(id) } })
        } catch (error) {
          Navigation.navigateToRoute(NavigableRoute.ErrorDialog, { error })
        }
        setWaitingForUnBlock(false)
      },
      onReject: () => {
        Navigation.goBack()
      },
      dismissOnReject: false,
      affirmText: translate("public-profile.unblock"),
      rejectText: translate("common-actions.cancel"),
      title: translate("public-profile.unblock-profile-dialog-title"),
      body: translate("public-profile.unblock-profile-dialog-description"),
    }
    Navigation.navigateToRoute(NavigableRoute.AffirmRejectDialog, modalProps)
  }

  const BlockedButton = () => {
    if (loading || !data) {
      return null
    }
    if (data.userRelationship.blocked) {
      return (
        <Button
          buttonSize="large"
          buttonType={waitingForUnblock ? "disabled" : "primary"}
          title={translate("public-profile.unblock-user")}
          onClick={onUnblockUserClicked}
        />
      )
    }
    return null
  }

  const NameAndRating = () => (
    <>
      <Spacer direction="row" sizeStep={MARGIN_SEPARATION_GAP} />
      <Stack direction="column" childSeparationStep={1}>
        <Text textType="primaryBody1" testID="public-profile.name">
          {name}
        </Text>
        {profile.dateJoined && (
          <Text textType="primaryBody2" testID="public-profile.joined-date">
            {translate("public-profile.joined-date", { date: DateFormatter.formatToMonthAndYear(profile.dateJoined) })}
          </Text>
        )}
        {location && (
          <Text textType="primaryBody2" color="primary">
            {location}
          </Text>
        )}
        {ratingCount > 0 && (
          <Stack direction="row" crossAxisDistribution="stretch">
            <StarRating
              rating={ratingAverage}
              halfStarRenderEnabled={true}
              disabled={true}
              testID="public-profile.star-rating"
            />
            <Spacer direction="row" sizeStep={2} />
            <Text textType="tertiaryBody2" color="secondary" testID="public-profile.star-rating.text">
              {translate("account-stack.account-screen.rating-count-formatted", { ratings: ratingCount })}
            </Text>
          </Stack>
        )}
      </Stack>
    </>
  )
  const purchasedFormatted = (profile.itemsPurchased || "0") as string
  const soldFormatted = (profile.itemsSold || "0") as string
  const followersFormatted = (profile.followers || 0).toLocaleString()

  return (
    <Margin marginLeftStep={MARGIN_SEPARATION_GAP} marginRightStep={MARGIN_SEPARATION_GAP} direction="column">
      <Stack direction="column" childSeparationStep={MARGIN_SEPARATION_GAP}>
        <Stack direction="row" axisDistribution="flex-start" crossAxisDistribution="flex-end">
          <AccountProfileAvatar avatarUri={avatar} isTruyouVerified={truYouVerified} testID="public-profile.avatar" />
          <NameAndRating />
        </Stack>
        <Stack direction="row" grow={1} crossAxisDistribution="center">
          <TwoLineStat
            displayNumber={purchasedFormatted}
            text={translate("public-profile.bought")}
            testID="public-profile.bought"
          />
          <TwoLineStat displayNumber={soldFormatted} text={translate("public-profile.sold")} testID="public-profile.sold" />
          {followersFormatted !== "0" && (
            <TwoLineStat
              displayNumber={followersFormatted}
              text={translate("public-profile.followers")}
              testID="public-profile.followers"
            />
          )}
        </Stack>
        <BlockedButton />
      </Stack>
    </Margin>
  )
}
