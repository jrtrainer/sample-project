import { UserProfile, translate } from 'myproject-shared'
import { Margin, Stack, Text, useColor, Flex } from 'myproject-ucl'
import React, { useState, useCallback } from 'react'
import { StyleSheet, View } from 'react-native'
import { MARGIN_SEPARATION_GAP } from 'myproject-app/src/screens/public-profile/public-profile-screen'
import { prettyNumberFormatter } from 'myproject-ucl/formatters/number-formatters'

const COMPLIMENTS_BEFORE_SHOW_MORE = 4

export const Compliments: React.FC<{ profile: UserProfile }> = props => {
  const [moreVisible, setMoreVisible] = useState(false)

  const complimentsArray = props.profile.ratingAttributes
  const numberOfCompliments = complimentsArray?.length || 0

  const toggleMoreVisible = () => {
    setMoreVisible(!moreVisible)
  }

  const compliments = useCallback(() => {
    if (!complimentsArray) {
      return null
    }

    return complimentsArray
      .map((value, idx) => {
        return <Compliment label={value.value} num={value.count} key={idx} />
      })
      .slice(0, moreVisible ? undefined : COMPLIMENTS_BEFORE_SHOW_MORE)
  }, [complimentsArray, moreVisible])

  return (
    <Margin marginLeftStep={MARGIN_SEPARATION_GAP} marginRightStep={MARGIN_SEPARATION_GAP} direction='column'>
      <Stack direction='column' childSeparationStep={MARGIN_SEPARATION_GAP}>
        <Text textType='headline3'>{translate('public-profile.compliments')}</Text>
        <Flex direction='row' wrap='wrap' basis='50%' grow={1} shrink={0}>
          {compliments()}
        </Flex>

        {numberOfCompliments > COMPLIMENTS_BEFORE_SHOW_MORE && (
          <Text color='link' textType='primaryBody1' onPress={toggleMoreVisible}>
            {moreVisible ? 'See less' : 'See more'}
          </Text>
        )}
      </Stack>
    </Margin>
  )
}

const ComplimentNumberLabel: React.FC<{ num: number }> = ({ num }) => {
  const { colors } = useColor()

  return (
    <View
      style={{
        borderColor: colors.limestone,
        borderWidth: StyleSheet.hairlineWidth,
        backgroundColor: colors.crystalHover,
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        width: 40,
        height: 32,
        borderRadius: 16
      }}
    >
      <Text textType='primaryBody1' textAlign='center'>
        {prettyNumberFormatter(num)}
      </Text>
    </View>
  )
}

const COMPLIMENT_HORIZONTAL_SEPARATION = 2

const COMPLIMENT_HEIGHT = 36

const Compliment: React.FC<{ num: number; label: string }> = props => {
  return (
    <Stack
      direction='row'
      childSeparationStep={COMPLIMENT_HORIZONTAL_SEPARATION}
      width='50%'
      crossAxisDistribution='center'
      height={COMPLIMENT_HEIGHT}
    >
      <ComplimentNumberLabel num={props.num} />
      <Text textType='primaryBody2'>{props.label}</Text>
    </Stack>
  )
}
