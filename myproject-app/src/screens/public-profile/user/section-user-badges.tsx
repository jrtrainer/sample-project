import { UserProfile, UserProfileBadge } from 'myproject-shared'
import { Flex, RemoteImage, Spacer, Stack, Text } from 'myproject-ucl'
import { ScrollView } from 'myproject-ucl/controls/myproject-scroll-view/myproject-scroll-view.native'
import React from 'react'

const USER_BADGE_WIDTH = 72

const USER_BADGE_ICON_SIZE = 32

const SPACER_SIZE_STEP = 4

export const UserBadges: React.FC<{ profile?: UserProfile }> = ({ profile }) => {
  if (!profile || !profile.badges) {
    return null
  }

  const badges = () => {
    const b = profile.badges as UserProfileBadge[]
    return b.map((badge: UserProfileBadge, index) => {
      if (!badge || !badge.icon || !badge.label) {
        return null
      }

      return (
        <Stack direction='column' key={index} width={USER_BADGE_WIDTH} crossAxisDistribution='center' childSeparationStep={1}>
          <RemoteImage resizeMode={'contain'} source={{ uri: badge.icon }} width={USER_BADGE_ICON_SIZE} height={USER_BADGE_ICON_SIZE} />
          <Flex direction='row' crossAxisDistribution='center' grow={1} shrink={1} wrap={'wrap'}>
            <Text textAlign='center' textType='tertiaryBody1'>
              {badge.label.replace(/\s/g, '\n')}
            </Text>
          </Flex>
        </Stack>
      )
    })
  }

  return (
    <Flex direction='row'>
      <ScrollView horizontal={true} onlyScrollsWhenNeeded={true} disableFlexGrowContentWhenNotScrolling={true}>
        <Flex direction='row' grow={1} axisDistribution='center' crossAxisDistribution='center'>
          <Stack direction='row' crossAxisDistribution='center' axisDistribution='center' shrink={1}>
            <Spacer direction='row' sizeStep={SPACER_SIZE_STEP} />
            {badges()}
            <Spacer direction='row' sizeStep={SPACER_SIZE_STEP} />
          </Stack>
        </Flex>
      </ScrollView>
    </Flex>
  )
}
