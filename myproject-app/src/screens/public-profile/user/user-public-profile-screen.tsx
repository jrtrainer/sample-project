import _ from "lodash"
import { Navigation, CommonNavs } from "myproject-app/src"
import {
  MARGIN_SEPARATION_GAP,
  SeparatorWithPageMargin,
  useShareUser,
} from "myproject-app/src/screens/public-profile/public-profile-screen"
import { ActionSheetParams } from "myproject-app/src/widgets/myproject-action-sheet"
import { useActionSheet } from "myproject-app/src/widgets/myproject-action-sheet/myproject-action-sheet"
import { Screen } from "myproject-app/src/widgets/myproject-screen"
import {
  Listing,
  AnalyticsDebug,
  PUBLIC_PROFILE_QUERY,
  Query,
  translate,
  PUBLIC_PROFILE_USER_ITEMS_QUERY,
  AccountDataContext,
} from "myproject-shared"
import { useLazyQuery, useQuery } from "@apollo/react-hooks"
import { AccountAnalyticsController } from "myproject-shared/analytics/account/myproject-analytics-account"
import { AccountScreenNames, PublicProfileScreenElement } from "myproject-shared/analytics/constants/account-constants"
import {
  NAVIGATION_BAR_HEIGHT,
  ActionClose,
  ActionEllipsisFill,
  ActionReportLine,
  ActivityIndicator,
  Center,
  Flex,
  NavigationBar,
  NavigationBarItem,
  Overlay,
  ShareLine,
  Stack,
  Text,
  EmptyState,
  ProfileItemBuyingIcon,
  isValidUserId,
} from "myproject-ucl"
import { AccountLoadingScreen } from "myproject-ucl/widgets/account"
import React, { useCallback, useEffect, useState, useContext } from "react"
import { Dimensions, NativeScrollEvent, NativeSyntheticEvent } from "react-native"
import { ScrollView } from "react-native-gesture-handler"
import { useSafeAreaInsets } from "react-native-safe-area-context"
import { NavigableRoute } from "../../../navigation/navigator"
import { UsersItemsForSale } from "../section-user-items"
import { Compliments } from "./section-compliments"
import { UserBadges } from "./section-user-badges"
import { PublicUserInfo } from "./section-user-header"
import { getNavigationBackButton } from "myproject-app/src/navigation/common"
/**
 * How many pixels to allow below users viewport before we begin another load of user pages.
 * Higher the value, earlier we load next request.
 */
const BOTTOM_THRESHOLD_PX = 200

/**
 * Controls how long before user can request next page by scrolling down
 */
const LOAD_NEXT_PAGE_DEBNCE_TIME = 500

const LOAD_PAGE_SIZE = 20

const SCROLL_EVENT_THROTTLE = 5

export const PublicProfileUserScreen: React.FC<{ userId: number }> = ({ userId }) => {
  const { loading: accountLoading, data: userData } = useContext(AccountDataContext)
  const insets = useSafeAreaInsets()
  const windowHeight = Dimensions.get("window").height

  const { error, loading, data } = useQuery<Query>(PUBLIC_PROFILE_QUERY, {
    variables: { userId },
    skip: !isValidUserId(userId),
  })

  const { show: showActionSheet } = useActionSheet()
  const { shareUser } = useShareUser()

  const onSharePressed = () => {
    AccountAnalyticsController.trackPublicProfileShareDialogueActionClick(
      PublicProfileScreenElement.Share,
      AccountScreenNames.PublicProfileUser,
    )
    shareUser(userId)
  }

  const onThreeDotMenuPressed = () => {
    AccountAnalyticsController.trackPublicProfileElementClick(
      AccountScreenNames.PublicProfileUser,
      PublicProfileScreenElement.ShareOrReport,
    )
    const ouActionSheetOptions: ActionSheetParams = {
      icons: [ShareLine, ActionReportLine, ActionClose],
      options: [translate("common-actions.share"), translate("common-actions.report"), translate("common-actions.cancel")],
      cancelButtonIndex: 2,
      destructiveButtonIndex: 1,
    }

    AccountAnalyticsController.trackPublicProfileShareDialogueShow(AccountScreenNames.PublicProfileUser)
    showActionSheet(ouActionSheetOptions, (pressed: number) => {
      switch (pressed) {
        case 0:
          AccountAnalyticsController.trackPublicProfileShareDialogueActionClick(
            PublicProfileScreenElement.Share,
            AccountScreenNames.PublicProfileUser,
          )
          shareUser(userId)
          break
        case 1:
          AccountAnalyticsController.trackPublicProfileShareDialogueActionClick(
            PublicProfileScreenElement.Report,
            AccountScreenNames.PublicProfileUser,
          )

          // report
          Navigation.performWhenAvailable(() => {
            Navigation.navigateToRoute(NavigableRoute.ReportUser, { userId })
          })
          break
        default:
        case 2:
          AccountAnalyticsController.trackPublicProfileShareDialogueActionClick(
            PublicProfileScreenElement.Cancel,
            AccountScreenNames.PublicProfileUser,
          )
          // do nothing
          break
      }
    })
  }

  const getRightNavItem = (ownProfile: boolean): NavigationBarItem => {
    if (ownProfile) {
      return {
        pressHandler: onSharePressed,
        icon: ShareLine,
        testID: "public-profile.navigation.bar.share",
      }
    } else {
      return {
        pressHandler: onThreeDotMenuPressed,
        icon: ActionEllipsisFill,
        testID: "public-profile.navigation.bar.ellipsis",
      }
    }
  }

  useEffect(() => {
    // log intial account loading errors
    if (error) {
      AnalyticsDebug.logError(error)
    }
  }, [error])

  useEffect(() => {
    loadMoreItems({ variables: { userId, page, pageSize: LOAD_PAGE_SIZE } })
  }, [])

  /**
   * Loading more items
   */
  const [canLoadMore, setCanLoadMore] = useState(true)
  const [loadingMore, setLoadingMore] = useState(false)
  const [items, setItems] = useState<Listing[]>([])
  const [page, setPage] = useState(1)
  const [loadMoreItems, { loading: itemLoading, called: itemLoadCalled }] = useLazyQuery<Query>(
    PUBLIC_PROFILE_USER_ITEMS_QUERY,
    {
      fetchPolicy: "network-only",
      onCompleted: response => {
        setPage(page + 1)
        const newItems = response.userItems || []
        setItems(items.concat(newItems))
        setCanLoadMore(newItems.length > 0)
        setLoadingMore(false)
      },
    },
  )
  const initialItemsLoad = items.length === 0 && (!data || itemLoading || !itemLoadCalled)
  const isOwnProfile = !accountLoading && !!userData && _.toString(userId) === _.toString(userData?.id)

  /**
   * Makes sure we're not loading an item before loading more
   */
  const onRequestMoreUserItems = () => {
    if (!loadingMore) {
      loadMore()
    }
  }

  /**
   * Kicks off request to load next page of items
   */
  const loadMore = () => {
    if (canLoadMore) {
      setLoadingMore(true)
      loadMoreItems({ variables: { userId, page, pageSize: LOAD_PAGE_SIZE } })
    }
  }

  const onBottomHit = useCallback(
    _.debounce(onRequestMoreUserItems, LOAD_NEXT_PAGE_DEBNCE_TIME, { leading: true, trailing: false }),
    [items],
  )

  if (loading || !data || error) {
    return (
      <Screen safeAreaMode="top">
        <Flex direction="column" grow={0} height={NAVIGATION_BAR_HEIGHT}>
          <NavigationBar
            title={translate("public-profile.profile")}
            leftItems={[getNavigationBackButton("user-public-profile-screen.loading.navigation-bar")]}
            testID="user-public-profile-screen.loading.navigation-bar"
          />
        </Flex>
        {loading ? (
          <AccountLoadingScreen />
        ) : error ? (
          <Center>
            <Text>{translate("public-profile.error-loading")}</Text>
          </Center>
        ) : null}
      </Screen>
    )
  }

  const userProfile = data?.publicProfile
  const hasCompliments = userProfile.ratingAttributes && userProfile.ratingAttributes.length > 0
  const hasBadges = userProfile.badges && userProfile.badges.length > 0
  const hasItem = _.size(items)
  /**
   * Handles infinite scroll, basic idea is that when the viewport of the scrollview visible to
   * the user is near the bottom of the available y space in the scrollview, kick off a
   * new request and append the items
   * @param event
   */
  const onScrollInternal = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    const { y } = event.nativeEvent.contentOffset
    const { height } = event.nativeEvent.contentSize

    const bottomScrollViewPort = y + windowHeight - insets.top - NAVIGATION_BAR_HEIGHT + BOTTOM_THRESHOLD_PX

    if (bottomScrollViewPort >= height) {
      onBottomHit()
    }
  }

  return (
    <Screen safeAreaMode="top">
      <Flex direction="column" grow={0} height={NAVIGATION_BAR_HEIGHT}>
        <NavigationBar
          title={translate("public-profile.profile")}
          leftItems={[getNavigationBackButton("user-public-profile-screen.navigation-bar")]}
          rightItems={[getRightNavItem(isOwnProfile)]}
          testID="user-public-profile.navigation-bar"
        />
      </Flex>
      <ScrollView style={{ flexGrow: 1, flex: 1 }} scrollEventThrottle={SCROLL_EVENT_THROTTLE} onScroll={onScrollInternal}>
        <Stack direction="column" grow={1} childSeparationStep={MARGIN_SEPARATION_GAP}>
          <PublicUserInfo userId={userId} profile={userProfile} />
          <SeparatorWithPageMargin />
          {hasBadges && <UserBadges profile={userProfile} />}
          {hasBadges && <SeparatorWithPageMargin />}

          {hasCompliments && <Compliments profile={userProfile} />}
          {hasCompliments && <SeparatorWithPageMargin />}
          <UsersItemsForSale
            sectionTitle={translate("public-profile.items-from-this-seller")}
            items={items}
            initialLoad={initialItemsLoad}
          />
          {!hasItem && (
            <EmptyState
              icon={ProfileItemBuyingIcon}
              title={translate("public-profile.no-items-for-sale")}
              subtitle={translate("public-profile.currently-this-seller-has-no-items-for-sale-check-back-soon")}
            />
          )}
          <Flex height={insets.bottom} />
        </Stack>
      </ScrollView>
      {loadingMore ? (
        <Overlay insetBottomStep={MARGIN_SEPARATION_GAP}>
          <Center>
            <ActivityIndicator size="large" />
          </Center>
        </Overlay>
      ) : null}
    </Screen>
  )
}
