import { ACCNT_TRUY_APPLICATION_QUERY, Query, translate, AccountDataContext } from "myproject-shared"
import { useLazyQuery, useApolloClient } from "@apollo/react-hooks"
import {
  AccountVerificationPhoneLine,
  Flex,
  Margin,
  PhoneIcon,
  Separator,
  Spacer,
  SpacerFlex,
  Stack,
  SVG,
  TabBarProfileFill,
  TabBarProfileOutline,
  Text,
  Touchable,
  TruYouShieldFill,
  Overlay,
  ActionClose,
  ActivityIndicator,
  StripeLockIcon,
} from "myproject-ucl"
import React, { FC, useContext, useEffect, useState } from "react"
import { NativeModules } from "react-native"
import { useSettingsWebViewLinks } from "../../../providers/myproject-settings-context"
import { AffirmRejectDialogScreenProps } from "../../screens/dialog"
import { OuTruYouStatus } from "myproject-shared/gql-tags/generated-types/myproject-types"
import { CommonNavs, Navigation } from "../../../navigation/navigation"
import { NavigableRoute } from "../../../navigation/navigator"
import { Screen } from "../../../widgets"
import { AccountScreenNames, AccountSettingsScreenElement } from "myproject-shared/analytics/constants/account-constants"
import { AccountAnalyticsController } from "myproject-shared/analytics/account/myproject-analytics-account"
import { ACCNT_QUERY } from "myproject-shared/gql-tags"
export const POnfido = NativeModules.OnfidoNative

const TRU_SHIED_ICON_SIZE = 48
const ICON_SIZE = 24

export const TruYouVerifyScreen: FC = () => {
  const { data: me, refetch: refetchAccount } = useContext(AccountDataContext)
  const apolloClient = useApolloClient()
  const webViewLinks = useSettingsWebViewLinks()
  const [startTruYouVerification, { loading, refetch }] = useLazyQuery<Query>(ACCNT_TRUY_APPLICATION_QUERY, {
    fetchPolicy: "no-cache",
    onCompleted: async response => {
      try {
        const createdApplicantId = response.me.truYouApplication?.applicantId
        await POnfido.startOnfidoFlow(createdApplicantId)
        setCompletedFido(true)
        confirmTruYou()
        await refetch()
        await refetchAccount()
        truYouVerificationCompleted()
      } catch (error) {
        if (error.message === "User exited flow") {
          // handle user cancel
        } else {
          CommonNavs.presentError({ error })
        }
      }
    },
  })

  // acts as a temporary measure to prevent user from entering flow until backend has refreshed
  const truYouVerificationCompleted = () => {
    apolloClient.writeQuery<Pick<Query, "me">>({
      query: ACCNT_QUERY,
      data: {
        // shallow copy of user except switch to pending
        me: {
          ...me,
          profile: {
            ...me.profile,
            truYouVerificationStatus: OuTruYouStatus.TruyouPending,
          },
        },
      },
    })
  }

  useEffect(() => {
    AccountAnalyticsController.trackUserTruYouVerifiedElementShow(AccountSettingsScreenElement.LearnMore)
    AccountAnalyticsController.trackUserTruYouVerifiedElementShow(AccountSettingsScreenElement.VerifyPhone)
    AccountAnalyticsController.trackUserTruYouVerifiedElementShow(AccountSettingsScreenElement.VerifyID)
    AccountAnalyticsController.trackUserTruYouVerifiedElementShow(AccountSettingsScreenElement.Close)
  }, [])

  const onPressLearnMore = () => {
    AccountAnalyticsController.trackUserTruYouVerifiedElementClick(AccountSettingsScreenElement.LearnMore)
    CommonNavs.presentWebView(webViewLinks.TruYouVerifyHelp, translate("webview.help_center_page_title"))
  }
  const confirmTruYou = () => {
    const dialogProps: AffirmRejectDialogScreenProps = {
      onAffirm: () => {},
      affirmText: translate("common-actions.got-it"),
      title: translate("truymyproject-verify-screen.confirmation-title"),
      body: translate("truymyproject-verify-screen.confirmation-body"),
    }
    Navigation.navigateToRoute(NavigableRoute.AffirmRejectDialog, dialogProps)
  }
  const onVerifyPhone = () => {
    AccountAnalyticsController.trackUserTruYouVerifiedElementClick(AccountSettingsScreenElement.VerifyPhone)
    Navigation.navigateToRoute(NavigableRoute.VerifyPhone)
  }

  const truYouVerificationStatus = me?.profile?.truYouVerificationStatus || OuTruYouStatus.TruyouUnknown
  const [completedFido, setCompletedFido] = useState(
    truYouVerificationStatus === OuTruYouStatus.TruyouVerified || truYouVerificationStatus === OuTruYouStatus.TruyouPending,
  )
  const { isTruyouVerified, isPhoneNumberVerified } = me?.profile || {}
  const onStartOnFidoApplication = async () => {
    AccountAnalyticsController.trackUserTruYouVerifiedElementClick(AccountSettingsScreenElement.VerifyID)
    startTruYouVerification()
  }
  const goBack = () => {
    AccountAnalyticsController.trackUserTruYouVerifiedElementClick(AccountSettingsScreenElement.Close)
    Navigation.goBack()
  }
  return (
    <Screen safeAreaMode={"all"} screenName={AccountScreenNames.TruYouVerify}>
      <Margin direction="column" grow={1} marginStep={4}>
        <Stack direction="column" grow={1} childSeparationStep={8}>
          <Flex direction="column" crossAxisDistribution="center">
            <Overlay insetTopStep={0} insetLeftStep={0}>
              <Touchable onPress={goBack}>
                <SVG tint="link" localSVG={ActionClose} />
              </Touchable>
            </Overlay>
            <Text textType="headline2">{translate("truymyproject-verify-screen.title")}</Text>
          </Flex>
          <Margin direction="column" marginLeftStep={6} marginRightStep={6}>
            <Stack direction="column" crossAxisDistribution="center" childSeparationStep={4}>
              <Flex direction="column" crossAxisDistribution="center">
                <SVG
                  localSVG={{
                    SVG: TruYouShieldFill.SVG,
                    size: {
                      width: TRU_SHIED_ICON_SIZE,
                      height: TRU_SHIED_ICON_SIZE,
                    },
                  }}
                />
              </Flex>
              <Text textType="headline2">{translate("truymyproject-verify-screen.truyou")}</Text>
              {completedFido ? (
                <Text textAlign="center">
                  <Text textType="primaryBody2">{translate("truymyproject-verify-screen.description-submitted")}</Text>
                  <Text textType="primaryBody2" color="link" onPress={onPressLearnMore}>
                    {translate("common-actions.learn-more-button")}
                  </Text>
                </Text>
              ) : (
                <>
                  <Text textType="primaryBody2" textAlign="center">
                    {translate("truymyproject-verify-screen.description")}
                  </Text>
                  <Touchable onPress={onPressLearnMore}>
                    <Text color="link" textType="primaryBody2">
                      {translate("common-actions.learn-more-button")}
                    </Text>
                  </Touchable>
                </>
              )}
            </Stack>
          </Margin>
          <Spacer direction="column" sizeStep={10} />
          <Stack direction="column" childSeparationStep={1}>
            <Margin direction="column" marginStep={2}>
              {isPhoneNumberVerified && (
                <Stack direction="row" childSeparationStep={2} crossAxisDistribution="center">
                  <SVG tint="primary" localSVG={{ SVG: PhoneIcon.SVG, size: { width: ICON_SIZE, height: ICON_SIZE } }} />
                  <Text textType="primaryBody2">{translate("truymyproject-verify-screen.verified-phone")}</Text>
                </Stack>
              )}
              {!isPhoneNumberVerified && (
                <Touchable onPress={onVerifyPhone}>
                  <Stack direction="row" childSeparationStep={2} crossAxisDistribution="center">
                    <SVG
                      tint="link"
                      localSVG={{ SVG: AccountVerificationPhoneLine.SVG, size: { width: ICON_SIZE, height: ICON_SIZE } }}
                    />
                    <Text textType="primaryBody2" color="link">
                      {translate("truymyproject-verify-screen.verify-phone")}
                    </Text>
                  </Stack>
                </Touchable>
              )}
            </Margin>
            <Separator />
            <Margin direction="column" marginStep={2}>
              {isTruyouVerified && (
                <Stack direction="row" childSeparationStep={2} crossAxisDistribution="center">
                  <SVG
                    tint="primary"
                    localSVG={{ SVG: TabBarProfileFill.SVG, size: { width: ICON_SIZE, height: ICON_SIZE } }}
                  />
                  <Text textType="primaryBody2">{translate("truymyproject-verify-screen.verified-official-id")}</Text>
                </Stack>
              )}
              {!isTruyouVerified && completedFido && (
                <Stack direction="row" childSeparationStep={2} crossAxisDistribution="center">
                  <SVG
                    tint="hint"
                    localSVG={{ SVG: TabBarProfileFill.SVG, size: { width: ICON_SIZE, height: ICON_SIZE } }}
                  />
                  <Text color={"hint"} textType="primaryBody2">
                    {translate("truymyproject-verify-screen.verify-official-id_processing")}
                  </Text>
                </Stack>
              )}
              {!isTruyouVerified && !completedFido && (
                <Touchable onPress={onStartOnFidoApplication}>
                  <Stack direction="row" childSeparationStep={2} crossAxisDistribution="center">
                    <SVG
                      tint="link"
                      localSVG={{ SVG: TabBarProfileOutline.SVG, size: { width: ICON_SIZE, height: ICON_SIZE } }}
                    />
                    <Text textType="primaryBody2" color="link">
                      {translate("truymyproject-verify-screen.verify-official-id")}
                    </Text>
                    {loading && <ActivityIndicator />}
                  </Stack>
                </Touchable>
              )}
            </Margin>
            <Separator />
          </Stack>
          <SpacerFlex />
          <Stack direction="column" crossAxisDistribution="center" childSeparationStep={2}>
            <SVG localSVG={StripeLockIcon} />
            <Text textType="tertiaryBody2">{translate("truymyproject-verify-screen.secured-by-onfido")}</Text>
          </Stack>
        </Stack>
      </Margin>
    </Screen>
  )
}
