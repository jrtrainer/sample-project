import { MultifactorHeaderInfo, AuthScreenContext } from 'myproject-shared'

export interface AccountMultifactorProps {
  mfaRequiredTask: (multifactorHeaderInfo: MultifactorHeaderInfo) => Promise<void>
  onCompleted: () => void
  context: AuthScreenContext
}
