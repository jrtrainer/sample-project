import React, { FC } from "react"
import Config from "react-native-config"
import RNRestart from "react-native-restart"
import { useAuth } from "myproject-shared/providers/auth-provider"
import { translate } from "myproject-shared/utilities/i18n"
import { Flex, Margin, Button, List } from "myproject-ucl/controls"
import { SelectableContextProvider, useSelectable } from "myproject-ucl/hooks"
import { NavigationBar, NAVIGATION_BAR_HEIGHT, SelectableRowProps, SelectableRow } from "myproject-ucl/widgets"
import { AffirmRejectDialogScreenProps } from "../../dialog/affirm-reject-dialog-screen-props"
import { getNavigationCancelButton } from "../../../navigation/common"
import { Navigation } from "../../../navigation/navigation"
import { NavigableRoute } from "../../../navigation/navigator"
import { Screen } from "../../../widgets"
import { useSettingsServerList } from "../../../providers/myproject-settings-context"
import { getServerTypeFromString } from "myproject-shared/utilities/settings"
import { ServerTypeProps } from "myproject-shared/utilities/settings/myproject-server-settings.native"

const ServerSettingContent: FC = () => {
  // Load initial state
  const { serverList, serverType, setServerType } = useSettingsServerList()
  const auth = useAuth()
  const { select, selected } = useSelectable()

  const onSave = async () => {
    if (selected.length !== 1) {
      return
    }
    const newServerType = getServerTypeFromString(selected[0])
    // Verify that setting has changed
    if (serverType === newServerType) {
      Navigation.popRootNavigator()
      return
    }
    // Display warning
    const props: AffirmRejectDialogScreenProps = {
      onAffirm: async () => {
        setServerType(newServerType).then(() => logOutAndRestart())
      },
      onReject: () => {
        // no op
      },
      dismissOnReject: true,
      affirmText: translate("common-actions.confirm"),
      rejectText: translate("common-actions.cancel"),
      title: translate("account-stack.server-setting.warning-title"),
      body: translate("account-stack.server-setting.warning-message"),
    }
    Navigation.navigateToRoute(NavigableRoute.AffirmRejectDialog, props)
  }

  const logOutAndRestart = async () => {
    await auth.handleLogout()
    // TODO: CLIENT-3333 Swap this out for DevSettings.reload()
    // https://reactnative.dev/docs/devsettings#reload
    RNRestart.Restart()
  }

  const onDidDeselect = (selectId: string) => {
    // There is no deselect!
    select(selectId)
  }

  const renderItem = (item: ServerTypeProps, _index: number) => {
    const props: SelectableRowProps = {
      selectId: item.type as string,
      mainContent: item.title,
      subContent: {
        subText: item.graphQL,
        clickableSubText: item.website,
      },
      onDidDeselect,
    }

    return <SelectableRow {...props} />
  }
  return (
    <>
      <Flex direction="column" grow={1}>
        <List data={serverList} renderItem={renderItem} />
      </Flex>
      <Margin direction="column" grow={0} marginStep={4}>
        <Button buttonSize="large" buttonType="primary" title={translate("common-actions.save")} onClick={onSave} />
      </Margin>
    </>
  )
}

export const ServerSettingScreen: FC = () => {
  // Load initial state
  const { serverType } = useSettingsServerList()

  // Screen is only available for internal builds
  if (Config.SHOW_SERVER_MENU !== "true") {
    return null
  }
  return (
    <Screen safeAreaMode={"all"}>
      <Flex direction="column" grow={0} height={NAVIGATION_BAR_HEIGHT}>
        <NavigationBar
          title={translate("account-stack.server-setting.title")}
          leftItems={[getNavigationCancelButton("server-setting.navigation-bar")]}
          testID="server-setting.navigation-bar"
        />
      </Flex>
      <SelectableContextProvider multiSelect={false} initialSelections={[serverType as string]}>
        <ServerSettingContent />
      </SelectableContextProvider>
    </Screen>
  )
}
