import { InboxScreenIdentifier } from 'myproject-shared/analytics/constants/inbox-constants'
import { translate } from 'myproject-shared/utilities/i18n'
import { NavigationBar, SegmentedControl } from 'myproject-ucl'
import React, { useContext } from 'react'
import { Screen } from '../../widgets/myproject-screen'
import { MessagesTab } from './messages-tab'
import { NotificationsTab } from './notifications-tab'
import { TabInfo } from 'myproject-ucl/widgets/myproject-segmented-control/myproject-segmented-control.d'
import { InboxDataContext } from 'myproject-shared/providers/chat/inbox-state-provider'
import { UnseenAlertCount } from 'myproject-shared'

enum Segments {
  Messages = 0,
  Notifications
}

const defaultSegment = (unseenAlertCount?: UnseenAlertCount): Segments => {
  const hasMessages = unseenAlertCount !== undefined && unseenAlertCount.inbox > 0
  const hasNotifications = unseenAlertCount !== undefined && unseenAlertCount.notifications > 0

  return !hasMessages && hasNotifications ? Segments.Notifications : Segments.Messages
}

export const InboxScreen: React.FC = () => {
  const { unseenAlertCount, refetchInbox, refetchNotification, clearUnseenInboxCount, clearUnseenNotificationsCount } = useContext(InboxDataContext)
  const [selectedIndex, setSelectedIndex] = React.useState<number>(defaultSegment(unseenAlertCount))
  const [showMessagesGleam, setShowMessagesGleam] = React.useState<boolean>(false)
  const [showNotificationsGleam, setShowNotificationsGleam] = React.useState<boolean>(false)
  const [focused, setFocused] = React.useState(false)

  const routes: TabInfo[] = [
    {
      key: InboxScreenIdentifier.Inbox,
      title: translate('inbox-screen.messages'),
      tabContent: <MessagesTab />,
      testID: 'inbox-screen.tabs.messages',
      showGleam: showMessagesGleam
    },
    {
      key: InboxScreenIdentifier.Notifications,
      title: translate('inbox-screen.notifications'),
      tabContent: <NotificationsTab />,
      testID: 'inbox-screen.tabs.notifications',
      showGleam: showNotificationsGleam
    }
  ]

  const onFocus = () => {
    setFocused(true)
  }

  const onBlur = () => {
    setFocused(false)
  }

  React.useEffect(() => {
    const currentUnseenMessages = unseenAlertCount !== undefined ? unseenAlertCount.inbox : 0
    const currentUnseenNotifications = unseenAlertCount !== undefined ? unseenAlertCount.notifications : 0
    setShowMessagesGleam(currentUnseenMessages > 0 && selectedIndex !== Segments.Messages)
    setShowNotificationsGleam(currentUnseenNotifications > 0 && selectedIndex !== Segments.Notifications)
  }, [selectedIndex, unseenAlertCount])

  React.useEffect(() => {
    if (focused) {
      return
    }

    setSelectedIndex(defaultSegment(unseenAlertCount))
  }, [unseenAlertCount])

  React.useEffect(() => {
    if (!focused) {
      return
    }

    if (selectedIndex === Segments.Messages) {
      clearUnseenInboxCount()
      refetchInbox()
    } else {
      clearUnseenNotificationsCount()
      refetchNotification()
    }
  }, [focused, selectedIndex])

  return (
    <Screen safeAreaMode='top' onFocus={onFocus} onBlur={onBlur}>
      <NavigationBar title={translate('inbox-screen.inbox')} isRootNavBar={true} testID='inbox-screen.navigation-bar' />
      <SegmentedControl tabs={routes} selectedIndex={selectedIndex} onSelect={setSelectedIndex} />
    </Screen>
  )
}
