import { AffirmRejectDialogProps } from 'myproject-ucl'
export type AffirmRejectDialogScreenProps = Pick<
  AffirmRejectDialogProps,
  'onAffirm' | 'onReject' | 'onShow' | 'affirmText' | 'rejectText' | 'title' | 'body' | 'dismissOnReject' | 'icon'
>
