import React, { FC, useEffect } from "react"
import { Text, Margin, Stack, RemoteImage, Separator, Flex, SVG, ActionRightChevron, Touchable } from "myproject-ucl"
import { AffirmRejectDialogScreenProps } from "../dialog"
import { ListingItemProps } from "./listing-item-props"
import { translate } from "myproject-shared"
import { Navigation } from "../../navigation/navigation"
import { NavigableRoute } from "../../navigation/navigator"

const SPACER_STEP = 2
const ITEM_IMAGE_SIZE = 60
const ITEM_IMAGE_BORDER_RADIUS = 4

export const ListingItem: FC<ListingItemProps> = ({
  title,
  price,
  photo,
  handleItemClick,
  handleDeleteClick,
  handleOnShow,
}) => {
  useEffect(() => {
    handleOnShow && handleOnShow()
  }, [])

  const onItemPress = () => {
    handleItemClick && handleItemClick()
  }

  const onDeletePress = () => {
    const props: AffirmRejectDialogScreenProps = {
      onAffirm: () => {
        handleDeleteClick && handleDeleteClick()
      },
      onReject: () => {},
      affirmText: translate("common-actions.delete"),
      rejectText: translate("common-actions.cancel"),
      title: translate("saved-items.delete-saved-item-dialog.title"),
      body: translate("saved-items.delete-saved-item-dialog.body"),
    }
    Navigation.navigateToRoute(NavigableRoute.AffirmRejectDialog, props)
  }

  return (
    <Touchable onPress={onItemPress}>
      <Flex direction={"column"}>
        <Margin marginStep={SPACER_STEP}>
          <Stack direction="row" childSeparationStep={SPACER_STEP} crossAxisDistribution="center" grow={1}>
            <RemoteImage
              height={ITEM_IMAGE_SIZE}
              aspectRatio={1}
              borderRadius={ITEM_IMAGE_BORDER_RADIUS}
              resizeMode="cover"
              source={{ uri: photo }}
            />
            <Margin marginStep={SPACER_STEP} grow={1}>
              <Stack direction="column" crossAxisDistribution="flex-start" childSeparationStep={1}>
                <Text textType={"primaryBody2"}>{title}</Text>
                <Text textType={"tertiaryBody2"} color="secondary">
                  {price}
                </Text>
                <Margin marginTopStep={1} grow={1}>
                  <Touchable onPress={onDeletePress}>
                    <Text textType="secondaryBody2" color="link">
                      {translate("common-actions.delete")}
                    </Text>
                  </Touchable>
                </Margin>
              </Stack>
            </Margin>
            <Flex direction="row" crossAxisDistribution="center">
              <SVG tint="secondary" localSVG={ActionRightChevron} />
            </Flex>
          </Stack>
        </Margin>
        <Margin marginLeftStep={SPACER_STEP}>
          <Separator />
        </Margin>
      </Flex>
    </Touchable>
  )
}
