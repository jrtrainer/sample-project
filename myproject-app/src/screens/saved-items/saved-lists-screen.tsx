import React, { useEffect, useState } from "react"
import { Screen } from "../../widgets/myproject-screen"
import {
  ActivityIndicator,
  Stack,
  Margin,
  Center,
  NavigationBar,
  PlusIcon,
  Flex,
  Separator,
  SVG,
  Text,
  ClickableHighlight,
} from "myproject-ucl"
import { translate, SAVED_LISTS, DELETE_SAVED_LIST, AnalyticsActionType, SavedItemsScreenElement } from "myproject-shared"
import { useMutation, useQuery } from "@apollo/react-hooks"
import { NavigableRoute } from "../../navigation/navigator/navigableroute"
import { SavedListItem } from "myproject-shared/components/saved/saved-list-item"
import { ScreenRouteAndStackNavigation } from "../../navigation/myproject-route"
import { NavigatorParamList } from "../../navigation/navigator"
import { getNavigationBackButton } from "../../navigation/common"
import { Navigation, CommonNavs } from "../../navigation/navigation"
import { useIsFocused } from "@react-navigation/native"
import { AffirmRejectDialogScreenProps } from "../dialog"
import { AnalyticsSavedItems } from "myproject-shared/analytics/saved-items"
import { StockPTRList } from "../../widgets"

const SPACER_STEP = 4
const STATUS_REFETCH = 4

/**
 * Shows list of uses saved lists.
 */
export const SavedListsScreen: React.FC<ScreenRouteAndStackNavigation<NavigatorParamList, NavigableRoute.SavedLists>> = ({
  route,
}) => {
  const { loading, data, refetch, networkStatus } = useQuery(SAVED_LISTS, {
    fetchPolicy: "network-only",
    notifyOnNetworkStatusChange: true,
  })
  const isFocused = useIsFocused()
  useEffect(() => {
    isFocused && refetch()
  }, [isFocused])

  useEffect(() => {
    if (!loading) {
      AnalyticsSavedItems.logSavedItemDashboardButtonEvent(AnalyticsActionType.Show, SavedItemsScreenElement.Back)
      AnalyticsSavedItems.logSavedItemDashboardButtonEvent(AnalyticsActionType.Show, SavedItemsScreenElement.CreateList)
    }
  }, [loading])

  const refetchLists = async (): Promise<void> => {
    return new Promise<void>((resolve, reject) => {
      refetch()
        .then(() => resolve())
        .catch(() => reject())
    })
  }

  const handleAddListClick = () => {
    const handleLeftButtonClick = () => {
      AnalyticsSavedItems.logSavedItemDashboardButtonEvent(AnalyticsActionType.Click, SavedItemsScreenElement.Close)
    }
    AnalyticsSavedItems.logSavedItemDashboardButtonEvent(AnalyticsActionType.Click, SavedItemsScreenElement.CreateList)
    Navigation.navigateToRoute(NavigableRoute.CreateSavedListModel, {
      handleLeftButtonClick,
      logButtonEvent: AnalyticsSavedItems.logSavedItemDashboardButtonEvent,
      logDialogEvent: AnalyticsSavedItems.logSavedItemDashboardDialogEvent,
    })
  }

  const [deleteList, { loading: deleting }] = useMutation(DELETE_SAVED_LIST)
  const onDeleteListAffirmClick = async (savedListId: string) => {
    try {
      await deleteList({
        variables: {
          savedListId,
        },
      })
      await refetchLists()
    } catch (error) {
      CommonNavs.presentError({ error })
    }
  }

  const renderItem = item => {
    const handleListClick = () => {
      const savedList = {
        savedListId: item.id,
        savedListName: item.name,
        isQuickSave: item.isQuickSave,
      }

      AnalyticsSavedItems.logSavedItemDashboardListItemEvent(AnalyticsActionType.Click, item.id)
      Navigation.navigateToRoute(NavigableRoute.SavedListings, { savedList })
    }

    const handleDeleteListClick = () => {
      // Just log that the row was clicked here, the delete will be logged from the dialog
      AnalyticsSavedItems.logSavedItemDashboardButtonEvent(AnalyticsActionType.Click, item.id)

      const props: AffirmRejectDialogScreenProps = {
        onAffirm: () => {
          AnalyticsSavedItems.logSavedItemDashboardDialogEvent(
            AnalyticsActionType.Click,
            SavedItemsScreenElement.DeleteInDeleteListDialogue,
          )
          onDeleteListAffirmClick(item.id)
        },
        onReject: () => {
          AnalyticsSavedItems.logSavedItemDashboardButtonEvent(AnalyticsActionType.Click, SavedItemsScreenElement.Cancel)
        },
        onShow: () => {
          AnalyticsSavedItems.logSavedItemDashboardDialogEvent(
            AnalyticsActionType.Show,
            SavedItemsScreenElement.DeleteListDialogue,
          )
          AnalyticsSavedItems.logSavedItemDashboardButtonEvent(
            AnalyticsActionType.Show,
            SavedItemsScreenElement.DeleteInDeleteListDialogue,
          )
          AnalyticsSavedItems.logSavedItemDashboardButtonEvent(AnalyticsActionType.Show, SavedItemsScreenElement.Cancel)
        },
        affirmText: translate("common-actions.delete"),
        rejectText: translate("common-actions.cancel"),
        title: translate("saved-items.delete-saved-list-dialog.title"),
        body: translate("saved-items.delete-saved-list-dialog.body"),
      }

      Navigation.navigateToRoute(NavigableRoute.AffirmRejectDialog, props)
    }

    const handleListOnShow = () => {
      AnalyticsSavedItems.logSavedItemDashboardListItemEvent(AnalyticsActionType.Show, item.id)
    }

    return (
      <SavedListItem
        name={item.name}
        isQuickSave={item.isQuickSave}
        itemCount={item.itemCount}
        photo={item.photo}
        tapHandler={handleListClick}
        handleDeleteClick={handleDeleteListClick}
        handleOnShow={handleListOnShow}
      />
    )
  }

  if (loading || deleting || networkStatus === STATUS_REFETCH) {
    return (
      <Screen safeAreaMode="all">
        <NavigationBar title={""} />
        <Center>
          <ActivityIndicator size={"large"} />
        </Center>
      </Screen>
    )
  }

  const savedLists = data.savedLists || []

  const onBackButton = () => {
    AnalyticsSavedItems.logSavedItemDashboardButtonEvent(AnalyticsActionType.Click, SavedItemsScreenElement.Back)
  }

  return (
    <Screen safeAreaMode="all">
      <NavigationBar
        title={translate("saved-items.title-saved-lists")}
        leftItems={[getNavigationBackButton("saved-lists-screen.navigation-bar", onBackButton)]}
        testID="saved-lists-screen.navigation-bar"
      />
      <Margin marginStep={SPACER_STEP} grow={1}>
        <Stack direction="column" childSeparationStep={SPACER_STEP} grow={1}>
          <Flex direction={"column"} grow={1}>
            <ClickableHighlight onClick={handleAddListClick} underlayColor="crystalPressed">
              <Margin marginStep={SPACER_STEP} grow={1}>
                <Stack direction="row" childSeparationStep={SPACER_STEP} grow={1}>
                  <SVG localSVG={PlusIcon} tint="link" />
                  <Text textType={"primaryBody2"} color="link">
                    {translate("saved-items.create-list-button-text")}
                  </Text>
                </Stack>
              </Margin>
            </ClickableHighlight>
            <Separator />
            <StockPTRList data={savedLists} renderItem={renderItem} leadingRefreshHandler={refetchLists} />
          </Flex>
        </Stack>
      </Margin>
    </Screen>
  )
}
