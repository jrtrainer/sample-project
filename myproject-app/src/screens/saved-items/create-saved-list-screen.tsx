import React from "react"
import { ScreenRouteAndStackNavigation } from "../../navigation/myproject-route"
import { NavigableRoute, NavigatorParamList } from "../../navigation/navigator"
import { CreateSavedListContent } from "./create-saved-list-content"
import { Screen } from "../../../src/widgets"
import { NavigationBar } from "myproject-ucl"
import { translate, AnalyticsActionType, SavedItemsScreenElement } from "myproject-shared"
import { getNavigationBackButton } from "../../navigation/common"
import { useRoute } from "@react-navigation/native"

/**
 * Create new saved list full screen.
 */
export const createSavedListScreen: React.FC<ScreenRouteAndStackNavigation<
  NavigatorParamList,
  NavigableRoute.CreateSavedListScreen
>> = ({}) => {
  const route = useRoute()
  const { handleLeftButtonClick, logButtonEvent, logDialogEvent } = route.params.props
  return (
    <Screen safeAreaMode="all">
      <NavigationBar
        title={translate("saved-items.title-create-saved-list-model")}
        leftItems={[getNavigationBackButton("create-saved-list-screen.navigation-bar", handleLeftButtonClick)]}
        testID="create-saved-list-screen.navigation-bar"
      />
      <CreateSavedListContent logButtonEvent={logButtonEvent} logDialogEvent={logDialogEvent} />
    </Screen>
  )
}
