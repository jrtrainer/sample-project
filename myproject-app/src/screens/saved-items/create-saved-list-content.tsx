import React, { useState, useEffect, useRef, useLayoutEffect } from 'react'
import _ from 'lodash'
import { translate, CREATE_SAVED_LIST, RENAME_SAVED_LIST, AnalyticsActionType, SavedItemsScreenElement } from 'myproject-shared'
import { useMutation } from '@apollo/react-hooks'
import { Navigation } from '../../navigation/navigation'
import { Screen } from '../../widgets'
import { Stack, Margin, Button, Input, Flex, Center, ActivityIndicator, SpacerFlex, Spacer, TextEntryRef } from 'myproject-ucl'
import { useRoute } from '@react-navigation/native'
import { KeyboardAvoidanceView } from '../../keyboardavoidance'

const SPACER_STEP = 2

export interface CreateSavedListContentProps {
  logButtonEvent: (actionType: AnalyticsActionType, elementName: SavedItemsScreenElement | string) => {}
  logDialogEvent: (actionType: AnalyticsActionType, elementName: SavedItemsScreenElement | string) => {}
}

/**
 * Create new saved list content.
 */
export const CreateSavedListContent: React.FC<CreateSavedListContentProps> = props => {
  const { logButtonEvent, logDialogEvent } = props
  const route = useRoute()
  const { savedListId } = route.params.props
  const [listName, setListName] = useState(route.params.props.savedListName || '')
  const [loading, setLoading] = useState(false)

  const [createList] = useMutation(CREATE_SAVED_LIST)
  const [renameSavedList] = useMutation(RENAME_SAVED_LIST)
  const listNameInputRef = useRef<TextEntryRef>(null)

  useLayoutEffect(() => {
    setTimeout(() => listNameInputRef.current?.focus(), 300)
  }, [])

  useEffect(() => {
    if (!loading) {
      logButtonEvent(AnalyticsActionType.Show, SavedItemsScreenElement.Close)
      logButtonEvent(AnalyticsActionType.Show, SavedItemsScreenElement.Save)

      if (savedListId) {
        logDialogEvent(AnalyticsActionType.Show, SavedItemsScreenElement.RenameListDialogue)
      } else {
        logDialogEvent(AnalyticsActionType.Show, SavedItemsScreenElement.CreateListDialogue)
      }
    }
  }, [loading])

  const onNameTextChangeHandler = (text?: string) => {
    setListName(text || '')
  }

  const handleSaveClick = async () => {
    if (!listName.trim()) {
      return
    }
    logButtonEvent(AnalyticsActionType.Click, SavedItemsScreenElement.Save)
    setLoading(true)
    try {
      if (savedListId) {
        await renameSavedList({
          variables: {
            savedListId,
            newName: listName
          }
        })
      } else {
        await createList({
          variables: {
            name: listName
          }
        })
      }
      setLoading(false)
      Navigation.goBack()
      route.params.props.onListUpdated && route.params.props.onListUpdated(listName)
    } catch (error) {}
  }

  if (loading) {
    return (
      <Screen safeAreaMode='none'>
        <Center>
          <ActivityIndicator size={'large'} />
        </Center>
      </Screen>
    )
  }

  return (
    <Margin marginStep={SPACER_STEP}>
      <KeyboardAvoidanceView stackOrder={0} activeInGroups={[]} direction='column' grow={1}>
        <Stack direction='column' childSeparationStep={SPACER_STEP} grow={1}>
          <Flex grow={1} direction='row'>
            <Input
              ref={listNameInputRef}
              autoCapitalize='words'
              title={translate('saved-items.list-name-input-title')}
              textChangeHandler={onNameTextChangeHandler}
              focusState='focused'
              text={listName}
            />
          </Flex>
          <SpacerFlex />
          <Button
            title={translate('common-actions.save')}
            buttonSize='large'
            buttonType={listName.trim() ? 'primary' : 'disabled'}
            onClick={handleSaveClick}
          />
          <Spacer direction='column' sizeStep={2} />
        </Stack>
      </KeyboardAvoidanceView>
    </Margin>
  )
}
