import React from "react"
import _ from "lodash"
import { translate } from "myproject-shared"
import { ModalCardProps, ModalCardHost, Screen } from "../../widgets"
import { CreateSavedListContent } from "./create-saved-list-content"
import { useRoute } from "@react-navigation/native"
import { KeyboardAvoidanceRollawayContainer } from "../../keyboardavoidance"
import { ScreenRouteAndStackNavigation } from "../../navigation/myproject-route"
import { NavigatorParamList, NavigableRoute } from "../../navigation/navigator"

/**
 * Create new saved list Bottom sheet.
 */
export const CreateSavedListModalScreen: React.FC<ScreenRouteAndStackNavigation<
  NavigatorParamList,
  NavigableRoute.CreateSavedListModel
>> = props => {
  const route = useRoute()
  const { title, logButtonEvent, logDialogEvent } = route.params.props
  const modalProps: ModalCardProps = {
    content: () => <CreateSavedListContent logButtonEvent={logButtonEvent} logDialogEvent={logDialogEvent} />,
    leftButtonType: "none",
    rightButtonText: "Cancel",
    onRightButtonOnClick: () => props.navigation.goBack(),
    initialSnap: 1,
    snapPoints: [0, "60%"],
    title: title || translate("saved-items.title-create-saved-list-model"),
    testID: "saved-items",
  }

  return (
    <Screen safeAreaMode="none">
      <KeyboardAvoidanceRollawayContainer grow={1}>
        <ModalCardHost navigator={props.navigation} modalProps={modalProps} />
      </KeyboardAvoidanceRollawayContainer>
    </Screen>
  )
}
