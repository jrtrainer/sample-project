import React, { useState, useEffect } from "react"
import { Screen } from "myproject-app/src/widgets/myproject-screen"
import {
  ActivityIndicator,
  Margin,
  Center,
  NavigationBar,
  NavigationBarItem,
  ActionEllipsisFill,
  EmptyState,
  Button,
  SavedListIcon,
  Stack,
} from "myproject-ucl"
import {
  SAVED_LIST_ITEMS,
  DELETE_ITEM_FROM_SAVED_LIST,
  DELETE_SAVED_LIST,
  translate,
  AnalyticsActionType,
  SavedItemsScreenElement,
} from "myproject-shared"
import { useMutation, useQuery } from "@apollo/react-hooks"
import { NavigableRoute } from "../../navigation/navigator/navigableroute"
import { ListingItem } from "./listing-item"
import { ScreenRouteAndStackNavigation } from "../../navigation/myproject-route"
import { NavigatorParamList } from "../../navigation/navigator"
import { getNavigationBackButton } from "../../navigation/common"
import _ from "lodash"
import { Navigation, CommonNavs } from "../../navigation/navigation"
import { useActionSheet, ActionSheetParams } from "../../widgets"
import { AffirmRejectDialogScreenProps } from "../dialog"
import { useNavigation } from "@react-navigation/native"
import { AnalyticsSavedItems } from "myproject-shared/analytics/saved-items"
import { AnalyticsDebug } from "myproject-shared"
import { StockPTRList } from "../../widgets"

const SPACER_STEP = 4

/**
 * Shows listing items added in a saved lists.
 */
export const SavedListingsScreen: React.FC<ScreenRouteAndStackNavigation<
  NavigatorParamList,
  NavigableRoute.SavedListings
>> = ({ route }) => {
  const { savedListId, isQuickSave } = route.params.props.savedList
  const [savedListName, setSavedListName] = useState(route.params.props.savedList.savedListName || "")
  const navigation = useNavigation()
  const [deleting, setDeleting] = useState(false)

  const { loading, data, refetch, fetchMore } = useQuery(SAVED_LIST_ITEMS, {
    variables: {
      savedListId,
      page: 1,
    },
    fetchPolicy: "network-only",
  })

  const [deleteItem] = useMutation(DELETE_ITEM_FROM_SAVED_LIST)
  const showProgress = loading || deleting
  const savedListItems = data ? data.savedListItems.items : {}

  useEffect(() => {
    if (!loading) {
      AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Show, SavedItemsScreenElement.Back)
      AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Show, SavedItemsScreenElement.More)

      if (savedListItems.length === 0) {
        AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Show, SavedItemsScreenElement.FindAnItem)
      }
    }
  }, [loading])

  const { show: showActionSheet } = useActionSheet()
  const onThreeDotMenuPressed = () => {
    AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Click, SavedItemsScreenElement.More)
    AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Show, SavedItemsScreenElement.DeleteInMoreDialogue)
    AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Show, SavedItemsScreenElement.CancelInMoreDialogue)
    AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Show, SavedItemsScreenElement.Rename)
    AnalyticsSavedItems.logSavedItemsListDialogEvent(AnalyticsActionType.Show, SavedItemsScreenElement.MoreDialogue)

    const ouActionSheetOptions: ActionSheetParams = {
      options: [
        translate("saved-items.rename-list-button-text"),
        translate("common-actions.delete"),
        translate("common-actions.cancel"),
      ],
      cancelButtonIndex: 2,
      destructiveButtonIndex: 1,
    }
    showActionSheet(ouActionSheetOptions, (pressed: number) => {
      switch (pressed) {
        case 0:
          AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Click, SavedItemsScreenElement.Rename)
          handleRenameListClick()
          break
        case 1:
          AnalyticsSavedItems.logSavedItemsListButtonEvent(
            AnalyticsActionType.Click,
            SavedItemsScreenElement.DeleteInMoreDialogue,
          )
          handleDeleteListClick()
          break
        case 2:
          AnalyticsSavedItems.logSavedItemsListButtonEvent(
            AnalyticsActionType.Click,
            SavedItemsScreenElement.CancelInMoreDialogue,
          )
          break
        default:
          // do nothing
          break
      }
    })
  }

  const handleRenameListClick = () => {
    const handleBackButtonClick = () => {
      AnalyticsSavedItems.logSavedItemsListButtonEvent(
        AnalyticsActionType.Click,
        SavedItemsScreenElement.CancelInRenameDialogue,
      )
    }
    Navigation.performWhenAvailable(() => {
      Navigation.navigateToRoute(NavigableRoute.CreateSavedListModel, {
        savedListId,
        savedListName,
        title: translate("saved-items.title-rename-saved-list-model"),
        onListUpdated: newName => onListUpdated(newName),
        handleLeftButtonClick: handleBackButtonClick,
        logButtonEvent: AnalyticsSavedItems.logSavedItemsListButtonEvent,
        logDialogEvent: AnalyticsSavedItems.logSavedItemDashboardDialogEvent,
      })
    })
  }

  const refetchListings = async (): Promise<void> => {
    return new Promise<void>((resolve, reject) => {
      refetch()
        .then(() => resolve())
        .catch(() => reject())
    })
  }

  const onListUpdated = newName => {
    AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Click, SavedItemsScreenElement.Save)
    setSavedListName(newName)
    refetch()
  }

  const ellpisis: NavigationBarItem = {
    pressHandler: onThreeDotMenuPressed,
    icon: ActionEllipsisFill,
    testID: "saved-listings-screen.navigation-bar.ellpisis",
  }

  const handleDeleteListClick = () => {
    const props: AffirmRejectDialogScreenProps = {
      onAffirm: () => {
        AnalyticsSavedItems.logSavedItemsListButtonEvent(
          AnalyticsActionType.Click,
          SavedItemsScreenElement.DeleteInDeleteItemDialogue,
        )
        onDeleteListAffirmClick()
      },
      onReject: () => {
        AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Click, SavedItemsScreenElement.Cancel)
      },
      onShow: () => {
        AnalyticsSavedItems.logSavedItemsListButtonEvent(
          AnalyticsActionType.Show,
          SavedItemsScreenElement.DeleteItemDialogue,
        )
        AnalyticsSavedItems.logSavedItemsListButtonEvent(
          AnalyticsActionType.Show,
          SavedItemsScreenElement.DeleteInDeleteItemDialogue,
        )
        AnalyticsSavedItems.logSavedItemsListButtonEvent(
          AnalyticsActionType.Show,
          SavedItemsScreenElement.CancelInDeleteItemDialogue,
        )
      },
      affirmText: translate("common-actions.delete"),
      rejectText: translate("common-actions.cancel"),
      title: translate("saved-items.delete-saved-list-dialog.title"),
      body: translate("saved-items.delete-saved-list-dialog.body"),
    }

    Navigation.performWhenAvailable(() => {
      Navigation.navigateToRoute(NavigableRoute.AffirmRejectDialog, props)
    })
  }

  const openSearchScreen = () => {
    AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Click, SavedItemsScreenElement.FindAnItem)
    Navigation.navigateToRoute(NavigableRoute.Search)
  }

  const [deleteList] = useMutation(DELETE_SAVED_LIST)
  const onDeleteListAffirmClick = async () => {
    setDeleting(true)
    try {
      await deleteList({
        variables: {
          savedListId,
        },
      })
      navigation.goBack()
    } catch (error) {}
  }

  const priceString = (price?: string | null) => {
    const dollarAmount = Number(price)
    return `$${(dollarAmount || 0).toFixed(2)}`
  }
  const onEndReached = async () => {
    if (!deleting) {
      try {
        await fetchMore({
          variables: {
            savedListId,
            page: data.savedListItems.currentPage + 1,
          },
          updateQuery: (prev, { fetchMoreResult }) => {
            if (!fetchMoreResult) {
              return prev
            }
            return {
              savedListItems: {
                currentPage: fetchMoreResult.savedListItems.currentPage,
                items: [...prev.savedListItems.items, ...fetchMoreResult.savedListItems.items],
                __typename: prev.savedListItems.__typename,
              },
            }
          },
        })
      } catch (err) {
        AnalyticsDebug.logError(err)
        CommonNavs.presentError({ err })
      }
    }
  }

  const renderItem = item => {
    const handleOnShow = () => {
      AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Show, SavedItemsScreenElement.ItemDetail)
    }

    const handleItemClick = () => {
      if (deleting) {
        return
      }
      AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Click, SavedItemsScreenElement.ItemDetail)
      const listingId = item.id
      Navigation.navigateToRoute(NavigableRoute.ListingDetail, { listingId })
    }

    const handleDeleteClick = async () => {
      setDeleting(true)
      AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Click, SavedItemsScreenElement.Delete)
      try {
        await deleteItem({
          variables: {
            savedListId,
            listingId: _.parseInt(item.id),
          },
        })
        await refetch()
        setDeleting(false)
      } catch (error) {
        setDeleting(false)
        CommonNavs.presentError({ error })
      }
    }

    return (
      <ListingItem
        title={item.title}
        price={priceString(item.price)}
        photo={item.photo}
        handleItemClick={handleItemClick}
        handleDeleteClick={handleDeleteClick}
        handleOnShow={handleOnShow}
      />
    )
  }

  const onBackButton = () => {
    AnalyticsSavedItems.logSavedItemsListButtonEvent(AnalyticsActionType.Click, SavedItemsScreenElement.Back)
  }

  return (
    <Screen safeAreaMode="all">
      <NavigationBar
        title={savedListName}
        leftItems={[getNavigationBackButton("saved-lists-screen.list-details.navigation-bar", onBackButton)]}
        rightItems={isQuickSave ? [] : [ellpisis]}
        testID="saved-listings-screen.list-details.navigation-bar"
      />
      {showProgress && (
        <Center>
          <ActivityIndicator size={"large"} />
        </Center>
      )}
      {savedListItems.length === 0 && (
        <Margin marginTopStep={20}>
          <Stack childSeparationStep={4} direction="column">
            <EmptyState
              title={translate("saved-items.empty-saved-list.empty-list-title")}
              subtitle={translate("saved-items.empty-saved-list.empty-list-subtitle")}
              icon={SavedListIcon}
            />
            <Margin axisDistribution="center">
              <Button
                buttonSize="small"
                title={translate("saved-items.empty-saved-list.find-item-button-text")}
                buttonType="primary"
                onClick={openSearchScreen}
              />
            </Margin>
          </Stack>
        </Margin>
      )}
      {savedListItems.length > 0 && !showProgress && (
        <Margin marginStep={SPACER_STEP} grow={1}>
          <StockPTRList
            data={savedListItems}
            renderItem={renderItem}
            onEndReached={onEndReached}
            leadingRefreshHandler={refetchListings}
          />
        </Margin>
      )}
    </Screen>
  )
}
