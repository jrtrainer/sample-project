export interface ListingItemProps {
  title: string
  price: string
  photo: string
  handleItemClick: () => void
  handleDeleteClick: () => void
  handleOnShow?: () => void
}
