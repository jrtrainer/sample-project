import { ActionClose, Colors, SVG, Text, TextColors, useColor, ClickableOpacity } from "myproject-ucl"
import { WebView, WebViewAPI, useWebViewContext, WebViewProps } from "myproject-ucl/widgets/myproject-webview/index.native"
import React from "react"
import { StyleSheet, TouchableOpacity, View } from "react-native"
import { useSafeAreaInsets } from "react-native-safe-area-context"
import invariant from "invariant"
import { NavigatorParamList } from "../navigation/navigator"
import { NavigableRoute } from "../navigation/navigator"
import { ScreenRouteAndStackNavigation } from "../navigation/myproject-route"
import { getNavigationBackIcon } from "../navigation/common"
import { Screen } from "../widgets/myproject-screen"

const HEADER_HEIGHT = 40
const HEADER_BUTTON_SIZE = 20

type WebViewProps = ScreenRouteAndStackNavigation<NavigatorParamList, NavigableRoute.WebViewScreen>

const WebViewScreenInteral: React.FC<WebViewProps> = ({ route, navigation }) => {
  const colors = useColor().colors
  const webViewProps: WebViewProps = route.params.props
  invariant(webViewProps !== undefined, "must have webview_props sent in as param")

  const safeArea = useSafeAreaInsets()

  const backgroundColor: keyof Colors = webViewProps.headerBackgroundColor
    ? webViewProps.headerBackgroundColor
    : "palouseGreen"
  const textAndIconTint: keyof TextColors = webViewProps.textAndIconTint ? webViewProps.textAndIconTint : "primaryAlt"

  const { goBack, canGoBack, title } = useWebViewContext()

  const styles = StyleSheet.create({
    headerContainer: {
      flexWrap: "nowrap",
      alignItems: "center",
      paddingTop: safeArea.top,
      backgroundColor: colors[backgroundColor],
      height: HEADER_HEIGHT + safeArea.top,
    },
    headerTitleContainer: {
      zIndex: 2,
      marginLeft: 48,
      marginRight: 48,
      height: HEADER_HEIGHT,
      justifyContent: "center",
    },
    headerLeftContainer: {
      position: "absolute",
      left: 8,
      bottom: 0,
      paddingHorizontal: 8,
      paddingVertical: 8,
      minHeight: 20,
      justifyContent: "flex-end",
    },
  })

  const getBackSvg = () => {
    if (canGoBack && canGoBack()) {
      return getNavigationBackIcon().SVG
    }
    return ActionClose.SVG
  }

  const onLeftActionButtonClicked = () => {
    if (canGoBack && canGoBack()) {
      goBack()
    } else {
      navigation.pop()
    }
  }

  return (
    <>
      <View style={styles.headerContainer}>
        <View style={styles.headerLeftContainer}>
          <ClickableOpacity onClick={onLeftActionButtonClicked} testID="web-view.back-button">
            <SVG
              tint={textAndIconTint}
              localSVG={{ SVG: getBackSvg(), size: { width: HEADER_BUTTON_SIZE, height: HEADER_BUTTON_SIZE } }}
            />
          </ClickableOpacity>
        </View>
        <View style={styles.headerTitleContainer}>
          <Text textAlign="center" color={textAndIconTint} numberOfLines={2} testID="web-view.title">
            {title}
          </Text>
        </View>
      </View>
      <WebView {...webViewProps} />
    </>
  )
}

export const WebViewScreen: React.FC<WebViewProps> = ({ route, navigation }) => {
  const webViewProps = route.params.props
  return (
    <Screen safeAreaMode="none">
      <WebViewAPI webViewProps={webViewProps}>
        <WebViewScreenInteral route={route} navigation={navigation} />
      </WebViewAPI>
    </Screen>
  )
}
