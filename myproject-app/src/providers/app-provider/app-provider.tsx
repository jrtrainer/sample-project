import React, { useEffect, useState } from "react"
import { AppState, AppStateStatus, StyleSheet, View } from "react-native"
import * as RNLocalize from "react-native-localize"
import Instabug from "instabug-reactnative"
import Config from "react-native-config"
import crashlytics from "@react-native-firebase/crashlytics"
import { ThemeProvider } from "myproject-ucl/themes"
import { LoadingContextProvider } from "myproject-ucl/hooks"
import { AnalyticsApp } from "myproject-shared/analytics"
import { RealtimeConnectionProvider, PhotoUploadProvider } from "myproject-shared/network"
import { AccountDataProvider, AuthProvider, SearchProvider, HomeContext } from "myproject-shared/providers"
import { InboxDataProvider } from "myproject-shared/providers/chat"
import { setI18nConfig } from "myproject-shared/utilities/i18n"
import { AppProviderProps } from "./myproject-app-provider.props"
import { useAutosFetchOnce } from "../myproject-autos-provider/myproject-autos-data-controller"
import { useShippingPostingInfoFetchOnce } from "../myproject-shipping-posting-info-controller"
import { useAppSandboxFilePathFetchOnce } from "../myproject-base-uri-provider"
import { CategoryTaxonomyProvider } from "../myproject-category-taxonomy-provider"
import { useListingDraftPreferencesFetchOnce } from "../../screens/post-flow/draft/listing-draft-preferences"
import { clearPostFlowDraftDataFromDisk } from "../../screens/post-flow/draft/post-flow-draft-loading-provider"
import { setupAppsflyer } from "../../third-party/appsflyer-controller"
import { AlertBadgeController } from "../../pushnotifications/alert-badge-controller"
import { PushManager } from "../../pushnotifications"
import { ModalContextProvider } from "../../widgets/myproject-modal-card/context/myproject-modal-provider"
import { AppUpgrade } from "../../screens/app-upgrade"
import { LetGoOnboardingProvider } from "../../widgets/myproject-letgo-onboarding/myproject-letgo-onboarding-provider"
import { SinglePhotoUploader } from "./myproject-photo-uploader"
import { useNativeThemeChangeOnce } from "../../utilities/native-theme-controller"
import { useSetupApplePayStatusEventListenerOnce } from "../../payments/apple-pay"
import { useSetupInAppPurchaseAcquireEventListenerOnce } from "../../payments/in-app-purchase"
import { ApolloProvider } from "../myproject-apollo-provider"
import { SettingsContextProvider } from "../myproject-settings-context"

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: "white",
    flex: 1,
  },
})

// tslint:disable-next-line: no-any
export const AppProvider: React.FC<AppProviderProps> = props => {
  const { children, translations } = props
  const [isTranslationLoaded, setIsTranslationLoaded] = useState(false)

  const handleAppStateChange = (state: AppStateStatus) => {
    AnalyticsApp.trackAppStateChange(state)
    const crashlyticsEnabled = !(__DEV__ || Config.CRASHLYTICS_ENABLED === "false")
    const instabugEnabled = !(__DEV__ || Config.INSTABUG_ENABLED === "false")

    crashlytics().setCrashlyticsCollectionEnabled(crashlyticsEnabled)

    if (instabugEnabled) {
      Instabug.startWithToken(Config.INSTABUG_APP_TOKEN, [Instabug.invocationEvent.shake])
    } else {
      Instabug.startWithToken(Config.INSTABUG_APP_TOKEN, [Instabug.invocationEvent.none])
      Instabug.setCrashReportingEnabled(false)
    }

    if (state === "active") {
      AnalyticsApp.trackAppStateActive()
    }
  }

  const handleLocalizationChange = () => {
    setIsTranslationLoaded(false)
    setI18nConfig(translations)
    setIsTranslationLoaded(true)
  }

  useEffect(() => {
    AppState.addEventListener("change", handleAppStateChange)
    RNLocalize.addEventListener("change", handleLocalizationChange)
    setI18nConfig(translations)
    setIsTranslationLoaded(true)
    setupAppsflyer()

    return () => {
      AppState.removeEventListener("change", handleAppStateChange)
      RNLocalize.removeEventListener("change", handleLocalizationChange)
    }
  }, [])

  if (!isTranslationLoaded) {
    return <View style={styles.safeArea} />
  }
  return (
    <ThemeProvider>
      <SettingsContextProvider>
        <ApolloProvider>
          <LoadDataAndThemeOnAppLaunch>
            <CategoryTaxonomyProvider>
              <AuthProvider cleanUpForLogOut={cleanUpForLogout}>
                <AccountDataProvider>
                  <RealtimeConnectionProvider>
                    <PhotoUploadProvider uploader={SinglePhotoUploader}>
                      <InboxDataProvider>
                        <AlertBadgeController>
                          <SearchProvider context={HomeContext}>
                            <SearchProvider>
                              <LoadingContextProvider>
                                <ModalContextProvider>
                                  <AppUpgrade>
                                    <LetGoOnboardingProvider>{children}</LetGoOnboardingProvider>
                                  </AppUpgrade>
                                </ModalContextProvider>
                              </LoadingContextProvider>
                            </SearchProvider>
                          </SearchProvider>
                        </AlertBadgeController>
                      </InboxDataProvider>
                    </PhotoUploadProvider>
                  </RealtimeConnectionProvider>
                </AccountDataProvider>
              </AuthProvider>
            </CategoryTaxonomyProvider>
          </LoadDataAndThemeOnAppLaunch>
        </ApolloProvider>
      </SettingsContextProvider>
    </ThemeProvider>
  )
}

const LoadDataAndThemeOnAppLaunch: React.FC = props => {
  useSetupApplePayStatusEventListenerOnce()
  useSetupInAppPurchaseAcquireEventListenerOnce()
  useNativeThemeChangeOnce()
  useAutosFetchOnce()
  useShippingPostingInfoFetchOnce()
  useAppSandboxFilePathFetchOnce()
  useListingDraftPreferencesFetchOnce()
  return <>{props.children}</>
}

const cleanUpForLogout = async () => {
  await clearPostFlowDraftDataFromDisk()
  PushManager.setBadgeCount(0)
}
