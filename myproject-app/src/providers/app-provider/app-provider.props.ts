import * as OuI18n from 'myproject-shared/utilities/i18n/oui18n.d'
import React from 'react'

export interface AppProviderProps {
  children?: React.ReactNode
  translations: OuI18n.TranslationsFuncDict
}
