import React, { FC } from "react"
import { ApolloProvider } from "@apollo/react-common"
import { useApolloGraphQl } from "myproject-shared/network/apollo/apollo-client.native"
import { useSettingsGraphQlUrl } from "../myproject-settings-context"

export const RNApolloProvider: FC = ({ children }) => {
  const graphQlUrl = useSettingsGraphQlUrl()
  const apolloClient = useApolloGraphQl(graphQlUrl)

  return <ApolloProvider client={apolloClient}>{children}</ApolloProvider>
}
