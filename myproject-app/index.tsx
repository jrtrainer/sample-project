import { NavigationContainer, NavigationContainerRef } from "@react-navigation/native"
import React from "react"
import { AppRegistry, Platform } from "react-native"
import { SafeAreaProvider, initialWindowMetrics } from "react-native-safe-area-context"
import { name as appName } from "./app.json"
import { AppProvider } from "./src"
import { initializeNavigation } from "./src/navigation/navigation"
import { Navigator } from "./src/navigation/navigator"
import { setupReactNativeLaunchConfiguration } from "./src/utilities/react-native-launch-configuration"

const translations = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require("myproject-shared/translations/en.json"),
}

const App = () => {
  setupReactNativeLaunchConfiguration()

  const navigationRef = React.createRef<NavigationContainerRef>()
  initializeNavigation(navigationRef)

  /**
   * Android has proper metrics on start and actually has a funny jump before content is on the screen to fill it up.
   */
  const initialMetrics = Platform.OS === "android" ? undefined : initialWindowMetrics

  return (
    <AppProvider translations={translations}>
      <SafeAreaProvider initialMetrics={initialMetrics}>
        <NavigationContainer ref={navigationRef}>
          <Navigator />
        </NavigationContainer>
      </SafeAreaProvider>
    </AppProvider>
  )
}

AppRegistry.registerComponent(appName, () => App)
