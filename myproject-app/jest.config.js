const jestPreset = require('@testing-library/react-native/jest-preset')

module.exports = {
  projects: [
    {
      displayName: 'rntl',
      preset: '@testing-library/react-native',
      setupFiles: [...jestPreset.setupFiles, './jestSetup.js', '../node_modules/react-native-gesture-handler/jestSetup.js'],
      transform: {
        '^.+\\.js$': '<rootDir>/../node_modules/react-native/jest/preprocessor.js'
      },
      transformIgnorePatterns: [
        'node_modules/(?!((jest-)?react-native|react-navigation|@react-navigation/.*|@react-native-community/masked-view|@react-native-community/async-storage|))'
      ],
      moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
      moduleNameMapper: {
        '@myproject-ucl': '<rootDir>/../component-library/shared'
      },
      testMatch: ['<rootDir>/**/*.rntl.test.*'],
      testPathIgnorePatterns: ['/node_modules/']
    },
    {
      displayName: 'rn',
      setupFiles: ['./jestSetup.js', '../node_modules/react-native-gesture-handler/jestSetup.js'],
      preset: 'react-native',
      moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
      testMatch: ['<rootDir>/**/*rn.test.*'],
      moduleNameMapper: {
        '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
      },
    },
    {
      displayName: 'enzyme',
      preset: 'react-native',
      setupFiles: ['./jestSetup.js'],
      moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
      testMatch: ['<rootDir>/**/*.enzyme.test.*']
    }
  ],
  rootDir: '../',
  testPathIgnorePatterns: ['<rootDir>/e2e/', '<rootDir>/node_modules/'],
  reporters: ['default', 'jest-junit']
}
