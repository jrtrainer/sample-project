module.exports = {
  expandProps: 'end',
  replaceAttrValues: {
    // Color Palette props pulled from Colors for themed multi-color svgs.
    palouseGreen: '{props.palouseGreen}',
    palouseGreenHover: '{props.palouseGreenHover}',
    palouseGreenPressed: '{props.palouseGreenPressed}',
    palouseGreenHighlight: '{props.palouseGreenHighlight}',
    larchYellow: '{props.larchYellow}',
    larchYellowHover: '{props.larchYellowHover}',
    larchYellowPressed: '{props.larchYellowPressed}',
    glacialBlue: '{props.glacialBlue}',
    paintbrushRed: '{props.paintbrushRed}',
    obsidian: '{props.obsidian}',
    basalt: '{props.basalt}',
    granite: '{props.granite}',
    limestone: '{props.limestone}',
    limestoneHover: '{props.limestoneHover}',
    limestonePressed: '{props.limestonePressed}',
    quartz: '{props.quartz}',
    crystal: '{props.crystal}',
    crystalHover: '{props.crystalHover}',
    crystalPressed: '{props.crystalPressed}',
    disabled: '{props.disabled}',
    alwaysDark: '{props.alwaysDark}',
    alwaysLight: '{props.alwaysLight}',
    overlay: '{props.overlay}',

    // Tint color props for mono-color svgs.
    tintColor: '{props.tintColor}'
  }
}
