module.exports = api => {
  api.cache(true)
  return {
    presets: [ "@babel/typescript", "@babel/preset-react", "@babel/preset-env" ],
    plugins: [
      "transform-es2015-arrow-functions",
      [
        "@babel/plugin-transform-runtime",
        {
          regenerator: true,
        },
      ],
      [ "@babel/plugin-proposal-class-properties" ],
      [
        "module-resolver",
        {
          alias: {
            '@shared': './project-shared',
            '@shared-features': './project-shared/features',
            '@web-components': './project-web/src/components',
            '@web-interfaces': './project-web/src/interfaces',
            '@web-pages': './project-web/src/pages',
            '@web-src': './project-web/src',
            '@web-assets': './project-web/src/assets',
            '@web-app': './project-web/src/app',
            '@web-features': './project-web/src/features',
          },
          extensions: [ ".js", ".ts", ".tsx", ".scss" ],
        },
      ],
    ],
  }
}
